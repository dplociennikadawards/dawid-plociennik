<?php
set_time_limit(3600);

function Zip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            $real_path = str_replace('\zip.php','',__FILE__).'\\';
            $fileName = str_replace($real_path,'',str_replace($source . '/', '', $file));
              
            
            if (is_dir($file) === true)
            {
                $zip->addEmptyDir($fileName);
            }
            else if (is_file($file) === true)
            {
                
                $zip->addFromString($fileName, file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}

if (isset($_GET['zip'])) {
    
    $dir = base64_decode($_GET['zip']);
   
    
    if (isset($_GET['force_overwrite']) || !is_file($dir.'.zip')) {
        /* zip */
        Zip($dir, $dir.'.zip');
        echo 'Zrobione!';
        
    } else {
        /* ask for overwite */
        echo 'Plik <b>'.$dir.'.zip</b> ju� istnieje, nadpisa� ?';
        echo '<p style="margin-top:20px;"><a href="zip.php?zip='.$_GET['zip'].'&force_overwrite=true">tak</p>';
        echo '<p><a href="zip.php">nie</p>';
    }
} else {
    /* list_files */
    if ($handle = opendir('.')) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != ".." && is_dir($entry)) {
                echo '<a href="zip.php?zip='.  base64_encode($entry).'">'.$entry.'</a><br />';
            }
        }
        closedir($handle);
    }
}