<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'podstrony') {
  $page = 'Podstrony';
  $icon = 'far fa-object-ungroup';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-30p">Nazwa strony</th>
            <th class="wd-25p">Tytuł strony (nagłówek)</th>
            <th class="wd-10p">Typ galerii</th>
            <th class="wd-10p">Powiększanie zdjęć</th>
            <th class="wd-20p text-center"></th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
            <tr>
              <td  class="align-middle"><?php echo $i; ?>.</td>
              <td  class="align-middle"><?php echo $row->page; ?></td>
              <td  class="align-middle"><?php echo $row->header_title; ?></td>
              <td  class="align-middle">
                <?php if($row->page == 'o_nas' || $row->page == 'krematorium'): ?>
                  <?php if($row->gallery_status == 1): ?>
                    <?php echo $row->type_gallery; ?></td>
                  <?php else: ?>
                    <i class="text-danger fas fa-times tx-20"></i>
                  <?php endif; ?>
                <?php else: ?>
                    -
                <?php endif; ?>
              <td  class="align-middle">
                <?php if($row->page == 'o_nas' || $row->page == 'krematorium' || $row->page == 'index'): ?>
                  <?php if($row->active_gallery == 1): ?>
                    <i class="text-success fas fa-check tx-20"></i>
                  <?php else: ?>
                    <i class="text-danger fas fa-times tx-20"></i>
                  <?php endif; ?>
                  <?php else: ?>
                    -
                <?php endif; ?>
              </td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url() . 'p/' . $row->page; ?>"><i class="text-primary far fa-eye tx-30  btn"></i></a>
                <a href="<?php echo base_url(); ?>panel/podstrony/edit/<?php echo $row->podstrony_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
              </td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->