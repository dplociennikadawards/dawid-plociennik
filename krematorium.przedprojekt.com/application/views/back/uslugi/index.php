<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'uslugi') {
  $page = 'Usługi pogrzebowe';
  $icon = 'fab fa-servicestack';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-55p">Tytuł</th>
            <th class="wd-20p">Data</th>
            <th class="wd-30p text-center"><a href="uslugi/add" class="add cursor"><i class="fas fa-plus"></i> Dodaj wpis</a></th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
            <tr>
              <td  class="align-middle"><?php echo $i; ?>.</td>
              <td  class="align-middle"><?php echo $row->title; ?></td>
              <td  class="align-middle"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/gallery_wpis/add/<?php echo $row->uslugi_id; ?>/uslugi_pogrzebowe"><i class="text-primary fas fa-images tx-30  btn"></i></a>
                <a href="<?php echo base_url(); ?>panel/uslugi/edit/<?php echo $row->uslugi_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <a href="<?php echo base_url(); ?>panel/admin/delete/uslugi/<?php echo $row->uslugi_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
              </td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->