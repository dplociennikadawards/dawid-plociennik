<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'admin') {
  $page = 'Strona główna';
  $icon = 'fas fa-home';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">

        <div class="row">
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/podstrony">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 far fa-object-ungroup"></i><br>Podstrony
              </button>
            </a>
          </div>  
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/glowna">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-cross"></i><br>Ustawienia strony głównej
              </button>
            </a>
          </div>    
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/onas">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-users"></i><br>O nas
              </button>
            </a>
          </div>   
        </div>  

        <div class="row mt-5">
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/miasta">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-shuttle-van"></i><br>Transport zmarłych
              </button>
            </a>
          </div> 
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/uslugi">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fab fa-servicestack"></i><br>Usługi pogrzebowe
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/krematorium">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-images"></i><br>Krematorium
              </button>
            </a>
          </div>   
        </div>  

        <div class="row mt-5">
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/poradnik">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-book"></i><br>Poradnik
              </button>
            </a>
          </div> 
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/kontakt">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-mail-bulk"></i><br>Kontakt
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/ustawienia">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-cogs"></i><br>Ustawienia
              </button>
            </a>
          </div>   
        </div>  

        <div class="row mt-5">
          <div class="col-md-2"></div>
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/media">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-box-open"></i><br>Media
              </button>
            </a>
          </div>    
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/login/logout">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-power-off"></i><br>Wyloguj
              </button>
            </a>
          </div> 
        </div> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->