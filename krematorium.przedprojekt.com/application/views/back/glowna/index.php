<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'glowna') {
  $page = 'Ustawienia strony głównej';
  $icon = 'fas fa-cross';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-35p">Część strony</th>
            <th class="wd-20p">Tytuł</th>
            <th class="wd-30p text-center"><a href="glowna/add" class="add cursor"><i class="fas fa-plus"></i> Dodaj slajd</a></th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
            <tr>
              <td  class="align-middle"><?php echo $i; ?>.</td>
              <td  class="align-middle"><?php echo $row->part_pl; ?></td>
              <td  class="align-middle"><?php echo $row->value1; ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/glowna/edit/<?php echo $row->glowna_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <?php if($row->part == 'slider'): ?>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/glowna/<?php echo $row->glowna_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->