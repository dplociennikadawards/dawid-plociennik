<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'gallery_wpis') {
  $page = 'id ' . $this->uri->segment(4);
  $subpage = 'Dodaj zdjęcie';
  $icon = 'fas fa-images';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-1"></div>
              <div class="col-md-2">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-r-0-force">
                          <input class="form-control" type="hidden" name="photo" id="zdjecie" required>
                          <input class="form-control" type="hidden" name="resolution" id="rozdzielczosc" required>
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/<?php echo $page; ?>" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-4">
                  <div class="pl-md-0 col-12">
                    <div class="form-group">
                      <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner2"></div>
                        <label class="form-control-label">Zdjęcie:</label>
                          <div id="uploaded_image" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box('');"></div>  
                          <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
                <div class="col-md-4">
                  <div class="pl-md-0 col-12">
                    <div class="form-group">
                      <span id="refreshTable">
                        <?php if(empty($gallery)){echo 'brak zdjęć';} ?>
                        <div class="row">
                        <?php foreach ($gallery as $row): ?>
                          <div class="col-3 align-self-center text-center">
                            <span onclick="upPriority(<?php echo $row->id; ?>,'<?php echo $row->page; ?>',<?php echo $row->wpis_id; ?>)">
                              <i class="fas fa-arrow-down cursor" style="font-size: 1.5rem;"></i>
                            </span>&nbsp;
                            <span onclick="downPriority(<?php echo $row->id; ?>,'<?php echo $row->page; ?>',<?php echo $row->wpis_id; ?>)">
                              <i class="fas fa-arrow-up cursor" style="font-size: 1.5rem;"></i>
                            </span>
                          </div>
                          <div class="col-9 align-self-center delete_photo cursor" onclick="delete_photo(<?php echo $row->id; ?>,'gallery_wpis')">
                            <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" width="100%" height="180" class="img-thumbnail" />
                          </div>
                        <?php endforeach ?>
                        </div>
                      </span>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script type="text/javascript">
  
  function upPriority(id,page,wpis)
  {
    var id = id;
    var page = page;
    var wpis = wpis;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/upPriority2", 
         data: {id:id, page:page, wpis:wpis}, 
         cache: false,
         complete:function(html)
         {  
            $('#refreshTable').load(document.URL +  ' #refreshTable');
         }
    });  
  }
  
  function downPriority(id,page,wpis)
  {
    var id = id;
    var page = page;
    var wpis = wpis;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/downPriority2", 
         data: {id:id, page:page, wpis:wpis}, 
         cache: false,
         complete:function(html)
         {
            $('#refreshTable').load(document.URL +  ' #refreshTable');
         }  
    });  
  }

  function delete_photo(id,table)
  {
    var id = id;
    var table = table;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/gallery/delete", 
         data: {id:id, table:table}, 
         cache: false,
         complete:function(html)
         {
      $('#refreshTable').load(document.URL +  ' #refreshTable');
         }  
    });  
  }

</script>