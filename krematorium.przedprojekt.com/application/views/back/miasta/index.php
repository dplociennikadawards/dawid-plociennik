<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'miasta') {
  $page = 'Transport zmarłych';
  $icon = 'fas fa-shuttle-van';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <input class="form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Przeszukaj po lokalizacji">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table id="myTable" class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-30p">Lokalizacja</th>
            <th class="wd-20p">Pracownik</th>
            <th class="wd-20p">Stanowisko</th>
            <th class="wd-25p text-center"><a href="miasta/add" class="add cursor"><i class="fas fa-plus"></i> Dodaj wpis</a></th>
          </thead>
          <tbody>
          <?php $i=0; foreach (array_reverse($rows, true) as $row): $i++; ?>
            <tr>
              <td  class="align-middle"><?php echo $i; ?>.</td>
              <td  class="align-middle"><?php echo $row->lokalizacja; ?></td>
              <td  class="align-middle"><?php echo $row->worker; ?></td>
              <td  class="align-middle"><?php echo $row->stanowisko; ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/miasta/edit/<?php echo $row->miasta_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <?php if($row->miasta_id != '109'): ?>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/miasta/<?php echo $row->miasta_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

    <!-- ########## END: MAIN PANEL ########## -->