<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'kontakt') {
  $page = 'Kontakt - Formularze kontaktowe';
  $icon = 'fas fa-mail-bulk';
  $main_page = 'active';
}
?>


<!-- Full Height Modal Right -->
<div class="modal fade right effect-slide-in-right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Ustawienia kontaktu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="change">Zmiany zapisują się automatycznie</p>
        <div class="form-layout form-layout-2">
          <div class="row no-gutters">
            <div class="pr-md-0 col-12">
              <div class="form-group">
                <label class="form-control-label">Tytuł:</label>
                <input class="form-control" type="text" name="title" value="<?php echo $settings->title; ?>" placeholder="Tytuł na stronie" onchange="update_contact('title');">
              </div>
            </div>
            <div class="pr-md-0 col-12">
              <div class="form-group bd-t-0-force">
                <label class="form-control-label">Podtytuł:</label>
                <textarea class="form-control" rows="5" name="subtitle" onchange="update_contact('subtitle');" placeholder="Podtytuł na stronie"><?php echo $settings->subtitle; ?></textarea>
              </div>
            </div>
            <div class="pr-md-0 col-12">
              <div class="form-group bd-t-0-force">
                <label class="form-control-label">Adres:</label>
                <input class="form-control" type="text" name="address" value="<?php echo $settings->address; ?>" placeholder="Wprowadź adres zakładu" onchange="update_contact('address');">
              </div>
            </div>
            <div class="pr-md-0 col-12">
              <div class="form-group bd-t-0-force">
                <label class="form-control-label">Telefon:</label>
                <input class="form-control" type="text" name="phone" value="<?php echo $settings->phone; ?>" placeholder="Wprowadź numer telefonu" onchange="update_contact('phone');">
              </div>
            </div>
            <div class="pr-md-0 col-12">
              <div class="form-group bd-t-0-force">
                <label class="form-control-label">Adres e-mail:</label>
                <input class="form-control" type="text" name="my_email" value='<?php echo $settings->my_email; ?>' placeholder="Wprowadź adres e-mail na który będą przychodzić maile" onchange="update_contact('my_email');">
              </div>
            </div>
            <div class="pr-md-0 col-12">
              <div class="form-group bd-t-0-force">
                <label class="form-control-label">Mapa Google:</label>
                <textarea class="form-control" rows="5" name="map" onchange="update_contact('map');" placeholder="Wprowadź link do mapy google"><?php echo $settings->map; ?></textarea>
              </div>
            </div>
          </div><!-- row no-gutters -->
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary cursor" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->



<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-5p">Zgoda RODO</th>
            <th class="wd-5p">Zgoda telefoniczna</th>
            <th class="wd-5p">Odpowiedź</th>
            <th class="wd-20p">Imię</th>
            <th class="wd-20p">Adres e-mail</th>
            <th class="wd-20p">Temat</th>
            <th class="wd-25p text-center"><a href="#" data-toggle="modal" data-target="#fullHeightModalRight" class="add cursor"><i class="fas fa-cog"></i> Ustawienia</a></th>
          </thead>
          <tbody>
          <?php $i=0; foreach (array_reverse($rows, true) as $row): $i++; ?>
            <?php if($row->kontakt_id > 1): ?>
              <tr>
                <td  class="align-middle"><?php echo $i; ?>.</td>
                <td  class="align-middle">
                  <?php if($row->zgoda == 1): ?>
                    <i class="text-success fas fa-check tx-20"></i>
                  <?php else: ?>
                    <i class="text-danger fas fa-times tx-20"></i>
                  <?php endif; ?>
                </td>
                <td  class="align-middle">
                  <?php if($row->zgoda2 == 1): ?>
                    <i class="text-success fas fa-check tx-20"></i>
                  <?php else: ?>
                    <i class="text-danger fas fa-times tx-20"></i>
                  <?php endif; ?>
                </td>
                <td  class="align-middle">
                  <?php if($row->odpowiedz == 1): ?>
                    <i class="text-success fas fa-check tx-20"></i>
                  <?php else: ?>
                    <i class="text-danger fas fa-times tx-20"></i>
                  <?php endif; ?>
                  </td>
                <td  class="align-middle"><?php echo $row->name; ?></td>
                <td  class="align-middle"><?php echo $row->email; ?></td>
                <td  class="align-middle"><?php echo $row->subject; ?></td>
                <td  class="align-middle text-center">
                  <a href="<?php echo base_url(); ?>panel/kontakt/show/<?php echo $row->kontakt_id; ?>"><i class="text-primary far fa-eye tx-30  btn"></i></a>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/kontakt/<?php echo $row->kontakt_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                </td>
              </tr>
            <?php endif; ?>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

    <script type="text/javascript">
      function update_contact(field)
      {
        var field = field;
        var value = document.getElementsByName(field)[0].value;
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/update", 
             data: {field:field, value:value}, 
             cache: false,
             complete:function(html)
             {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
             }  
        });  
      }
    </script>