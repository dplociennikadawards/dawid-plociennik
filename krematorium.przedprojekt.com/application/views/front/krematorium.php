  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>

 <section class="bg-white p-md-5 py-3">


<!-- Card -->
<div class="card card-cascade wider reverse">

        <?php if($rows->photo != null): ?>
          <!-- Card image -->
          <div class="view view-cascade overlay" style="min-height: 250px;">
            <img class="card-img-top" src="<?php echo base_url(); ?>upload/<?php echo $rows->photo; ?>" alt="Card image cap" style="min-height: 250px;">
            <a href="#gallery">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        <?php endif; ?>

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center mb-5">

    <!-- Title -->
    <h4 class="card-title text-white text-shadow "><strong><?php echo $rows->title; ?></strong></h4>
    <p class="card-text text-white text-shadow font-weight-bold mb-4" style="font-size: 1rem;"><?php echo $rows->subtitle; ?></p>
    <!-- Text -->
    <p class="card-text text-white text-shadow font-weight-bold " style="font-size: 1rem;"><?php echo $rows->content; ?></p>

  </div>

</div>
<!-- Card -->

 <div id="mdb-lightbox-ui"></div>
<a id="gallery"></a>

<?php if(!empty($galeria_zdjec) && $podstrony->gallery_status == 1): ?>
<?php if($podstrony->type_gallery == 'gallery'): ?>
   <div id="mdb-lightbox-ui"></div>
   <div class="gallery <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?> row px-5 mb-5" id="gallery" style="margin-top: 1rem;">
    <?php foreach ($galeria_zdjec as $photo): ?>
      <!-- Grid column -->
      <figure class="col-md-4 text-center">
      <?php if($podstrony->active_gallery == 1): ?>
        <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
      <?php endif; ?>
          <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
          style="
          background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
          background-size: cover; 
          background-position: center;
          height: 350px; 
          width: 450px;">
        <?php if($podstrony->active_gallery == 1): ?>
          </a>
        <?php endif; ?>
      </figure>
      <!-- Grid column -->
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?php if($podstrony->type_gallery == 'slider'): ?>
     <div id="mdb-lightbox-ui"></div>
<div class="container" style="margin-bottom: 10rem; margin-top: 17rem;">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?>" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators mb-5">
        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
        <li data-target="#carousel-example-1z" data-slide-to="<?php echo $i - 1; ?>" <?php if($i==1){echo 'class="active"';} ?>><?php echo $i; ?></li>
        <?php endforeach; ?>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
          <!--First slide-->
            <figure class="text-center carousel-item  my-shadow <?php if($i==1){echo 'active';} ?>">
              <?php if($podstrony->active_gallery == 1): ?>
              <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
              <?php endif; ?>
                <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
                style="
                background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
                background-size: cover; 
                background-position: center;
                height: 500px; 
                width: 100%;">
              <?php if($podstrony->active_gallery == 1): ?>
                </a>
              <?php endif; ?>
            </figure>
          <!--/First slide-->
        <?php endforeach; ?>

      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</div>
<?php endif; ?>
<?php endif; ?>

 </section>

  <script type="text/javascript">
    function lightbox() {
      $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
      }
  </script>