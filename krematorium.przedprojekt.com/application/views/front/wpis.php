  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $rows->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $rows->header_title; ?></h1>
        <?php if($rows->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $rows->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section>
    <div class="container my-5">
      <!-- Card -->
      <div class="card card-cascade wider reverse">

        <?php if($rows->photo != null): ?>
          <!-- Card image -->
          <div class="view view-cascade overlay">
            <img class="card-img-top" src="<?php echo base_url(); ?>upload/<?php echo $rows->photo; ?>" alt="Card image cap">
            <a href="#gallery">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        <?php endif; ?>
        <!-- Card content -->
        <div class="card-body card-body-cascade text-center mb-5">

          <!-- Title -->
          <h4 class="card-title text-white text-shadow "><strong><?php echo $rows->title; ?></strong> - <small><?php echo date('d.m.Y', strtotime($rows->selected_data)); ?></small></h4>
          <!-- Text -->
          <p class="card-text text-white text-shadow font-weight-bold " style="font-size: 1rem;"><?php echo $rows->subtitle; ?></p>

        </div>

      </div>
      <!-- Card -->
      <p><?php echo $rows->content; ?></p>
    </div>
<div class="container">
<a id="gallery"></a>
<?php if(!empty($galeria_zdjec) && $rows->gallery_status == 1): ?>
<?php if($rows->type_gallery == 'gallery'): ?>
   <div id="mdb-lightbox-ui"></div>
   <div class="gallery <?php if($rows->active_gallery == 1){echo 'mdb-lightbox';} ?> row mb-5" id="gallery">
    <?php foreach ($galeria_zdjec as $photo): ?>
      <!-- Grid column -->
      <figure class="col-md-4 text-center">
      <?php if($rows->active_gallery == 1): ?>
        <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
      <?php endif; ?>
          <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
          style="
          background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
          background-size: cover; 
          background-position: center;
          height: 350px; 
          width: 450px;">
        <?php if($rows->active_gallery == 1): ?>
          </a>
        <?php endif; ?>
      </figure>
      <!-- Grid column -->
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?php if($rows->type_gallery == 'slider'): ?>
     <div id="mdb-lightbox-ui"></div>
<div class="container  mt-5 mt-md-0" style="margin-bottom: 10rem;">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade <?php if($rows->active_gallery == 1){echo 'mdb-lightbox';} ?>" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators mb-5">
        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
        <li data-target="#carousel-example-1z" data-slide-to="<?php echo $i - 1; ?>" <?php if($i==1){echo 'class="active"';} ?>><?php echo $i; ?></li>
        <?php endforeach; ?>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
          <!--First slide-->
            <figure class="text-center carousel-item  my-shadow <?php if($i==1){echo 'active';} ?>">
              <?php if($rows->active_gallery == 1): ?>
              <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
              <?php endif; ?>
                <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
                style="
                background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
                background-size: cover; 
                background-position: center;
                height: 500px; 
                width: 100%;">
              <?php if($rows->active_gallery == 1): ?>
                </a>
              <?php endif; ?>
            </figure>
          <!--/First slide-->
        <?php endforeach; ?>

      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</div>
<?php endif; ?>
<?php endif; ?></div>
  </section>

  
<script type="text/javascript">
    function lightbox() {
      $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
      }
</script>
  