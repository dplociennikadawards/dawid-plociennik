  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
 
 <section class="bg-white p-md-5 p-3">
  <div class="row">
    <?php foreach ($rows as $row): ?>
      <div class="col-md-4 my-4">
        <!-- Card -->
        <div class="card promoting-card bg-img ">

          <!-- Card content -->
          <div class="card-body d-flex flex-row">

            <?php if($row->icon != null): ?>
            <!-- Avatar -->
            <img src="<?php echo base_url(); ?>upload/<?php echo $row->icon; ?>" class="mr-3" height="50px" width="50px" alt="avatar">
            <?php endif; ?>
            <!-- Content -->
            <div class="col align-self-center">
              <?php $slug = $row->title;
              $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')');
              $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '');
              $slug = str_replace($cenzura, $zamiana, $slug); ?>
              <!-- Title -->
              <h4 class="card-title font-weight-bold text-white mb-2"><a href="wpis/<?php echo $slug; ?>/<?php echo $row->uslugi_id; ?>" class="text-white"><?php echo $row->title; ?></a></h4>
              <p class="text-white"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></p>
            </div>

          </div>

          <!-- Card image -->
          <?php if($row->photo != null): ?>
            <div class="view overlay">
              <img class="card-img-top rounded-0" src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" alt="Card image cap">
              <a href="wpis/<?php echo $slug; ?>/<?php echo $row->uslugi_id; ?>">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          <?php endif; ?>
          <!-- Card content -->
          <div class="card-body">

            <div class="collapse-content">

              <!-- Text -->
              <p class="card-text collapse text-white" id="collapseContent"><?php echo $row->subtitle; ?></p>
              <!-- Button -->
              <a href="wpis/<?php echo $slug; ?>/<?php echo $row->uslugi_id; ?>" class="btn btn-block color-yellow text-shadow mt-3">Czytaj więcej</a>

            </div>

          </div>

        </div>
        <!-- Card -->
      </div>
    <?php endforeach ?>
  </div>
 </section>