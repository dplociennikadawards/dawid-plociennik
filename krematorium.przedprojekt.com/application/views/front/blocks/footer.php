<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "rgba(241,231,103,1)",
      "text": "#000"
    },
    "button": {
      "background": "#000",
      "text": "#fff"
    }
  },
  "type": "opt-out",
  "content": {
    "message": "<?php echo $ustawienia->cookies; ?>",
    "dismiss": "Rozumiem",
    "deny": "",
    "allow": "Rozumiem",
    "link": "Czytaj więcej...",
    "href": "<?php echo base_url() . 'upload/' . $ustawienia->pdf;  ?>"
  }
})});
</script>

  <section class="contact">
    <div class='container'>
      <div class='row'>
        <div class='col-lg-3 col-md-6 col-sm-6'>
          <div>
            <p class='contact-header'>Kontakt</p>
            <a href="tel:<?php echo $kontakt->phone; ?>" class="contact-link"><img src="<?php echo base_url(); ?>assets/front/img/phone-yellow.png" class="mr-2 mb-1"><?php echo $kontakt->phone; ?></a>
            <a href="mailto:<?php echo $kontakt->my_email; ?>" class="contact-link-thin"><img src="<?php echo base_url(); ?>assets/front/img/monkey.png" class="mr-2 mb-1"><?php echo $kontakt->my_email; ?></a>
            <p class="contact-link-thin"><img src="<?php echo base_url(); ?>assets/front/img/location.png" class="mr-2 mb-1"><?php echo $kontakt->address; ?></p>
          </div>
          <div class="for-clients">
            <p class='contact-header'>Dla klientów</p>
            <a href="<?php echo base_url(); ?>p/transport_zmarlych" class="contact-link-thin">Transport zmarłych</a>
            <a href="<?php echo base_url(); ?>p/uslugi_pogrzebowe" class="contact-link-thin">Usługi pogrzebowe</a>
            <a href="<?php echo base_url(); ?>p/krematorium" class="contact-link-thin">Krematorium</a>
            <a href="<?php echo base_url(); ?>p/poradnik" class="contact-link-thin">Poradniki</a>

          </div>
        </div>
        <div class='col-lg-3 transport-column col-md-6 col-sm-6'>
          <p class='contact-header'>Transport zwłok</p>
          <?php foreach ($miasta_footer as $miasto): ?>
            <?php $czy = strpos($miasto->lokalizacja, "Polska"); ?>
              <?php if($czy == FALSE): ?>
                <a href="<?php echo base_url(); ?>p/transport_zmarlych?miasto=<?php echo $miasto->lokalizacja; ?>" class="contact-link-thin">
                  <?php echo $miasto->nazwa; ?>
                </a>
            <?php   endif; ?>
          <?php endforeach; ?>
                <a href="<?php echo base_url(); ?>p/transport_zmarlych" class="contact-link-thin">
                  Transport zwłok z innych krajów
                </a>
        </div>
        <div class='col-lg-3 transport-column col-md-6 col-sm-6'>
          <p class='contact-header'>Transport wg miast</p>
          <?php foreach ($miasta_footer as $miasto): ?>
            <?php $czy = strpos($miasto->lokalizacja, "Polska"); ?>
              <?php if($czy == TRUE): ?>
                <a href="<?php echo base_url(); ?>p/transport_zmarlych?miasto=<?php echo $miasto->lokalizacja; ?>" class="contact-link-thin">
                  <?php echo $miasto->nazwa; ?>
                </a>
            <?php   endif; ?>
          <?php endforeach; ?>
                <a href="<?php echo base_url(); ?>p/transport_zmarlych" class="contact-link-thin">
                  Transport zwłok z innych miast
                </a>
        </div>
        <div class='col-lg-3 col-md-6 col-sm-6'>
          <p class='contact-header'>Ostatnio dodane poradniki</p>
          <?php $i=0; foreach ($poradnik as $row): $i++; ?>
          <?php if($i < 4): ?>
            <div>
              <a href="<?php echo base_url(); ?>p/poradnik">
                <div class="advice advice1" style="background-image: url('<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>')">
                  <div class="advice-overlay"><?php echo $row->title; ?></div>
                </div>

              </a>
            </div>
          <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
  </section>
  <footer class="main-footer">
    <div class="container">
      <p class="footer-para">Copyright 2018 by <a href="<?php echo base_url(); ?>" class="text-white">krematorium.pl</a><?php if($ustawienia->pdf != null): ?> | <a href="<?php echo base_url(); ?>upload/<?php echo $ustawienia->pdf; ?>" class="text-white">Polityka prywatności</a><?php endif; ?></p>
    </div>
  </footer>

  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.min.js"></script>

<script type="text/javascript">
    var mapsAPI = "AIzaSyCJ4dpcfW41zqwNu_lJ4EcT7ON-SNXDZrA";
  $.getScript('https://maps.google.com/maps/api/js?key=' + mapsAPI).done(function() {
    initMap();
  });

  function initMap() {
  <?php foreach ($miasta as $row) : ?>
  var miasto<?php echo $row->miasta_id; ?> = new google.maps.LatLng(<?php echo $row->lat; ?>,<?php echo $row->lng; ?> );
   // var   pointB = new google.maps.LatLng(52.919438, 19.145136);
  <?php endforeach; ?>
  <?php foreach ($miasta as $row) : ?>
    <?php if($row->lokalizacja == $_GET['miasto']): ?>
    myOptions = {
      zoom: 5,
      center: miasto<?php echo $row->miasta_id; ?>

    },
  	<?php endif; ?>
    <?php endforeach; ?>
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
     <?php foreach ($miasta as $row) : ?>
    miasto<?php echo $row->miasta_id; ?> = new google.maps.Marker({
      position: miasto<?php echo $row->miasta_id; ?>,
      title: "<?php echo $row->nazwa; ?>",
      label: "",
      map: map
    });
    <?php endforeach; ?>
    //  markerB = new google.maps.Marker({
    //   position: pointB,
    //   title: "Marker B",
    //   label: "B",
    //   map: map
    // });
    <?php foreach ($miasta as $row) : ?>
  miasto<?php echo $row->miasta_id ?>.addListener('click', function(e) {
    map.setCenter(this.position);
    window.location.href = "<?php echo base_url(); ?>p/transport_zmarlych?miasto=<?php echo $row->lokalizacja; ?>";
  });
   <?php endforeach; ?>
}
</script>

    <script type="text/javascript">
      function validateMyForm(nr)
      {
        if (grecaptcha.getResponse(nr) == ""){
            document.getElementsByClassName('warning_captcha')[nr].innerHTML = 'Potwierdź captche';
            return false;
        } else {
            return true;
        }
      }
    </script>
    
</body>

</html>