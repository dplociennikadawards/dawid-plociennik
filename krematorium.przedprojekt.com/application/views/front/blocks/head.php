<?php 
if($this->uri->segment(2) == 'o_nas') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'transport_zmarlych') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'uslugi_pogrzebowe') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'krematorium') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'poradnik') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'kontakt') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'wpis') {
$first = $rows->title . ' - ';
$second = $ustawienia->page_title;
}
else {
$first = $ustawienia->page_title; 
$second = '';
}
?>


<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $first . ' ' . $second; ?></title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front/map/cssmap-europe/cssmap-europe.css" media="screen" />
  <link rel="shortcut icon" href="<?php echo base_url(); ?>upload/<?php echo $ustawienia->icon; ?>" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body style="overflow-x: hidden;" onload="lightbox();">

    <div class="phone-container">
      <a href="tel:<?php echo $kontakt->phone; ?>">
        <div class='circle'><img src="<?php echo base_url(); ?>assets/front/img/phone-big.png"></div>

      </a>
      <p class="text-center mobile">Skontaktuj się!</p>
    </div>