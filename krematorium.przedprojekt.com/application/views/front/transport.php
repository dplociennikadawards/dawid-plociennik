  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="bg-white pb-0">

      <div class="row pt-5 align-items-center"> 

          <div class="col-md-7 offset-md-1">
            <div id="map-canvas" class="pl-md-3" style="width: 100%;height: 500px;"></div>
          </div>

          <div class="col-md-3 mt-4 mt-md-0">
            <div class="row px-5 px-md-0">
              <!-- News jumbotron -->
                  <div class="jumbotron text-left hoverable p-4 my-shadow">
                    <?php if($select_miasto->photo != null): ?>
                      <div class="view overlay">
                        <img src="<?php echo base_url(); ?>upload/<?php echo $select_miasto->photo; ?>" class="img-fluid" alt="Sample image for first version of blog listing">
                        <a>
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                    <?php endif; ?>
                    <div class="text-md-justify ml-3 mt-3 text-white">

                      <h4 class="h4 mb-4"><?php echo $select_miasto->worker; ?></h4>

                      <p class="font-weight-bold"><?php echo $select_miasto->stanowisko; ?> - <?php echo $select_miasto->lokalizacja; ?></p>
                      <?php if($select_miasto->phone != null): ?>
                        <p class="font-weight-normal"><a href="tel:<?php echo $select_miasto->phone; ?>" class="text-white"><i class="fas fa-phone"></i> <?php echo $select_miasto->phone; ?></a></p>
                      <?php endif; ?>
                      <?php if($select_miasto->mobile != null): ?>
                        <p class="font-weight-normal"><a href="tel:<?php echo $select_miasto->mobile; ?>" class="text-white"><i class="fas fa-mobile-alt"></i> <?php echo $select_miasto->mobile; ?></a></p>
                      <?php endif; ?>
                      <?php if($select_miasto->email != null): ?>
                        <p class="font-weight-normal"><a href="mailto:<?php echo $select_miasto->email; ?>" class="text-white"><i class="far fa-envelope"></i> <?php echo $select_miasto->email; ?></a></p>
                      <?php endif; ?>

                    </div>
                  </div>
                  <!-- News jumbotron -->
            </div>
          </div>
          <div class="col-10 offset-1 py-5">
            <?php if(!isset($_GET['brakMiasta'])): ?>
              <div class="" style="
              background-image: url(<?php echo base_url(); ?>upload/miasto3.jpg);
              height: 400px;
              background-position: bottom;
              background-size: cover;
              background-repeat: no-repeat;"></div>
              <div class="text-center main-header text-dark shadow-none" style="text-shadow: none;"><small><?php echo substr($select_miasto->lokalizacja, 0, strpos($select_miasto->lokalizacja, ",")); ?></small></div>
            <p><?php echo $select_miasto->text; ?></p>
            <?php else: ?>
              <div class="mb-3" style="
              background-image: url(<?php echo base_url(); ?>upload/miasto3.jpg);
              height: 400px;
              background-position: bottom;
              background-size: cover;
              background-repeat: no-repeat;"></div>
              <div class="text-center main-header text-dark shadow-none" style="text-shadow: none;"><small>Transport zwłok z innych krajów</small></div>
            <p>
<strong style="font-size: 18px;">Transport zwłok - definicja</strong><hr>

Transport zwłok ma miejsce, gdy dojdzie do śmierci jednej z bliskich nam osób. Dokładne wytyczne dotyczące sposobu transportu zwłok zawarte są w Rozporządzeniu Ministra Zdrowia o wydawaniu pozwoleń oraz zaświadczeń na przewóz zwłok oraz szczątków ludzkich (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). W tym miejscu należy nadmienić, że do organizowania transportu zwłok uprawnione są wyłącznie firmy, które uzyskały odpowiednie zezwolenia. Do składania wniosków o wydanie zaświadczeń i pozwoleń na przewóz ciała zmarłego uprawnieni są członkowie najbliższej rodziny zmarłego. 

 <br><br> 

<strong style="font-size: 18px;">Transport zwłok z innych krajów - jak przebiega?</strong><hr>

Składamy Państwu szczere wyrazy współczucia w związku z utratą ukochanych bliskich. Wiemy, jak trudny jest to okres w życiu. Transport zwłok z innych krajów przebiega według różnorakich procedur w zależności od tego, w jakim kraju miał miejsce zgon danej osoby. Aby dowiedzieć się więcej na temat kosztów sprowadzenia zwłok zapraszamy do kontaktu z naszymi konsultantami. 

<br><br> 

<strong style="font-size: 18px;">Zadzwoń i dowiedz się więcej</strong><hr>

Aby uzyskać szczegółowe informacje odnośnie kosztów transportu zwłok z innych krajów, zapraszamy do skontaktowania się z naszymi konsultantami. 
            </p>
            <?php endif; ?>
          </div> 

      </div>

      <div class="container pb-5">
        <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card1 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla1->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla1->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla1->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla1->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla1->value3; ?></p>
              <a href="<?php echo $kafla1->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla1->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card2 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla2->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla2->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla2->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla2->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla2->value3; ?></p>
              <a href="<?php echo $kafla2->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla2->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card3 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla3->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla3->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla3->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla3->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla3->value3; ?></p>
              <a href="<?php echo $kafla3->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla3->value5; ?></a>

            </div>
          </div>
        </div>
      </div>


  </section>



