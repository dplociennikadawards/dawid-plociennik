  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="bg-white pb-0">


<div class="row p-md-5 p-5">
  <?php foreach($rows as $row): ?>
    <?php if ($row->onas_id > 1): ?>
      <!-- News jumbotron -->
    <div class="jumbotron text-left hoverable w-100 py-4">


        <!-- Grid row -->
        <div class="row">

          <?php if($row->photo != null): ?>
            <!-- Grid column -->
            <div class="col-md-4 offset-md-1 mx-md-3 my-3">

              <!-- Featured image -->
              <div class="view overlay">
                  <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" class="img-fluid">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->
          <?php endif; ?>

          <!-- Grid column -->
          <div class="col-md-7 <?php if($row->photo == null){echo 'col-md-12';} ?> text-md-justify ml-md-3 text-white">

            <p class="font-weight-normal pull-right"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></p>
            <h4 class="h4"><?php echo $row->title; ?></h4>
            <p class="font-weight-bold mb-4"><?php echo $row->subtitle; ?></p>
            <p class="font-weight-normal"><?php echo $row->content; ?></p>


          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->
    </div>
    <!-- News jumbotron -->
  <?php endif; ?>
  <?php endforeach; ?>
    
<!-- Jumbotron -->
<div class="card card-image mb-5 mb-md-0" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $onas->photo ?>);">
  <div class="text-white text-center rgba-black-light py-5 px-4" style="height: 100%;">
    <div class="py-5">

      <!-- Content -->
      <h5 class="h5 icon"><i class="fas fa-book-open"></i> <?php echo $onas->title ?></h5>
      <h2 class="card-title h2 my-4 py-2"><?php echo $onas->subtitle ?></h2>
      <p class="mb-4 pb-2 px-md-5 mx-md-5"><?php echo $onas->content ?></p>
      <a href="<?php echo base_url(); ?>p/uslugi_pogrzebowe" class="btn transport-button color-yellow text-shadow "><i class="fas fa-clone left"></i> Zobacz ofertę</a>

    </div>
  </div>
</div>
<!-- Jumbotron -->


</div>
<?php if(!empty($galeria_zdjec) && $podstrony->gallery_status == 1): ?>
<?php if($podstrony->type_gallery == 'gallery'): ?>
   <div id="mdb-lightbox-ui"></div>
   <div class="gallery <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?> row px-5 mb-5" id="gallery">
    <?php foreach ($galeria_zdjec as $photo): ?>
      <!-- Grid column -->
      <figure class="col-md-4 text-center">
      <?php if($podstrony->active_gallery == 1): ?>
        <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
      <?php endif; ?>
          <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
          style="
          background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
          background-size: cover; 
          background-position: center;
          height: 350px; 
          width: 450px;">
        <?php if($podstrony->active_gallery == 1): ?>
          </a>
        <?php endif; ?>
      </figure>
      <!-- Grid column -->
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?php if($podstrony->type_gallery == 'slider'): ?>
     <div id="mdb-lightbox-ui"></div>
<div class="container mt-5 mt-md-0" style="margin-bottom: 10rem;">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?>" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators mb-5">
        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
        <li data-target="#carousel-example-1z" data-slide-to="<?php echo $i - 1; ?>" <?php if($i==1){echo 'class="active"';} ?>><?php echo $i; ?></li>
        <?php endforeach; ?>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
          <!--First slide-->
            <figure class="text-center carousel-item  my-shadow <?php if($i==1){echo 'active';} ?>">
              <?php if($podstrony->active_gallery == 1): ?>
              <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
              <?php endif; ?>
                <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
                style="
                background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
                background-size: cover; 
                background-position: center;
                height: 500px; 
                width: 100%;">
              <?php if($podstrony->active_gallery == 1): ?>
                </a>
              <?php endif; ?>
            </figure>
          <!--/First slide-->
        <?php endforeach; ?>

      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</div>
<?php endif; ?>
<?php endif; ?>






  </section>

<script type="text/javascript">
    function lightbox() {
      $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
      }
</script>
  