  <section class="main-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="main-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>

        <a href="<?php echo $button->value2; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $button->value1; ?></a>

      </div>
    </div>
    <div class="container icons-container">
      <div class="icons-wrapper">
        <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card1 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla1->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla1->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla1->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla1->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla1->value3; ?></p>
              <a href="<?php echo $kafla1->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla1->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card2 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla2->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla2->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla2->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla2->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla2->value3; ?></p>
              <a href="<?php echo $kafla2->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla2->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card3 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla3->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla3->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla3->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla3->value2; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla3->value3; ?></p>
              <a href="<?php echo $kafla3->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla3->value5; ?></a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="my-carousel">
    <div class="container">
        <div class="container icons-container mobile-container">
            <div class="icons-wrapper">
              <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card1 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla1->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla1->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla1->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla1->value1; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla1->value3; ?></p>
              <a href="<?php echo $kafla1->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla1->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card2 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla2->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla2->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla2->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla2->value1; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla2->value3; ?></p>
              <a href="<?php echo $kafla2->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla2->value5; ?></a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card3 text-center'>
              <img src="<?php echo base_url(); ?>upload/<?php echo $kafla3->value4; ?>" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header"><?php echo $kafla3->value1; ?></p>
                <div class="short-line"></div>
              </div>
                <?php if($kafla3->value2 != null): ?>
                  <p class="my-card-header"><?php echo $kafla3->value1; ?></p>
                <?php endif; ?>
              <p class="card-para"><?php echo $kafla3->value3; ?></p>
              <a href="<?php echo $kafla3->value6; ?>" role="button" class="btn mt-3 color-yellow text-shadow"><?php echo $kafla3->value5; ?></a>

            </div>
          </div>
              </div>
            </div>
          </div>
  <?php if(!empty($slider)): ?>
      <h1 class="carousel-header"><?php echo $center->value1; ?></h1>
      <p class='carousel-para'><span class="yellow-span"><?php echo $center->value2; ?></span></p>
      <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
          <?php $i=0; foreach ($slider as $slajd): $i++;?>
              <li data-target="#carousel-example-1z" data-slide-to="<?php echo ($i - 1); ?>" class="<?php if($i==1){echo 'active';} ?>">
                <?php echo $i; ?>
              </li>
          <?php endforeach; ?>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
          <?php $i=0; foreach ($slider as $slajd): $i++; ?>
              <!--First slide-->
              <div class="carousel-item <?php if($i==1){echo 'active';} ?>">
                <div class="my-item">
                  <div class="row">
                    <?php if($slajd->value4 != null): ?>
                      <?php if($podstrony->active_gallery == 1): ?>
                         <div class="mdb-lightbox col-md-3 offset-md-1">
                            <figure class="text-center">
                              <a href="<?php echo base_url(); ?>upload/<?php echo $slajd->value4 ?>" data-size="<?php echo $slajd->value5 ?>">
                                <img src="<?php echo base_url(); ?>upload/<?php echo $slajd->value4; ?>">
                                </a>
                            </figure>
                            <!-- Grid column -->
                        </div>
                      <?php else: ?>
                        <div class="col-md-3 offset-md-1">
                          <img src="<?php echo base_url(); ?>upload/<?php echo $slajd->value4; ?>"> 
                        </div>
                      <?php endif; ?>
                    <?php endif; ?>
                    <div class="col-md-7 text-left <?php if($slajd->value4 == null){echo 'col-md-11 offset-md-1 text-center';} ?>">
                      <p class="carousel-contact">
                        <?php echo $slajd->value1; ?>
                      </p>
                      <p class="carousel-contact">
                        <?php echo $slajd->value2; ?>
                      </p>
                      <p class="carousel-para text-left <?php if($slajd->value4 == null){echo 'text-center';} ?> p-0">
                        <?php echo $slajd->value3; ?>
                      </p>
                    </div>
                  </div>
                </div>

              </div>
              <!--/First slide-->
          <?php endforeach; ?>
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
          <img src="<?php echo base_url(); ?>assets/front/img/arrow-left.png">
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
          <img src="<?php echo base_url(); ?>assets/front/img/arrow-right.png">
          <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
      </div>
      <!--/.Carousel Wrapper-->
<?php endif; ?>
    </div>
  </section>
  <section class="transport" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $contact->value4; ?>);">
    <div class='container text-center'>
      <h1 class="transport-header text-white text-shadow"><?php echo $contact->value1; ?></h1>
      <a href="tel:<?php echo $kontakt->phone; ?>" class="transport-link text-white text-shadow"><img class="mr-2 mb-2"><i class="fas fa-phone"></i> <?php echo $kontakt->phone; ?></a>
      <p class='transport-para text-white text-shadow font-weight-bold'><?php echo $contact->value3; ?></p>
      <a href="/p/kontakt" role="button" class="transport-button text-white text-shadow"><?php echo $contact->value2; ?></a>
    </div>
  </section>

                         <div id="mdb-lightbox-ui"></div>
<script type="text/javascript">
    function lightbox() {
      $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
      }
</script>
  