  <section class="header-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $podstrony->header_photo ?>) ">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header"><?php echo $podstrony->header_title; ?></h1>
        <?php if($podstrony->header_subtitle != null): ?>
          <p class="main-para text-shadow"><?php echo $podstrony->header_subtitle; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
 
 <section class="bg-white p-md-5 p-3">
  <div class="row mt-3">
    <?php $i=0; foreach ($rows as $row): $i++; ?>
    <div class="col-md-6 mb-3">
      <!-- Rotating card -->
      <div class="card-wrapper" style="height: 500px !important">
        <div id="card-<?php echo $i; ?>" class="card card-rotating text-center bg-img">

          <!-- Front Side -->
          <div class="face front">

            <?php if($row->photo != null): ?>
              <!-- Image-->
              <div class="card-up">
                <img class="card-img-top" src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" alt="Image with a photo of clouds.">
              </div>
            <?php endif; ?>
            <!-- Content -->
            <div class="card-body">
              <h4 class="font-weight-bold text-white mb-3"><?php echo $row->title; ?></h4>
              <p class="font-weight-bold text-gold"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></p>
              <!-- Triggering button -->
              <a class="rotate-btn text-white" data-card="card-<?php echo $i; ?>"><i class="fas fa-redo-alt"></i> Czytaj więcej</a>
            </div>
          </div>
          <!-- Front Side -->

          <!-- Back Side -->
          <div class="face back">
            <div class="card-body">

              <!-- Content -->
              <h4 class="font-weight-bold text-white mb-0"><?php echo $row->subtitle; ?></h4>
              <hr>
              <p class="text-white mb-3"><?php echo $row->content; ?></p>
                <!-- Triggering button -->
                <a class="rotate-btn text-white" data-card="card-<?php echo $i; ?>"><i class="fas fa-undo"></i> Powrót</a>

            </div>
          </div>
          <!-- Back Side -->

        </div>
      </div>
      <!-- Rotating card -->
    </div>
  <?php endforeach; ?>
  </div>
 </section>
