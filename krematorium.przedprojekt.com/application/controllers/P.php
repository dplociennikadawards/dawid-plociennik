<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', 'index');
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 


		$data['button'] = $this->users_m->get_element('glowna',1);
		$data['kafla1'] = $this->users_m->get_element('glowna',2);
		$data['kafla2'] = $this->users_m->get_element('glowna',3);
		$data['kafla3'] = $this->users_m->get_element('glowna',4);
		$data['center'] = $this->users_m->get_element('glowna',5);
		$data['contact'] = $this->users_m->get_element('glowna',6);
		$data['slider'] = $this->users_m->get_slide('glowna' , 'slider');

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/index.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function o_nas()
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get('onas');
		$data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'onas');
		$data['onas'] = $this->users_m->get_element('onas',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/o_nas.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function transport_zmarlych()
	{	
		if(!isset($_GET['miasto'])) { $_GET['miasto'] = 'Białystok, Polska'; $_GET['brakMiasta'] = 'Y';}
		$data['miasta'] = $this->users_m->get('miasta');
		$data['select_miasto'] = $this->users_m->get_city('miasta', $_GET['miasto']);
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 
		$data['kafla1'] = $this->users_m->get_element('glowna',2);
		$data['kafla2'] = $this->users_m->get_element('glowna',3);
		$data['kafla3'] = $this->users_m->get_element('glowna',4);

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/transport.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function uslugi_pogrzebowe()
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get_sortByDate('uslugi');
		$data['uslugi'] = $this->users_m->get_element('uslugi',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/uslugi.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function wpis($slug,$id)
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get_element('uslugi', $id);
		$data['uslugi'] = $this->users_m->get_element('uslugi',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 
		$data['galeria_zdjec'] = $this->users_m->get_sortByPriorityWPIS('gallery_wpis', 'uslugi_pogrzebowe', $id);

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/wpis.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function krematorium()
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get_element('krematorium',1);
		$data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'krematorium');
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/krematorium.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function poradnik()
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get_sortByDate('poradnik');
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/poradnik.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function kontakt()
	{
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poradnik'] = $this->users_m->get_sortByDate('poradnik');
		$data['miasta_footer'] = $this->users_m->get('miasta'); 

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/kontakt.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}
}
