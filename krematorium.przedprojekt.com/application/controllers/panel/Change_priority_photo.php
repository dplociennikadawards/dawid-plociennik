<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_priority_photo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


public function upPriority()  
      {  
		$data['row'] = $this->users_m->get_element_gallery('gallery', $_POST['id']);
		$data['check'] = $this->users_m->get_sortByPriority('gallery',$_POST['page']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority < $key->priority && $_POST['page'] == $key->page)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('gallery', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('gallery', $update2);			



      }  
public function downPriority()  
      {  
		$data['row'] = $this->users_m->get_element_gallery('gallery', $_POST['id']);
		$data['check'] = $this->users_m->get_sortByPriorityDESC('gallery',$_POST['page']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority > $key->priority && $_POST['page'] == $key->page) {
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('gallery', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('gallery', $update2);			

      }  

public function upPriority2()  
      {  
		$data['row'] = $this->users_m->get_element_gallery('gallery_wpis', $_POST['id']);
		$data['check'] = $this->users_m->get_sortByPriority2('gallery_wpis',$_POST['wpis'],$_POST['page']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority < $key->priority && $_POST['page'] == $key->page && $_POST['wpis'] == $key->wpis_id)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('gallery_wpis', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('gallery_wpis', $update2);			



      }  
public function downPriority2()  
      {  
		$data['row'] = $this->users_m->get_element_gallery('gallery_wpis', $_POST['id']);
		$data['check'] = $this->users_m->get_sortByPriorityDESC2('gallery_wpis',$_POST['wpis'],$_POST['page']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority > $key->priority && $_POST['page'] == $key->page && $_POST['wpis'] == $key->wpis_id) {
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('gallery_wpis', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('gallery_wpis', $update2);			

      }  
	

}
