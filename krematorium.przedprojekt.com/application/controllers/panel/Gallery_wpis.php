<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_wpis extends CI_Controller {

public function __construct() { parent::__construct(); }

public function add($id,$page) {
		$data['gallery'] = $this->users_m->get_sortByPriority2('gallery_wpis', $id, $page);
		$this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/galeria/add_wpis',$data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$data['gallery'] = $this->users_m->get_sortByPriority2('gallery_wpis', $id, $page);
				$new_number_priority = 0;
				foreach ($data['gallery'] as $last_priority) {
					$new_number_priority = $last_priority->priority;
				}
				$new_number_priority = $new_number_priority + 1;

				$insert['priority'] = $new_number_priority;
				$insert['wpis_id'] = $id;
				$insert['page'] = $page;
				$insert['active'] = 1;
				$insert['photo'] = $this->input->post('photo');
				$insert['resolution'] = $this->input->post('resolution');

				$this->users_m->insert('gallery_wpis', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Zdjęcie zostało dodane!</p>');

        		redirect('panel/gallery_wpis/add/'.$id.'/'.$page);
			}

	}
}
