<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/main/main_page');
		$this->load->view('back/blocks/footer');
	}

public function delete($table,$id)
	{
		$where = array($table.'_id' => $id);
		$this->users_m->delete($table , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś wpis!</p>');
		redirect('panel/'.$table);
	}
}
