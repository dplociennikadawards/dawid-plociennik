<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uslugi extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get_sortByDate('uslugi');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/uslugi/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[uslugi.title]');
		$this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/uslugi/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('header_photo') != null) {
					$insert['header_photo'] = $this->input->post('header_photo');
				} else {
					$insert['header_photo'] = 'header.png';
				}	

				if($this->input->post('header_title') != null) {
					$insert['header_title'] = $this->input->post('header_title');
				} else {
					$insert['header_title'] = $this->input->post('title');
				}

				if($this->input->post('header_subtitle') != null) {
					$insert['header_subtitle'] = $this->input->post('header_subtitle');
				}

				if($this->input->post('icon') != null) {
					$insert['icon'] = $this->input->post('icon');
				}

				$insert['type_gallery'] = $this->input->post('type_gallery');

				if($this->input->post('gallery_status') != null) {
					$insert['gallery_status'] = $this->input->post('gallery_status');
			 	} else {
			 		$insert['gallery_status'] = 0;
			 	}
				if($this->input->post('active_gallery') != null) {
					$insert['active_gallery'] = $this->input->post('active_gallery');
			 	} else {
			 		$insert['active_gallery'] = 0;
			 	}

				if($this->input->post('photo') != null) {
					$insert['photo'] = $this->input->post('photo');
					$insert['resolution'] = $this->input->post('resolution');
				}

				$insert['title'] = $this->input->post('title');

				if($this->input->post('subtitle') != null) {
					$insert['subtitle'] = $this->input->post('subtitle');
				}

				if($this->input->post('content') != null) {
					$insert['content'] = $this->input->post('content');
					$insert['content'] = str_replace('<p>', '', $insert['content']);
					$insert['content'] = str_replace('</p>', '<br>', $insert['content']);
				}

				if($this->input->post('selected_data') != null) {
					$insert['selected_data'] = date('Y-m-d', strtotime($this->input->post('selected_data')));					
				} else {
					$insert['selected_data'] = date('Y-m-d');
				}


				$this->users_m->insert('uslugi', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/uslugi');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('uslugi', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');
		$this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/uslugi/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				if($this->input->post('header_photo') != null) {
					$update['header_photo'] = $this->input->post('header_photo');
				} else {
					$update['header_photo'] = 'header.png';
				}	
				if($this->input->post('header_title') != null) {
					$update['header_title'] = $this->input->post('header_title');
				} else {
					$update['header_title'] = $this->input->post('title');
				}
				$update['header_subtitle'] = $this->input->post('header_subtitle');
				$update['type_gallery'] = $this->input->post('type_gallery');

				if($this->input->post('gallery_status') != null) {
					$update['gallery_status'] = $this->input->post('gallery_status');
			 	} else {
			 		$update['gallery_status'] = 0;
			 	}
				if($this->input->post('active_gallery') != null) {
					$update['active_gallery'] = $this->input->post('active_gallery');
			 	} else {
			 		$update['active_gallery'] = 0;
			 	}
				$update['photo'] = $this->input->post('photo');
				$update['icon'] = $this->input->post('icon');
				$update['resolution'] = $this->input->post('resolution');
				$update['title'] = $this->input->post('title');
				$update['subtitle'] = $this->input->post('subtitle');
				$update['content'] = $this->input->post('content');
				$update['content'] = str_replace('<p>', '', $update['content']);
				$update['content'] = str_replace('</p>', '<br>', $update['content']);
				if($this->input->post('selected_data') != null) {
					$update['selected_data'] = date('Y-m-d', strtotime($this->input->post('selected_data')));					
				} else {
					$update['selected_data'] = date('Y-m-d');
				}

				$this->db->where('uslugi_id', $id);
				$this->db->update('uslugi', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/uslugi');
			}

	}

}
