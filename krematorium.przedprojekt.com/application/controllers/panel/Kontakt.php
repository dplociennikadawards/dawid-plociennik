<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontakt extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['settings'] = $this->users_m->get_element('kontakt',1);
		$data['rows'] = $this->users_m->get('kontakt');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/kontakt/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function show($id) {
		$data['settings'] = $this->users_m->get_element('kontakt',1);
		$data['row'] = $this->users_m->get_element('kontakt', $id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/kontakt/show', $data);
		$this->load->view('back/blocks/javascript', $data);
		$this->load->view('back/blocks/footer');
	}


public function update() {

		$update[$_POST['field']] = $_POST['value'];

		$this->db->where('kontakt_id', 1);
		$this->db->update('kontakt', $update);
	}

public function send() {

		$update['odpowiedz'] = 1;

		$this->db->where('kontakt_id', $_POST['id']);
		$this->db->update('kontakt', $update);

		header('Location: http://localhost/adawards/My_pages/krematorium/mailer2/thankyou.php?title='.$_POST["title"].'&content='.$_POST["content"].'&liame='.$_POST["liame"].'');
	}

}
