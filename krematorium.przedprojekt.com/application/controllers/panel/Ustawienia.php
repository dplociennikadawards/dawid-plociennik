<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {

		$data['row'] = $this->users_m->get_element('ustawienia', 1);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');
		$this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/ustawienia/index', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['photo'] = $this->input->post('photo');
				$update['icon'] = $this->input->post('icon');
				$update['cookies'] = $this->input->post('cookies');
				$update['pdf'] = $this->input->post('pdf');
				$update['page_title'] = $this->input->post('page_title');

				$this->db->where('ustawienia_id', 1);
				$this->db->update('ustawienia', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został edytowany!</p>');

        		redirect('panel/ustawienia');
			}

	}

}
