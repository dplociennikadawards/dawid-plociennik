<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Podstrony extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get_sortByPriorityPages('podstrony');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/podstrony/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function edit($id) {

		$data['row'] = $this->users_m->get_element('podstrony', $id);

		$this->form_validation->set_rules('header_title', 'tytuł w nagłówku', 'min_length[2]|trim');
		$this->form_validation->set_rules('header_photo', 'zdjęcie nagłówka', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/podstrony/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['header_photo'] = $this->input->post('header_photo');
				$update['header_title'] = $this->input->post('header_title');
				$update['header_subtitle'] = $this->input->post('header_subtitle');
				$update['gallery_status'] = $this->input->post('gallery_status');
				$update['type_gallery'] = $this->input->post('type_gallery');
				$update['active_gallery'] = $this->input->post('active_gallery');

				$this->db->where('podstrony_id', $id);
				$this->db->update('podstrony', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został edytowany!</p>');

        		redirect('panel/podstrony');
			}

	}

}
