<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>DrOko App- logowanie</title>

    <!-- vendor css -->
    <link href="<?php echo base_url(); ?>application/views/template/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>application/views/template/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>application/views/template/css/bracket.css">

      <!-- My styles -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="shortcut icon" href="/assets/img/favicon-32x32.png">

  </head>

  <body>

    <form action="" method="post">
        <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v green-bg">

          <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
            <div class="signin-logo tx-center tx-28 tx-bold tx-inverse mb-3"> 
                <img src="/assets/img/logotyp.png">
            </div>
            <div class="tx-center mg-b-60">Panel zarządzania <a href="/link-do-aplikacji-mobilnej" class="text-success">aplikacją mobilną</a></div>
            <?php if($this->session->flashdata('success'))
                {
                    echo $this->session->flashdata('success');
                }

                if($this->session->flashdata('logged_failed'))
                {
                    echo $this->session->flashdata('logged_failed');
                }
            ?>
            <?php echo validation_errors(); ?>
            <div class="form-group">
              <input name="login" type="text" class="form-control" placeholder="Wprowadź login">
            </div><!-- form-group -->
            <div class="form-group">
              <input name="password" type="password" class="form-control" placeholder="Wprowadź hasło">

            </div><!-- form-group -->
            <button type="submit" class="btn btn-success btn-block" style="cursor: pointer;">Zaloguj</button>

            <!-- <div class="mg-t-60 tx-center">Not yet a member? <a href="" class="tx-info">Sign Up</a></div> -->

          </div><!-- login-wrapper -->
        </div><!-- d-flex -->
    </form>

    <script src="<?php echo base_url(); ?>application/views/template/lib/jquery/jquery.js"></script>
    <script src="<?php echo base_url(); ?>application/views/template/lib/popper.js/popper.js"></script>
    <script src="<?php echo base_url(); ?>application/views/template/lib/bootstrap/js/bootstrap.js"></script>

  </body>
</html>
