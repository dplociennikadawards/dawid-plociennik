<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_priority_photo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


public function upPriority()  
      {  
		$data['row'] = $this->users_m->get_element('field', $_POST['id']);
		$data['check'] = $this->users_m->get_sort('field', 'priority', 'asc');

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority < $key->priority)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->field_id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('field_id', $_POST['id']);
				$this->db->update('field', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('field_id', $old_id);
				$this->db->update('field', $update2);			



      }  
public function downPriority()  
      {  
		$data['row'] = $this->users_m->get_element('field', $_POST['id']);
		$data['check'] = $this->users_m->get_sort('field', 'priority', 'desc');

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority > $key->priority)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->field_id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('field_id', $_POST['id']);
				$this->db->update('field', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('field_id', $old_id);
				$this->db->update('field', $update2);			



      }    

}
