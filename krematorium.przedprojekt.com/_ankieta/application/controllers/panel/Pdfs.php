<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pdfs extends CI_Controller {

    public function showpdf()
    {

        $data['customers'] = $this->users_m->get('customers');
        $data['grupa'] = $this->users_m->get('grupa');
        $this->load->view('back/pdf/index', $data);
    }

    public function printpdf($id)
    {   

		$data['rows'] = $this->users_m->get_parts('formularze', 'formularze_id', $id);
		$data['field'] = $this->users_m->get('field');
		$data['date'] = $this->users_m->get_element('formularze', $id);
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);

        $html = $this->load->view('back/main/pdf', $data, true);
		$pdfFilePath = "Ankieta_AdAwards.pdf";

	    $this->load->library('m_pdf');
    	$this->m_pdf->pdf->WriteHTML($html);
    	$this->m_pdf->pdf->Output($pdfFilePath, "D");   
    }
}