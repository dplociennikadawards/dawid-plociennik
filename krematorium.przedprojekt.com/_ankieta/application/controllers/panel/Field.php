<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {

		$data['rows'] = $this->users_m->get_sortByPriorityPages('field');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/field/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[field.title]');
		$this->form_validation->set_rules('name', 'nazwa', 'min_length[2]|trim|is_unique[field.name]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/field/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				$data['field'] = $this->users_m->get_sortByPriorityPages('field');
				$new_number_priority = 0;
				foreach ($data['field'] as $last_priority) {
					$new_number_priority = $last_priority->priority;
				}
				$new_number_priority = $new_number_priority + 1;

				$insert['priority'] = $new_number_priority;
				$insert['type'] = $this->input->post('type');
				$insert['title'] = $this->input->post('title');
				$insert['value'] = $this->input->post('value');


				$this->users_m->insert('field', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pole zostało dodane!</p>');

        		redirect('panel/field');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('field', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/field/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['type'] = $this->input->post('type');
				$update['title'] = $this->input->post('title');
				$update['value'] = $this->input->post('value');

				$this->db->where('field_id', $id);
				$this->db->update('field', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pole zostało edytowane!</p>');

        		redirect('panel/field');
			}

	}

}
