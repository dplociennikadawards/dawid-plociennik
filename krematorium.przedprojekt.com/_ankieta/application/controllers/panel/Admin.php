<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		
		$data['rows'] = $this->users_m->get_sort('formularze', 'date', 'asc');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/main/main_page', $data);
		$this->load->view('back/blocks/footer');
	}

public function add_field() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/fields/add');
		$this->load->view('back/blocks/footer');
	}

public function show($id) {

		$data['rows'] = $this->users_m->get_parts('formularze', 'formularze_id', $id);
		$data['field'] = $this->users_m->get('field');
		$data['date'] = $this->users_m->get_element('formularze', $id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/main/show', $data);
		$this->load->view('back/blocks/footer');
	}



public function delete($table,$id)
	{
		$where = array($table.'_id' => $id);
		$this->users_m->delete($table , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś wpis!</p>');
		if($table == 'formularze') {
		redirect('panel/admin');
		}
		else {
		redirect('panel/'.$table);
		}
	}
}
