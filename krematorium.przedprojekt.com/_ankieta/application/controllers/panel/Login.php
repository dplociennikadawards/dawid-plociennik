<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data['users'] = $this->users_m->get('users');

		$this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim');
    	$this->form_validation->set_rules('password', 'Hasło', 'min_length[6]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/logowanie/signin-simple.php');
			}
		else
			{
				$login = strtolower($this->input->post('login'));
				$password = md5($this->input->post('password'));
				$logged = false;
				foreach($data['users'] as $check):

					if (($login == $check->login || $login == $check->email) && $password == $check->password && $check->active == '1'):
					
						$session_data['first_name'] = $check->first_name;
						$session_data['last_name'] = $check->last_name;
						$session_data['email'] = $check->email;
						$session_data['name'] = $check->login;
						$session_data['rola'] = $check->rola;
						$session_data['login'] = TRUE;

						$this->session->set_userdata($session_data);
			 			$logged = true;
			 			redirect('panel/admin');
			 			break;
			 		else:
						$logged = false;
					endif;
				endforeach;

				if($logged == false){
					$this->session->set_flashdata('logged_failed', '<p class="text-danger font-weight-bold"> Błędny login lub hasło</p>');
					redirect('panel/login');
				}
			}
	}

public function recovery_password() {

		$data['users'] = $this->users_m->get('users');

		$this->form_validation->set_rules('last_name', 'Nazwisko', 'min_length[2]|trim');
    	$this->form_validation->set_rules('email', 'E-mail', 'valid_email|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s musi zawierać więcej znaków</p>');
    	$this->form_validation->set_message('valid_email', '<p style="color: red;">Pole %s musi być adresem e-mail</p>');

    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/logowanie/recovery_password.php');
			}
		else {	
			$recovery_success = FALSE;

			$last_name = $this->input->post('last_name');
			$email = strtolower($this->input->post('email'));

			foreach($data['users'] as $check):

				if ($last_name == $check->last_name && $email == $check->email && $check->active == 1):
							
					$recovery_success = TRUE;

					$recovery_password_code = md5($check->user_id);

					$update['recovery_password'] = $recovery_password_code;

					$this->db->where(['user_id' => $check->user_id]);
					$this->db->update('users', $update);

			 		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Na Twój adres E-mail została wysłana instrukcja przywracania hasła.</p>');
					redirect('../../recovery_password/thankyou.php?liame='.$email.'&first_name='.$check->first_name.'&id='.$recovery_password_code);
			 		break;

			 	else:
			 		$recovery_success = FALSE;
				endif;

			endforeach;

			if($recovery_success == FALSE)
			{
			 	$this->session->set_flashdata('logged_failed', '<p class="text-danger font-weight-bold">Podałeś złe naziwsko lub adres e-mail</p>');
				$this->load->view('back/logowanie/recovery_password.php');
			}

		}
	}

public function new_password()
	{
    	$this->form_validation->set_rules('password', 'Hasło', 'min_length[6]|required|trim');
    	$this->form_validation->set_rules('confirm_password', 'Hasło', 'min_length[6]|trim|matches[password]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Hasło musi zawierać minmum 6 znaków</p>');
    	$this->form_validation->set_message('matches', '<p style="color: red;">Hasła róznią się od siebie</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/logowanie/new_password.php');
			}
		else
			{		
				$where = $this->input->post('where');	

				$update['password'] = md5($this->input->post('password'));
				$update['recovery_password'] = '';

				$this->db->where(['recovery_password' => $where]);
				$this->db->update('users', $update);
				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Hasło zostało zmienione! Teraz możesz się zalogować</p>');
				redirect('panel/login');
			}
	}



	public function logout()
	{
		$this->session->sess_destroy();
		redirect('p');
	}
	
}
