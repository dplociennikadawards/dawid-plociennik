<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {

		$data['row'] = $this->users_m->get_element('ustawienia', 1);

		$this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/ustawienia/index', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['photo'] = $this->input->post('photo');
				$update['icon'] = $this->input->post('icon');
				$update['cookies'] = $this->input->post('cookies');
				$update['pdf'] = $this->input->post('pdf');
				$update['email'] = $this->input->post('email');
				$update['phone'] = $this->input->post('phone');

				$update['value1'] = $this->input->post('value1');
				$update['value2'] = $this->input->post('value2');
				$update['value3'] = $this->input->post('value3');
				$update['value4'] = $this->input->post('value4');
				$update['value5'] = $this->input->post('value5');
				$update['value6'] = $this->input->post('value6');
				$update['value7'] = $this->input->post('value7');

				$this->db->where('ustawienia_id', 1);
				$this->db->update('ustawienia', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Ustawienia zostały edytowane!</p>');

        		redirect('panel/ustawienia');
			}

	}

}
