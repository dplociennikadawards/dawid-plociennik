<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'field') {
  $page = 'Pola w ankiecie';
  $subpage = 'Dodaj pole';
  $icon = 'fas fa-folder-plus';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Ad Awards</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-12">
                <form action="" method="POST">
                  <div class="row no-gutters">
                        <div class="pr-md-0 col-12">
                          <div class="form-group">
                            <label class="form-control-label">Typ pola:</label>
                              <div class="row">
                                  <div class="col-md-2">
                                    <label class="rdiobox">
                                      <input name="type" type="radio" value="input" required>
                                      <span>Pole krótkiej wiadomości</span>
                                    </label>
                                  </div>
                                  <div class="col-md-2">
                                    <label class="rdiobox">
                                      <input name="type" type="radio" value="radio" required>
                                      <span>Pole jednokrotnego wyboru</span>
                                    </label>
                                  </div>
                                  <div class="col-md-2">
                                    <label class="rdiobox">
                                      <input name="type" type="radio" value="checkbox" required>
                                      <span>Pole wielokrotnego wyboru</span>
                                    </label>
                                  </div>
                                  <div class="col-md-2">
                                    <label class="rdiobox">
                                      <input name="type" type="radio" value="stars" required>
                                      <span>Pole ocen (gwiazdki)</span>
                                    </label>
                                  </div>
                                  <div class="col-md-2">
                                    <label class="rdiobox">
                                      <input name="type" type="radio" value="textarea" required>
                                      <span>Pole wiadomości</span>
                                    </label>
                                  </div>
                              </div>
                          </div>
                        </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Pytanie: <span class="tx-danger">*</span></label>
                          <input type="text" class="form-control" name="title" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Wartość (dla pola jednokrotnego i wielokrotnego wyboru wartości pól rozdzielane są po przecinku): <span class="tx-danger">*</span></label>
                          <textarea class="form-control" rows="6" name="value"></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/field" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
