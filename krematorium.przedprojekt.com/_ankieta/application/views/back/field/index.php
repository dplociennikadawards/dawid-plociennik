<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'field') {
  $page = 'Pola w ankiecie';
  $icon = 'fas fa-folder-plus';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Ad Awards</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <div class="table-wrapper">
          <span id="refreshTable">
            <table id="datatable1" class="table display responsive">
              <thead>
                <tr>
                  <th id="mobile-hide" class="wd-5p">L.p.</th>
                  <th class="wd-10p">Kolejność</th>
                  <th class="wd-20p">Typ pola</th>
                  <th class="wd-40p">Pytanie</th>
                  <th class="wd-15p">Data dodania</th>
                  <td class="wd-10p"><a href="<?php echo base_url(); ?>panel/field/add">Dodaj pole</a></td>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach ($rows as $row): $i++; ?>
                        <tr>
                          <td id="mobile-hide" class="align-middle"><?php echo $i; ?>.</td>
                          <td id="mobile-hide" class="align-middle">
                            <span onclick="upPriority(<?php echo $row->field_id; ?>)">
                              <i class="fas fa-arrow-down cursor" style="font-size: 1.5rem;"></i>
                            </span>&nbsp;
                            <span onclick="downPriority(<?php echo $row->field_id; ?>)">
                              <i class="fas fa-arrow-up cursor" style="font-size: 1.5rem;"></i>
                            </span>
                          </td>
                          <td class="align-middle">
                            <?php if($row->type == 'input') {echo 'Pole krótkiej wiadomości';} ?>
                            <?php if($row->type == 'radio') {echo 'Pole jednokrotnego wyboru';} ?>
                            <?php if($row->type == 'checkbox') {echo 'Pole wielokrotnego wyboru';} ?>
                            <?php if($row->type == 'stars') {echo 'Pole ocen (gwiazdki)';} ?>
                            <?php if($row->type == 'textarea') {echo 'Pole wiadomości';} ?>
                          </td>
                          <td class="align-middle">
                            <?php echo $row->title; ?>
                          </td>
                          <td class="align-middle"><?php echo date('d/m/Y', strtotime($row->date)); ?></td>
                          <td>
                            <a href="<?php echo base_url(); ?>panel/field/edit/<?php echo $row->field_id; ?>">Edytuj</a> | 
                            <a href="<?php echo base_url(); ?>panel/admin/delete/field/<?php echo $row->field_id; ?>">Usuń</a>
                          </td>
                        </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </span>
          </div><!-- table-wrapper -->

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script type="text/javascript">
  
  function upPriority(id)
  {
    var id = id;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/upPriority", 
         data: {id:id}, 
         cache: false,
         success:function(html){
         },
         complete:function(html)
         {  
            $('#refreshTable').load(document.URL +  ' #refreshTable');
         }
    });  
  }
  
  function downPriority(id)
  {
    var id = id;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/downPriority", 
         data: {id:id}, 
         cache: false,
         complete:function(html)
         {
            $('#refreshTable').load(document.URL +  ' #refreshTable');
         }  
    });  
  }
</script>