<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'ustawienia') {
  $page = 'Ustawienia';
  $icon = 'icon fas fa-cogs';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">
                  <div class="row no-gutters">
                        <div class="pr-md-0 col-md-6 col-12">
                          <div class="form-group">
                            <label class="form-control-label">Adres E-mail (na który będą przychodzić ankiety):</label>
                            <input class="form-control" type="text" name="email" value="<?php echo $row->email; ?>" required>
                          </div>
                        </div>
                        <div class="pr-md-0 col-md-6 col-12">
                          <div class="form-group bd-l-0-force">
                            <label class="form-control-label">Numer telefonu:</label>
                            <input class="form-control" type="text" name="phone" value="<?php echo $row->phone; ?>" required>
                          </div>
                        </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść informacji cookies:</label>
                          <textarea class="form-control" rows="6" name="cookies" required><?php echo $row->cookies; ?></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł w nagłówku: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="value1" value="<?php echo $row->value1; ?>" required>
                          <input class="form-control" type="hidden" value="<?php echo $row->photo; ?>" name="photo" id="zdjecie" required>
                          <input class="form-control" type="hidden" value="<?php echo $row->pdf; ?>" name="pdf" id="pdf" required>
                          <input class="form-control" type="hidden" value="<?php echo $row->icon; ?>" name="icon" id="zdjecie3" required>
                          <input class="form-control" type="hidden" name="resolution" id="rozdzielczosc" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="value2" value="<?php echo $row->value2; ?>" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Pod-pod tytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="value3" value="<?php echo $row->value3; ?>" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł w ankiecie:</label>
                          <input class="form-control" type="text" name="value4" value="<?php echo $row->value4; ?>" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł w ankiecie:</label>
                          <input class="form-control" type="text" name="value5" value="<?php echo $row->value5; ?>" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł wiadomości dla ankietowanych:</label>
                          <input class="form-control" type="text" name="value6" value="<?php echo $row->value6; ?>" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wiadomości dla ankietowanych:</label>
                          <textarea class="form-control" rows="6" name="value7" required><?php echo $row->value7; ?></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/uslugi" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-5">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
                      <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner2"></div>
                        <label class="form-control-label">Logo:</label>
                          <div id="uploaded_image" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box('');"style="width: 20%">
                            <?php if($row->photo != null): ?>
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" width="100%" height="180" class="img-thumbnail" />
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force bd-t-0-force">
                      <form method="post" id="upload_pdf" enctype="multipart/form-data">
                        <div id="spinnerPDF"></div>
                        <label class="form-control-label">Plik PDF polityki prywatności:</label>
                          <div id="uploaded_pdf" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_pdf();" >
                            <?php if($row->pdf != null): ?>
                              <a href="<?php echo base_url(); ?>upload/<?php echo $row->pdf; ?>">
                                <i class="far fa-file-pdf"></i> <?php echo $row->pdf; ?>
                              </a>
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="pdf_file" name="pdf_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-t-0-force bd-l-0-force">
                      <form method="post" id="upload_form3" enctype="multipart/form-data">
                        <div id="spinner3"></div>
                        <label class="form-control-label">Ikonka:</label>
                          <div id="uploaded_image3" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box(3);" style="width: 20%">
                            <?php if($row->icon != null): ?>
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->icon; ?>" width="100%" height="180" class="img-thumbnail" />
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="image_file3" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
