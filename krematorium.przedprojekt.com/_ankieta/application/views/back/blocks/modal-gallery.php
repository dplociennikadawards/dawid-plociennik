<!-- Full Height Modal Right -->
<div class="modal fade right effect-slide-in-right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Galeria zdjęć - <?php echo ucfirst($this->uri->segment(2)); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <span id="refreshTable">
        <?php if(empty($gallery)){echo 'brak zdjęć';} ?>
	        <div class="row">
		        <?php foreach ($gallery as $row): ?>
		        	<div class="col-3 align-self-center text-center">
		        		<span onclick="upPriority(<?php echo $row->id; ?>,'<?php echo $row->page; ?>')">
		        			<i class="fas fa-arrow-down cursor" style="font-size: 1.5rem;"></i>
		        		</span>&nbsp;
		        		<span onclick="downPriority(<?php echo $row->id; ?>,'<?php echo $row->page; ?>')">
		        			<i class="fas fa-arrow-up cursor" style="font-size: 1.5rem;"></i>
		        		</span>
		        	</div>
		        	<div class="col-9 align-self-center delete_photo cursor" onclick="delete_photo(<?php echo $row->id; ?>,'gallery')">
		        		<img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" width="100%" height="180" class="img-thumbnail" />
		        	</div>
		        <?php endforeach; ?>
		    </div>
	    </span>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary cursor" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->

    <!-- Button trigger modal -->
<button type="button" class="btn btn-primary gallery cursor" data-toggle="modal" data-target="#fullHeightModalRight">
<i class="fas fa-arrow-alt-circle-left"></i>
</button>



<script type="text/javascript">
  
  function upPriority(id,page)
  {
    var id = id;
    var page = page;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/upPriority", 
         data: {id:id, page:page}, 
         cache: false,
         complete:function(html)
         {	
			$('#refreshTable').load(document.URL +  ' #refreshTable');
         }
    });  
  }
  
  function downPriority(id,page)
  {
    var id = id;
    var page = page;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/Change_priority_photo/downPriority", 
         data: {id:id, page:page}, 
         cache: false,
         complete:function(html)
         {
			$('#refreshTable').load(document.URL +  ' #refreshTable');
         }  
    });  
  }

  function delete_photo(id,table)
  {
    var id = id;
    var table = table;
    $.ajax({  
         type: "post", 
         url:"<?php echo base_url(); ?>panel/gallery/delete", 
         data: {id:id, table:table}, 
         cache: false,
         complete:function(html)
         {
      $('#refreshTable').load(document.URL +  ' #refreshTable');
         }  
    });  
  }

</script>