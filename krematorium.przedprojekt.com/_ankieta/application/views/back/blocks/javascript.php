<script type="text/javascript">
$( document ).ready(function() {
$($(".summernote").summernote({
      height: 250,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        },
                  toolbar: [
                ["style", ["style"]],
                ["font", ["bold", "underline", "clear"]],
                //["fontname", ["fontname"]],
                ['fontsize', ['fontsize']],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["table", ["table"]],
                ["insert", ["link", "picture", "video"]],
                ["view", ["fullscreen", "codeview", "help"]]
            ],
    }));
});
</script>


<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_form3').change('#image_file3', function(e){  
           e.preventDefault();  
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/photo2",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { 
                      document.getElementById('spinner3').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  
                     {  
                        $('#uploaded_image3').html(data);  
                     },
                    complete:function(data) {
                        document.getElementById('zdjecie3').value = document.getElementById('name_photo2').value;
                        document.getElementById('spinner3').innerHTML = '';
                     }     
                });  
      });  
 });  

</script>

<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_form').change('#image_file', function(e){  
           e.preventDefault();  
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/index",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { 
                      document.getElementById('spinner2').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  
                     {  
                        $('#uploaded_image').html(data);  
                     },
                    complete:function(data) {
                      	document.getElementById('rozdzielczosc').value = document.getElementById('send_resolution').value;
                      	document.getElementById('zdjecie').value = document.getElementById('name_photo').value;
                        document.getElementById('spinner2').innerHTML = '';
                     }     
                });  
      });  
 });  

</script>

<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_form2').change('#image_file2', function(e){  
           e.preventDefault();   
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/photo1",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { 
                      document.getElementById('spinner').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  
                     {  
                        $('#uploaded_image2').html(data);  
                     },
                    complete:function(data) {
                      	document.getElementById('header_zdjecie').value = document.getElementById('name_photo1').value;
                        document.getElementById('spinner').innerHTML = '';
                     }   
                });    
      });  
 });  

</script>

<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_pdf').change('#pdf_file', function(e){  
           e.preventDefault();   
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/pdf",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { 
                      document.getElementById('spinnerPDF').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  
                     {  
                        $('#uploaded_pdf').html(data);  
                     },
                    complete:function(data) {
                        document.getElementById('pdf').value = document.getElementById('pdf_file').value;
                        document.getElementById('spinnerPDF').innerHTML = '<p class="text-success"><i class="fas fa-check"></i> Plik został dodany!</p>';
                     }   
                });    
      });  
 });  

</script>

<script type="text/javascript">

  function formatDate()
    {  
      var date = document.getElementsByName('date')[0].value;
      var mouth = date.substring(0, 2);
      var day = date.substring(3, 5);
      var year = date.substring(6, 10);
      document.getElementsByName('date')[0].value = day + '.' + mouth + '.' + year;
    }


</script>

<script type="text/javascript">
    function clear_box(number_box)
  {
    document.getElementById('uploaded_image'+number_box).innerHTML = '';
    document.getElementById('zdjecie'+number_box).value = '';
  }
</script>

<script type="text/javascript">
    function clear_pdf()
  {
    document.getElementById('uploaded_pdf').innerHTML = '';
    document.getElementById('pdf').value = '';
  }
</script>