<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'admin') {
  $page = 'Ankiety z opiniami';
  $icon = 'fab fa-wpforms';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Ad Awards</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">

        <div class="table-wrapper">
            <table id="datatable1" class="table display responsive">
              <thead>
                <tr>
                  <th id="mobile-hide" class="wd-10p">L.p.</th>
                  <th class="wd-30p">Ankieta</th>
                  <th class="wd-40p">Data dodania</th>
                  <th class="wd-20p"></th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach ($rows as $row): $i++; ?>
                        <tr>
                          <td id="mobile-hide" class="align-middle"><?php echo $i ?>. </td>
                          <td class="align-middle">
                            Ankieta Ad Awards
                          </td>
                          <td class="align-middle"><?php echo date('d/m/Y', strtotime($row->date)); ?></td>
                          <td>
                            <a href="<?php echo base_url(); ?>panel/pdfs/printpdf/<?php echo $row->formularze_id; ?>">Drukuj PDF</a> | 
                            <a href="<?php echo base_url(); ?>panel/admin/show/<?php echo $row->formularze_id; ?>">Wyświetl</a> | 
                            <a href="<?php echo base_url(); ?>panel/admin/delete/formularze/<?php echo $row->formularze_id; ?>">Usuń</a>
                          </td>
                        </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
    <script type="text/javascript">
    
    function table()
    {
        $('#datatable1').DataTable({
          bLengthChange: false,
          searching: true,
          responsive: true
        });
    }

</script>