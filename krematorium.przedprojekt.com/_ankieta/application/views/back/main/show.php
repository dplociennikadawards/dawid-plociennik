<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'admin') {
  $page = 'Ankieta z dnia ';
  $icon = 'fab fa-wpforms';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Ad Awards</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> - <?php echo date('H:i d/m/Y', strtotime($date->date)); ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

<style type="text/css">
  p span {
    color: black;
  }
</style>
      <div class="br-pagebody">
      	<div class="row">
      		<div class="col-md-12">
                <?php foreach ($rows as $row => $value): ?>
                      <?php $j=0; foreach ($value as $val): $j++; ?>
                        <?php if($j > 2): ?>
                          <?php echo '<p>' . $val . '</p>'; ?> 
                        <?php endif; ?>
                      <?php endforeach ?>
                <?php endforeach ?>
      		</div>
      	</div>



      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
    <script type="text/javascript">
    
    function table()
    {
        $('#datatable1').DataTable({
          bLengthChange: false,
          searching: true,
          responsive: true
        });
    }

</script>