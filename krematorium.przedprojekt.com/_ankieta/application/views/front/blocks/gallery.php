  <section class="article gallery">
    <div class="container">
      <div class="row">
<?php if(!empty($galeria_zdjec) && $podstrony->gallery_status == 1): ?>
<?php if($podstrony->type_gallery == 'gallery'): ?>
   <div id="mdb-lightbox-ui"></div>
   <div class="gallery <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?> row px-5 mb-5" id="gallery">
    <?php foreach ($galeria_zdjec as $photo): ?>
      <!-- Grid column -->
      <figure class="col-md-4 text-center">
      <?php if($podstrony->active_gallery == 1): ?>
        <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
      <?php endif; ?>
          <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
          style="
          background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
          background-size: cover; 
          background-position: center;
          height: 350px; 
          width: 450px;">
        <?php if($podstrony->active_gallery == 1): ?>
          </a>
        <?php endif; ?>
      </figure>
      <!-- Grid column -->
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?php if($podstrony->type_gallery == 'slider'): ?>
     <div id="mdb-lightbox-ui"></div>
<div class="container mt-5 mt-md-0" style="margin-bottom: 10rem;">
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators mb-5">
        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
        <li data-target="#carousel-example-1z" data-slide-to="<?php echo $i - 1; ?>" <?php if($i==1){echo 'class="active"';} ?>><?php echo $i; ?></li>
        <?php endforeach; ?>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($galeria_zdjec as $photo): $i++; ?>
          <!--First slide-->
            <figure class="text-center carousel-item <?php if($podstrony->active_gallery == 1){echo 'mdb-lightbox';} ?> my-shadow <?php if($i==1){echo 'active';} ?>">
              <?php if($podstrony->active_gallery == 1): ?>
              <a href="<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>" data-size="<?php echo $photo->resolution ?>">
              <?php endif; ?>
                <img class="img-fluid my-shadow" src="<?php echo base_url(); ?>assets/front/img/invisible.png"  
                style="
                background: url('<?php echo base_url(); ?>upload/<?php echo $photo->photo ?>'); 
                background-size: cover; 
                background-position: center;
                height: 500px; 
                width: 100%;">
              <?php if($podstrony->active_gallery == 1): ?>
                </a>
              <?php endif; ?>
            </figure>
          <!--/First slide-->
        <?php endforeach; ?>

      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</div>
<?php endif; ?>
<?php endif; ?>
    </div>
  </section>