
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "rgba(241,231,103,1)",
      "text": "#000"
    },
    "button": {
      "background": "#000",
      "text": "#fff"
    }
  },
  "type": "opt-out",
  "content": {
    "message": "<?php echo $ustawienia->cookies; ?>",
    "dismiss": "Rozumiem",
    "deny": "",
    "allow": "Rozumiem",
    "link": "Czytaj więcej...",
    "href": "<?php echo base_url() . 'upload/' . $ustawienia->pdf;  ?>"
  }
})});
</script>
  <footer class="footer">
    <div class="container">
      <div class="footer__link-box">
        <a class="footer__link" href="<?php echo base_url(); ?>p/o_mnie">O mnie</a>
        <a class="footer__link" href="<?php echo base_url(); ?>p/biorezonans">Biorezonans</a>
        <a class="footer__link" href="<?php echo base_url(); ?>p/bioenergoterapia">Bioenergoterapia </a>
        <a class="footer__link" href="<?php echo base_url(); ?>p/metody_pracy">Metody pracy</a>
        <a class="footer__link" href="<?php echo base_url(); ?>p/kontakt">Kontakt</a>
        <a class="footer__link" href="<?php echo base_url(); ?>p/blog">Blog</a>
      </div>
      <div class="footer__logo-frame">
        <div><img class="footer__logo" src="<?php echo base_url(); ?>assets/front/img/footer_logo.png"></div>
      </div>

      <div class="footer__contact-box">

        <div><a class="footer__contact-link" href="tel:<?php echo $kontakt->phone; ?>"><i class="footer__icon fas fa-phone fa-flip-horizontal"></i>
            <?php echo $kontakt->phone; ?></a>
          <a class="footer__contact-link" href="mailto:<?php echo $kontakt->my_email; ?>"><i class="footer__icon far fa-envelope"></i>
            <?php echo $kontakt->my_email; ?></a></div>

        <div>
          <?php if($ustawienia->facebook != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->facebook; ?>"><i class="top-bar__socialicon fab fa-facebook-f"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->twitter != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->twitter; ?>"><i class="top-bar__socialicon fab fa-twitter"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->instagram != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->instagram; ?>"><i class="top-bar__socialicon fab fa-instagram"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->youtube != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->youtube; ?>"><i class="top-bar__socialicon fab fa-youtube"></i></a>
          <?php endif; ?>
        </div>

      </div>
    </div>
    <span class="footer__separator"></span>
    <div class="container">
      <div class="footer__copyrights-box">
        <span class="footer__copyrights-text">
          Wszystkie prawa zastrzeżone © 2019 ReLife | <a href="<?php echo base_url() ?>upload/<?php echo $ustawienia->pdf; ?>" class="footer__author" target="_blank">Polityka prywatności</a>
        </span>
        <span class="footer__copyrights-text">
            Created by <a href="https://agencjamedialna.pro"><span class="footer__author">Ad Awards</span></a>
        </span>
      </div>
    </div>
  </footer>


  <!-- /project-->
<script type="text/javascript">

$('#example').popover()
function lightbox() {
    $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
    $('[data-toggle="popover-hover"]').popover()
  }
</script>
    <script type="text/javascript">
      function validateMyForm(nr)
      {
        if (grecaptcha.getResponse(nr) == ""){
            document.getElementsByClassName('warning_captcha')[nr].innerHTML = 'Potwierdź captche';
            return false;
        } else {
            return true;
        }
      }
    </script>
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
</body>

</html>