 <header>
    <div class="top-bar">
      <div class="container top-bar__container">
        <div><a class="top-bar__contact-link" href="tel:<?php echo $kontakt->phone; ?>"><i class="top-bar__icon fas fa-phone fa-flip-horizontal"></i>
            <?php echo $kontakt->phone; ?></a>
          <a class="top-bar__contact-link" href="mailto:<?php echo $kontakt->my_email; ?>"><i class="top-bar__icon far fa-envelope"></i>
            <?php echo $kontakt->my_email; ?></a></div>

        <div>
          <?php if($ustawienia->facebook != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->facebook; ?>"><i class="top-bar__socialicon fab fa-facebook-f"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->twitter != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->twitter; ?>"><i class="top-bar__socialicon fab fa-twitter"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->instagram != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->instagram; ?>"><i class="top-bar__socialicon fab fa-instagram"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->youtube != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->youtube; ?>"><i class="top-bar__socialicon fab fa-youtube"></i></a>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <!--Navbar-->
    <nav class="navbar-style navbar navbar-expand-lg navbar-light">

      <!-- Navbar brand -->
      <div class="container">
        <a class="navbar-style__brand-link navbar-brand" href="/"><img class="navbar-style__logo" src="<?php echo base_url(); ?>upload/<?php echo $ustawienia->photo; ?>" alt="logo reLife"></a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

          <!-- Links -->
          <ul class="navbar-style__list navbar-nav">
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/o_mnie">O mnie</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/biorezonans">Biorezonans</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/bioenergoterapia">Bioenergoterapia</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/metody_pracy">Metody pracy</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/kontakt">Kontakt</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/blog">Blog</a>
            </li>

          </ul>
          <!-- /Links -->

        </div>
        <!-- /Collapsible content -->
      </div>
    </nav>
    <!--/.Navbar-->

    <div class="header-carousel">
      <!--Carousel Wrapper-->
      <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="header-carousel__carousel-indicators carousel-indicators">
          <?php $i=0; foreach ($slider as $row): $i++; ?>
            <li class="header-carousel__single-indicator" data-target="#carousel-example-1z" data-slide-to="<?php echo ($i - 1); ?>" class="<?php if($i==1){echo 'active';} ?>"></li>
          <?php endforeach; ?>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">

        <?php $i=0; foreach ($slider as $row): $i++; ?>
          <div class="header-carousel__slide carousel-item <?php if($i==1){echo 'active';} ?>">
            <div class="header-carousel__img" style="background-image: url('<?php echo base_url(); ?>upload/<?php echo $row->value4; ?>')"></div>
            <div class="container">
              <div class="header-carousel__title-box">
                <h4 class="header-carousel__title"><?php echo $row->value1; ?></h4>
                <p class="slider-subtitle"><?php echo $row->value2; ?></p>
                <p class="header-carousel__paragraph"><?php echo $row->value3; ?></p>
                <a class="header-carousel__link" href="<?php echo $row->value7; ?>"><button class="header-carousel__btn"><?php echo $row->value6; ?></button></a>
              </div>
            </div>
          </div>
        <?php endforeach ?>

        </div>
        <!--/.Slides-->
      </div>
      <!--/.Carousel Wrapper-->
    </div>
  </header>