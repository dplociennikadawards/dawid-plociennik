 <header>
    <div class="top-bar">
      <div class="container top-bar__container">
        <div><a class="top-bar__contact-link" href="tel:<?php echo $kontakt->phone; ?>"><i class="top-bar__icon fas fa-phone fa-flip-horizontal"></i>
            <?php echo $kontakt->phone; ?></a>
          <a class="top-bar__contact-link" href="mailto:<?php echo $kontakt->my_email; ?>"><i class="top-bar__icon far fa-envelope"></i>
            <?php echo $kontakt->my_email; ?></a></div>

        <div>
          <?php if($ustawienia->facebook != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->facebook; ?>"><i class="top-bar__socialicon fab fa-facebook-f"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->twitter != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->twitter; ?>"><i class="top-bar__socialicon fab fa-twitter"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->instagram != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->instagram; ?>"><i class="top-bar__socialicon fab fa-instagram"></i></a>
          <?php endif; ?>
          <?php if($ustawienia->youtube != null): ?>
            <a class="top-bar__socialmedia-link" href="<?php echo $ustawienia->youtube; ?>"><i class="top-bar__socialicon fab fa-youtube"></i></a>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <!--Navbar-->
    <nav class="navbar-style navbar navbar-expand-lg navbar-light">

      <!-- Navbar brand -->
      <div class="container">
        <a class="navbar-style__brand-link navbar-brand" href="/"><img class="navbar-style__logo" src="<?php echo base_url(); ?>upload/<?php echo $ustawienia->photo; ?>" alt="logo reLife"></a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

          <!-- Links -->
          <ul class="navbar-style__list navbar-nav">
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/o_mnie">O mnie</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/biorezonans">Biorezonans</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/bioenergoterapia">Bioenergoterapia</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/metody_pracy">Metody pracy</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/kontakt">Kontakt</a>
            </li>
            <li class="navbar-style__nav-item nav-item">
              <a class="navbar-style__nav-link nav-link" href="<?php echo base_url(); ?>p/blog">Blog</a>
            </li>

          </ul>
          <!-- /Links -->

        </div>
        <!-- /Collapsible content -->
      </div>
    </nav>
    <style type="text/css">
      .podstrony-bg {
        background-image: url('<?php echo base_url() . 'upload/' . $podstrony->header_photo; ?>');
        background-position: center;
        background-size: cover;
      }
    </style>
    <!--/.Navbar-->
    <div class="container-fluid header-photo podstrony-bg">
      <div class="row align-items-center">
          <div class="col-12 align-self-center text-center">
            <h3 class="header-title"><?php echo $podstrony->header_title; ?></h3>
            <?php if($podstrony->header_subtitle != null): ?>
              <p class="header-subtitle"><?php echo $podstrony->header_subtitle; ?></p>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </header>