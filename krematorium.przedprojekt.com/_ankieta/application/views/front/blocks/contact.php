  <section class="contact" style="background-image: url('<?php echo base_url(); ?>upload/<?php echo $contact->value4; ?>')">
    <div class="container">
      <div class="contact__container">
        <h3 class="contact__title"><?php echo $contact->value1; ?></h3>

        <span class="single__star-separator single__star-separator--double">
          <span class="single__star-line"></span>
          <img class="single__star-img" src="<?php echo base_url(); ?>assets/front/img/star.png" alt="gwiazda">
          <span class="single__star-line"></span>
        </span>

        <div class="contact__link-box">
          <a href="tel:<?php echo $kontakt->phone; ?>" class="contact__link"><i class="contact__icon fas fa-phone fa-flip-horizontal"></i><?php echo $kontakt->phone; ?></a>
          <a href="mailto:<?php echo $kontakt->my_email; ?>" class="contact__link"><i class="contact__icon far fa-envelope"></i><?php echo $kontakt->my_email; ?></a>
        </div>

        <h3 class="contact__title contact__title--second-title"><?php echo $contact->value2; ?></h3>

        <div class="contact__form-box">
          <form class="contact__form" action="<?php echo base_url(); ?>mailer/thankyou.php" method="post">
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" id="form-contact-name" class="form-control" name="my_email" value="<?php echo $kontakt->my_email ?>">
                <input class="contact__text-input" type="text" name="first_name" id="" placeholder="Twoje imię">
              </div>
              <div class="col-md-6">
                <input class="contact__text-input" type="text" name="last_name" id="" placeholder="Twoje nazwisko">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <input class="contact__text-input" type="tel" name="phone" id="" placeholder="Twój numer telefonu">
              </div>
              <div class="col-md-6">
                <input class="contact__text-input" type="email" name="liame" id="" placeholder="Twój adres e-mail">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <input class="contact__text-input" type="text" name="subject" id="" placeholder="Temat">
              </div>
              <div class="col-md-12">
                <textarea class="contact__text-input contact__text-input--textarea" name="message" id="" cols="30" rows="10"
              placeholder="Wiadomości"></textarea>
              </div>
            </div>
            <div class="contact__form-row">

              <label class="contact__personal-data-chechbox"><span class="contact__form-label"><small>Zapoznałem się z
                  informacją o
                  administratorze i przetwarzaniu danych.</small></span>
                <input type="checkbox" name="zgoda" id="" value="1">
                <span class="contact__checkmark"></span>
              </label>


            </div>
            <div class="contact__form-row">

              <label class="contact__personal-data-chechbox"><span class="contact__form-label"><small>Wyrażam zgode na otrzymywanie informacji handlowych drogą elektroniczną.</small></span>
                <input type="checkbox" name="zgoda2" id="" value="1">
                <span class="contact__checkmark"></span>
              </label>


            </div>
            <div class="contact__form-row">

              <label class="contact__personal-data-chechbox"><span class="contact__form-label"><small>Wyrażam zgode na otrzymywanie informacji handlowych drogą telefoniczną.</small></span>
                <input type="checkbox" name="zgoda3" id="" value="1">
                <span class="contact__checkmark"></span>
              </label>
            </div>
            <div class="contact__form-row">

              <div class="g-recaptcha mx-auto" data-sitekey="6Lf3h5QUAAAAAIb3CFuKUWKU26DjCeb5iX2CbxTc"></div>
              <input class="contact__submit-btn" type="submit" value="wyślij"  style="height: 50px;">
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>