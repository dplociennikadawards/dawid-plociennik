<?php 
if($this->uri->segment(2) == 'o_mnie') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'biorezonans') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'bioenergoterapia') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'metody_pracy') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'kontakt') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'blog') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'wpis') {
$first = $row->title . ' - ';
$second = $ustawienia->page_title;
}
else {
$first = $ustawienia->page_title; 
$second = '';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $first . ' ' . $second; ?></title>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>upload/<?php echo $ustawienia->icon; ?>" />
  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Poppins:400,500,600" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.prefixed.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/mediaqueries.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/mystyles.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>

<body style="overflow-x: hidden;" onload="lightbox();">