<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script type="text/javascript">

      setTimeout(function(){

        $("#preloader").removeClass("before");
        $("#preloader").addClass("on-top");

      }, 1000);

      setTimeout(function(){

        document.getElementById("preloader").style.color = "white";
        $("#preloader").removeClass("on-top");
        $("#preloader").addClass("top");
        document.getElementById('logo').innerHTML = '<img src="<?php echo base_url(); ?>assets/front/img/logoAdAwardsWH.png" class="animated fadeIn img-fluid">';
        document.getElementById('header').innerHTML = '<h1 class="animated fadeIn mb-2 text-center">Ad Awards Sp. J.</h1><h5 class="animated fadeIn mb-1 text-center">Podziel się swoją opinią na temat naszej firmy</h5><p class="animated fadeIn text-center">Twoja opinia jest dla nas bardzo cenna. Powiedz nam, co sądzisz o naszych produktach / usługach - dobrych lub złych i pomóż nam lepiej Ci służyć.</p>';
        $("#showBg").addClass("show-black");
        $("#ankieta").fadeIn(1000);
        $("#stopka").fadeIn(1000);
      }, 3000);

  </script>
</head>

<body>

  <!-- Start your project here-->
  <div id="preloader" class="animated before">
    <div class="flex-center flex-column">
      <span id="logo">
        <img src="<?php echo base_url(); ?>assets/front/img/logoAdAwards.png" class="animated fadeIn img-fluid">
      </span>
      <span id="header">
      <h1 class="animated fadeIn mb-2 text-center"><?php echo $ustawienia->value1; ?></h1>

      <h5 class="animated fadeIn mb-1 text-center"><?php echo $ustawienia->value2; ?></h5>

      <p class="animated fadeIn text-center"><?php echo $ustawienia->value3; ?></p>
      </span>
    </div>
    <div id="showBg"></div>
  </div>

  <div id="ankieta" class="container margin-top">
    <div  class="jumbotron">
      <h1 class="text-center"><?php echo $ustawienia->value4; ?></h1>
      <p class="text-center"><?php echo $ustawienia->value5; ?></p>
      <hr>
      <p class="text-muted font-weight-bold"><?php echo $ustawienia->value6; ?></p>
      <p class="text-muted info-customer"><?php echo $ustawienia->value7; ?></p>
<form class="text-center" style="color: #757575;">

            <div class="form-row">
              <input type="hidden" name="liame" value="<?php echo $ustawienia->email; ?>">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>1. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="form1" name="form1" value="Wyszukiwarka">
                          <label class="form-check-label" for="form1">Wyszukiwarka</label><br>
                          <input type="radio" class="form-check-input" id="form1" name="form1">
                          <label class="form-check-label" for="form1">Media społecznościowe</label><br>
                          <input type="radio" class="form-check-input" id="form1" name="form1">
                          <label class="form-check-label" for="form1">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="form1" name="form1">
                          <label class="form-check-label" for="form1">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="form1" name="form1">
                          <label class="form-check-label" for="form1">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>2. Jak oceniasz swoje ostatnie / ostatnie doświadczenie z nami?</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>3. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>4. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>5. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>6. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>7. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <div class="md-form">
                        <input type="text" id="materialRegisterFormFirstName" class="form-control">
                        <label for="materialRegisterFormFirstName">8. Jeśli mógłbyś zmienić tylko jedną rzecz dotyczącą naszego produktu / usługi, co by to było? *</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <div class="md-form">
                        <input type="text" id="materialRegisterFormFirstName" class="form-control">
                        <label for="materialRegisterFormFirstName">9. Jakie inne opcje rozważałeś przed wyborem naszego produktu / usługi? *</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>10. Oceń nas za - Profesjonalizm Koordynatora Projektu dołączony do Twojego projektu</p>
                        <div class="form-check">
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>11. Oceń nas za - Profesjonalizm Koordynatora Projektu dołączony do Twojego projektu</p>
                        <div class="form-check">
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>12. Oceń nas za - Profesjonalizm Koordynatora Projektu dołączony do Twojego projektu</p>
                        <div class="form-check">
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>13. Oceń nas za - Profesjonalizm Koordynatora Projektu dołączony do Twojego projektu</p>
                        <div class="form-check">
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>14. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <div class="md-form">
                        <input type="text" id="materialRegisterFormFirstName" class="form-control">
                        <label for="materialRegisterFormFirstName">15. Jakie inne opcje rozważałeś przed wyborem naszego produktu / usługi? *</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>16. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p>17. Jak dowiedziałeś się o firmie Ad Awards</p>
                        <div class="form-check info-customer">
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">5 - Doskonały</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">4 - Dobrze</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">3 - Satysfakcjonujący</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">2 - Niezadowalający</label><br>
                          <input type="radio" class="form-check-input" id="materialUnchecked" name="materialExampleRadios">
                          <label class="form-check-label" for="materialUnchecked">1 - Zły</label><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <div class="md-form">
                        <input type="text" id="materialRegisterFormFirstName" class="form-control">
                        <label for="materialRegisterFormFirstName">18. Jakie inne opcje rozważałeś przed wyborem naszego produktu / usługi? *</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                  <div class="md-form">
                    <textarea type="text" id="form7" class="md-textarea form-control" rows="3"></textarea>
                    <label for="form7">Pozostaw nam swoje referencje</label>
                  </div>
                </div>
            </div>
            <!-- Sign up button -->

            <div class="form-row">
              <div class="col text-center my-auto">
                <div class="g-recaptcha" data-sitekey="your_site_key"></div>
              </div>
                <div class="col">
                  <button class="btn btn-outline-info btn-block my-4 waves-effect z-depth-0" type="submit">Wyślij nam swoją opinie</button>
                </div>
            </div>

        </form>
        <!-- Form -->
    </div>
  
  </div>
  <div id="stopka" class="container-fluid">
      <div class="container">
        <div class="row text-center">
          <div class="col">Wszystkie prawa zastrzeżone © 2019 Ad Awards</div>
          <div class="col">Masz jakieś pytania? Skontaktuj się z nami:</div>
        </div>
        <div class="row text-center">
          <div class="col">Created by Ad Awards <?php if($ustawienia->pdf != null){echo ' | <a href="tel:' . $ustawienia->pdf . '">Polityka prywatności</a>';} ?></div>
          <div class="col"><a href="tel:<?php echo $ustawienia->phone; ?>"><?php echo $ustawienia->phone; ?></a> | <a href="email:<?php echo $ustawienia->email; ?>"><?php echo $ustawienia->email; ?></a></div>
        </div>
      </div>
    </div>

  <!-- /Start your project here-->

  <!-- SCRIPTS -->

  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.min.js"></script>
</body>

</html>