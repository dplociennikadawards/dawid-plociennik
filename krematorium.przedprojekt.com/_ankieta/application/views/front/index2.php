<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script type="text/javascript">

      setTimeout(function(){

        $("#preloader").removeClass("before");
        $("#preloader").addClass("on-top");

      }, 1000);

      setTimeout(function(){

        document.getElementById("preloader").style.color = "white";
        $("#preloader").removeClass("on-top");
        $("#preloader").addClass("top");
        document.getElementById('logo').innerHTML = '<img src="<?php echo base_url(); ?>assets/front/img/logoAdAwardsWH.png" class="animated fadeIn img-fluid">';
        document.getElementById('header').innerHTML = '<h1 class="animated fadeIn mb-2 text-center">Ad Awards Sp. J.</h1><h5 class="animated fadeIn mb-1 text-center">Podziel się swoją opinią na temat naszej firmy</h5><p class="animated fadeIn text-center">Twoja opinia jest dla nas bardzo cenna. Powiedz nam, co sądzisz o naszych produktach / usługach - dobrych lub złych i pomóż nam lepiej Ci służyć.</p>';
        $("#showBg").addClass("show-black");
        $("#ankieta").fadeIn(1000);
        $("#stopka").fadeIn(1000);
      }, 3000);

  </script>
</head>

<body>

  <!-- Start your project here-->
  <div id="preloader" class="animated before">
    <div class="flex-center flex-column">
      <span id="logo">
        <img src="<?php echo base_url(); ?>assets/front/img/logoAdAwards.png" class="animated fadeIn img-fluid">
      </span>
      <span id="header">
      <h1 class="animated fadeIn mb-2 text-center"><?php echo $ustawienia->value1; ?></h1>

      <h5 class="animated fadeIn mb-1 text-center"><?php echo $ustawienia->value2; ?></h5>

      <p class="animated fadeIn text-center"><?php echo $ustawienia->value3; ?></p>
      </span>
    </div>
    <div id="showBg"></div>
  </div>

  <div id="ankieta" class="container margin-top">
    <div  class="jumbotron">
      <h1 class="text-center"><?php echo $ustawienia->value4; ?></h1>
      <p class="text-center"><?php echo $ustawienia->value5; ?></p>
      <hr>
      <p class="text-muted font-weight-bold"><?php echo $ustawienia->value6; ?></p>
      <p class="text-muted info-customer"><?php echo $ustawienia->value7; ?></p>
<form class="text-center" method="post" action="<?php echo base_url(); ?>p/test" style="color: #757575;">
  <input type="hidden" name="liame" value="<?php echo $ustawienia->email; ?>">

<?php $i=0; foreach ($rows as $row): $i++; ?>

<!-- INPUT -->
  <?php if($row->type == 'input'): ?>
            <div class="form-row">
                <div class="col">
                    <div class="md-form">
                        <input type="text" id="<?php echo $i; ?>" class="form-control" name="form<?php echo $i; ?>">
                        <label for="<?php echo $i; ?>"><?php echo $i; ?>. <?php echo $row->title; ?></label>
                    </div>
                </div>
            </div>
  <?php endif; ?>
<!-- INPUT -->

<!-- RADIO -->
  <?php if($row->type == 'radio'): ?>
    <?php $values = explode(",", $row->value); ?>
            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p><?php echo $i; ?>. <?php echo $row->title; ?></p>
                        <div class="form-check info-customer">
                          <?php $j=0; foreach ($values as $value): $j++; ?>
                            <input type="radio" class="form-check-input" id="<?php echo 'radiobox' . $i . $j; ?>" name="form<?php echo $i; ?>" value="<?php echo $value; ?>">
                            <label class="form-check-label" for="<?php echo 'radiobox' . $i . $j; ?>"><?php echo $value; ?></label><br>
                          <?php endforeach; ?>
                          <br>
                        </div>
                    </div>
                </div>
            </div>
  <?php endif; ?>
  <!-- RADIO -->

  <!-- CHECKBOX -->
  <?php if($row->type == 'checkbox'): ?>
    <?php $values = explode(",", $row->value); ?>
            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p><?php echo $i; ?>. <?php echo $row->title; ?></p>
                        <div class="form-check info-customer">
                          <?php $j=0; foreach ($values as $value): $j++; ?>
                            <input type="checkbox" class="form-check-input" id="<?php echo 'checkbox' . $i . $j; ?>" name="form<?php echo $i; ?>[]" value="<?php echo $value; ?>">
                            <label class="form-check-label" for="<?php echo 'checkbox' . $i . $j; ?>"><?php echo $value; ?></label><br>
                          <?php endforeach; ?>
                          <br>
                        </div>
                    </div>
                </div>
            </div>
  <?php endif; ?>
  <!-- CHECKBOX -->

  <!-- STARS -->
  <?php if($row->type == 'stars'): ?>
            <div class="form-row">
                <div class="col">
                    <!-- First name -->
                    <div class="md-form text-left">
                        <p><?php echo $i; ?>. <?php echo $row->title; ?></p>
                        <div class="form-check">
                          <span id="form<?php echo $i; ?>_id1" class="fa fa-star checked" onclick="check_start(1 , <?php echo $i; ?>);"></span>
                          <span id="form<?php echo $i; ?>_id2" class="fa fa-star" onclick="check_start(2, <?php echo $i; ?>);"></span>
                          <span id="form<?php echo $i; ?>_id3" class="fa fa-star" onclick="check_start(3, <?php echo $i; ?>);"></span>
                          <span id="form<?php echo $i; ?>_id4" class="fa fa-star" onclick="check_start(4, <?php echo $i; ?>);"></span>
                          <span id="form<?php echo $i; ?>_id5" class="fa fa-star" onclick="check_start(5, <?php echo $i; ?>);"></span>
                          <input id="opinion<?php echo $i; ?>" value="1" type="hidden" name="form<?php echo $i; ?>">
                        </div>
                    </div>
                </div>
            </div>
  <?php endif; ?>
  <!-- STARS -->

  <!-- TEXTAREA -->
  <?php if($row->type == 'textarea'): ?>
            <div class="form-row">
                <div class="col">
                  <div class="md-form">
                    <textarea type="text" id="form7" name="form<?php echo $i; ?>" class="md-textarea form-control" rows="3"></textarea>
                    <label for="form7"><?php echo $i; ?>. <?php echo $row->title; ?></label>
                  </div>
                </div>
            </div>
  <?php endif; ?>
  <!-- TEXTAREA -->

<?php endforeach; ?>


            <div class="form-row">
              <div class="col text-center my-auto">
                <div class="g-recaptcha" data-sitekey="your_site_key"></div>
              </div>
                <div class="col">
                  <button class="btn btn-outline-info btn-block my-4 waves-effect z-depth-0" type="submit">Wyślij nam swoją opinie</button>
                </div>
            </div>

        </form>
        <!-- Form -->
    </div>
  
  </div>
  <div id="stopka" class="container-fluid">
      <div class="container">
        <div class="row text-center">
          <div class="col">Wszystkie prawa zastrzeżone © 2019 Ad Awards</div>
          <div class="col">Masz jakieś pytania? Skontaktuj się z nami:</div>
        </div>
        <div class="row text-center">
          <div class="col">Created by Ad Awards <?php if($ustawienia->pdf != null){echo ' | <a href="tel:' . $ustawienia->pdf . '">Polityka prywatności</a>';} ?></div>
          <div class="col"><a href="tel:<?php echo $ustawienia->phone; ?>"><?php echo $ustawienia->phone; ?></a> | <a href="email:<?php echo $ustawienia->email; ?>"><?php echo $ustawienia->email; ?></a></div>
        </div>
      </div>
    </div>


<script type="text/javascript">
    function check_start(id_star, nr_form) {

      for(i=1; i<=5; i++) {
        var element = document.getElementById("form"+nr_form+"_id"+i);
        element.classList.remove("checked");
      }

      for(i=1; i<=id_star; i++) {
        var element = document.getElementById("form"+nr_form+"_id"+i);
        element.classList.add("checked");
      }

      document.getElementById('opinion'+nr_form).value = id_star;
    }
</script>

  <!-- /Start your project here-->

  <!-- SCRIPTS -->

  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.min.js"></script>
</body>

</html>