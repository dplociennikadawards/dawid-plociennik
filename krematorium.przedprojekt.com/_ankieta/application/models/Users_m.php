    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model
{
   public function __construct() {
        parent::__construct();
    }

    public function get($table) {
        $query = $this->db->get($table);
        
        return $query->result();            
    }


    public function get_sort($table, $sort, $type) {
        $this->db->order_by($sort, $type);
        $query = $this->db->get($table);
        
        return $query->result();            
    }

    public function get_parts($table, $where, $what) {
        $this->db->where([$where => $what]);
        $query = $this->db->get($table);
        
        return $query->result();            
    }

    public function get_element($table,$where) {
        $this->db->where([$table.'_id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_page($table,$where) {
        $this->db->where(['page' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_slide($table,$where) {
        $this->db->where(['part' => $where]);
        $query = $this->db->get($table);
        
        return $query->result();            
    }

    public function get_city($table,$where) {
        $this->db->where(['lokalizacja' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_pageGallery($table,$where) {
        $this->db->where(['page' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_element_gallery($table,$where) {
        $this->db->where(['id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }


    public function get_element_gallery_wpis($table,$where,$page) {
        $this->db->where(['wpis_id' => $where]);
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_sortByLikes($table) {
        $this->db->order_by("likes", "desc");
        $this->db->order_by("selected_data", "desc");
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_sortByDate($table) {
        $this->db->order_by("selected_data", "desc");
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_sortByPriorityPages($table) {
        $this->db->order_by("priority", "asc");
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_sortByPriority($table, $where) {
        $this->db->order_by("priority", "asc");
        $this->db->where(['page' => $where]);
        $query = $this->db->get($table);

        return $query->result();
    }


    public function get_sortByPriorityWPIS($table, $where, $id) {
        $this->db->order_by("priority", "asc");
        $this->db->where(['page' => $where]);
        $this->db->where(['wpis_id' => $id]);
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_sortByPriorityDESC($table, $where) {
        $this->db->order_by("priority", "desc");
        $this->db->where(['page' => $where]);
        $query = $this->db->get($table);

        return $query->result();
    }


    public function get_sortByPriority2($table,$where,$page) {
        $this->db->order_by("priority", "asc");
        $this->db->where(['wpis_id' => $where]);
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);

        return $query->result();
    }


    public function get_sortByPriorityDESC2($table,$where,$page) {
        $this->db->order_by("priority", "desc");
        $this->db->where(['wpis_id' => $where]);
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);

        return $query->result();
    }


    public function insert($table, $data) {
        $this->db->insert($table,$data);
    }

    public function delete($table,$where) {
        $this->db->where($where);
        $this->db->delete($table);
    }

}  
?>