<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Managment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		}

	public function admin() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/admin/index');
		$this->load->view('back/blocks/footer');
	}

	public function table() {

		$data['row'] = $this->users_m->get_all($_GET['base']);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/data-table', $data);
		$this->load->view('back/blocks/footer');
	}

	public function add_page() {

		$data['row'] = $this->users_m->order_by($_GET['base'], 'priority', 'asc');

		$this->form_validation->set_rules('type', 'Typ strony', 'trim');
		$this->form_validation->set_rules('name', 'Nazwa strony', 'min_length[3]|trim|is_unique[pages.name]');
    	$this->form_validation->set_rules('menu', 'Nazwa w menu', 'min_length[3]|trim|is_unique[pages.menu]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Pole %s już istnieje</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/pages/add_page', $data);
				$this->load->view('back/blocks/footer');
			}
		else
			{	

				$table_name = str_replace(' ', '_', $this->input->post('name'));
				$table_name = strtolower($table_name);

				foreach ($data['row'] as $key) {
					$next_priority = $key->priority;
				}

				$insert['type'] = $this->input->post('type');
				$insert['priority'] = $next_priority + 1;
				$insert['active'] = 1;
				$insert['name'] = $this->input->post('name');
				$insert['menu'] = $this->input->post('menu');
				$insert['table_name'] = $table_name;

				$this->users_m->insert('pages', $insert);

				if($insert['type'] == 'blog') {
					$this->base_m->blog($table_name);					
				}
				else {
					$this->base_m->regular_page($table_name);	
				}

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Nowa strona została dodana!</p>');

				redirect('../../managment/table?base='.$_GET['base']);
			}
		}

	public function delete(){
		$id = $this->uri->segment(4);
		$table = $this->uri->segment(3);
		$this->users_m->delete('pages', $id);
		$this->base_m->delete_table($table);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś stronę!</p>');

		redirect('../../../../managment/table?base='.$_GET['base']);
		}
	}