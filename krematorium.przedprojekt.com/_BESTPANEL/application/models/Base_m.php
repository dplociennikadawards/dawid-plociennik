<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_m extends CI_Model
{
   public function __construct() {
        parent::__construct();
    }     

    public function delete_table($table) {
        $query = $this->db->query('DROP TABLE '.$table.';');        
    }

    public function blog($table) {
        $query = $this->db->query('CREATE TABLE '.$table.' (blog_id int,type text,active int,photo text,title text,description text,data text)');        
    }

    public function news($table) {
        $query = $this->db->query('CREATE TABLE '.$table.' (news_id int,type text,active int,photo text,title text,description text,data text)');        
    }

}  
?>