<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_all($table) {
        $query = $this->db->get($table);
        return $query->result();     
    }
    public function insert($table, $data) {
        $this->db->insert($table,$data);
    }
    public function order_by($table,$column,$order) {
    	$this->db->order_by($column, $order);
        $query = $this->db->get($table);
        return $query->result();     
    }
    public function delete($table,$where) {
        $this->db->where('page_id', $where);
        $this->db->delete($table);
    }

}  
?>