<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <link type="image/x-icon" rel="shortcut icon" href="../assets/bracket_panel/images/faviconBT.ico" />

    <link rel="shortcut icon" href="../assets/img/logo.png" />
    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title><?php echo $web_page; ?> - panel administracyjny</title>

    <!-- vendor css -->
    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/Ionicons/css/ionicons.css">

    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/jquery-switchbutton/jquery.switchButton.css">

    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/rickshaw/rickshaw.min.css">
    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/select2/css/select2.min.css">
    <link rel="stylesheet" href="../assets/bracket_panel/template/lib/datatables/jquery.dataTables.css"> 
    <link href="../assets/bracket_panel/template/lib/jquery-toggles/toggles-full.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../assets/bracket_panel/template/css/bracket.css">
    <link href="../assets/bracket_panel/template/lib/SpinKit/spinkit.css" rel="stylesheet">

    <link rel="stylesheet" href="../assets/bracket_panel/template/css/style.css">
    <link rel="shortcut icon" href="/assets/img/favicon-32x32.png">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  </head>