<?php 
$admin = '';
if($this->uri->segment(2) == 'admin') {
  $admin = 'active';
}
$managment = '';
if($this->uri->segment(2) == 'table' || $this->uri->segment(2) == 'add_page') {
  $managment = 'active';

}
?>
<body class="collapsed-menu with-subleft">

<div id="preloader">
    <div id="status">
      <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
      </div>
    </div>
</div>

    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo">
      <a href="<?php echo base_url(); ?>" class="mx-auto">
        <img src="../assets/img/logo.png" width="150">
      </a>
    </div>
    <div class="br-sideleft overflow-y-auto green-dark-bg">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Menu</label>
      <!-- br-sideleft-menu -->
      <ul class="br-sideleft-menu">

        <li class="br-menu-item">
          <a href="../managment/admin" class="br-menu-link <?php echo $admin; ?>">
            <i class="menu-item-icon icon fas fa-home tx-20"></i><span class="menu-item-label menu-text">&nbsp;Strona główna</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub <?php echo $managment; ?>">
            <i class="menu-item-icon icon far fa-object-ungroup tx-20"></i><span class="menu-item-label menu-text">&nbsp;Zarządzanie stronami</span>
          </a>
          <ul class="br-menu-sub">
            <li class="sub-item">
              <a href="../managment/table?base=pages" class="sub-link">Wyświetl wszystkie strony</a>
            </li>
            <li class="sub-item">
              <a href="../managment/add_page?base=pages" class="sub-link">Dodaj nowa podstronę</a>
            </li>
            <li class="sub-item">
              <a href="/" class="sub-link">Strony archiwalne</a>
            </li>
          </ul>
        </li><!-- br-menu-item -->


        <li class="br-menu-item">
          <a href="../panel/logout" class="br-menu-link">
            <i class="menu-item-icon icon ion-power tx-20"></i><span class="menu-item-label menu-text">&nbsp;Wyloguj</span>
          </a>
        </li><!-- br-menu-item -->

      </ul><!-- br-sideleft-menu -->

      <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->


    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>

      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?></span>
              <img src="<?php if($_SESSION['avatar'] == null){echo '../assets/img/logo.png';} else { echo '/upload/workers/'.$_SESSION['avatar'];} ?>" class="wd-32 rounded" alt="">
              <span class="square-10 bg-success"></span>
            </a>
          </div>
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->