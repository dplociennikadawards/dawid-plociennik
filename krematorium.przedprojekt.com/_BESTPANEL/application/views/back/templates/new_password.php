    <form action="" method="post">
        <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v green-bg">

          <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
            <div class="signin-logo tx-center tx-28 tx-bold tx-inverse mb-3"> 
                <img src="../assets/img/logo.png">
            </div>
            <div class="tx-center mg-b-60">Panel zarządzania <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo base_url(); ?></a></div>
            <?php echo validation_errors(); ?>
            <?php
                if($this->session->flashdata('success')) {
                    echo $this->session->flashdata('success');
                }
                if($this->session->flashdata('logged_failed')) {
                    echo $this->session->flashdata('logged_failed');
                }
            ?>
            <div class="form-group">
              <input name="where" type="hidden" value="<?php echo $_GET['id']; ?>">
              <input name="password" type="password" class="form-control" placeholder="Wprowadź hasło" required>
            </div><!-- form-group -->
            <div class="form-group">
              <input name="confirm_password" type="password" class="form-control" placeholder="Potwierdź hasło" required>

            </div><!-- form-group -->
            <button type="submit" class="btn btn-gold btn-block" style="cursor: pointer;">Zaloguj</button>

          </div><!-- login-wrapper -->
        </div><!-- d-flex -->
    </form>