<?php 
$main_page = '';
$page = '';
if(isset($_GET) && $_GET['base'] == 'pages') {
  $page = 'Zarządzanie stronami';
  $sub_page = 'Wyświetl wszystkie strony';
  $icon = 'fas fa-folder-plus';
  $main_page = 'active';
}
?>
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item" onclick="test();"><?php echo $page; ?></span>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $sub_page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0"><?php echo $sub_page; ?></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
            <?php
                if($this->session->flashdata('success')) {
                    echo $this->session->flashdata('success');
                }
            ?>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-5p">Np.</th>
                  <th class="wd-10p">Aktywna</th>
                  <th class="wd-20p">Kolejność</th>
                  <th class="wd-20p">Nazwa strony</th>
                  <th class="wd-20p">Typ strony</th>
                  <th class="wd-20p">Data utworzenia</th>
                  <th class="wd-25p">Akcja</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach ($row as $key): $i++; ?>
                  <tr>
                    <td class="align-middle"><?php echo $i; ?>.</td>
                    <td class="align-middle"><?php echo $key->active; ?></td>
                    <td class="align-middle">
                      <span onclick="upPriority(<?php echo $key->page_id; ?>)" style="cursor: pointer;">
                        <i class="fas fa-arrow-down"></i>
                      </span>
                      <span onclick="downPriority(<?php echo $key->page_id; ?>)" style="cursor: pointer;">
                        <i class="fas fa-arrow-up"></i>
                      </span>
                    </td>
                    <td class="align-middle"><?php echo $key->name; ?></td>
                    <td class="align-middle"><?php echo $key->type; ?></td>
                    <td class="align-middle"><?php echo date('H:i d.m.Y', strtotime($key->created)); ?></td>
                    <td class="align-middle">
                      <a href="../managment/delete/<?php echo $key->table_name; ?>/<?php echo $key->name; ?>?base=pages" data-toggle="modal" data-target="#modaldemo1"><i class="text-success far fa-edit tx-30 btn"></i></a>
                      <a data-toggle="modal" data-target="#modaldemo<?php echo $key->page_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                    </td>
                  </tr>

                  <div id="modaldemo<?php echo $key->page_id; ?>" class="modal fade">
                    <div class="modal-dialog modal-dialog-vertical-center" role="document">
                      <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-20 pd-x-25">
                          <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Usuń stronę </h6>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body pd-25">
                          <p class="mg-b-5">
                            Czy na pewno chcesz usunąć tą stronę ?
                          </p>
                        </div>
                        <div class="modal-footer">
                          <a  href="../managment/delete/<?php echo $key->table_name; ?>/<?php echo $key->page_id; ?>?base=pages" class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Tak</a>
                          <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Anuluj</button>
                        </div>
                      </div>
                    </div><!-- modal-dialog -->
                  </div><!-- modal -->

                <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->


<!-- href="../managment/delete/<?php echo $key->table_name; ?>/<?php echo $key->name; ?>?base=pages" -->
    <!-- ########## END: MAIN PANEL ########## -->


<script type="text/javascript">
$( document ).ready(function() {
  $('#datatable1').DataTable({
    responsive: true,
    language: {
      searchPlaceholder: 'Szukaj...',
      sSearch: '',
      lengthMenu: '_MENU_ pozycji na stronie',
    }
  });
});
</script>