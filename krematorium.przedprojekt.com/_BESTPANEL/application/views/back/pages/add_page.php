<?php 
$main_page = '';
$page = '';
if(isset($_GET) && $_GET['base'] == 'pages') {
  $page = 'Zarządzanie stronami';
  $sub_page = 'Wyświetl wszystkie strony';
  $icon = 'fas fa-folder-plus';
  $main_page = 'active';
}
?>

          <div id="modaldemo1" class="modal fade">
            <div class="modal-dialog modal-dialog-vertical-center" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Wybierz typ podstrony</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('blog'); layout_script();" data-dismiss="modal">
                    BLOG
                  </button>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('wiadomosci'); layout_script();" data-dismiss="modal">
                    WIADOMOŚCI
                  </button>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('aktualnosci'); layout_script();" data-dismiss="modal">
                    AKTUALNOŚCI
                  </button><BR>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('oferta'); layout_script();" data-dismiss="modal">
                    OFERTA
                  </button>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('pliki'); layout_script();" data-dismiss="modal">
                    PLIKI
                  </button>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('galeria'); layout_script();" data-dismiss="modal">
                    GALERIA
                  </button>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('mapa'); layout_script();" data-dismiss="modal">
                    MAPA
                  </button><BR>
                  <button type="button" class="btn btn-secondary mg-b-5" onclick="page_type('ustawienia'); layout_script();" data-dismiss="modal">
                    USTAWIENIA
                  </button>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Wybierz</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item" onclick="test();"><?php echo $page; ?></span>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $sub_page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0"><?php echo $sub_page; ?></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <form method="post" action="">
          <div class="form-layout form-layout-2 p-3">
            <?php echo validation_errors(); ?>
            <div class="row no-gutters">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="form-control-label">Wybierz typ strony: <span class="tx-danger">*</span></label>
                  <input id="type" data-toggle="modal" onchange="layout();" data-target="#modaldemo1" readonly class="form-control" type="text" name="type" placeholder="Typ strony" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                  <label class="form-control-label">Nazwa strony: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="name" placeholder="Wprowadź nazwę strony " required>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                  <label class="form-control-label">Nazwa nawigacyjna: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="menu" placeholder="Wprowadź nazwę strony w menu">
                </div>
              </div><!-- col-4 -->
              <div class="col-md-3">
                <div class="form-group bd-r-0-force bd-t-0-force ">
                      <label class="form-control-label">Wybierz zdjęcie nagłówkowe: <span class="tx-danger">*</span></label>
                      <label class="custom-file" style="z-index: 1;">
                        <input type="file" id="file" class="custom-file-input">
                        <span class="custom-file-control"></span>
                      </label>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4">
                <div class="form-group bd-l-0-force bd-t-0-force ">
                    <div id="uploaded_image" class="delete_photo" title="Usuń zdjęcie" style="cursor: pointer" onclick="clear_box();" style="max-width: 200px;">
                      <img src="../upload/img/glasses-P78LDTA.jpg" width="100%" height="100" class="img-thumbnail">
                    </div>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-2 mg-t--1 mg-md-t-0">
                <div class="form-group   bd-t-0-force mg-md-l--1">
                  <label class="ckbox">
                    <input type="checkbox">
                    <span>Strona aktywna: </span>
                  </label>
                  <label class="ckbox">
                    <input type="checkbox">
                    <span>Data widoczna: </span>
                  </label>
                  <label class="ckbox">
                    <input type="checkbox">
                    <span>Galeria zdjęć: </span>
                  </label>
                  <label class="ckbox">
                    <input type="checkbox">
                    <span>Powiększanie zdjęć: </span>
                  </label>
                  <label class="rdiobox">
                    <input name="rdio" type="radio">
                    <span>Zdjęcia w sliderze</span>
                  </label>
                  <label class="rdiobox">
                    <input name="rdio" type="radio">
                    <span>Zdjęcia w kafelkach</span>
                  </label>
          <div class="toggle-wrapper">
            <div class="toggle toggle-light"></div>
          </div>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-3 mg-t--1 mg-md-t-0">
                <div class="form-group   bd-t-0-force mg-md-l--1">
                  <label class="form-control-label">Wybierz datę utworzenia strony (domyślnie obecna): <span class="tx-danger">*</span></label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                    <input onchange="formatDate();" value="<?php echo date('d.m.Y'); ?>" placeholder="DD.MM.YYYY" type="text" id="date-picker-example" class="form-control datepicker bordered" name="date">
                  </div>
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->

            <div id="layout" class="row no-gutters mt-3" style="display: none;">
              <nav class="navbar navbar-light gradient form-group bd-t-0 w-100">
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="column();">Kolumna</a>
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="picture();">Obrazek</a>
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="pictureColumn();">Obrazek + Kolumna</a>
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="columnPicture();">Kolumna + Obrazek</a>
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="picturePicture();">Obrazek + Obrazek</a>
                <a class="btn btn-secondary text-white text-uppercase navbar-brand" onclick="columnColumn();">Kolumna + Kolumna</a>
              </nav>
              <span id="grid" class="w-100">

              </span>
            </div><!-- row -->

            <div class="form-layout-footer bd pd-20 bd-t-0">
              <button class="btn btn-info">Dodaj</button>
              <button class="btn btn-secondary">Anuluj</button>
            </div><!-- form-group -->
          </div><!-- form-layout -->
        </form>
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script type="text/javascript">
  function page_type(type) {
    document.getElementById('type').value = type;
  }
</script>


<script type="text/javascript">
  function layout_script() {
    document.getElementById('layout').style.display = '';
  }
  function column() {
    document.getElementById('grid').innerHTML += '<div class="col-md-12 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="text" name="value[]" placeholder="Kolumna"></div></div>';
  }
  function picture() {
    document.getElementById('grid').innerHTML += '<div class="col-md-12 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="file" name="image[]" placeholder="Obrazek"></div></div>';
  }
  function pictureColumn() {
    document.getElementById('grid').innerHTML += '<div class="row no-gutters"><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="file" name="image[]" placeholder="Obrazek"></div></div><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force bd-l-0-force"><input class="form-control" type="text" name="value[]" placeholder="Kolumna"></div></div></div>';
  }
  function columnPicture() {
    document.getElementById('grid').innerHTML += '<div class="row no-gutters"><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="text" name="value[]" placeholder="Kolumna"></div></div><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force bd-l-0-force"><input class="form-control" type="file" name="image[]" placeholder="Obrazek"></div></div></div>';
  }
  function picturePicture() {
    document.getElementById('grid').innerHTML += '<div class="row no-gutters"><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="file" name="image[]" placeholder="Obrazek"></div></div><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force bd-l-0-force"><input class="form-control" type="file" name="image[]" placeholder="Obrazek"></div></div></div>';
  }
  function columnColumn() {
    document.getElementById('grid').innerHTML += '<div class="row no-gutters"><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force"><input class="form-control" type="text" name="value[]" placeholder="Kolumna"></div></div><div class="col-md-6 px-0 mg-t--1 mg-md-t-0 "><div class="form-group bd-t-0-force bd-l-0-force"><input class="form-control" type="text" name="value[]" placeholder="Kolumna"></div></div></div>';
  }
</script>

<script type="text/javascript">
    function clear_box()
  {
    document.getElementById('uploaded_image').innerHTML = '';
    document.getElementById('zdjecie').value = '';
  }
</script>

<script type="text/javascript">
  $('#datepickerNoOfMonths').datepicker({
  showOtherMonths: true,
  selectOtherMonths: true,
  numberOfMonths: 2
  });
</script>

<script type="text/javascript">

  function formatDate()
    {  
      var date = document.getElementsByName('date')[0].value;
      var mouth = date.substring(0, 2);
      var day = date.substring(3, 5);
      var year = date.substring(6, 10);
      document.getElementsByName('date')[0].value = day + '.' + mouth + '.' + year;
    }


</script>


<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_form').change('#image_file', function(e){  
           e.preventDefault();  
           if($('#image_file').val() == '')  
           {  
                alert("Please Select the File");  
           }  
           else  
           {  
              var file = document.getElementById('image_file').value;

              var replace = file.replace('C:\\fakepath\\', '');

              document.getElementById('zdjecie').value = replace; 

                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     success:function(data)  
                     {  
                          $('#uploaded_image').html(data);  
                     },  
                     complete:function(data)
                     {
                      document.getElementById('resolution').value = document.getElementById('send_resolution').value;
                      document.getElementById('zdjecie').value = document.getElementById('name_photo').value;
                     }  
                });  
           }  
      });  
 });  

</script>