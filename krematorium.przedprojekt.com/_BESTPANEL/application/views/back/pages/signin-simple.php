<?php //header("location: ../../panel/login"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title><?php echo $web_page; ?></title>

    <!-- vendor css -->
    <link href="../assets/bracket_panel/template/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/bracket_panel/template/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../assets/bracket_panel/template/css/bracket.css">

      <!-- My styles -->
    <link rel="stylesheet" href="../assets/bracket_panel/template/css/style.css">
    <link rel="shortcut icon" href="../assets/bracket_panel/img/favicon-32x32.png">

  </head>

  <body>

    <?php 
    if($this->uri->segment(2) == 'login') {
        $this->load->view('back/templates/login');
    }
    elseif($this->uri->segment(2) == 'recovery_password') {
        $this->load->view('back/templates/recovery_password');
    }
    elseif($this->uri->segment(2) == 'new_password') {
        $this->load->view('back/templates/new_password');
    } ?>

    <script src="../assets/bracket_panel/template/lib/jquery/jquery.js"></script>
    <script src="../assets/bracket_panel/template/lib/popper.js/popper.js"></script>
    <script src="../assets/bracket_panel/template/lib/bootstrap/js/bootstrap.js"></script>

  </body>
</html>
