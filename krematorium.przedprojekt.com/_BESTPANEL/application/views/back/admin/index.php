<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'admin') {
  $page = 'Strona główna';
  $icon = 'fas fa-home';
  $main_page = 'active';
}
?>
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania strona <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $web_page; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">

        <div class="row">
          <div class="col-md-4 col-6 text-center">
            <a href="">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 <?php echo $icon; ?>"></i><br>Strona główna
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="../managment/table?base=pages">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 far fa-object-ungroup"></i><br>Wszystkie strony
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="../managment/add_page?base=pages">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-folder-plus"></i><br>Dodaj nową stronę
              </button>
            </a>
          </div> 
        </div>  

        <div class="row mt-5">
          <div class="col-md-4 col-6 text-center">
            <a href="">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 <?php echo $icon; ?>"></i><br>Lorem
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0" style="cursor: pointer; box-shadow: 3px 3px 10px black;">
                <i class="tx-80 <?php echo $icon; ?>"></i><br>Lorem
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0" style="cursor: pointer; box-shadow: 3px 3px 10px black;">
                <i class="tx-80 <?php echo $icon; ?>"></i><br>Lorem
              </button>
            </a>
          </div>   
        </div>

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->