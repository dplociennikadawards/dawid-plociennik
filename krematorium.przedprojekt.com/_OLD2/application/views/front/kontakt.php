  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">Kontakt</h1>
      </div>
    </div>
  </section>
 <div id="mdb-lightbox-ui"></div>
 <section class="bg-white p-md-5 py-3 py-md-0">

  <div class="row">
    <div class="col-md-4">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2503.221583164363!2d17.09134211522663!3d51.141264845228775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470fe8575ca855a9%3A0x2817e82580af4550!2sKowalska+8%2C+51-423+Wroc%C5%82aw!5e0!3m2!1spl!2spl!4v1549879448752" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-8">
       <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Kontakt z nami</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">Masz jakieś pytania? Nie wahaj się skontaktować z nami bezpośrednio. Nasz zespół odpisze do Ciebie w ciągu kilku godzin, aby Ci pomóc.</p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="mail.php" method="POST">

                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Twoje imię</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Twój adres e-mail</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">Temat</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message">Twoja wiadomość</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->

            </form>

            <div class="text-center text-md-left px-3 px-md-0">
                <button type="submit" class="btn mt-3 color-yellow text-shadow">Wyślij</button>
            </div>
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>ul. Kowalska 8, Kowalowo 82-133</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+ 01 234 567 89</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>biuro@krematorium.pl</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
    </div>
  </div>
 </section>