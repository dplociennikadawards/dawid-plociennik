  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">O nas</h1>
      </div>
    </div>
  </section>
  <section class="bg-white pb-0">


<div class="row p-md-5 p-5">
      <!-- News jumbotron -->
    <div class="jumbotron text-left hoverable">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-4 offset-md-1 mx-md-3 my-3">

          <!-- Featured image -->
          <div class="view overlay">
            <img src="../assets/front/img/przyklad1.jpg" class="img-fluid" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-7 text-md-justify ml-md-3 mt-3 text-white">

          <h4 class="h4 mb-4">This is title of the news</h4>

          <p class="font-weight-normal">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque, totam
            rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur.</p>
          <p class="font-weight-normal">by <a><strong>Carine Fox</strong></a>, 6/02/2019</p>


        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- News jumbotron -->

      <!-- News jumbotron -->
    <div class="jumbotron text-left hoverable p-4">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-4 offset-md-1 mx-md-3 my-3">

          <!-- Featured image -->
          <div class="view overlay">
            <img src="../assets/front/img/przyklad2.jpg" class="img-fluid" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-7 text-md-justify ml-md-3 mt-3 text-white">

          <h4 class="h4 mb-4">This is title of the news</h4>

          <p class="font-weight-normal">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque, totam
            rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur.</p>
          <p class="font-weight-normal">by <a><strong>Carine Fox</strong></a>, 6/02/2019</p>


        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- News jumbotron -->

<!-- Jumbotron -->
<div class="card card-image mb-5 mb-md-0" style="background-image: url(../assets/front/img/header.png);">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">

      <!-- Content -->
      <h5 class="h5 icon"><i class="fas fa-book-open"></i> Nasze usługi</h5>
      <h2 class="card-title h2 my-4 py-2">Jumbotron with image overlay</h2>
      <p class="mb-4 pb-2 px-md-5 mx-md-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur obcaecati vero aliquid libero doloribus ad, unde tempora maiores, ullam, modi qui quidem minima debitis perferendis vitae cumque et quo impedit.</p>
      <a href="../p/uslugi_pogrzebowe" class="btn transport-button color-yellow text-shadow "><i class="fas fa-clone left"></i> Zobacz ofertę</a>

    </div>
  </div>
</div>
<!-- Jumbotron -->


</div>


  </section>
  