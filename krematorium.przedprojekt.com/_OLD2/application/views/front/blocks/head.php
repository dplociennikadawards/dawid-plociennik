<!DOCTYPE html>
<html lang="pl">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Krematorium - międzynarodowy transport zmarłych</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../assets/front/css/style.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="../assets/front/map/cssmap-europe/cssmap-europe.css" media="screen" />
</head>

<body style="overflow-x: hidden;" onload="lightbox();">

    <div class="phone-container">
      <a href="tel:xxxx">
        <div class='circle'><img src="../assets/front/img/phone-big.png"></div>

      </a>
      <p class="text-center mobile">Skontaktuj się!</p>
    </div>