  <section class="contact">
    <div class='container'>
      <div class='row'>
        <div class='col-lg-3 col-md-6 col-sm-6'>
          <div>
            <p class='contact-header'>Kontakt</p>
            <a href="tel:xxxx" class="contact-link"><img src="../assets/front/img/phone-yellow.png" class="mr-2 mb-1">+12 345-678-987</a>
            <a href="mailto:xxxx" class="contact-link-thin"><img src="../assets/front/img/monkey.png" class="mr-2 mb-1">biuro@krematorium.pl</a>
            <p class="contact-link-thin"><img src="../assets/front/img/location.png" class="mr-2 mb-1">ul. Kowalska 8, Kowalowo 82-133</p>
          </div>
          <div class="for-clients">
            <p class='contact-header'>Dla klientów</p>
            <a href="#" class="contact-link-thin">Transport zmarłych</a>
            <a href="#" class="contact-link-thin">Usługi pogrzebowe</a>
            <a href="#" class="contact-link-thin">Krematorium</a>
            <a href="#" class="contact-link-thin">Poradniki</a>

          </div>
        </div>
        <div class='col-lg-3 transport-column col-md-6 col-sm-6'>
          <p class='contact-header'>Transport zwłok</p>
          <a href="#" class="contact-link-thin">Transport z Anglii</a>
          <a href="#" class="contact-link-thin">Transport z Austrii</a>
          <a href="#" class="contact-link-thin">Transport z Belgii</a>
          <a href="#" class="contact-link-thin">Transport z Chorwacji</a>
          <a href="#" class="contact-link-thin">Transport z Danii</a>
          <a href="#" class="contact-link-thin">Transport z Francji</a>
          <a href="#" class="contact-link-thin">Transport z Grecji</a>
          <a href="#" class="contact-link-thin">Transport z Hiszpanii</a>
          <a href="#" class="contact-link-thin">Transport z Holandii</a>
          <a href="#" class="contact-link-thin">Transport z Niemiec</a>
          <a href="#" class="contact-link-thin">Transport z Norwegii</a>
          <a href="#" class="contact-link-thin">Transport ze Szwecji</a>
          <a href="#" class="contact-link-thin">Transport z USA</a>
          <a href="#" class="contact-link-thin">Transport z Włoch</a>
          <a href="#" class="contact-link-thin">Transport z innych krajów</a>
        </div>
        <div class='col-lg-3 transport-column col-md-6 col-sm-6'>
          <p class='contact-header'>Transport wg miast</p>
          <a href="#" class="contact-link-thin">Transport zwłok Białystok</a>
          <a href="#" class="contact-link-thin">Transport zwłok Bydgoszcz</a>
          <a href="#" class="contact-link-thin">Transport zwłok Gdańsk</a>
          <a href="#" class="contact-link-thin">Transport zwłok Katowice</a>
          <a href="#" class="contact-link-thin">Transport zwłok Kielce</a>
          <a href="#" class="contact-link-thin">Transport zwłok Kraków</a>
          <a href="#" class="contact-link-thin">Transport zwłok Lublin</a>
          <a href="#" class="contact-link-thin">Transport zwłok Olsztyn</a>
          <a href="#" class="contact-link-thin">Transport zwłok Opole</a>
          <a href="#" class="contact-link-thin">Transport zwłok Poznań</a>
          <a href="#" class="contact-link-thin">Transport zwłok Rzeszów</a>
          <a href="#" class="contact-link-thin">Transport zwłok Toruń</a>
          <a href="#" class="contact-link-thin">Transport zwłok Warszawa</a>
          <a href="#" class="contact-link-thin">Transport zwłok Zielona Góra</a>
          <a href="#" class="contact-link-thin">Transport do innych miast</a>
        </div>
        <div class='col-lg-3 col-md-6 col-sm-6'>
          <p class='contact-header'>Ostatnio dodane poradniki</p>
          <div>
            <a href="#">
              <div class="advice advice1">
                <div class="advice-overlay">Jak dbamy o bezpieczeństwo?</div>
              </div>

            </a>
          </div>
          <div>
            <a href="#">
              <div class="advice advice2">
                <div class="advice-overlay">Czym transportujemy?</div>
              </div>

            </a>
          </div>
          <div>
            <a href="#">
              <div class="advice advice1">
                <div class="advice-overlay">Lorem ipsum...</div>
              </div>

            </a>
          </div>


        </div>
      </div>
  </section>
  <footer class="main-footer">
    <div class="container">
      <p class="footer-para">Copyright 2018 by krematorium.pl</p>
    </div>
  </footer>

  <!-- JQuery -->
  <script type="text/javascript" src="../assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../assets/front/js/mdb.min.js"></script>

<script type="text/javascript">
    var mapsAPI = "AIzaSyCJ4dpcfW41zqwNu_lJ4EcT7ON-SNXDZrA";
  $.getScript('https://maps.google.com/maps/api/js?key=' + mapsAPI).done(function() {
    initMap();
  });

  function initMap() {
  <?php foreach ($miasta as $row) : ?>
  var miasto<?php echo $row->id; ?> = new google.maps.LatLng(<?php echo $row->lat; ?>,<?php echo $row->lng; ?> );
   // var   pointB = new google.maps.LatLng(52.919438, 19.145136);
  <?php endforeach; ?>
  <?php foreach ($miasta as $row) : ?>
    myOptions = {
      zoom: 5,
      center: miasto100
    },
    <?php break; ?>
    <?php endforeach; ?>
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
     <?php foreach ($miasta as $row) : ?>
    miasto<?php echo $row->id; ?> = new google.maps.Marker({
      position: miasto<?php echo $row->id; ?>,
      title: "<?php echo $row->nazwa; ?>",
      label: "",
      map: map
    });
    <?php endforeach; ?>
    //  markerB = new google.maps.Marker({
    //   position: pointB,
    //   title: "Marker B",
    //   label: "B",
    //   map: map
    // });
}
</script>
  
</body>

</html>