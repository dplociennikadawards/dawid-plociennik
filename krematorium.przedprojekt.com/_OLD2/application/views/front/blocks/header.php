  <header>

    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="/"><img src="../assets/front/img/logo.png" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../p/o_nas">O nas</a>
              <span class="mr-1 ml-1 line-span">|</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../p/transport_zmarlych">Transport zmarłych</a>
              <span class="mr-1 ml-1 line-span">|</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../p/uslugi_pogrzebowe">Usługi pogrzebowe</a>
              <span class="mr-1 ml-1 line-span">|</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../p/krematorium">Krematorium</a>
              <span class="mr-1 ml-1 line-span">|</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../p/poradnik">Poradnik</a>
              <span class="mr-1 ml-1 line-span">|</span>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../p/kontakt">Kontakt</a>
            </li>
            <li class="nav-item language-item">
              <div class="btn-group trip-btn">
                <div data-toggle="dropdown" aria-expanded="false">
                  <div class="nav-icon-wrapper">
                    <div class="nav-circle mr-1">
                      <img src="../assets/front/img/ukrainine.png">
                    </div>
                    <div class="my-bounce">
                      <img class="down-arrow" src="../assets/front/img/arrow-down.png" class="ml-2">
                    </div>
                  </div>
                </div>
                <div class="dropdown-menu" style="left:-50px!important;">
                  <a class="dropdown-item" href="#">
                    ukraiński </a>
                  <a class="dropdown-item" href="#">
                    polski</a>
                  <a class="dropdown-item" href="#">
                    niemiecki </a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  </header>