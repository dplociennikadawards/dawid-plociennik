  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">Krematorium</h1>
      </div>
    </div>
  </section>
 <div id="mdb-lightbox-ui"></div>
 <section class="bg-white p-md-5 py-3">


<!-- Card -->
<div class="card card-cascade wider reverse">

  <!-- Card image -->
  <div class="view view-cascade overlay">
    <img class="card-img-top" src="../assets/front/img/header.png" alt="Card image cap">
    <a href="#gallery">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center mb-5">

    <!-- Title -->
    <h4 class="card-title text-white text-shadow "><strong>Krematorium</strong></h4>
    <!-- Text -->
    <p class="card-text text-white text-shadow font-weight-bold " style="font-size: 1rem;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

  </div>

</div>
<!-- Card -->


<a id="gallery"></a>
  <!-- Grid row -->
<div class="gallery mdb-lightbox row" id="gallery">

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../assets/front/img/przyklad2.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad2.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../assets/front/img/przyklad1.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad1.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../assets/front/img/przyklad2.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad2.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../img/przyklad1.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad1.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../assets/front/img/przyklad2.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad2.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

  <!-- Grid column -->
  <figure class="col-md-4">
    <a href="../assets/front/img/przyklad1.jpg" data-size="1600x1067">
      <img class="img-fluid my-shadow" src="../assets/front/img/przyklad1.jpg" alt="Card image cap">
    </a>
  </figure>
  <!-- Grid column -->

</div>
<!-- Grid row -->
 </section>

  <script type="text/javascript">
    function lightbox() {
      $("#mdb-lightbox-ui").load("../assets/front/mdb-addons/mdb-lightbox-ui.html");
      }
  </script>