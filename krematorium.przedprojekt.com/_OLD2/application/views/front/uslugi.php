  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">Usługi</h1>
      </div>
    </div>
  </section>
 
 <section class="bg-white p-md-5 p-3">
  <div class="row">
    <div class="col-md-4 mb-4">
      <!-- Card -->
      <div class="card promoting-card bg-img ">

        <!-- Card content -->
        <div class="card-body d-flex flex-row">

          <!-- Avatar -->
          <img src="../assets/front/img/statue.png" class="mr-3" height="50px" width="50px" alt="avatar">

          <!-- Content -->
          <div class="col align-self-center">

            <!-- Title -->
            <h4 class="card-title font-weight-bold text-white mb-2">Usługi pogrzebowe</h4>

          </div>

        </div>

        <!-- Card image -->
        <div class="view overlay">
          <img class="card-img-top rounded-0" src="../assets/front/img/przyklad1.jpg" alt="Card image cap">
          <a href="#!">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>

        <!-- Card content -->
        <div class="card-body">

          <div class="collapse-content">

            <!-- Text -->
            <p class="card-text collapse text-white" id="collapseContent">Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.</p>
            <!-- Button -->
            <a href="#" class="btn btn-block color-yellow text-shadow mt-3">Czytaj więcej</a>

          </div>

        </div>

      </div>
      <!-- Card -->
    </div>

    <div class="col-md-4 mb-4">
      <!-- Card -->
      <div class="card promoting-card bg-img">

        <!-- Card content -->
        <div class="card-body d-flex flex-row">

          <!-- Avatar -->
          <img src="../assets/front/img/coffin.png" class="mr-3" height="50px" width="50px" alt="avatar">

          <!-- Content -->
          <div class="col align-self-center">

            <!-- Title -->
            <h4 class="card-title font-weight-bold text-white mb-2">Krematorium</h4>

          </div>

        </div>

        <!-- Card image -->
        <div class="view overlay">
          <img class="card-img-top rounded-0" src="../assets/front/img/przyklad2.jpg" alt="Card image cap">
          <a href="#!">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>

        <!-- Card content -->
        <div class="card-body">

          <div class="collapse-content">

            <!-- Text -->
            <p class="card-text collapse text-white" id="collapseContent">Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.</p>
            <!-- Button -->
            <a href="#" class="btn btn-block color-yellow text-shadow mt-3">Czytaj więcej</a>

          </div>

        </div>

      </div>
      <!-- Card -->
    </div>

    <div class="col-md-4 mb-4">
      <!-- Card -->
      <div class="card promoting-card bg-img">

        <!-- Card content -->
        <div class="card-body d-flex flex-row">

          <!-- Avatar -->
          <img src="../assets/front/img/world.png" class="mr-3" height="50px" width="50px" alt="avatar">

          <!-- Content -->
          <div class="col align-self-center">

            <!-- Title -->
            <h4 class="card-title font-weight-bold text-white mb-2">Transport międzynarodowy</h4>

          </div>

        </div>

        <!-- Card image -->
        <div class="view overlay">
          <img class="card-img-top rounded-0" src="../assets/front/img/przyklad1.jpg" alt="Card image cap">
          <a href="#!">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>

        <!-- Card content -->
        <div class="card-body">

          <div class="collapse-content">

            <!-- Text -->
            <p class="card-text collapse text-white" id="collapseContent">Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.</p>
            <!-- Button -->
            <a href="#" class="btn btn-block color-yellow text-shadow mt-3">Czytaj więcej</a>

          </div>

        </div>

      </div>
      <!-- Card -->
    </div>
  </div>
 </section>