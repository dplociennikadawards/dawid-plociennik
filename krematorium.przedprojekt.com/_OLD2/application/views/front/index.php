  <section class="main-photo">
    <div class='container'>
      <div class="main-text-wrapper">
        <h1 class="main-header">Międzynarodowy</h1>
        <p class="main-para text-shadow">Transport zmarłych</p>

        <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Dowiedz się więcej</a>

      </div>
    </div>
    <div class="container icons-container">
      <div class="icons-wrapper">
        <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card1 text-center'>
              <img src="../assets/front/img/world.png" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header">Transport zmarłych</p>
                <div class="short-line"></div>
              </div>
              <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                bibendum auctor, nisi elit ...</p>
              <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Transport zmarłych</a>

            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card2 text-center'>
              <img src="../assets/front/img/statue.png" alt="world-icon" class="world-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header">Usługi pogrzebowe</p>
                <div class="short-line"></div>
              </div>
              <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                bibendum auctor, nisi elit ...</p>
              <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Usługi pogrzebowe</a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class='icon-card card3 text-center'>
              <img src="../assets/front/img/coffin.png" alt="world-icon" class="coffin-icon">
              <div class="lines-wrapper">
                <div class="short-line"></div>
                <p class="my-card-header">Krematorium</p>
                <div class="short-line"></div>
              </div>
              <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                bibendum auctor, nisi elit ...</p>
              <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Krematorium</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="my-carousel">
    <div class="container">
        <div class="container icons-container mobile-container">
            <div class="icons-wrapper">
              <div class="row">
                <div class="col-lg-4 col-md-4">
                  <div class='icon-card card1 text-center'>
                    <img src="../assets/front/img/world.png" alt="world-icon" class="world-icon">
                    <div class="lines-wrapper">
                      <div class="short-line"></div>
                      <p class="my-card-header">Transport zmarłych</p>
                      <div class="short-line"></div>
                    </div>
                    <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                      bibendum auctor, nisi elit ...</p>
                    <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Transport zmarłych</a>
      
                  </div>
                </div>
                <div class="col-lg-4 col-md-4">
                  <div class='icon-card card2 text-center'>
                    <img src="../assets/front/img/statue.png" alt="world-icon" class="world-icon">
                    <div class="lines-wrapper">
                      <div class="short-line"></div>
                      <p class="my-card-header">Usługi pogrzebowe</p>
                      <div class="short-line"></div>
                    </div>
                    <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                      bibendum auctor, nisi elit ...</p>
                    <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Usługi pogrzebowe</a>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4">
                  <div class='icon-card card3 text-center'>
                    <img src="../assets/front/img/coffin.png" alt="world-icon" class="coffin-icon">
                    <div class="lines-wrapper">
                      <div class="short-line"></div>
                      <p class="my-card-header">Krematorium</p>
                      <div class="short-line"></div>
                    </div>
                    <p class="card-para">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                      bibendum auctor, nisi elit ...</p>
                    <a href="#" role="button" class="btn mt-3 color-yellow text-shadow">Krematorium</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <h1 class="carousel-header">Jak działamy?</h1>
      <p class='carousel-para'>Proin <span class="yellow-span">gravida</span> nibh!</p>
      <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-1z" data-slide-to="0" class="active">
            1
          </li>
          <li data-target="#carousel-example-1z" data-slide-to="1">
            2
          </li>
          <li data-target="#carousel-example-1z" data-slide-to="2">
            3
          </li>
          <li data-target="#carousel-example-1z" data-slide-to="3">
            4
          </li>
          <li data-target="#carousel-example-1z" data-slide-to="4">
            5
          </li>
          <li data-target="#carousel-example-1z" data-slide-to="5">
            6
          </li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
          <!--First slide-->
          <div class="carousel-item active">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>

          </div>
          <!--/First slide-->
          <!--Second slide-->
          <div class="carousel-item">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>
          </div>
          <!--/Second slide-->
          <!--Third slide-->
          <div class="carousel-item">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet.Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>
          </div>
          <div class="carousel-item">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>
          </div>
          <div class="carousel-item">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>
          </div>
          <div class="carousel-item">
            <div class="my-item">
              <p class="carousel-contact">
                1. Kontakt z nami pod numerem tel. <a href="tel:xxxx" class="carousel-link">123-456-789</a>
              </p>
              <p class="carousel-para">
                Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem
                quis bibendum auctor, nisi elit ...
              </p>
            </div>
          </div>
          <!--/Third slide-->
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
          <img src="../assets/front/img/arrow-left.png">
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
          <img src="../assets/front/img/arrow-right.png">
          <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
      </div>
      <!--/.Carousel Wrapper-->

    </div>
  </section>
  <section class="transport">
    <div class='container text-center'>
      <h1 class="transport-header text-white text-shadow">Zapewnij transport anenan sollicitudin, lorem quis bibendum!</h1>
      <a href="tel:xxxxx" class="transport-link text-white text-shadow"><img class="mr-2 mb-2"><i class="fas fa-phone"></i> +01 234 567 899</a>
      <p class='transport-para text-white text-shadow font-weight-bold'>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
        himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque
        elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat,
        velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
      <a href="#" role="button" class="transport-button text-white text-shadow">Pozostaw dane, oddzwonimy!</a>
    </div>
  </section>
