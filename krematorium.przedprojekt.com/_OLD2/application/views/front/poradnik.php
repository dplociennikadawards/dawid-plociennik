  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">Poradnik</h1>
      </div>
    </div>
  </section>
 
 <section class="bg-white p-md-5 p-3">
  <div class="row">
    <div class="col-md-4 mb-3">
      <!-- Rotating card -->
      <div class="card-wrapper">
        <div id="card-1" class="card card-rotating text-center bg-img">

          <!-- Front Side -->
          <div class="face front">

            <!-- Image-->
            <div class="card-up">
              <img class="card-img-top" src="../assets/front/img/przyklad1.jpg" alt="Image with a photo of clouds.">
            </div>

            <!-- Content -->
            <div class="card-body">
              <h4 class="font-weight-bold text-white mb-3">Przygotowanie ceremonii</h4>
              <p class="font-weight-bold text-gold">10.02.2019</p>
              <!-- Triggering button -->
              <a class="rotate-btn text-white" data-card="card-1"><i class="fas fa-redo-alt"></i> Czytaj więcej</a>
            </div>
          </div>
          <!-- Front Side -->

          <!-- Back Side -->
          <div class="face back">
            <div class="card-body">

              <!-- Content -->
              <h4 class="font-weight-bold text-white mb-0">Przygotowanie ceremoni</h4>
              <hr>
              <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <!-- Triggering button -->
                <a class="rotate-btn text-white" data-card="card-1"><i class="fas fa-undo"></i> Powrót</a>

            </div>
          </div>
          <!-- Back Side -->

        </div>
      </div>
      <!-- Rotating card -->
    </div>

    <div class="col-md-4 mb-3">
      <!-- Rotating card -->
      <div class="card-wrapper">
        <div id="card-2" class="bg-img card card-rotating text-center">

          <!-- Front Side -->
          <div class="face front">

            <!-- Image-->
            <div class="card-up">
              <img class="card-img-top" src="../assets/front/img/przyklad2.jpg" alt="Image with a photo of clouds.">
            </div>

            <!-- Content -->
            <div class="card-body">
              <h4 class="font-weight-bold text-white mb-3">Jak zamówić transport?</h4>
              <p class="font-weight-bold text-gold">09.02.2018</p>
              <!-- Triggering button -->
              <a class="rotate-btn text-white" data-card="card-2"><i class="fas fa-redo-alt"></i> Czytaj więcej</a>
            </div>
          </div>
          <!-- Front Side -->

          <!-- Back Side -->
          <div class="face back">
            <div class="card-body">

              <!-- Content -->
              <h4 class="font-weight-bold text-white mb-0">Jak zamówić transport?</h4>
              <hr>
              <p class="text-white">
                Wystarczy się z nami skontaktować na jeden z trzech sposobów, a jeden z naszych konsultantów ustali z Państwem dogodny termin. Wszystkimi formalnościami zajmuję się nasza firma.
                <hr>
                <!-- Social Icons -->
          <div>
            <a href="tel:xxxx" class="contact-link text-white"><img class="mr-2 mb-1"><i class="fas fa-phone"></i> +12 345-678-987</a>
            <a href="mailto:xxxx" class="contact-link-thin text-white"><img class="mr-2 mb-1"><i class="far fa-envelope"></i> biuro@krematorium.pl</a>
            <p class="contact-link-thin text-white"><img class="mr-2 mb-1"><i class="fas fa-map-marker-alt"></i> ul. Kowalska 8, Kowalowo 82-133</p>
          </div>
                <!-- Triggering button -->
                <a class="rotate-btn text-white" data-card="card-2"><i class="fas fa-undo"></i> Powrót</a>

            </div>
          </div>
          <!-- Back Side -->

        </div>
      </div>
      <!-- Rotating card -->
    </div>

    <div class="col-md-4 mb-3">
      <!-- Rotating card -->
      <div class="card-wrapper">
        <div id="card-3" class="bg-img card card-rotating text-center">

          <!-- Front Side -->
          <div class="face front">

            <!-- Image-->
            <div class="card-up">
              <img class="card-img-top" src="../assets/front/img/przyklad1.jpg" alt="Image with a photo of clouds.">
            </div>

            <!-- Content -->
            <div class="card-body">
              <h4 class="font-weight-bold text-white mb-3">Lorem ipsum</h4>
              <p class="font-weight-bold text-gold">08.02.2018</p>
              <!-- Triggering button -->
              <a class="rotate-btn text-white" data-card="card-3"><i class="fas fa-redo-alt"></i> Czytaj więcej</a>
            </div>
          </div>
          <!-- Front Side -->

          <!-- Back Side -->
          <div class="face back">
            <div class="card-body">

              <!-- Content -->
              <h4 class="font-weight-bold text-white mb-0">Lorem ipsum</h4>
              <hr>
              <p class="text-white">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat tenetur odio suscipit non commodi vel
                eius veniam maxime?</p>
                <!-- Triggering button -->
                <a class="rotate-btn text-white" data-card="card-3"><i class="fas fa-undo"></i> Powrót</a>

            </div>
          </div>
          <!-- Back Side -->

        </div>
      </div>
      <!-- Rotating card -->
    </div>
  </div>
 </section>
