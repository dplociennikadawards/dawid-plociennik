

  <section class="header-photo">
    <div class='container'>
      <div class="header-text-wrapper">
        <h1 class="main-header">Transport zmarłych</h1>
      </div>
    </div>
  </section>
  <section class="bg-white pb-0">

      <div class="row py-5 align-items-center"> 

          <div class="col-md-7 offset-md-1">
            <div id="map-canvas" class="pl-md-3" style="width: 100%;height: 500px;"></div>
          </div>

          <div class="col-md-3 mt-4 mt-md-0">
            <div class="row px-5 px-md-0">
              <!-- News jumbotron -->
                  <div class="jumbotron text-left hoverable p-4 my-shadow">
                    <div class="view overlay">
                      <img src="../assets/front/img/przyklad1.jpg" class="img-fluid" alt="Sample image for first version of blog listing">
                      <a>
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                    <div class="text-md-justify ml-3 mt-3 text-white">

                      <h4 class="h4 mb-4">Jan Kowalski</h4>

                      <p class="font-weight-bold">Kierowca karawanu - Polska</p>
                      <p class="font-weight-normal"><a href="tel:772 32 41 21" class="text-white"><i class="fas fa-phone"></i> 772 32 41 21</a></p>
                      <p class="font-weight-normal"><a href="tel:509 541 324" class="text-white"><i class="fas fa-mobile-alt"></i> 509 541 324</a></p>
                      <p class="font-weight-normal"><a href="mailto:kowalski.j@krematroium.pl" class="text-white"><i class="far fa-envelope"></i> kowalski.j@krematroium.pl</a></p>


                    </div>
                  </div>
                  <!-- News jumbotron -->
            </div>
          </div>


      </div>

  </section>



