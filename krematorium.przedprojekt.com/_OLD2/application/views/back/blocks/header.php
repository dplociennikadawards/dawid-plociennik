<body class="collapsed-menu">

<div id="preloader">
    <div id="status">
      <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
      </div>
    </div>
</div>

    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo">
      <a href="/" class="mx-auto">
        <img src="/assets/front/img/logo.png" width="150">
      </a>
    </div>
    <div class="br-sideleft overflow-y-auto green-dark-bg">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Menu</label>
      <!-- br-sideleft-menu -->
      <ul class="br-sideleft-menu">

        <li class="br-menu-item">
          <a href="/panel/admin" class="br-menu-link <?php if($this->uri->segment(2)=="admin"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-home tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Strona główna</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="/panel/onas" class="br-menu-link <?php if($this->uri->segment(2)=="onas"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-users tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;O nas</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="/panel/login/logout" class="br-menu-link">
            <i class="menu-item-icon icon ion-power tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Wyloguj</span>
          </a>
        </li><!-- br-menu-item -->

      </ul><!-- br-sideleft-menu -->

      <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->


    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>

      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo $_SESSION['first_name']; ?></span>
              <img src="<?php echo $ico; ?>" class="wd-32 rounded" alt="">
              <span class="square-10 bg-success"></span>
            </a>
          </div>
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->