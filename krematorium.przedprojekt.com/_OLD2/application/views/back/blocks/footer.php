    <!-- ########## END: MAIN PANEL ########## -->
          <footer class="br-footer">
         <div class="footer-left d-flex align-items-center"></div>
        <div class="footer-right">
          <div class="mg-b-2">Copyright &copy; 2019 AdAwards. All Rights Reserved.</div>
          <div>Attentively and carefully made by Dawid.</div>
        </div>

      </footer>
   <script type="text/javascript" src="../../assets/back/template/js/copy.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/add.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/menu.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/main.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/main1.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery/jquery.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/popper.js/popper.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery-toggles/toggles.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/moment/moment.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery-switchbutton/jquery.switchButton.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/peity/jquery.peity.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/d3/d3.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/rickshaw/rickshaw.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/Flot/jquery.flot.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/flot-spline/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery.sparkline.bower/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/gmaps/gmaps.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/bracket.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/map.shiftworker.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/ResizeSensor.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/dashboard.dark.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/jquery.maskedinput/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="../../assets/back/template/js/loader.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/peity/jquery.peity.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/highlightjs/highlight.pack.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/medium-editor/medium-editor.js"></script>


    <script type="text/javascript" src="../../assets/back/template/lib/datatables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../assets/back/template/lib/datatables-responsive/dataTables.responsive.js"></script>
    <script>
      $(function(){
        'use strict'

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
          minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }
      });
    </script>
  </body>
</html>