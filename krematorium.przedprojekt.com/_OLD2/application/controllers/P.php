<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/index.php');
		$this->load->view('front/blocks/footer.php');
	}

	public function o_nas()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/o_nas.php');
		$this->load->view('front/blocks/footer.php');
	}

	public function transport_zmarlych()
	{
		$data['miasta'] = $this->users_m->get('miasta');

		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/transport.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function uslugi_pogrzebowe()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/uslugi.php');
		$this->load->view('front/blocks/footer.php');
	}

	public function krematorium()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/krematorium.php');
		$this->load->view('front/blocks/footer.php');
	}

	public function poradnik()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/poradnik.php');
		$this->load->view('front/blocks/footer.php');
	}

	public function kontakt()
	{
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/kontakt.php');
		$this->load->view('front/blocks/footer.php');
	}
}
