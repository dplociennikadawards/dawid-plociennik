<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/glowna/main_page');
		$this->load->view('back/blocks/footer');
	}

public function profil()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/profil/main_page', $data);
		$this->load->view('blocks/footer');

		$save = $this->input->post('save');

		if(isset($save))
			{
				$update['photo'] = $this->input->post('photo');
				$update['first_name'] = $this->input->post('firs_name');
				$update['last_name'] = $this->input->post('last_name');
				$update['email'] = $this->input->post('email');
				$update['phone'] = $this->input->post('phone');

				$this->db->where('id', $_SESSION['id']);
				$this->db->update('workers', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie wprowadziłeś zmiany!</p>');

				redirect('adpanel/profil');
			}	
	}

public function nagrody()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['awards'] = $this->users_m->get('awards', $_SESSION['id']);

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/nagrody/main_page', $data);
		$this->load->view('blocks/footer');

	}

public function pracownicy()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['workers'] = $this->users_m->get('workers');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/pracownicy/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function uzytkownicy()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['customers'] = $this->users_m->get('customers');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/uzytkownicy/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function punkty()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['customers'] = $this->users_m->get('customers');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/punkty/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function przelicznik()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['okulary'] = $this->users_m->edit_element('counter', 1);
		$data['soczewki'] = $this->users_m->edit_element('counter', 2);
		$data['wizyta'] = $this->users_m->edit_element('counter', 3);
		$data['badanie'] = $this->users_m->edit_element('counter', 4);
		$data['bonus'] = $this->users_m->edit_element('bonuspointstat', 1);

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/przelicznik/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function wiadomosci()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['news'] = $this->users_m->get('news');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/wiadomosci/main_page', $data);
		$this->load->view('blocks/footer');
	}


public function o_nas()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['about_us'] = $this->users_m->get_element('text', 3);

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/o_nas/main_page', $data);
		$this->load->view('blocks/footer');

		$save = $this->input->post('save');

		if(isset($save))
			{
				$update['title'] = $_POST['title'];
				$update['content'] = $_POST['content'];
				
				$this->db->where(['id' => 3]);
		        $this->db->update('text', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie wprowadziłeś zmiany!</p>');

				redirect('adpanel/o_nas');
			}
	
	}

public function ranking()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['customers'] = $this->users_m->rank('customers');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/ranking/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function powiadomienia()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['customers'] = $this->users_m->get('customers');
		$data['note'] = $this->users_m->get('notifications');

		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/powiadomienia/main_page', $data);
		$this->load->view('blocks/footer');
	}

public function przyznaj_nagrody()
	{
		$data['row'] = $this->users_m->get_element('workers', $_SESSION['id']);
		$data['customers'] = $this->users_m->get('customers');
		$data['awards'] = $this->users_m->get('awards');


		$this->load->view('blocks/head');
		$this->load->view('blocks/header', $data);
		$this->load->view('pages/przyznaj_nagrody/main_page', $data);
		$this->load->view('blocks/footer');

		$save = $this->input->post('save');

		if(isset($save))
			{
				$data['customers_points'] = $this->users_m->add_points('customers', $_POST['award_customer']);
				$update['points'] = $data['customers_points']->points - $_POST['cost'];
				
				$this->db->where(['keyword' => $_POST['award_customer']]);
		        $this->db->update('customers', $update);

				$insert['worker_added'] = $_SESSION['email'];
				$insert['customer_keyword'] = $this->input->post('award_customer');
				$insert['cost'] = $this->input->post('cost');
				$insert['for_what'] = $this->input->post('award');

				$this->users_m->insert('granted_awards', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie przyznałeś nagrodę!</p>');

				redirect('adpanel/przyznaj_nagrody');
			}
	}
}
