<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onas extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['row'] = $this->users_m->get('onas');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/onas/index');
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/onas/add');
		$this->load->view('back/blocks/footer');
	}
}
