    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model
{
   public function __construct()
    {
        parent::__construct();
    }

    public function insert($table, $data)
    {
        $this->db->insert($table,$data);

    }

    public function get($table)
    {
        $query = $this->db->get($table);
        
        return $query->result();            
    }

    public function add_points($table,$where)
    {
        $this->db->where(['keyword' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function edit_element($table,$where)
    {
        $this->db->where(['id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_element($table,$where)
    {
        $this->db->where(['id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function get_customer($table,$where){

        $this->db->where(['keyword' => $where]);

        $query = $this->db->get($table);

        return $query->row();
    }

    public function get_customer_points($table,$where){

        $this->db->where(['customer_id' => $where]);

        $query = $this->db->get($table);

        return $query->result();
    }


    public function rank($table){

        $this->db->order_by("points", "desc");
        $query = $this->db->get($table);

        return $query->result();
    }


    public function delete($table,$where)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

}  
?>