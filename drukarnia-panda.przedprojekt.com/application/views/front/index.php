  <main>
    <section class="default default--background">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 my-order-mobile-2">
            <h3 class="default__title default__title--uppercase"><?php echo $onas_1->title; ?></h3>
              <p class="default__text"><?php echo $onas_1->content; ?></p>
          </div>
            <?php $pieces = explode(",", $onas_1->gallery); ?>
            <div class="col-lg-6 my-order-mobile-1">
              <?php if(count($pieces) > 1): $i=0; ?>
                  <div class="owl-carousel owl-theme owl-carousel-1">
                  <?php foreach($pieces as $piece): $i++; ?>
                  <div class="item">
                      <img class="d-block w-100" src="<?php echo base_url(); ?>uploads/media/<?php echo substr($piece, 0, strpos($piece, "|")); ?>"
                           alt="<?php echo substr($piece, strpos($piece, "|") + 1); ?>">
                  </div>
                <?php endforeach; ?>
                  </div>
              <?php else: foreach($pieces as $piece):?>
                <img class="d-block w-100" src="<?php echo base_url(); ?>uploads/media/<?php echo substr($piece, 0, strpos($piece, "|")); ?>"
                     alt="<?php echo substr($piece, strpos($piece, "|") + 1); ?>">
              <?php endforeach; endif; ?>
          </div>
        </div>
      </div>
    </section>

    <section class="default">
      <div class="container">
        <div class="default__title-box">
          <h3 class="default__title"><?php echo $onas_2->title; ?></h3>
          <p class="default__text"><?php echo $onas_2->content; ?></p>
        </div>
      </div>
    </section>

    <section class="default default--background">
      <div class="default__title-box">
        <h3 class="default__title">Najczęściej wykonywanymi pracami są</h3>
      </div>

      <div class="container">

        <div class="tabs__box">
          <ul class="tabs__list">
              <?php $i=0; foreach(array_reverse($products) as $item): if($item->produkty_id > 1 && $i != 6): $i++; ?>
            <li class="tabs__link <?php if($i == 1) {echo 'tabs__link--first tabs__link-active';} ?>" data-tab="tab-<?php echo $i; ?>"><?php echo $item->title; ?></li>
              <?php endif; endforeach; ?>
              <li class="tabs__link"><a href="<?php echo base_url(); ?>p/produkty" class="text-dark">Inne</a></li>
          </ul>

            <?php $i=0; foreach (array_reverse($products) as $item): if($item->produkty_id > 1): $i++; ?>
          <div id="tab-<?php echo $i; ?>" class="tabs__content <?php if($i == 1) {echo 'tabs__content-active';} ?>">
              <p class="default__text default__text--center">
                  <?php echo $item->subtitle; ?>
              </p>

            <div class="tabs__carousel">
              <div class="owl-carousel owl-theme">
                  <?php $pieces = explode(",", $item->gallery); ?>
                  <?php foreach ($pieces as $piece): ?>

                <div class="owl-carousel__item">
                  <div>
                      <a class="" href="<?php echo base_url(); ?>p/produkt/<?php echo $this->slugify_m->slugify($item->title) . '/' . $item->produkty_id; ?>">
                    <img class="img-fluid" src="<?php echo base_url(); ?>uploads/media/<?php echo substr($piece, 0, strpos($piece, "|")); ?>" alt="<?php echo substr($piece, strpos($piece, "|") + 1); ?>">
                      </a>
                  </div>
                  <a class="owl-carousel__button" href="<?php echo base_url(); ?>p/produkt/<?php echo $this->slugify_m->slugify($item->title) . '/' . $item->produkty_id; ?>">sprawdź</a>
                </div>
                  <?php endforeach; ?>

              </div>
            </div>

          </div>
            <?php endif; endforeach; ?>
        </div>

      </div>
    </section>

    <section class="default">
      <div class="default__title-box">
        <h3 class="default__title">Nasze wyróżnienia</h3>
      </div>

      <div class="container">
          <div class="awards">
              <?php foreach ($awards as $item): if($item->wyroznienia_id > 1): ?>
                  <a href="<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $item->photo; ?>" class="awards__frame" data-lightbox="image-1" data-title="<?php echo $item->alt; ?>" style="background-image: url('<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $item->photo; ?>');"></a>
              <?php endif; endforeach; ?>
          </div>
      </div>
    </section>

