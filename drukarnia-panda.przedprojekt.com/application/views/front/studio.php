<main>

    <section class="default about">
        <div class="default__title-box">
            <h3 class="default__title"><?php echo $row_1->title; ?></h3>
        </div>

        <div class="container">
            <p class="default__text default__text--center">
                <?php echo $row_1->content; ?>
            </p>

            <?php $i=0; foreach($rows as $row): if($row->studio_id > 1): $i++; if($i%2!=0): ?>
            <!-- Grid row -->
            <div class="row mt-5">

                <!-- Grid column -->
                <div class="col-lg-5">

                    <!-- Featured image -->
                    <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                        <img class="img-fluid" src="<?php echo base_url(); ?>uploads/studio/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
                    </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-7">

                    <!-- Post title -->
                    <h3 class="font-weight-bold mb-3"><strong><?php echo $row->title; ?></strong></h3>
                    <h5 class="font-weight-bold mb-3"><strong><?php echo $row->subtitle; ?></strong></h5>
                    <!-- Excerpt -->
                    <p><?php echo $row->content; ?></p>
                    <!-- Post data -->
                    <p><a><strong>Drukarnia Panda</strong></a>, <?php echo date('d.m.Y', strtotime($row->create_date)); ?></p>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <hr class="my-5">

            <?php else: ?>

                <!-- Grid row -->
                <div class="row mt-5">

                    <!-- Grid column -->
                    <div class="col-lg-7">

                        <!-- Post title -->
                        <h3 class="font-weight-bold mb-3"><strong><?php echo $row->title; ?></strong></h3>
                        <h5 class="font-weight-bold mb-3"><strong><?php echo $row->subtitle; ?></strong></h5>
                        <!-- Excerpt -->
                        <p><?php echo $row->content; ?></p>
                        <!-- Post data -->
                        <p><a><strong>Drukarnia Panda</strong></a>, <?php echo date('d.m.Y', strtotime($row->create_date)); ?></p>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-5">

                        <!-- Featured image -->
                        <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                            <img class="img-fluid" src="<?php echo base_url(); ?>uploads/studio/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
                        </div>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

                <hr class="my-5">

            <?php endif; endif; endforeach; ?>
        </div>

    </section>



