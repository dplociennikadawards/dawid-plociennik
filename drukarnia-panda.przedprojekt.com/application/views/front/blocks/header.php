<header class="main-header">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="navbar__logo-frame"><a class="navbar__logo-link" href="<?php echo base_url(); ?>"><img class="navbar__logo"
            src="<?php echo base_url(); ?>uploads/ustawienia/<?php echo $settings->photo; ?>" alt="<?php echo $settings->alt; ?>"></a></div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == ''){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>">home</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'produkty'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>p/produkty">produkty</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'onas'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>p/onas">o nas</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'studio'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>/p/studio">studio graficzne</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'wyroznienia'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>p/wyroznienia">wyroznienia</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'druk'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>p/druk">druk</a>/
          </li>
          <li class="nav-item">
            <div class="<?php if($this->uri->segment(2) == 'kontakt'){echo 'navbar__active';} else {echo 'navbar__separator';} ?>"></div>
            <a class="nav-link" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
          </li>

        </ul>
      </div>
    </nav>
  </div>
  <div class="main-header__background" style="background-image: url('<?php echo base_url(); ?>uploads/podstrony/<?php echo $header->photo; ?>');">
    <div class="container">
      <div class="main-header__content-box">
        <h4 class="main-header__sm-title"><?php echo $header->title; ?></h4>
        <p class="main-header__subtitle"><?php echo $header->subtitle; ?></p>
      </div>
      <div class="main-header__button-box">
        <p class="main-header__button-desc">Odpowiedź do 24h!</p>
        <a href="<?php echo base_url(); ?>p/kontakt" class="main-header__button text-white"><img class="main-header__arrow-icon" src="<?php echo base_url(); ?>assets/front/img/arrow-check.png"
            alt="symbol zaznaczenia">ZŁÓŻ ZAPYTANIE ON-LINE</a>
      </div>
    </div>
  </div>
</header>