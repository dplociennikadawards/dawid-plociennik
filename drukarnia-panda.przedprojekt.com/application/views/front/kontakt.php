<main>

    <section class="default contact">
      <div class="container">
        <div class="default__title-box mb-5">
          <h3 class="default__title"><?php echo $row_1->title; ?></h3>

          <p class="default__text default__text--center">
              <?php echo $row_1->content; ?>
          </p>

        </div>

        <div class="contact__row row">
          <div class="col-lg-4">
            <div class="row">
              <div class="col-2"><img src="<?php echo base_url(); ?>assets/front/img/phone-icon.png" alt="ikona telefonu"></div>
              <div class="col-10">
                <h5 class="contact__title">
                  Kontakt telefoniczny
                </h5>
                <p class="contact__desc"><?php echo $row_2->liame; ?></p>

                <div class="default__text">
                  <a class="contact__link" href="tel:<?php echo $row_2->title; ?>">tel./fax <?php echo $row_2->title; ?></a><br>
                  <a class="contact__link" href="tel:<?php echo $row_2->subtitle; ?>">tel. kom. <?php echo $row_2->subtitle; ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-2"><img src="<?php echo base_url(); ?>assets/front/img/envelope.png" alt="ikona koperty"></div>
              <div class="col-10">
                <h5 class="contact__title">
                  Kontakt e-mail
                </h5>
                <p class="contact__desc"><?php echo $row_3->liame; ?></p>

                <div class="default__text">
                  <a class="contact__link" href="mailto:<?php echo $row_3->title; ?>"><?php echo $row_3->title; ?></a><br>
                  <a class="contact__link" href="mailto:<?php echo $row_3->subtitle; ?>"><?php echo $row_3->subtitle; ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-2"><img src="<?php echo base_url(); ?>assets/front/img/location.png" alt="ikona lokalizacji"></div>
              <div class="col-10">
                <h5 class="contact__title">
                  Biuro PANDA
                </h5>
                <p class="contact__desc"><?php echo $row_4->title; ?></p>

                <div class="default__text">
                    <?php echo $row_4->content; ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="contact__form-container">
          <h5 class="contact__title mb-5">Lub skorzystaj z formularza kontaktowego</h5>

          <div class="contact__form" action="" method="post">
              <span id="mailInfo2"></span>
            <div class="d-flex justify-content-between contact__inputs-box">
              <input class="contact__input" type="text" name="" id="nameMessage2" placeholder="Imię i nazwisko">
              <input class="contact__input" type="email" name="" id="liameMessage2" placeholder="E-mail">
              <input class="contact__input" type="text" name="" id="selectMessage2" placeholder="Temat">
            </div>
            <textarea class="contact__textarea" name="" id="contentMessage2" cols="30" rows="3" placeholder="Wiadomość"></textarea>
            <div class="d-flex justify-content-between">
              <div>
                <a href="https://www.facebook.com/PPHUPanda/"><img class="contact__icon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="ikona facebooka"></a>
<!--                <a href="#"><img class="contact__icon" src="--><?php //echo base_url(); ?><!--assets/front/img/instagram.png" alt="ikona instagrama"></a>-->
              </div><div class="form-check">
                    <input type="checkbox" class="form-check-input" id="materialUnchecked">
                    <label class="form-check-label" for="materialUnchecked"><small>Zgoda na przetwarzanie danych osobowych</small></label>
                </div>
              <input class="contact__submit" onclick="send_message2()" type="submit" value="wyślij">
            </div>
          </div>
        </div>
      </div>
    </section>


    <script type="text/javascript">
        function send_message2()
        {
            var name = document.getElementById('nameMessage2').value;
            var liame = document.getElementById('liameMessage2').value;
            var content = document.getElementById('contentMessage2').value;
            var select = document.getElementById('selectMessage2').value;

            if(liame != '' && liame.indexOf("@") != -1 && liame.indexOf(".") != -1 && name != '' && content != '') {
                $.ajax({
                    type: "post",
                    url:"<?php echo base_url(); ?>mail/send",
                    data: {name:name, liame:liame, content:content, select:select},
                    cache: false,
                    beforeSend:function(data)
                    {
                        document.getElementById('mailInfo2').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Wysyłanie wiadomości.</span>';
                    },
                    complete:function(html)
                    {
                        document.getElementById('mailInfo2').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Wiadomośc została wysłana!</span>';
                    }
                });
            } else {
                document.getElementById('mailInfo2').innerHTML = '<span class="text-danger"><i class="fas fa-times"></i> Pola zostały niepoprawnie wypełnione.</span>';
            }
        }
    </script>