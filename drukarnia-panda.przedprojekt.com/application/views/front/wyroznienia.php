<main>
  <section class="default">
      <div class="default__title-box mb-5">
        <h3 class="default__title"><?php echo $row_1->title; ?></h3>
        <p class="default__text default__text--center"><?php echo $row_1->content; ?></p>
      </div>

      <div class="container">

        <div class="row">
            <?php foreach ($rows as $row): if($row->wyroznienia_id > 1): ?>
          <div class="col-md-6 col-lg-6">
            <!-- miejsce dla lightboxa, link oraz img -->
            <a class="lightbox__link lightbox__link--img1" data-lightbox="image-1" data-title="<?php echo $row->alt; ?>" href="<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $row->photo; ?>" style="background-image: url('<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $row->photo; ?>')">
              <img class="lightbox__img" src="">
              <div class="light-box__icon-box"><img class="lightbox__icon" src="<?php echo base_url(); ?>assets/front/img/loupe.png" alt="ikona lupy"></div>
            </a>

            <div class="lightbox__text-container">
              <h4 class="lightbox__title">
                CERTYFIKAT "FIRMA ROKU 2005" dla DRUKARNI PANDA
              </h4>
              Stowarzyszenie Klub Ludzi Życzliwych<br>
              W uznaniu szczególnych zasług w propagowaniu<br>
              wartości humanitarnych.<br>
              Międzynarodowa Kapituła Nagród Humanitarnych
            </div>
          </div>
            <?php endif; endforeach; ?>
        </div>

      </div>
    </section>

