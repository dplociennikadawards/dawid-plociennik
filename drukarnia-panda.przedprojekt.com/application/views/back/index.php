<main class="main-section water-logo">
	<section>
		<div class="container-fluid">

	        <div class="row p-3">

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/home.svg');"></div>Strona główna
                </button>
              </a>
            </div>

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/podstrony">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/folder.svg');"></div>Podstrony
		            </button>
		        </a>
	          </div>  

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/produkty">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/online-shop.svg');"></div>Produkty
		            </button>
		        </a>
	          </div>  

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/onas">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/teamwork.svg');"></div>O nas
		            </button>
		        </a>
	          </div> 

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/studio">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/gallery.svg');"></div>Studio graficzne
		            </button>
		        </a>
	          </div> 

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/wyroznienia">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/winner.svg');"></div>Wyróżnienia
		            </button>
		        </a>
	          </div>

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/druk">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/print.svg');"></div>Druk
		            </button>
		        </a>
	          </div> 

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/media">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/photo-camera.svg');"></div>Media
		            </button>
		        </a>
	          </div> 

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/kontakt">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/email.svg');"></div>Kontakt
		            </button>
		        </a>
	          </div> 

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/ustawienia">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/settings.svg');"></div>Ustawienia
		            </button>
		        </a>
	          </div>

	        </div>

      	</div>
	</section>
</main>