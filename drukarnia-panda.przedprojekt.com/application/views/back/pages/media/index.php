<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-5 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	</div>
	<section>
		<div class="container">
			<div class="card card-cascade narrower">
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			    <div class="md-form my-0">
					<a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>
			    </div>
			    <div class="text-right" style="width: 181px;">
				    <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#fullHeightModalRight">
				    	<i class="fas fa-plus mt-0"></i>
				    </button>
			    </div>
			  </div>
			  <div class="px-4">
			    <div class="table-wrapper">
			    	<span id="change_alt_info"></span>
				    <span id="refreshTable">
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">
				        <thead>
				          <tr>
				            <th class="th-lg cursor" style="width: 20%;">
				              Zdjęcie
				            </th>
				            <th class="th-lg cursor" style="width: 50%;">
				              Link
				            </th>
				            <th class="th-lg cursor" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 10%;"></th>
				          </tr>
				        </thead>
				        <tbody>
				        	<?php foreach ($rows as $key): ?>
							<tr>
								<td class="align-middle">
									<img src="<?php echo base_url(); ?>uploads/media/<?php echo $key->photo; ?>" width="250"><br>
				                    <div class="md-form mb-5">
				                      <input type="text" id="alt_change" class="form-control" name="alt_change" onchange="change_alt(<?php echo $key->media_id; ?>)" value="<?php echo $key->alt; ?>">
				                      <label for="alt_change">Tekst alternatywny</label>
				                    </div>
								</td>
								<td class="align-middle">
									<?php echo base_url(); ?>uploads/media/<?php echo $key->photo; ?>
								</td>
								<td class="align-middle"><?php echo date('Y-m-d H:i', strtotime($key->create_date)); ?></td>
								<td class="align-middle text-center">
								    <a data-toggle="modal" title="Usuń" data-target="#centralModalSm" onclick="delete_data('<?php echo $this->uri->segment(2); ?>',<?php echo $key->media_id; ?>);">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									    </button>
								    </a>
								</td>
							</tr>
							<?php endforeach; ?>
				        </tbody>
				      </table>
				      <script type="text/javascript">filtrTable2();</script>
			      </span>
			    </div>
			  </div>
			</div>
		</div>
	</section>
</main>


<!-- Full Height Modal Right -->
<div class="modal fade right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Dodaj nowe zdjęcie</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="change"></p>
		<div class="md-form">
			<input type="text" id="alt_text" class="form-control" name="title" required>
			<label for="alt_text">Tekst alternatywny zdjęcia</label>
		</div>
		<div class="md-form">
			<div class="file-field">
				<div id="photo_show" class="text-center"></div>
					<div class="btn btn-primary btn-sm float-left gold-gradient">
						<span>Wybierz zdjęcie</span>
						<input type="file" name="photo" id="photo">
					</div>
				<div class="file-path-wrapper">
					<input id="name_photo" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" required readonly>
				</div>
			</div>
		</div>
		<div class="md-form">
			<button type="button" class="btn btn-primary btn-block gold-gradient" onclick="add_photo();">Zapisz</button>
		</div>
      </div>
    </div>
  </div>
</div>


<!-- Full Height Modal Right -->
    <script type="text/javascript">
      function add_photo()
      {
      	var alt_text = document.getElementById('alt_text').value;
      	var photo = document.getElementById('photo');

      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('alt_text',alt_text);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/media/add", 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);
             	document.getElementById('alt_text').value = '';
             	document.getElementById('name_photo').value = '';
             	document.getElementById('photo').value = '';
             	document.getElementById('photo_show').innerHTML = '';
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                
                setTimeout(function(){ filtrTable2(); }, 1000);
             }  
        });  
      }
    </script>

    <script type="text/javascript">
      function change_alt(id)
      {
      	var alt_text = document.getElementById('alt_change').value;
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/media/change_alt", 
             data: {id:id, alt_text:alt_text}, 
             cache: false,
             complete:function(html)
             {
             	console.log(html);
                document.getElementById('change_alt_info').style.display = 'block';
                document.getElementById('change_alt_info').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
		          setTimeout(function(){ 
		            $(document).ready(function(){
		                $("#change_alt_info").fadeOut("slow");
		            });
		          }, 6000);

             }  
        });  
      }
    </script>