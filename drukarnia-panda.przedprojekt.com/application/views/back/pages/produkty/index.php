<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-5 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	</div>
	<section>
		<div class="container">
			<div class="card card-cascade narrower">
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			    <div class="md-form my-0">
					<a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>
			    </div>
			    <div class="text-right" style="width: 181px;">
				    <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#infoModal">
				    	<i class="far fa-file-alt mt-0"></i>
				    </button>
				    <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#fullHeightModalRight">
				    	<i class="fas fa-plus mt-0"></i>
				    </button>
			    </div>
			  </div>
			  <div class="px-4">
			    <div class="table-wrapper">
			    	<span id="change_alt_info"></span>
				    <span id="refreshTable">
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">
				        <thead>
				          <tr>
				            <th class="th-lg cursor" style="width: 100%;">
				              Nazwa produktu
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <tbody>
				        	<?php foreach ($rows as $key): ?>
				        	<?php if($key->produkty_id > 1): ?>
							<tr>
								<td class="align-middle"><?php echo $key->title; ?></td>
								<td class="align-middle"><?php echo date('Y-m-d H:i', strtotime($key->create_date)); ?></td>
								<td class="align-middle text-center">
								    <a data-toggle="modal" title="Edytuj" data-target="#editModal<?php echo $key->produkty_id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-edit mt-0"></i>
									    </button>
								    </a>
								    <a data-toggle="modal" title="Usuń" data-target="#centralModalSm" onclick="delete_data('<?php echo $this->uri->segment(2); ?>',<?php echo $key->produkty_id; ?>);">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									    </button>
								    </a>
								</td>
							</tr>
							<?php endif; ?>
							<?php endforeach; ?>
				        </tbody>
				      </table>
				      <script type="text/javascript">filtrTable2();</script>
			      </span>
			    </div>
			  </div>
			</div>
		</div>
	</section>
</main>


<!-- Full Height Modal Right -->
<div class="modal fade right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-lg modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Dodaj nowy wpis</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="change"></p>
        <div class="row">
        	<div class="col-md-4">
				<div class="md-form">
					<input type="text" id="title" class="form-control" name="title" required>
					<label for="title">Nazwa produktu</label>
				</div>
				<div class="md-form">
					<input type="text" id="subtitle" class="form-control" name="subtitle" required>
					<label for="subtitle">Podtytuł</label>
				</div>
				<div class="md-form">
					<input type="text" id="alt" class="form-control" name="alt" required>
					<label for="alt">Tekst alternatywny zdjęcia</label>
				</div>
				<div class="md-form">
					<div class="file-field">
						<div id="photo_show" class="text-center"></div>
							<div class="btn btn-primary btn-sm float-left gold-gradient">
								<span>Wybierz zdjęcie</span>
								<input type="file" name="photo" id="photo">
							</div>
						<div class="file-path-wrapper">
							<input id="name_photo" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" required readonly>
						</div>
					</div>
				</div>
				<div class="md-form">
					<button type="button" class="btn btn-primary btn-block gold-gradient" data-toggle="modal" data-target="#galleryModal" >Galeria</button>
					<input type="hidden" id="gallery" name="gallery">
				</div>
				<div class="md-form">
					<button id="button_ajax" type="button" class="btn btn-primary gold-gradient" onclick="add_record();">Zapisz</button>
				</div>	
			</div>
			<div class="col-md-8">
				<div class="md-form mt-md-0">
					<textarea id="content" class="form-control specialArea" name="content" required></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>


<!-- Central Modal Small -->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-fluid" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Galeria zdjęć</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<span id="refreshGallery">
	      	<div class="row">
	        <?php foreach ($media as $key): ?>
	        	<div class="col-md-2 mb-3 checked  align-self-center">
	        		<img id="photo<?php echo $key->media_id; ?>" src="<?php echo base_url(); ?>uploads/media/<?php echo $key->photo; ?>" class="img-fluid img-thumbnail modal-photo" onclick="add_gallery('<?php echo $key->photo; ?>','<?php echo $key->alt; ?>',<?php echo $key->media_id; ?>);">
	        		<div id="selected_photo<?php echo $key->media_id; ?>" class="centered text-success"></div>
	        	</div>
	        <?php endforeach; ?>
	        </div>
        </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->


<!-- Full Height Modal Right -->



<span id="refreshTableEdit">
<?php foreach ($rows as $key): ?>
<!-- Full Height Modal Right -->
<div class="modal fade right" id="editModal<?php echo $key->produkty_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-lg modal-left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Edytuj wpis "<?php echo $key->title; ?>"</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="changeEdit<?php echo $key->produkty_id; ?>"></p>
        <div class="row">
        	<div class="col-md-4">
				<div class="md-form">
					<input type="text" id="titleEdit<?php echo $key->produkty_id; ?>" class="form-control" name="title" value="<?php echo $key->title; ?>" required>
					<label for="title">Nazwa produktu</label>
				</div>
				<div class="md-form">
					<input type="text" id="subtitleEdit<?php echo $key->produkty_id; ?>" class="form-control" name="subtitle" value="<?php echo $key->subtitle; ?>" required>
					<label for="subtitle">Podtytuł</label>
				</div>
				<div class="md-form">
					<input type="text" id="altEdit<?php echo $key->produkty_id; ?>" class="form-control" name="alt" value="<?php echo $key->alt; ?>" required>
					<label for="alt">Tekst alternatywny zdjęcia</label>
				</div>
				<div class="md-form">
					<div class="file-field">
						<div id="photo_showEdit<?php echo $key->produkty_id; ?>" class="text-center">
							<img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $key->photo; ?>" width="250">
						</div>
							<div class="btn btn-primary btn-sm float-left gold-gradient">
								<span>Wybierz zdjęcie</span>
								<input type="file" name="photo" id="photoEdit<?php echo $key->produkty_id; ?>">
							</div>
						<div class="file-path-wrapper">
							<input id="name_photoEdit<?php echo $key->produkty_id; ?>" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" required readonly>
						</div>
					</div>
				</div>
				<div class="md-form">
					<button type="button" class="btn btn-primary btn-block gold-gradient" data-toggle="modal" data-target="#editModalGallery<?php echo $key->produkty_id; ?>" >Galeria</button>
					<input type="hidden" id="galleryEdit<?php echo $key->produkty_id; ?>" name="gallery" value="<?php echo $key->gallery; ?>">
				</div>
				<div class="md-form">
					<button id="button_ajax" type="button" class="btn btn-primary gold-gradient" onclick="edit_record(<?php echo $key->produkty_id; ?>);">Zapisz</button>
				</div>	
			</div>
			<div class="col-md-8">
				<div class="md-form mt-md-0">
					<textarea id="contentEdit<?php echo $key->produkty_id; ?>" class="form-control specialArea" name="content" required><?php echo $key->content; ?></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>


<!-- Central Modal Small -->
<div class="modal fade" id="editModalGallery<?php echo $key->produkty_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-fluid" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Galeria zdjęć</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<span id="refreshGallery">
	      	<div class="row">
	        <?php foreach ($media as $m): ?>
	        	<?php $pieces = explode(",", $key->gallery); ?>
	        	<div class="col-md-2 mb-3 checked align-self-center">
	        		<img id="photo<?php echo $key->produkty_id; ?><?php echo $m->media_id; ?>" src="<?php echo base_url(); ?>uploads/media/<?php echo $m->photo; ?>" class="img-fluid img-thumbnail modal-photo" onclick="edit_gallery('<?php echo $m->photo; ?>','<?php echo $m->alt; ?>',<?php echo $m->media_id; ?>,<?php echo $key->produkty_id; ?>);">
	        		<div id="selected_photo<?php echo $key->produkty_id; ?><?php echo $m->media_id; ?>" class="centered text-success">
	        			<?php foreach ($pieces as $p): ?>
	        				<?php if($m->photo == substr($p, 0, strpos($p, "|"))): ?>
	        				<i onclick="edit_gallery('<?php echo $m->photo; ?>','<?php echo $m->alt; ?>',<?php echo $m->media_id; ?>,<?php echo $key->produkty_id; ?>)" class="fas fa-check"></i>  	
	        				<?php endif; ?>			
	        			<?php endforeach ?>

	        		</div>
	        	</div>
	        <?php endforeach; ?>
	        </div>
        </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<script type="text/javascript">

//single header
window.addEventListener('load', function() {
  document.getElementById('photoEdit<?php echo $key->produkty_id; ?>').addEventListener('change', function() {
    document.getElementById('photo_showEdit<?php echo $key->produkty_id; ?>').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_showEdit<?php echo $key->produkty_id; ?>').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});



function imageIsLoaded(e) { }

</script>
<!-- Full Height Modal Right -->
<?php endforeach; ?>
</span>



<!-- Full Height Modal Right -->
<div class="modal fade right" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-md modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Edytuj informacje o stronie</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="changeInfo">Zmiany zapisują się automatycznie</p>
        <div class="row">
        	<div class="col-md-12">
				<div class="md-form">
					<input type="text" id="1title" class="form-control" name="title" value="<?php echo $row_1->title; ?>" onchange="update_change(1,'title')" required>
					<label for="title">Początek strony</label>
				</div>
				<div class="md-form mt-md-0">
					<textarea id="1content" class="form-control" rows="6" name="content" onchange="update_change(1,'content')"><?php echo $row_1->content; ?></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>

<!-- Full Height Modal Right -->


    <script type="text/javascript">
      function add_record()
      {
      	var title = document.getElementById('title').value;
      	var subtitle = document.getElementById('subtitle').value;
      	var alt = document.getElementById('alt').value;
      	var gallery = document.getElementById('gallery').value;
      	var content = document.getElementById('content').value;
      	var photo = document.getElementById('photo');
      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('title',title);
		data.append('subtitle',subtitle);
		data.append('alt',alt);
		data.append('gallery',gallery);
		data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/produkty/add", 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);
             	document.getElementById('title').value = '';
             	document.getElementById('subtitle').value = '';
             	document.getElementById('alt').value = '';
             	document.getElementById('gallery').value = '';
             	document.getElementById('content').value = '';
             	document.getElementById('name_photo').value = '';
             	document.getElementById('photo').value = '';
             	document.getElementById('photo_show').innerHTML = '';
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                $('#refreshGallery').load(document.URL +  ' #refreshGallery');
                
                setTimeout(function(){ filtrTable2(); }, 1500);
             }  
        });  
      }
    </script>

    <script type="text/javascript">
      function edit_record(id)
      {
      	var title = document.getElementById('titleEdit'+id).value;
      	var subtitle = document.getElementById('subtitleEdit'+id).value;
      	var alt = document.getElementById('altEdit'+id).value;
      	var gallery = document.getElementById('galleryEdit'+id).value;
      	var content = document.getElementById('contentEdit'+id).value;
      	var photo = document.getElementById('photoEdit'+id);
      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('title',title);
		data.append('subtitle',subtitle);
		data.append('alt',alt);
		data.append('gallery',gallery);
		data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/produkty/edit/"+id, 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);

                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                $('#refreshGalleryEdit'+id).load(document.URL +  ' #refreshGalleryEdit'+id);

                
                setTimeout(function(){ filtrTable2(); }, 1500);
             }  
        });  
      }
    </script>

<script type="text/javascript">
	function add_gallery(photo,alt,id) {
		if(document.getElementById('selected_photo'+id).innerHTML == '') {
			document.getElementById('gallery').value += photo + '|' + alt + ',';
			document.getElementById('selected_photo'+id).innerHTML = '<i onclick="add_gallery(\''+photo+'\',\''+alt+'\',\''+id+'\')" class="fas fa-check"></i>';	
		} else {
			var ret = document.getElementById('gallery').value;
			var photo = photo + '|' + alt + ',';
			var ret = ret.replace(photo,'');
			document.getElementById('gallery').value = ret;
			document.getElementById('selected_photo'+id).innerHTML = '';
		}

	}
</script>


<script type="text/javascript">
	function edit_gallery(photo,alt,id,record_id) {
		if(document.getElementById('selected_photo'+record_id+id).innerHTML == '') {
			document.getElementById('galleryEdit'+record_id).value += photo + '|' + alt + ',';
			document.getElementById('selected_photo'+record_id+id).innerHTML = '<i onclick="edit_gallery(\''+photo+'\',\''+alt+'\',\''+id+'\',\''+record_id+'\')" class="fas fa-check"></i>';	
		} else {
			var ret = document.getElementById('galleryEdit'+record_id).value;
			var photo = photo + '|' + alt + ',';
			var ret = ret.replace(photo,'');
			document.getElementById('galleryEdit'+record_id).value = ret;
			document.getElementById('selected_photo'+record_id+id).innerHTML = '';
		}

	}
</script>

    <script type="text/javascript">
      function update_change(id,field)
      {
        var field = field;
        var value = document.getElementById(id+field).value;

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/produkty/update_change/"+id+"/"+field, 
             data: {id:id, value:value}, 
             cache: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('changeInfo').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie zmian.</span>';
        	 },
             complete:function(html)
             {
                document.getElementById('changeInfo').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
             }  
        });  
      }
    </script>