<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->get_sort('media');
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/media/index', $data);
		$this->load->view('back/blocks/footer');
		$this->load->view('back/blocks/ajax');
	}
	public function add() {
 
            echo $this->upload->display_errors(); 
            print_r($_POST);

		    $now = date('Y-m-d');
	        if (!is_dir('uploads/media/'.$now)) {
		    	mkdir('./uploads/media/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/media/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) { 
				$insert['alt'] = $_POST['alt_text'];
				$insert['photo'] = 'blad dodawania zdjecia';
				$this->base_m->insert('media', $insert);     
            } else {
            	$data = $this->upload->data();
				$insert['alt'] = $_POST['alt_text'];
				$insert['photo'] = $now.'/'.$data['file_name'];
				$this->base_m->insert('media', $insert);  	
            }

	}

	public function change_alt() {
		$update['alt'] = $_POST['alt_text'];
		$this->base_m->update('media', $update, $_POST['id']);  	
        }

}
