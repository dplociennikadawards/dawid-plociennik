<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	public function index()	{
        $data['onas_1'] = $this->base_m->get_record('onas',1);
        $data['onas_2'] = $this->base_m->get_record('onas',2);
        $data['awards'] = $this->base_m->get_sort('wyroznienia');

        $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage('home');
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/index', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function produkty() {
        $data['rows'] = $this->base_m->get_sort('produkty');
        $data['row_1'] = $this->base_m->get_record('produkty',1);

        $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/produkty', $data);
		$this->load->view('front/blocks/footer', $data);
	}

    public function produkt($slug,$id) {
        $data['row'] = $this->base_m->get_record('produkty', $id);
        $data['rows'] = $this->base_m->get_sort('produkty');

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

        $this->load->view('front/blocks/head', $data);
        $this->load->view('front/blocks/subheader', $data);
        $this->load->view('front/produkt', $data);
        $this->load->view('front/blocks/footer', $data);
    }

	public function onas() {
        $data['rows'] = $this->base_m->get_sort('onas');
        $data['row_1'] = $this->base_m->get_record('onas',1);
        $data['row_2'] = $this->base_m->get_record('onas',2);
        $data['awards'] = $this->base_m->get('wyroznienia');

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/onas', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function studio() {
        $data['rows'] = $this->base_m->get_sort('studio');
        $data['row_1'] = $this->base_m->get_record('studio',1);

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/studio', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function wyroznienia() {
        $data['rows'] = $this->base_m->get_sort('wyroznienia');
        $data['row_1'] = $this->base_m->get_record('wyroznienia',1);

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/wyroznienia', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function druk() {
        $data['rows'] = $this->base_m->get_sort('druk');
        $data['row_1'] = $this->base_m->get_record('druk',1);

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/druk', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function kontakt() {
        $data['row_1'] = $this->base_m->get_record('kontakt',1);
        $data['row_2'] = $this->base_m->get_record('kontakt',2);
        $data['row_3'] = $this->base_m->get_record('kontakt',3);
        $data['row_4'] = $this->base_m->get_record('kontakt',4);

                $data['media'] = $this->base_m->get('media');
        $data['home'] = $this->base_m->get_record('home', 1);
        $data['header'] = $this->base_m->get_subpage($this->uri->segment(2));
        $data['settings'] = $this->base_m->get_record('ustawienia',1);
        $data['contact_1'] = $this->base_m->get_record('kontakt',1);
        $data['contact_2'] = $this->base_m->get_record('kontakt',2);
        $data['contact_3'] = $this->base_m->get_record('kontakt',3);
        $data['contact_4'] = $this->base_m->get_record('kontakt',4);
        $data['products'] = $this->base_m->get_sort('produkty');
        $data['newsletter'] = $this->base_m->get_record('newsletter', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/kontakt', $data);
		$this->load->view('front/blocks/footer', $data);
	}
}
