<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-5 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	</div>
	<section>
		<div class="container">
			<div class="card card-cascade narrower">
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			    <div class="md-form my-0">
					<a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>
			    </div>
			    <div class="text-right" style="width: 181px;">
				    <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#infoModal">
				    	<i class="far fa-file-alt mt-0"></i>
				    </button>
			    </div>
			  </div>
			  <div class="px-4">
			    <div class="table-wrapper">
			    	<span id="change_alt_info"></span>
				    <span id="refreshTable">
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">
				        <thead>
				          <tr>
				            <th class="th-lg cursor" style="width: 30%;">
				              Imię i nazwisko
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 30%;">
				              Adres e-mail
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 30%;">
				              Temat
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <tbody>
				        	<?php foreach ($rows as $key): ?>
				        	<?php if($key->kontakt_id > 4): ?>
							<tr>
								<td class="align-middle"><?php echo $key->title; ?></td>
								<td class="align-middle"><?php echo $key->liame; ?></td>
								<td class="align-middle"><?php echo $key->subtitle; ?></td>
								<td class="align-middle"><?php echo date('Y-m-d H:i', strtotime($key->create_date)); ?></td>
								<td class="align-middle text-center">
								    <a data-toggle="modal" title="Edytuj" data-target="#editModal<?php echo $key->kontakt_id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-eye mt-0"></i>
									    </button>
								    </a>

								    <a data-toggle="modal" title="Usuń" data-target="#centralModalSm" onclick="delete_data('<?php echo $this->uri->segment(2); ?>',<?php echo $key->kontakt_id; ?>);">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									    </button>
								    </a>
								</td>
							</tr>
							<?php endif; ?>
							<?php endforeach; ?>
				        </tbody>
				      </table>
				      <script type="text/javascript">filtrTable2();</script>
			      </span>
			    </div>
			  </div>
			</div>
		</div>
	</section>
</main>



<span id="refreshTableEdit">
<?php foreach ($rows as $key): ?>
<!-- Full Height Modal Right -->
<div class="modal fade right" id="editModal<?php echo $key->kontakt_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-lg modal-left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Wiadomość od "<?php echo $key->title; ?>"</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="changeEdit<?php echo $key->kontakt_id; ?>"></p>
        <div class="row">
        	<div class="col-md-4">
				<div class="md-form">
					<input type="text" id="titleEdit<?php echo $key->kontakt_id; ?>" class="form-control" name="title" value="<?php echo $key->title; ?>" required readonly>
					<label for="title">Imię i nazwisko</label>
				</div>
				<div class="md-form">
					<input type="text" id="altEdit<?php echo $key->kontakt_id; ?>" class="form-control" name="alt" value="<?php echo $key->liame; ?>" required readonly>
					<label for="alt">Adres E-mail</label>
				</div>
				<div class="md-form">
					<input type="text" id="subject<?php echo $key->kontakt_id; ?>" class="form-control" name="subject" value="<?php echo $key->subtitle; ?>" required readonly>
					<label for="subject">Temat</label>
				</div>
			</div>
			<div class="col-md-8">
				<div class="md-form mt-md-0">
					<textarea id="contentEdit<?php echo $key->kontakt_id; ?>" rows="8" class="form-control" name="content" required readonly><?php echo $key->content; ?></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

//single header
window.addEventListener('load', function() {
  document.getElementById('photoEdit<?php echo $key->kontakt_id; ?>').addEventListener('change', function() {
    document.getElementById('photo_showEdit<?php echo $key->kontakt_id; ?>').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_showEdit<?php echo $key->kontakt_id; ?>').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});



function imageIsLoaded(e) { }

</script>
<!-- Full Height Modal Right -->
<?php endforeach; ?>
</span>




<!-- Full Height Modal Right -->
<div class="modal fade right" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-lg modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Edytuj informacje o stronie</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="changeInfo">Zmiany zapisują się automatycznie</p>
        <div class="row">
        	<div class="col-md-3">
        		POCZĄTEK STRONY
				<div class="md-form">
					<input type="text" id="1title" class="form-control" name="title" value="<?php echo $row_1->title; ?>" onchange="update_change(1,'title')" required>
					<label for="title">Początek strony</label>
				</div>
				<div class="md-form mt-md-0">
					<textarea id="1content" class="form-control" rows="6" name="content" onchange="update_change(1,'content')"><?php echo $row_1->content; ?></textarea>
				</div>
			</div>
        	<div class="col-md-3">
        		KONTAKT TELEFONICZNY
				<div class="md-form">
					<input type="text" id="2liame" class="form-control" name="liame" value="<?php echo $row_2->liame; ?>" onchange="update_change(2,'liame')" required>
					<label for="2liame">Godziny otwarcia</label>
				</div>
				<div class="md-form">
					<input type="text" id="2title" class="form-control" name="title" value="<?php echo $row_2->title; ?>" onchange="update_change(2,'title')" required>
					<label for="2title">Telefon</label>
				</div>
				<div class="md-form">
					<input type="text" id="2subtitle" class="form-control" name="subtitle" value="<?php echo $row_2->subtitle; ?>" onchange="update_change(2,'subtitle')" required>
					<label for="2subtitle">Telefon 2</label>
				</div>
			</div>
        	<div class="col-md-3">
        		KONTAKT E-MAIL
				<div class="md-form">
					<input type="text" id="3liame" class="form-control" name="liame" value="<?php echo $row_3->liame; ?>" onchange="update_change(3,'liame')" required>
					<label for="3liame">Godziny otwarcia</label>
				</div>
				<div class="md-form">
					<input type="text" id="3title" class="form-control" name="title" value="<?php echo $row_3->title; ?>" onchange="update_change(3,'title')" required>
					<label for="3title">E-mail</label>
				</div>
				<div class="md-form">
					<input type="text" id="3subtitle" class="form-control" name="subtitle" value="<?php echo $row_3->subtitle; ?>" onchange="update_change(3,'subtitle')" required>
					<label for="3subtitle">E-mail 2</label>
				</div>
			</div>
        	<div class="col-md-3">
        		BIURO
				<div class="md-form">
					<input type="text" id="4liame" class="form-control" name="liame" value="<?php echo $row_2->liame; ?>" onchange="update_change(4,'liame')" required>
					<label for="4liame">Godziny otwarcia</label>
				</div>
				<div class="md-form mt-md-0">
					<textarea id="4content" class="form-control" rows="6" name="content" onchange="update_change(4,'content')"><?php echo $row_4->content; ?></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>

<!-- Full Height Modal Right -->



    <script type="text/javascript">
      function add_record()
      {
      	var title = document.getElementById('title').value;
      	var alt = document.getElementById('alt').value;
      	var content = document.getElementById('content').value;
      	var photo = document.getElementById('photo');
      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('title',title);
		data.append('alt',alt);
		data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/add", 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);
             	document.getElementById('title').value = '';
             	document.getElementById('alt').value = '';
             	document.getElementById('content').value = '';
             	document.getElementById('name_photo').value = '';
             	document.getElementById('photo').value = '';
             	document.getElementById('photo_show').innerHTML = '';
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                $('#refreshGallery').load(document.URL +  ' #refreshGallery');
                
                setTimeout(function(){ filtrTable2(); }, 1500);
             }  
        });  
      }
    </script>

    <script type="text/javascript">
      function edit_record(id)
      {
      	var title = document.getElementById('titleEdit'+id).value;
      	var alt = document.getElementById('altEdit'+id).value;
      	var content = document.getElementById('contentEdit'+id).value;
      	var photo = document.getElementById('photoEdit'+id);
      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('title',title);
		data.append('alt',alt);
		data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/edit/"+id, 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);

                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                $('#refreshGalleryEdit'+id).load(document.URL +  ' #refreshGalleryEdit'+id);

                
                setTimeout(function(){ filtrTable2(); }, 1500);
             }  
        });  
      }
    </script>


    <script type="text/javascript">
      function update_change(id,field)
      {
        var field = field;
        var value = document.getElementById(id+field).value;

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/update_change/"+id+"/"+field, 
             data: {id:id, value:value}, 
             cache: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('changeInfo').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie zmian.</span>';
        	 },
             complete:function(html)
             {
                document.getElementById('changeInfo').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
             }  
        });  
      }
    </script>