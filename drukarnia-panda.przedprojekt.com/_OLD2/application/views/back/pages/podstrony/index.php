<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-5 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	</div>
	<section>
		<div class="container">
			<div class="card card-cascade narrower">
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			    <div class="md-form my-0">
					<a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>
			    </div>
			    <div class="text-right" style="width: 181px;">

			    </div>
			  </div>
			  <div class="px-4">
			    <div class="table-wrapper">
			    	<span id="change_alt_info"></span>
				    <span id="refreshTable">
				      <table id="filtrableTable" class="table table-hover table-responsive mb-0">
				        <thead>
				          <tr>
				            <th class="th-lg cursor" style="width: 20%;">
				              Strona
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 100%;">
				              Tytuł
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				            </th>
				            <th class="th-lg cursor" style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <tbody>
				        	<?php foreach ($rows as $key): ?>
							<tr>
								<td class="align-middle"><?php echo $key->page; ?></td>
								<td class="align-middle"><?php echo $key->title; ?></td>
								<td class="align-middle"><?php echo date('Y-m-d H:i', strtotime($key->create_date)); ?></td>
								<td class="align-middle text-center">
								    <a data-toggle="modal" title="Edytuj" data-target="#editModal<?php echo $key->podstrony_id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-edit mt-0"></i>
									    </button>
								    </a>
								</td>
							</tr>
							<?php endforeach; ?>
				        </tbody>
				      </table>
				      <script type="text/javascript">filtrTable2();</script>
			      </span>
			    </div>
			  </div>
			</div>
		</div>
	</section>
</main>



<span id="refreshTableEdit">
<?php foreach ($rows as $key): ?>
<!-- Full Height Modal Right -->
<div class="modal fade right" id="editModal<?php echo $key->podstrony_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-lg modal-left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Edytuj wpis "<?php echo $key->title; ?>"</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="changeEdit<?php echo $key->podstrony_id; ?>"></p>
        <div class="row">
        	<div class="col-md-4">
				<div class="md-form">
					<input type="text" id="titleEdit<?php echo $key->podstrony_id; ?>" class="form-control" name="title" value="<?php echo $key->title; ?>" required>
					<label for="titleEdit<?php echo $key->podstrony_id; ?>">Tytuł</label>
				</div>
				<div class="md-form">
					<input type="text" id="subtitleEdit<?php echo $key->podstrony_id; ?>" class="form-control" name="subtitle" value="<?php echo $key->subtitle; ?>" required>
					<label for="subtitleEdit<?php echo $key->podstrony_id; ?>">Podtytuł</label>
				</div>
				<div class="md-form">
					<input type="text" id="altEdit<?php echo $key->podstrony_id; ?>" class="form-control" name="alt" value="<?php echo $key->alt; ?>" required>
					<label for="altEdit<?php echo $key->podstrony_id; ?>">Tekst alternatywny zdjęcia</label>
				</div>
				<div class="md-form">
					<div class="file-field">
						<div id="photo_showEdit<?php echo $key->podstrony_id; ?>" class="text-center">
							<img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/podstrony/<?php echo $key->photo; ?>" width="250">
						</div>
							<div class="btn btn-primary btn-sm float-left gold-gradient">
								<span>Wybierz zdjęcie</span>
								<input type="file" name="photo" id="photoEdit<?php echo $key->podstrony_id; ?>">
							</div>
						<div class="file-path-wrapper">
							<input id="name_photoEdit<?php echo $key->podstrony_id; ?>" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" required readonly>
						</div>
					</div>
				</div>
				<div class="md-form">
					<button id="button_ajax" type="button" class="btn btn-primary gold-gradient" onclick="edit_record(<?php echo $key->podstrony_id; ?>);">Zapisz</button>
				</div>	
			</div>
			<div class="col-md-8">
				<div class="md-form mt-md-0">
					<textarea id="contentEdit<?php echo $key->podstrony_id; ?>" class="form-control specialArea" name="content" required><?php echo $key->content; ?></textarea>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

//single header
window.addEventListener('load', function() {
  document.getElementById('photoEdit<?php echo $key->podstrony_id; ?>').addEventListener('change', function() {
    document.getElementById('photo_showEdit<?php echo $key->podstrony_id; ?>').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_showEdit<?php echo $key->podstrony_id; ?>').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});



function imageIsLoaded(e) { }

</script>
<!-- Full Height Modal Right -->
<?php endforeach; ?>
</span>


    <script type="text/javascript">
      function edit_record(id)
      {
      	var title = document.getElementById('titleEdit'+id).value;
      	var subtitle = document.getElementById('subtitleEdit'+id).value;
      	var alt = document.getElementById('altEdit'+id).value;
      	var content = document.getElementById('contentEdit'+id).value;
      	var photo = document.getElementById('photoEdit'+id);
      	var data=new FormData();
		data.append(photo.name,photo.files[0]);
		data.append('title',title);
		data.append('subtitle',subtitle);
		data.append('alt',alt);
		data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/podstrony/edit/"+id, 
             data: data, 
             cache: false,
        	 contentType: false,
        	 processData: false,
        	 beforeSend:function(data) 
        	 {
                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie plików.</span>';
        	 },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
             	console.log(html);

                document.getElementById('changeEdit'+id).innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
                $('#refreshTable').load(document.URL +  ' #refreshTable');
                $('#refreshGalleryEdit'+id).load(document.URL +  ' #refreshGalleryEdit'+id);

                
                setTimeout(function(){ filtrTable2(); }, 1500);
             }  
        });  
      }
    </script>


<script type="text/javascript">
	function edit_gallery(photo,alt,id,record_id) {
		if(document.getElementById('selected_photo'+record_id+id).innerHTML == '') {
			document.getElementById('galleryEdit'+record_id).value += photo + '|' + alt + ',';
			document.getElementById('selected_photo'+record_id+id).innerHTML = '<i onclick="edit_gallery(\''+photo+'\',\''+alt+'\',\''+id+'\',\''+record_id+'\')" class="fas fa-check"></i>';	
		} else {
			var ret = document.getElementById('galleryEdit'+record_id).value;
			var photo = photo + '|' + alt + ',';
			var ret = ret.replace(photo,'');
			document.getElementById('galleryEdit'+record_id).value = ret;
			document.getElementById('selected_photo'+record_id+id).innerHTML = '';
		}

	}
</script>