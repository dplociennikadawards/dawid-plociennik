<main>
    <section class="default products">
      <div class="container">
        <div class="default__title-box">
          <h3 class="default__title"><?php echo $row->title; ?></h3>
          <p class="default__text"><?php echo $row->subtitle; ?></p>
        </div>
      </div>
    <hr class="my-5">
      <div class="container">
        <div class="products__row">
            <div class="col-md-8">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php $pieces = explode(",", $row->gallery); $i=0; foreach($pieces as $piece): $i++; ?>
                        <div class="carousel-item <?php if($i==1){echo 'active';} ?>">
                            <img class="d-block w-100" src="<?php echo base_url(); ?>uploads/media/<?php echo substr($piece, 0, strpos($piece, "|")); ?>"
                                 alt="<?php echo substr($piece, strpos($piece, "|") + 1); ?>">
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <a class="" href="#carouselExampleControls" role="button" data-slide="prev">
                    <i class="far fa-arrow-alt-circle-left" style="font-size:2.5rem; color:#FF5F00;"></i>
                </a>
                <a class="" href="#carouselExampleControls" role="button" data-slide="next">
                     <i class="far fa-arrow-alt-circle-right" style="font-size:2.5rem; color:#FF5F00;"></i>
                </a>
                <h3 class="mt-3 font-weight-bold"><?php echo $row->title; ?></h3>
                <p class="mt-3"><?php echo $row->subtitle; ?></p>
            </div>
            <div class="col-md-12">
                <p class="mt-3"><?php echo $row->content; ?></p>
            </div>
        </div>
      </div>
    </section>


    <section class="default products">
        <div class="container">
            <div class="default__title-box">
                <h3 class="default__title">Sprawdź inne nasze oferty</h3>
            </div>
        </div>

        <div class="container">
            <div class="products__row">
                <?php $i=0; foreach ($rows as $key): if($key->produkty_id > 1 && $i < 5): $i++?>
                    <a href="<?php echo base_url(); ?>p/produkt/<?php echo $this->slugify_m->slugify($key->title) . '/' . $key->produkty_id; ?>" class="products__cell">
                        <img class="products__img" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $key->photo; ?>">
                        <img class="products__img products__img--hover" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $key->photo; ?>">
                        <h5 class="products__title"><?php echo $key->title; ?></h5>
                    </a>
                <?php endif; endforeach; ?>
            </div>

        </div>
    </section>