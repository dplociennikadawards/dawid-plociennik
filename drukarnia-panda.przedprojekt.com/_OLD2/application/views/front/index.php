  <main>
    <section class="default default--background">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 my-order-mobile-2">
            <h3 class="default__title default__title--uppercase">trochę o nas...</h3>
            <p class="default__text">Nasza firma powstała w <b>1998</b> roku. Wieloletnie doświadczenie na rynku pozwala
              nam zaoferować: profesjonalną obsługę, wysoką jakość usług, oraz bezkonkurencyjne ceny. Oferujemy obsługę
              na każdym etapie - zaczynając od projektu graficznego, poprzez profesjonalne przygotowanie do druku,
              wysokiej jakości druk offsetowy - aż po oprawę i inne zabiegi introligatorskie.</p>
          </div>
          <div class="col-lg-6 my-order-mobile-1">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/img/notes.png" alt="zeszyty">
          </div>
        </div>
      </div>
    </section>

    <section class="default">
      <div class="container">
        <div class="default__title-box">
          <h3 class="default__title">Nasi klienci oraz produkty</h3>
          <p class="default__text">Posiadamy pełne zaplecze techniczne, umożliwiające wykonanie każdego, nawet
            najbardziej wymagającego zlecenia.
            Dział przygotowania do druku pomoże w zaprojektowaniu pracy. Dzięki technice CTP bez zbędnej zwłoki zostaną
            wykonane matryce CTP.
            Druk jest realizowany na maszynach pełnokolorowych z systemem sw umożliwiającym druk 2 stronny.
            Dział introligatorni zajmuje się wykończeniem technicznym prac oraz ich uszlachetnieniem.
            Posiadamy możliwość foliowania, sztancowania oraz lakierowania UV, można u nas wykonać oprawe miekką, szytą
            oraz sztywną.
            Do naszych klientów należą wielkie konsorcja z branży meblowej, AGD RTV, ubezpieczeniowej, nieruchomości
            oraz małe rodzinne firmy.</p>
        </div>
      </div>
    </section>

    <section class="default default--background">
      <div class="default__title-box">
        <h3 class="default__title">Najczęściej wykonywanymi pracami są</h3>
      </div>

      <div class="container">

        <div class="tabs__box">
          <ul class="tabs__list">
            <li class="tabs__link tabs__link--first tabs__link-active" data-tab="tab-1">Katalogi</li>
            <li class="tabs__link" data-tab="tab-2">Czasopisma</li>
            <li class="tabs__link" data-tab="tab-3">Gazetki reklamowe</li>
            <li class="tabs__link" data-tab="tab-4">Albumy</li>
            <li class="tabs__link" data-tab="tab-5">Teczki</li>
            <li class="tabs__link" data-tab="tab-6">Ulotki</li>
            <li class="tabs__link" data-tab="tab-7">Inne</li>
          </ul>

          <div id="tab-1" class="tabs__content tabs__content-active">

            <div class="tabs__carousel">
              <div class="owl-carousel owl-theme">
                <div class="owl-carousel__item">
                  <p class="default__text default__text--center">
                    Specjalizujemy się w opracowaniu i druku gazetek reklamowych,
                    Zapewniamy krótkie terminy, kopmleksową realizację na najwyższym poziomie jakościowym.
                  </p>
                  <div>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/img/slider1.png" alt="ulotka">
                  </div>
                  <a class="owl-carousel__button" href="#">sprawdź</a>
                </div>
                <div class="owl-carousel__item">
                  <p class="default__text default__text--center">
                    Specjalizujemy się w opracowaniu i druku gazetek reklamowych,
                    Zapewniamy krótkie terminy, kopmleksową realizację na najwyższym poziomie jakościowym.
                  </p>
                  <div>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/img/slider1.png" alt="ulotka">
                  </div>
                  <a class="owl-carousel__button" href="#">sprawdź</a>
                </div>
                <div class="owl-carousel__item">
                  <p class="default__text default__text--center">
                    Specjalizujemy się w opracowaniu i druku gazetek reklamowych,
                    Zapewniamy krótkie terminy, kopmleksową realizację na najwyższym poziomie jakościowym.
                  </p>
                  <div>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/img/slider1.png" alt="ulotka">
                  </div>
                  <a class="owl-carousel__button" href="#">sprawdź</a>
                </div>
                <div class="owl-carousel__item">
                  <p class="default__text default__text--center">
                    Specjalizujemy się w opracowaniu i druku gazetek reklamowych,
                    Zapewniamy krótkie terminy, kopmleksową realizację na najwyższym poziomie jakościowym.
                  </p>
                  <div>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/img/slider1.png" alt="ulotka">
                  </div>
                  <a class="owl-carousel__button" href="#">sprawdź</a>
                </div>
              </div>
            </div>

          </div>
          <div id="tab-2" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum </div>
          </div>
          <div id="tab-3" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum 1</div>
          </div>
          <div id="tab-4" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum 2</div>
          </div>
          <div id="tab-5" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum 3</div>
          </div>
          <div id="tab-6" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum 4</div>
          </div>
          <div id="tab-7" class="tabs__content">
            <div
              style="background: gray; display: flex; justify-content: center; align-items: center; height: 340px; width: 100%;">
              Lorem Ipsum 5</div>
          </div>
        </div>

      </div>
    </section>

    <section class="default">
      <div class="default__title-box">
        <h3 class="default__title">Nasze wyróżnienia</h3>
      </div>

      <div class="container">
        <div class="awards">
          <a href="#" class="awards__frame awards__frame--award1"></a>
          <a href="#" class="awards__frame awards__frame--award2"></a>
          <a href="#" class="awards__frame awards__frame--award3"></a>
          <a href="#" class="awards__frame awards__frame--award4"></a>
          <a href="#" class="awards__frame awards__frame--award5"></a>
          <a href="#" class="awards__frame awards__frame--award6"></a>
        </div>
      </div>
    </section>

