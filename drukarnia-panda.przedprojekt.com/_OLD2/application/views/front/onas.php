<main>

    <section class="default about">
      <div class="default__title-box">
        <h3 class="default__title"><?php echo $row_1->title; ?></h3>
      </div>

      <div class="container">
        <p class="default__text default__text--center">
            <?php echo $row_1->content; ?>
        </p>

        <div class="about__row mt-5">
            <?php $i=0; foreach(array_reverse($rows) as $row): if($row->onas_id > 2): $i++; ?>
          <div class="about__cell">
            <img class="about__number" src="<?php echo base_url(); ?>uploads/onas/<?php echo $row->photo; ?>">

            <h5 class="about__desc"><?php echo $row->title; ?></h5>
          </div>
            <?php if($i>4){break;} endif; endforeach; ?>
        </div>
          <div class="about__row mt-5">
              <?php $i=0; $j=0; foreach(array_reverse($rows) as $row): $j++; if($row->onas_id > 2 && $j > 7): $i++; ?>
                  <div class="about__cell">
                      <img class="about__number" src="<?php echo base_url(); ?>uploads/onas/<?php echo $row->photo; ?>">

                      <h5 class="about__desc"><?php echo $row->title; ?></h5>
                      <p class="default__text"><?php echo $row->content; ?></p>
                  </div>
                  <?php endif; endforeach; ?>
          </div>
      </div>

    </section>

    <section class="default">
      <div class="container">
        <div class="default__title-box">
          <h3 class="default__title"><?php echo $row_2->title; ?></h3>
          <p class="default__text"><?php echo $row_2->content; ?></p>
        </div>
      </div>
    </section>

    <section class="default default__text--center">
      <div class="default__title-box">
        <h3 class="default__title">Nasze wyróżnienia</h3>
      </div>

      <div class="container">
        <div class="awards">
            <?php foreach ($awards as $item): if($item->wyroznienia_id > 1): ?>
          <a href="<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $item->photo; ?>" class="awards__frame" data-lightbox="image-1" data-title="<?php echo $item->alt; ?>" style="background-image: url('<?php echo base_url(); ?>uploads/wyroznienia/<?php echo $item->photo; ?>');"></a>
            <?php endif; endforeach; ?>
        </div>
      </div>

      <a href="<?php echo base_url(); ?>p/wyroznienia" class="awards__link">zobacz więcej</a>
    </section>


