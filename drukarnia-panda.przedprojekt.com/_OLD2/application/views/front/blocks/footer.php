<section class="default support">
    <div class="default__title-box">
        <h3 class="default__title">Wspieramy</h3>
    </div>

    <div class="container">
        <p class="support__text">Muzeum miejskie <span class="support__bold-text">Wrocławia</span> (sponsor roku 2005,
            2006)
            <br>
            <br>
            Klub <span class="support__bold-text">ludzi życzliwych</span> Jerzego Janoszki
            <br>
            <br>
            Pilkarską akademię olimpik wrocław <span class="support__bold-text">Krzysztofa Wołczka i Dariusza
            Sztylka</span>
            <br>
            <br>
            Jesteśmy złotym sponsorem ekstraklasowego zespołu <span class="support__bold-text">Miedzi Legnica</span>
            <img class="support__icon" src="<?php echo base_url(); ?>assets/front/img/miedz.png" alt="logo miedzi legnica">
        </p>
    </div>
</section>

<section class="mailing-list">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h3 class="default__title">Lista mailingowa</h3>
                <p class="mailing-list__text">Aby otrzymywać informacje o nowościach i promocjach, dodaj swój adres e-mail
                    do naszej bazy danych. <span class="default__bold-text">Zapraszamy!</span></p>
            </div>
            <div class="col-lg-6 d-flex align-items-center">
                <form class="mailing-list__form" action="" method="get">
                    <input class="mailing-list__input" type="email" name="" id="" placeholder="Wpisz adres e-mail">
                    <input class="mailing-list__submit" type="submit" value="zapisz się">
                </form>
            </div>
        </div>
    </div>
</section>
</main>

<footer class="main-footer">
    <div class="footer-shape container">
      <div class="footer-shape__background"></div>
      <div class="footer-shape__title"><div>Dostawa na terenie całej Polski <span class="default__bold-text">całkowicie za darmo!</span></div></div>
      <div class="footer-shape__small-part"></div>
      <img class="footer-shape__scissors" src="<?php echo base_url(); ?>assets/front/img/scissors.png" alt="ikonka nożyczek">
    </div>

    <div class="main-footer__content-box">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 main-footer__column mobile-order-1">
            <h3 class="default__title">Napisz do nas</h3>

            <form class="main-footer__form" action="" method="post">
              <input class="main-footer__input" type="text" name="" id="" placeholder="Imię i nazwisko">
              <input class="main-footer__input" type="email" name="" id="" placeholder="E-mail">
              <select class="main-footer__input"  name="" id="">
                  <?php foreach ($products as $item): ?>
                <option value="<?php echo $item->title; ?>"><?php echo $item->title; ?></option>
                  <?php endforeach; ?>
              </select>
              <textarea class="main-footer__textarea" name="" id="" cols="30" rows="4" placeholder="Wiadomość"></textarea>

              <input class="main-footer__submit" type="submit" value="wyślij">
            </form>
          </div>
          <div class="col-lg-4 main-footer__column position-relative mobile-order-3">
            <h3 class="default__title">Kontakt</h3>

            <p class="default__text default__text--address">
                <?php echo $contact_4->content; ?>
                <a class="main-footer__contact-link" href="tel:<?php echo $contact_2->title; ?>">tel./fax <?php echo $contact_2->title; ?></a><br>
                <a class="main-footer__contact-link" href="tel:<?php echo $contact_2->subtitle; ?>">tel. kom. <?php echo $contact_2->subtitle; ?></a><br>
            </p>

            <a href="<?php echo base_url(); ?>p/kontakt" class="main-footer__button">zapytanie o wycenę on-line</a>

            <p class="main-footer__copyrights">Wszystkie prawa zastrzeżone.<br>Copyright by <span class="default__bold-text">Drukarnia Panda</span></p>
          </div>
          <div class="col-lg-4 main-footer__column mobile-order-2">
            <h3 class="default__title">Dla klientów</h3>

            <div class="main-footer__link-box">
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/produkty">Produkty</a><br>
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/studio">Studio graficzne</a><br>
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/wyroznienia">Wyróżnienia</a><br>
              <a class="main-footer__link" href="<?php echo base_url(); ?>p/kontakt">Napisz wiadomość</a><br>
            </div>

            <div>
              <a href="https://www.facebook.com/PPHUPanda/"><img class="main-footer__icon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="ikona facebooka"></a>
<!--              <a href="#"><img class="main-footer__icon" src="--><?php //echo base_url(); ?><!--assets/front/img/instagram.png" alt="ikona instagrama"></a>-->

              <button class="main-footer__up-button">na górę</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- /Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
  <!-- owl carousel -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/owl.carousel.min.js"></script>
  <!-- my scripts -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/lightbox.js"></script>
</body>

</html>