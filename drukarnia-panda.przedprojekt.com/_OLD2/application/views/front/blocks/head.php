<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php if($this->uri->segment(2) == null) {
        $title = $settings->title;
    } elseif($this->uri->segment(2) == 'produkt') {
        $title = $row->title . ' - ' . $settings->title;
    } else {
        $title = ucfirst($this->uri->segment(2)) . ' - ' . $settings->title;
    } ?>
    <meta name="keywords" content="<?php echo $title . ',' . $settings->subtitle; ?>">
    <meta name="description" content="<?php echo $settings->content; ?>">
  <title><?php echo $title; ?></title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- OWL carousel -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/owl.theme.default.min.css">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/subpages.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/mediaqueries.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/front/css/lightbox.css" rel="stylesheet">
</head>

<body>