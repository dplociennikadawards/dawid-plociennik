<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_m extends CI_Model  
{
  
    public function get($table) {
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_sort($table) {
        $this->db->order_by('create_date', 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_record($table,$id) {
        $this->db->where([$table.'_id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_gallery($table,$item_id) {
        $this->db->where(['table_name' => $table]);
        $this->db->where(['item_id' => $item_id]);
        $query = $this->db->get('gallery');
        
        return $query->result();        
    }

    public function get_subpage($page) {
        $this->db->where(['page' => $page]);
        $query = $this->db->get('podstrony');
        return $query->row();
    }

    public function insert($table,$data) {
        $insert_query = $this->db->insert($table, $data);
        return $insert_query;
    }


    public function update($table,$data,$id) {
        $this->db->where([$table.'_id' => $id]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }

    public function drop_record($id,$table) {
        $this->db->where([$table.'_id' => $id]);
        $this->db->delete($table);
    }

    public function priority_ASC($table) {
        $this->db->order_by('priority', 'ASC');
        $query = $this->db->get($table);
        
        return $query->result();        
    }

    public function priority_DESC($table) {
        $this->db->order_by('priority', 'DESC');
        $query = $this->db->get($table);
        
        return $query->result();        
    }

    public function log($typ) {
        $data['user_id'] = $_SESSION['id'];
        $data['typ'] = $typ;
        $insert_query = $this->db->insert('logs', $data);
        return $insert_query;

    }

}