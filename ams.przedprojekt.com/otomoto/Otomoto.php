<?php

/**
 * Created by PhpStorm.
 * User: masterix
 * Date: 08.05.2017
 * Time: 12:45
 */
class Otomoto
{
    private $otomoto_config;

    private $credentials;

    private $access_token;

    public function __construct($config, $credentials)
    {
        $this->otomoto_config = $config;
        $this->credentials = $credentials;
    }

    public function login()
    {
        $curl = curl_init($this->otomoto_config['url'] . "/oauth/token/");
        curl_setopt($curl, CURLOPT_HEADER, "Content-Type: application/x-www-form-urlencoded");
        curl_setopt($curl, CURLOPT_HEADER, "Accept: application/json");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic " . base64_encode($this->otomoto_config['client_id'] . ":" . $this->otomoto_config['secret_key'])));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, rawurldecode(http_build_query(array("username" => $this->credentials['login'], "password" => $this->credentials['password'], "grant_type" => "password"))));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $JSON = curl_exec($curl);
        curl_close($curl);

        $JSON_Array = json_decode($JSON, true);
        $this->access_token = $JSON_Array['access_token'];
    }

    public function getUserAdverts()
    {
        $curl = curl_init($this->otomoto_config['url'] . "/account/adverts/");
        curl_setopt($curl, CURLOPT_HEADER, "Content-Type: application/json");
        curl_setopt($curl, CURLOPT_HEADER, "Accept: application/json");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $this->access_token));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $JSON = curl_exec($curl);
        $JSON_Array = json_decode($JSON, true);

        if (isset($JSON_Array['results'])) {
            return $JSON_Array['results'];
        } else {
            return array();
        }

    }

}