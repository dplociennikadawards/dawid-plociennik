<?php

function build_mail_body($post){
    $content = file_get_contents('./templates/mail.html');
    foreach($post as $k => $v){
        $content = str_replace('{'.$k.'}', $v, $content);
    }

    return $content;
}

/**
 * @param $pdo
 * @param $adverts
 * @param $dealer_name
 */
function update_otomoto_adverts($pdo, $adverts, $dealer_name, $dealer_id)
{
    $delete_array = array();
    $stmt_insert = $pdo->prepare('INSERT INTO `cars` (`otomoto_id`, `name`, `slug`, `meta_title`, `mileage`, `state`, `price`, `production_year`, `engine`, `body`, `intro`, `content`, `path`, `published`) 
VALUES(:otomoto_id, :name, :slug, :meta_title, :mileage, :state, :price, :production_year, :engine, :body, :intro, :content, :path, :published)');

    if($stmt_insert){echo 'good';} else {echo 'bad';}

    $stmt_insert_photos = $pdo->prepare('INSERT INTO `photos` (`car_id`, `path`) VALUES(:car_id, :path)');

    $stmt_update = $pdo->prepare('UPDATE `cars` SET `published` = :published, `name` = :name, `slug` = :slug, `mileage` = :mileage, `state` = :state, `price` = :price, `production_year` = :production_year, `engine` = :engine, `body` = :body, `content` = :content, `path` = :path, `meta_title` = :meta_title WHERE otomoto_id = :otomoto_id');

    $stmt_delete = $pdo->prepare('DELETE FROM `cars` WHERE otomoto_id = :otomoto_id');

    //$count = $pdo->query('SELECT COUNT(id) as cnt, id FROM `cars` WHERE `otomoto_id` = 1');
    $count_active = 0;

    foreach ($adverts as $offer) {

        $count = $pdo->query('SELECT COUNT(id) AS cnt FROM `cars` WHERE `otomoto_id` = ' . $offer['id']);
        $rows_count = $count->fetch();



        // main image
        $mainImg = '';
        $photos = array();
        if (count($offer["photos"]) > 0) {
            foreach ($offer["photos"] as $photo) {
                if ($mainImg != "") {
                    $photos[] = $photo['732x488'];
                } else {
                    $mainImg = $photo['732x488'];
                }
            }
        }
        //$car_id = -1;

        if($offer['status'] == 'active'){
            $count_active++;
        }else{
            $delete_array[] = $offer['id'];
        }

        if ($rows_count['cnt'] == 0) {
            if($offer['status'] == 'active'){
                // insert new advert
                $stmt_insert->bindValue(':otomoto_id', $offer['id']);
                $stmt_insert->bindValue(':name', $offer['title']);
                $stmt_insert->bindValue(':slug', Slugger::slugify($offer['title']));
                $stmt_insert->bindValue(':meta_title', $offer['title']);
                $stmt_insert->bindValue(':mileage', $offer['params']['mileage']);
                $stmt_insert->bindValue(':state', ($offer['new_used'] == 'new') ? 'nowe' : 'uzywane');
                $stmt_insert->bindValue(':price', $offer['params']['price'][1]);
                $stmt_insert->bindValue(':production_year', $offer['params']['year']);
                $stmt_insert->bindValue(':engine', $offer['params']['fuel_type']);
                $stmt_insert->bindValue(':body', $offer['params']['body_type']);
                $stmt_insert->bindValue(':published', 1);
                $stmt_insert->bindValue(':intro', '');
                $stmt_insert->bindValue(':content', $offer['description'].'<br><br>');
                $stmt_insert->bindValue(':path', $mainImg);
                $stmt_insert->execute();
                $car_id = $pdo->lastInsertId();
            }


        } else {

            $car_id = $rows_count['id'];
            // delete inactive ads
            if ($offer['status'] != 'active') {

                $stmt_delete->bindValue(':otomoto_id', $offer['id']);
                $stmt_delete->execute();
            } else {
                // update current advert
                $stmt_update->bindValue(':otomoto_id', $offer['id']);
                $stmt_update->bindValue(':name', $offer['title']);
                $stmt_update->bindValue(':slug', Slugger::slugify($offer['title']));
                $stmt_update->bindValue(':meta_title', $offer['title']);
                $stmt_update->bindValue(':mileage', $offer['params']['mileage']);
                $stmt_update->bindValue(':state', ($offer['new_used'] == 'new') ? 'nowe' : 'uzywane');
                $stmt_update->bindValue(':price', $offer['params']['price'][1]);
                $stmt_update->bindValue(':production_year', $offer['params']['year']);
                $stmt_update->bindValue(':engine', $offer['params']['fuel_type']);
                $stmt_update->bindValue(':body', $offer['params']['body_type']);
                $stmt_update->bindValue(':published', 1);
                $stmt_update->bindValue(':intro', '');
                $stmt_update->bindValue(':content', $offer['description'].'<br><br>');
                $stmt_update->bindValue(':path', $mainImg);
                $stmt_update->execute();


                // delete all photos
                $pdo->exec('DELETE FROM `photos` WHERE `car_id` = ' . $car_id);

            }
        }
        // add photos
        foreach ($photos as $photo) {
            $stmt_insert_photos->bindValue(':car_id', $car_id);
            $stmt_insert_photos->bindValue(':path', $photo);
            $stmt_insert_photos->bindValue(':path', $photo);
            $stmt_insert_photos->execute();
        }
    }

    // echo '<br />aaaa: '.$count_active.'';


        //$items = $pdo->query('SELECT * FROM `cars` WHERE 1');
        //$items = $items->fetchAll();
        if(count($delete_array)){
            foreach ($delete_array as $item) {
                $stmt_delete->bindValue(':otomoto_id', $item);
                $stmt_delete->execute();
            }
        }
}