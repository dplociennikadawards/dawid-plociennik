<?php

require_once 'config.php';
require_once 'functions.php';
require_once 'Otomoto.php';
require_once 'Slugger.php';

$pdo = new PDO('mysql:dbname='.$db['database'].";host=".$db['host'].";charset=utf8", $db['user'], $db['password']);

foreach($otomoto_dealers as $dealer_name => $dealer_credentials){

    $otomoto = new Otomoto($otomoto_config, $dealer_credentials);
    $otomoto->login();
    $adverts = $otomoto->getUserAdverts();

    update_otomoto_adverts($pdo, $adverts, $dealer_name, $dealer_credentials['id']);
}
