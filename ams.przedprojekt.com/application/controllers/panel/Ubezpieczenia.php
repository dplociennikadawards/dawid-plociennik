<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ubezpieczenia extends CI_Controller {

public function index() {
    $data['rows'] = $this->base_m->get('ubezpieczenia');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/ubezpieczenia/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/ubezpieczenia/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/ubezpieczenia/'.$now)) {
            mkdir('./uploads/ubezpieczenia/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/ubezpieczenia/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }
            }


            $insert['title'] = $this->input->post('title');
            $insert['alt'] = $this->input->post('alt');
                $insert['content'] = $this->input->post('content');

            $this->base_m->insert('ubezpieczenia', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
            redirect('panel/ubezpieczenia');
        }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('ubezpieczenia', $id);

    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/ubezpieczenia/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/ubezpieczenia/'.$now)) {
            mkdir('./uploads/ubezpieczenia/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/ubezpieczenia/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if($this->input->post('name_photo') != null) {
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $insert['photo'] = $now.'/'.$data['file_name'];        
            }
        }

            $insert['title'] = $this->input->post('title');
            $insert['alt'] = $this->input->post('alt');
                $insert['content'] = $this->input->post('content');
        $this->base_m->update('ubezpieczenia', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/ubezpieczenia');
    }
}
}
