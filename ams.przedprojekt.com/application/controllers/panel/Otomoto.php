<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Otomoto extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

public function index() {
    $data['rows'] = $this->base_m->get('otomoto');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/otomoto/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/otomoto/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

        $insert['url'] = $this->input->post('url');
        $insert['client_id'] = $this->input->post('client_id');
        $insert['secret_key'] = $this->input->post('secret_key');
        $insert['name_account'] = $this->input->post('name_account');
        $insert['login'] = $this->input->post('login');
        $insert['password'] = $this->input->post('password');

        $this->base_m->insert('otomoto', $insert);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
        redirect('panel/otomoto');

        }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('otomoto', $id);

    $this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/otomoto/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
            

        $insert['url'] = $this->input->post('url');
        $insert['client_id'] = $this->input->post('client_id');
        $insert['secret_key'] = $this->input->post('secret_key');
        $insert['name_account'] = $this->input->post('name_account');
        $insert['login'] = $this->input->post('login');
        $insert['password'] = $this->input->post('password');

        $this->base_m->update('otomoto', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/otomoto');
    }
}
}
