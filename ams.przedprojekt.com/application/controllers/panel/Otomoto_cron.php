<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Otomoto_cron extends CI_Controller {

    public function login() {

        $this->db->query('DELETE FROM samochody');  
        $this->db->query('ALTER TABLE samochody AUTO_INCREMENT=1');  

        $data['row'] = $this->base_m->get('otomoto');

        foreach ($data['row'] as $key) {

            $curl = curl_init($key->url . "/oauth/token/");
            curl_setopt($curl, CURLOPT_HEADER, "Content-Type: application/x-www-form-urlencoded");
            curl_setopt($curl, CURLOPT_HEADER, "Accept: application/json");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic " . base64_encode($key->client_id . ":" . $key->secret_key)));
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, rawurldecode(http_build_query(array("username" => $key->login, "password" => $key->password, "grant_type" => "password"))));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $JSON = curl_exec($curl);
            curl_close($curl);

            $JSON_Array = json_decode($JSON, true);
            $this->access_token = $JSON_Array['access_token'];

            $curl = curl_init($key->url . "/account/adverts/");
            curl_setopt($curl, CURLOPT_HEADER, "Content-Type: application/json");
            curl_setopt($curl, CURLOPT_HEADER, "Accept: application/json");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $this->access_token));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $JSON = curl_exec($curl);
            $JSON_Array = json_decode($JSON, true);

            if (isset($JSON_Array['results'])) {
                $data_json = $JSON_Array['results'];
            }   

            //print_r($data_json[0]);
            $i = 0;
            $insert['gallery'] = '';
            $insert['params'] = '';
            $insert['name_account'] = $key->name_account;

            foreach ($data_json as $array) {
                foreach ($array as $key => $value) {

                    if($key != 'city' && $key != 'district' && $key != 'coordinates' && $key != 'contact' && $key != 'params' && $key != 'photos')
                    {
                        //echo 'Klucz [1]: ' . $key . '<br>Wartość [1]: ' . $value . '<hr>';
                        $insert[$key] = $value;
                    } 

                    elseif($key == 'contact') 
                    {
                        foreach ($value as $key_1_contact => $value_1_contact) 
                        {
                            if($key_1_contact != 'phone_numbers') 
                            {
                                //echo 'Klucz [2]: ' . $key_1_contact . '<br>Wartość [2]: ' . $value_1_contact . '<hr>';
                                $insert[$key_1_contact] = $value_1_contact;
                            } 
                            else 
                            {
                               foreach ($value_1_contact as $value_2_contact) 
                                {
                                    //echo 'Wartość [3]: ' . $value_2_contact . '<hr>';
                                    $insert['phone_numbers'] = $value_2_contact;
                                } 
                            }
                        }
                    }

                    elseif($key == 'params') 
                    {   
                        foreach ($value as $key_1_params => $value_1_params) 
                        {   
                            if($key_1_params != 'features' && $key_1_params != 'price') 
                            {
                                //echo 'Klucz [2]: ' . $key_1_params . '<br>Wartość [2]: ' . $value_1_params . '<hr>';
                                $insert[$key_1_params] = $value_1_params;
                            }
                            elseif($key_1_params == 'price') 
                            {   
                                foreach ($value_1_params as $key_1_price => $value_1_price) 
                                {   
                                    //echo 'Klucz [2]: ' . $key_1_price . '<br>Wartość [2]: ' . $value_1_price . '<hr>';
                                    $insert[$key_1_price] = $value_1_price;
                                }
                            }
                            else
                            {
                                foreach ($value_1_params as $value_2_params) {
                                    $value_2_params .= ',|,';
                                    //echo 'Wartość [3]: ' . $value_2_params . '<hr>';
                                    $insert['params'] .= $value_2_params;
                                }
                            } 
                        }
                    }

                    elseif($key == 'photos') 
                    {   
                        foreach ($value as $key_1_photos => $value_1_photos) 
                        {   
                            if($key_1_photos == '1') 
                            {
                                foreach ($value_1_photos as $key_2_photos => $value_2_photos) {
                                    if ($key_2_photos == '1280x800') {
                                        //echo $insert['model'] . ' Klucz [3]: ' . $key_2_photos . '<br>Wartość [3]: ' . $value_2_photos . '<hr>';
                                        $insert['photo'] = $value_2_photos;
                                        $value_2_photos .= ',|,';
                                        $insert['gallery'] .= $value_2_photos;
                                        break;
                                    }
                                }
                            }
                            else 
                            {
                                foreach ($value_1_photos as $key_2_photos => $value_2_photos) {
                                    if ($key_2_photos == '1280x800') {
                                        $value_2_photos .= ',|,';
                                        //echo $insert['model'] . ' Klucz [3]: ' . $key_2_photos . '<br>Wartość [3]: ' . $value_2_photos . '<hr>';
                                        $insert['gallery'] .= $value_2_photos;
                                    }
                                }
                            }
                             
                        }
                    }
                }
                foreach ($insert as $key => $value) {
                    if($key != 'valid_to' && $key != 'category_id' && $key != 'region_id' && $key != 'city_id' && $key != 'district_id' && $key != 'rhd' && $key != '0' && $key != 'currency' && $key != 'gross_net' && $key != 'image_collection_id' && $key != 'date_registration' && $key != 'reason_id')
                        {   
                            if($key == 1) 
                            {
                                $key = 'price';
                            }
                            //echo 'Klucz [1]: ' . $key . '<br>Wartość [1]: ' . $value . '<hr>';
                            if (!$this->db->field_exists($key, 'samochody'))
                            {   
                                $this->db->query('ALTER TABLE `samochody` ADD `'.$key.'` TEXT');          
                            }
                            $add_record[$key] = $value;          
                        }
                }

                $add_record['params'] = rtrim($add_record['params'], ",|,");
                $add_record['gallery'] = rtrim($add_record['gallery'], ",|,");
                $this->base_m->insert('samochody', $add_record);
                $insert['gallery'] = '';
                $insert['params'] = '';

            }
        }
        redirect('panel/cars');
        exit;
    }
}
?>
