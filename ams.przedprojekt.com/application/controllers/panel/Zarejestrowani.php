<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Zarejestrowani extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

public function index() {
    $data['rows'] = $this->base_m->get('users');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/zarejestrowani/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim|required|is_unique[users.login]');
    $this->form_validation->set_rules('email', 'E-mail', 'min_length[2]|trim|required|is_unique[users.login]');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/zarejestrowani/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

            $options = ['cost' => 12];

            $login = strtolower($this->input->post('login'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);

            $insert['rola'] = $this->input->post('rola');
            $insert['login'] = $login;
            $insert['email'] = $email;
            $insert['first_name'] = $this->input->post('first_name');
            $insert['last_name'] = $this->input->post('last_name');
            $insert['password'] = $encripted_pass;

            $this->base_m->insert('users', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

                    $data['contact'] = $this->base_m->get_record('contact_settings', 1);

                    $this->load->library('email');
                        $config = array(
                            'smtp_host' =>"smtp.adawards.pl",
                            'smtp_user' =>"no-reply-car@adawards.pl",
                            'smtp_pass' =>"8lllRuWL4H",
                            'smtp_port' =>"587",
                            'smtp_timeout' =>"30",
                            'mailtype'  =>"html",
                            'charset'  =>"utf-8",
                        );
                        $this->email->clear();
                        $this->email->initialize($config);
                        $this->email->to($insert['email']);
                        $this->email->from($data['contact']->email);
                        $this->email->subject('Potwierdzenie założenia konta w serwisie AMS Toyota');
                        $this->email->message(
                            'Imię i nazwisko: '.$insert['first_name'] . ' ' .$insert['last_name'] .'<br>

                            Login: '.$login .'<br>

                            Email: '.$insert['email'] .'<br>

                            Hasło: '.$password .'<br>');
                        $this->email->send();

            redirect('panel/zarejestrowani');
        }


}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('users', $id);

    $this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('email', 'E-mail', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/zarejestrowani/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

            $options = ['cost' => 12];

            $login = strtolower($this->input->post('login'));
            $email = strtolower($this->input->post('email'));

            $insert['rola'] = $this->input->post('rola');
            $insert['login'] = $login;
            $insert['email'] = $email;
            $insert['first_name'] = $this->input->post('first_name');
            $insert['last_name'] = $this->input->post('last_name');

            if($this->input->post('password') != ''){
            $password = $this->input->post('password');
            $encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
            $insert['password'] = $encripted_pass;
            }

                    $data['contact'] = $this->base_m->get_record('contact_settings', 1);

                    $this->load->library('email');
                        $config = array(
                            'smtp_host' =>"smtp.adawards.pl",
                            'smtp_user' =>"no-reply-car@adawards.pl",
                            'smtp_pass' =>"8lllRuWL4H",
                            'smtp_port' =>"587",
                            'smtp_timeout' =>"30",
                            'mailtype'  =>"html",
                            'charset'  =>"utf-8",
                        );
                        $this->email->clear();
                        $this->email->initialize($config);
                        $this->email->to($insert['email']);
                        $this->email->from($data['contact']->email);
                        $this->email->subject('Potwierdzenie zmian wprowadzonych na Państwa koncie w serwisie AMS Toyota');
                        $this->email->message(
                            'Imię i nazwisko: '.$insert['first_name'] . ' ' .$insert['last_name'] .'<br>

                            Login: '.$login .'<br>

                            Email: '.$insert['email'] .'<br>

                            Hasło: '.$password .'<br>');
                        $this->email->send();


        $this->base_m->update('users', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/zarejestrowani');
            }
    }

public function reset($id) {

    $data['row'] = $this->base_m->get_record('users', $id);

    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    $password = implode($pass);
            $options = ['cost' => 12];

            $email = $data['row']->email;

            $encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);


                    $data['contact'] = $this->base_m->get_record('contact_settings', 1);

                    $this->load->library('email');
                        $config = array(
                            'smtp_host' =>"smtp.adawards.pl",
                            'smtp_user' =>"no-reply-car@adawards.pl",
                            'smtp_pass' =>"8lllRuWL4H",
                            'smtp_port' =>"587",
                            'smtp_timeout' =>"30",
                            'mailtype'  =>"html",
                            'charset'  =>"utf-8",
                        );
                        $this->email->clear();
                        $this->email->initialize($config);
                        $this->email->to($email);
                        $this->email->from($data['contact']->email);
                        $this->email->subject('Potwierdzenie zmiany hasła na Państwa koncie w serwisie AMS Toyota');
                        $this->email->message(
                            'Nowe hasło: '.$password .'<br>');
                        $this->email->send();

                        $insert['password'] = $encripted_pass;
        $this->base_m->update('users', $insert, $id);

        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Hasło zostało pomyślnie zresetowane</p>');
        redirect('panel/zarejestrowani');
            }
    

}
