<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Podstrony extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

	public function index() {
		$data['rows'] = $this->base_m->get('podstrony');
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/podstrony/index', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}
	public function edytuj($id) {
		$data['row'] = $this->base_m->get_record('podstrony', $id);
		$this->form_validation->set_rules('title', 'Tytuł w nagłówku', 'min_length[2]|trim|required');
		$this->form_validation->set_rules('name_header_photo', 'Zdjęcie', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/podstrony/edytuj', $data);
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/podstrony/'.$now)) {
		    	mkdir('./uploads/podstrony/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/podstrony/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    

            if($this->input->post('name_header_photo') != null) {
                if ($this->upload->do_upload('header_photo')) {
                    $data = $this->upload->data();
                    $insert['header_photo'] = $now.'/'.$data['file_name'];         
                }
            }
    		
    		
			$insert['title'] = $this->input->post('title');
			$insert['subtitle'] = $this->input->post('subtitle');
			$insert['content'] = $this->input->post('content');
			$insert['alt'] = $this->input->post('alt');
			


			$this->base_m->update('podstrony', $insert, $id);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
			redirect('panel/podstrony');
		}
	}
}

