<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class P extends CI_Controller {

	public function index() {

		$data['rows'] = $this->base_m->get('samochody');
		$data['glowna'] = $this->base_m->get_record('glowna', 1);
		$data['slider'] = $this->base_m->get('slider');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$data['make'] = $this->base_m->distinct('samochody', 'make');
		$data['model'] = $this->base_m->distinct('samochody', 'model');
		$data['body_type'] = $this->base_m->distinct('samochody', 'body_type');
		$data['fuel_type'] = $this->base_m->distinct('samochody', 'fuel_type');
		$data['year'] = $this->base_m->distinct('samochody', 'year');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/index', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function finansowanie() {
		$data['rows'] = $this->base_m->get('finansowanie');
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/finansowanie', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function serwis() {
		$data['rows'] = $this->base_m->get('serwis');
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/serwis', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function lakiernia() {
		$data['rows'] = $this->base_m->get('lakiernia');
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/lakiernia', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function ubezpieczenia() {
		$data['rows'] = $this->base_m->get('ubezpieczenia');
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/ubezpieczenia', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function salony() {
		$data['rows'] = $this->base_m->get('salony');
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/salony', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	// public function kontakt() {
	// 	$data['rows'] = $this->base_m->get('kontakt');
	// 	$data['slider'] = $this->base_m->get('slider');
	// 	$data['oferta'] = $this->base_m->get('oferta');
	// 	$data['oferta'] = $this->base_m->recommended('oferta');
	// 	$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
	// 	$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

	// 	$this->load->view('front/blocks/head', $data);
	// 	$this->load->view('front/blocks/subheader', $data);
	// 	$this->load->view('front/kontakt', $data);
	// 	$this->load->view('front/blocks/footer', $data);
	// }

	// public function oferty() {
	// 	$data['rows'] = $this->base_m->get('oferta');
	// 	$data['slider'] = $this->base_m->get('slider');
	// 	$data['oferta'] = $this->base_m->get('oferta');
	// 	$data['recommended'] = $this->base_m->recommended('oferta');
	// 	$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
	// 	$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);


	// 	$data['category'] = $this->base_m->distinct('oferta', 'category');
	// 	$data['type'] = $this->base_m->distinct('oferta', 'type');
	// 	$data['city'] = $this->base_m->distinct('oferta', 'city');
	// 	$data['price_max'] = $this->base_m->max('oferta', 'price');
	// 	$data['price_min'] = $this->base_m->min('oferta', 'price');
	// 	$data['area_max'] = $this->base_m->max('oferta', 'area');
	// 	$data['area_min'] = $this->base_m->min('oferta', 'area');

	// 	if(!empty($_POST)){
	// 		$filtr = '';
	// 		if($_POST['category'] != 'none'){
	// 			$filtr .= '`category` = "' . $_POST['category'].'" AND ';
	// 		}
	// 		if($_POST['type'] != 'none'){
	// 			$filtr .= '`type` = "' . $_POST['type'].'" AND ';
	// 		}
	// 		if($_POST['city'] != 'none'){
	// 			$filtr .= '`city` = "' . $_POST['city'].'" AND ';
	// 		}

	// 		$filtr .= '`price` BETWEEN ' . $_POST['price_min'].' AND ' . $_POST['price_max'] .' AND ';

	// 		$filtr .= '`area` BETWEEN ' . $_POST['area_min'].' AND ' . $_POST['area_max'] .' AND ';

	// 		$filtr=rtrim($filtr," AND ");

	// 		$data['oferta'] = $this->base_m->special_filters('oferta', $filtr);
	// 	}
		
	// 	$this->load->view('front/blocks/head', $data);
	// 	$this->load->view('front/blocks/subheader', $data);
	// 	$this->load->view('front/oferty', $data);
	// 	$this->load->view('front/blocks/footer', $data);
	// }

	public function szczegoly($slug, $id) {

		$data['row'] = $this->base_m->get_record('samochody', $id);
		$data['slider'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);


    	$this->db->order_by('rand()');
    	$this->db->limit(4);
    	$query = $this->db->get('samochody');
		$data['random'] = $query->result();
		//print_r($data['random']);
		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/szczegoly', $data);
		$this->load->view('front/blocks/footer', $data);
	}
	
}
