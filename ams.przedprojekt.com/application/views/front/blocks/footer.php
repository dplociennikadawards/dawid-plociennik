  <footer class="main-footer">
    <div class="container">
      <div class="row">
        <div class="main-footer__col col-lg-4">
          <img class="main-footer__logo" src="<?php echo base_url() ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="<?php echo $ustawienia->meta_title; ?>">
        </div>
        <div class="main-footer__col col-lg-4">
          <div class="main-footer__text-box">
            <?php echo $kontakt->company; ?><br>
            <?php echo $kontakt->city; ?><br>
            <?php echo $kontakt->address; ?>
          </div>
        </div>
        <div class="main-footer__col col-lg-4">
          <div class="main-footer__text-box">
            <?php echo $kontakt->name_phone1; ?> <?php echo $kontakt->phone; ?><br>
            <?php echo $kontakt->name_email; ?> <?php echo $kontakt->email; ?>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/mdb.min.js"></script>
  <script src="<?php echo base_url() ?>assets/front/dist/js/lightbox.js"></script>

  <script type="text/javascript">
    $(function () {
      $("#mdb-lightbox-ui").load("<?php echo base_url(); ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
      });
  </script>
  <script type="text/javascript">
  function loadRecords() {
    document.getElementById('records').innerHTML = '<div class="text-center"><i class="fas fa-circle-notch fa-spin" style="font-size: 3rem;"></i></div>';


      $(".pagination li").removeClass("list__pagination-item--active");
      $(".pagination a").removeClass("list__pagination-link--active");

      $("#activePagination0").addClass("list__pagination-item--active");
      $("#linkPagination0").addClass("list__pagination-link--active");
      $("#activePaginationF0").addClass("list__pagination-item--active");
      $("#linkPaginationF0").addClass("list__pagination-link--active");



    setTimeout(function(){ 
      make = document.getElementById('makeInput').value;
      model = document.getElementById('modelInput').value;
      rok_od = document.getElementById('rok_odInput').value;
      rok_do = document.getElementById('rok_doInput').value;
      body_type = document.getElementById('body_typeInput').value;
      fuel_type = document.getElementById('fuel_typeInput').value;
      sort = document.getElementById('sort').value;

      sort = sort.split('|');

      sort_col = sort[0];
      how_sort = sort[1];

      console.log('Marka: ' + make);
      console.log('Model: ' + model);
      console.log('Rok od: ' + rok_od);
      console.log('Rok do: ' + rok_do);
      console.log('Nadwozie: ' + body_type);
      console.log('Paliwo: ' + fuel_type);
      console.log('Kolumna: ' + sort_col);
      console.log('Jak sortowana: ' + how_sort);

      document.getElementById('records').innerHTML = '';
          $("#records").load("<?php echo base_url() ?>elements/records/?sort_col="+sort_col+"&how_sort="+how_sort+"&make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

          $("#pagin_page1").load("<?php echo base_url() ?>elements/records/paginpage/?sort_col="+sort_col+"&how_sort="+how_sort+"&make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

          $("#pagin_page2").load("<?php echo base_url() ?>elements/records/paginpage2/?sort_col="+sort_col+"&how_sort="+how_sort+"&make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

        }, 1000);
    }
  </script>

  <script type="text/javascript">
  function resetRecords(sort_col,how_sort) {
    document.getElementById('records').innerHTML = '<div class="text-center"><i class="fas fa-circle-notch fa-spin" style="font-size: 3rem;"></i></div>';


    setTimeout(function(){ 
      document.getElementById('records').innerHTML = '';
          $("#records").load("<?php echo base_url() ?>elements/records/?sort_col="+sort_col+"&how_sort="+how_sort);
        }, 1000);
    }
  </script>


  <script type="text/javascript">
  function updateFilters(type_filtr) {
    make = document.getElementById('makeInput').value;
    model = '';
    rok_od = document.getElementById('rok_odInput').value;
    rok_do = '';
    body_type = '';
    fuel_type = '';

    console.log(type_filtr);

      console.log('2 Marka: ' + make);
      console.log('2 Model: ' + model);
      console.log('2 Rok od: ' + rok_od);
      console.log('2 Rok do: ' + rok_do);
      console.log('2 Nadwozie: ' + body_type);
      console.log('2 Paliwo: ' + fuel_type);

    //$("#makeInput").load("<?php echo base_url() ?>elements/records/make/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

    $("#modelInput").load("<?php echo base_url() ?>elements/records/model/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

    $("#rok_odInput").load("<?php echo base_url() ?>elements/records/rok_od/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

    $("#rok_doInput").load("<?php echo base_url() ?>elements/records/rok_do/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

    $("#body_typeInput").load("<?php echo base_url() ?>elements/records/body_type/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

    $("#fuel_typeInput").load("<?php echo base_url() ?>elements/records/fuel_type/?make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type);

  }
  </script>


  <script type="text/javascript">
    function pagination(nr_page) {
      element = document.getElementById('linkPagination'+nr_page);
      test = element.classList.contains('list__pagination-link--active');
      if(test != true) {
      document.getElementById('records').innerHTML = '<div class="text-center"><i class="fas fa-circle-notch fa-spin" style="font-size: 3rem;"></i></div>';

      $(".pagination li").removeClass("list__pagination-item--active");
      $(".pagination a").removeClass("list__pagination-link--active");

      $("#activePagination"+nr_page).addClass("list__pagination-item--active");
      $("#linkPagination"+nr_page).addClass("list__pagination-link--active");
      $("#activePaginationF"+nr_page).addClass("list__pagination-item--active");
      $("#linkPaginationF"+nr_page).addClass("list__pagination-link--active");

      setTimeout(function(){ 
        make = document.getElementById('makeInput').value;
        model = document.getElementById('modelInput').value;
        rok_od = document.getElementById('rok_odInput').value;
        rok_do = document.getElementById('rok_doInput').value;
        body_type = document.getElementById('body_typeInput').value;
        fuel_type = document.getElementById('fuel_typeInput').value;
        sort = document.getElementById('sort').value;

        sort = sort.split('|');

        sort_col = sort[0];
        how_sort = sort[1];

        console.log('Marka: ' + make);
        console.log('Model: ' + model);
        console.log('Rok od: ' + rok_od);
        console.log('Rok do: ' + rok_do);
        console.log('Nadwozie: ' + body_type);
        console.log('Paliwo: ' + fuel_type);
        console.log('Kolumna: ' + sort_col);
        console.log('Jak sortowana: ' + how_sort);

        document.getElementById('records').innerHTML = '';
            $("#records").load("<?php echo base_url() ?>elements/records/pagination/?sort_col="+sort_col+"&how_sort="+how_sort+"&make="+make+"&model="+model+"&rok_od="+rok_od+"&rok_do="+rok_do+"&body_type="+body_type+"&fuel_type="+fuel_type+"&nr_page="+nr_page);
          }, 1000 );
      }
    }

  </script>
  <script type="text/javascript">resetRecords('created_at', 'DESC');</script>

    <script type="text/javascript">
      function validateMyForm(nr)
      {
        if (grecaptcha.getResponse(nr) == ""){
            document.getElementsByClassName('warning_captcha')[nr].innerHTML = 'Potwierdź captche';
            return false;
        } else {
            return true;
        }
      }
    </script>
    <script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#B90000",
      "text": "#fff"
    },
    "button": {
      "background": "#000",
      "text": "#fff"
    }
  },
  "type": "opt-out",
  "content": {
    "message": "<?php echo $ustawienia->cookies; ?>",
    "dismiss": "Rozumiem",
    "deny": "",
    "allow": "Rozumiem",
    "link": "Czytaj więcej...",
    "href": "<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->pdf ?>"
  }
})});
new WOW().init();
</script>
</body>

</html>