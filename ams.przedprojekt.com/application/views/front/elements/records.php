<?php 
$nadwozie['city-car'] = 'Auto miejskie'; 
$nadwozie['combi'] = 'Kombi'; 
$nadwozie['compact'] = 'Kompakt'; 
$nadwozie['minivan'] = 'Minivan'; 
$nadwozie['panel-van'] = 'Furgon'; 
$nadwozie['sedan'] = 'Sedan'; 
$nadwozie['suv'] = 'SUV'; 
$nadwozie['vans'] = 'Samochód dostawczy'; 

$paliwo['diesel'] = 'Diesel'; 
$paliwo['petrol'] = 'Benzyna'; 
$paliwo['hybrid'] = 'Hybryda'; 
$paliwo['petrol-lpg'] = 'Benzyna - LPG'; 

$skrzynia['manual'] = 'Manualna'; 
$skrzynia['automatic'] = 'Automatyczna'; 
$skrzynia['am'] = 'Amracing'; 
$skrzynia['cvt'] = 'Bezstopniowa'; 
$skrzynia['semi-automatic'] = 'Półautomatyczna'; 

?>

          <div class="container">
            <?php if(empty($rows)){echo 'Brak samochodów spełniające powyższe wymagania.';} ?>
            <?php foreach ($rows as $row): ?>
            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  <?php echo ucfirst($row->make); ?>
                  <?php echo ucfirst($row->model); ?>
                  <?php echo ucfirst($row->version); ?>
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value"><?php echo $row->year; ?></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value"><?php echo $paliwo[$row->fuel_type]; ?></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value"><?php echo str_replace(',', ' ', number_format($row->engine_capacity)); ?> cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value"><?php echo $skrzynia[$row->gearbox]; ?></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value"><?php echo str_replace(',', ' ', number_format($row->mileage)); ?> km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price"><?php echo str_replace(',', ' ', number_format($row->price)); ?> <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, <?php if($row->vat == 1){echo 'Faktura VAT';} ?></span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="
                        <?php echo base_url() ?>p/szczegoly/<?php echo $this->slugify_m->slugify($row->make.$row->model.$row->version); ?>/<?php echo $row->samochody_id; ?>"
                      >SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="<?php echo base_url() ?>p/finansowanie">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('<?php echo $row->photo; ?>');"></div>
            </div>
            <?php endforeach ?>

          </div>