<section>
  <div class="container py-5">
    <div class="row">
      <?php if($slider->content != ''): ?>
        <div class="col-md-12 mb-5"><?php echo $slider->content; ?></div>
      <?php endif; ?>
      <?php foreach ($rows as $value):?>
        <div class="col-md-12">
          <h3 class="cell__title"><?php echo $value->title; ?></h3>
          <div class="mt-4 mb-5 line-separate-red"></div>
        </div>
        <div class="col-md-5 wow fadeIn">
        <div class="view overlay rounded z-depth-2 mb-4" style="
                      background-image: url('<?php echo base_url(); ?>uploads/lakiernia/<?php echo $value->photo; ?>'); 
                      height: 300px; 
                      background-size: cover;    
                      background-position: center;
                      background-repeat: no-repeat; "></div>
        </div>
        <div class="col-md-7">
          <div><?php echo $value->content; ?></div>
        </div>
        <div class="mt-4 mb-5 line-separate"></div>
      <?php endforeach ?>
    </div>
  </div>
</section>