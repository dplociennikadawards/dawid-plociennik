  <main>
    <section class="search">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="search__text-box">
              <h4 class="search__light-title"><?php echo $glowna->content; ?></h4>
            </div>
          </div>
          <div class="col-lg-8">
            <form action="" method="post">
              <div class="search__form-row">
                <select class="search__top-select" name="" id="">
                  <option value="">marka</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
                <select class="search__top-select" name="" id="">
                  <option value="">model</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
              </div>
              <div class="search__form-row">
                <span class="search__text">ROCZNIK</span>
                <select class="search__down-select" name="" id="">
                  <option value="">od</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
                <select class="search__down-select" name="" id="">
                  <option value="">do</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
                <select class="search__down-select" name="" id="">
                  <option value="">typ nadwozia</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
                <select class="search__down-select" name="" id="">
                  <option value="">rodzaj paliwa</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
              </div>
              <div class="search__form-row">
                <a class="search__clear-link" href="#">wyczyść filtrowanie</a>
                <input class="search__submit" type="submit" value="filtruj">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <section class="list">
      <div class="container">
        <div class="list__nav-bar">
          <div class="list__sort">
            sortuj:
            <!-- Basic dropdown -->
            <button class="list__dropdown" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">Najnowsze <i class="fas fa-angle-down"></i></button>

            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Separated link</a>
            </div>
            <!-- Basic dropdown -->
          </div>

          <div class="list__pagination-box">
            <nav aria-label="navigation">
              <ul class="pagination">
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/«.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/‹.png"></a>
                </li>
                <li class="list__pagination-item list__pagination-item--active page-item"><a
                    class="list__pagination-link list__pagination-link--active page-link">1</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">2</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">3</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">4</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">5</a></li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/›.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/».png"></a>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="list__box">
          <div class="container">

            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  Toyota Auris II
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value">2016</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value">hybryda</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value">1 798 cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value">automatyczna</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value">50 032 km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price">73 900 <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, Faktura VAT</span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="#">SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="#">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('img/car.jpg');"></div>
            </div>

            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  Toyota Auris II
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value">2016</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value">hybryda</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value">1 798 cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value">automatyczna</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value">50 032 km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price">73 900 <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, Faktura VAT</span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="#">SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="#">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('img/car.jpg');"></div>
            </div>

            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  Toyota Auris II
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value">2016</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value">hybryda</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value">1 798 cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value">automatyczna</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value">50 032 km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price">73 900 <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, Faktura VAT</span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="#">SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="#">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('img/car.jpg');"></div>
            </div>

            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  Toyota Auris II
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value">2016</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value">hybryda</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value">1 798 cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value">automatyczna</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value">50 032 km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price">73 900 <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, Faktura VAT</span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="#">SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="#">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('img/car.jpg');"></div>
            </div>

            <div class="cell">
              <div class="cell__content-box">
                <h3 class="cell__title">
                  Toyota Auris II
                </h3>
                <div class="cell__row">
                  <div class="cell__list">
                    <div class="cell__list-item">
                      <span class="cell__parameter">rok produkcji</span>
                      <span class="cell__value">2016</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">typ paliwa</span>
                      <span class="cell__value">hybryda</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">pojemność</span>
                      <span class="cell__value">1 798 cm<sup>3</sup></span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">rodzaj skrzyni</span>
                      <span class="cell__value">automatyczna</span>
                    </div>
                    <div class="cell__list-item">
                      <span class="cell__parameter">przebieg</span>
                      <span class="cell__value">50 032 km</span>
                    </div>
                  </div>

                  <div class="cell__price-box">
                    <div>
                      <h3 class="cell__price">73 900 <span class="cell__light-text">PLN</span></h3>
                      <span class="cell__desc">Cena Brutto, Faktura VAT</span>
                    </div>

                    <div class="cell__link-box">
                      <a class="cell__link cell__link--red" href="#">SPRAWDŹ SZCZEGÓŁY <i
                          class="fas fa-angle-right"></i></a>
                      <a class="cell__link cell__link--gray" href="#">SPRAWDŹ FINANSOWANIE <i
                          class="fas fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell__img" style="background-image: url('img/car.jpg');"></div>
            </div>

          </div>
        </div>

        <div class="list__nav-bar list__nav-bar--down">


          <div class="list__pagination-box">
            <nav aria-label="navigation">
              <ul class="pagination">
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/«.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/‹.png"></a>
                </li>
                <li class="list__pagination-item list__pagination-item--active page-item"><a
                    class="list__pagination-link list__pagination-link--active page-link">1</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">2</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">3</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">4</a></li>
                <li class="list__pagination-item page-item"><a class="list__pagination-link page-link">5</a></li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/›.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/».png"></a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </section>

    <section class="contact">
      <div class="container">
        <h3 class="contact__title">
          ZAPRASZAMY DO KONTAKTU
        </h3>
        <p class="contact__subtitle">Wypełnij formularz, skontaktujemy się z Tobą najszybciej jak to będzie możliwe.</p>

        <form action="" method="post">
          <div class="row">
            <div class="col-lg-6">
              <div class="contact__row contact__row--top">
                <span class="contact__desc">Oddział *</span> <select class="contact__select" name="" id="">
                  <option value=""></option>
                  <option value=""></option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="contact__row">
                <span class="contact__desc">Imie *</span> <input class="contact__input" type="text" name="" id="">
              </div>
              <div class="contact__row">
                <span class="contact__desc">Nazwisko *</span> <input class="contact__input" type="text" name="" id="">
              </div>
              <div class="contact__row">
                <span class="contact__desc">Adres e-mail *</span> <input class="contact__input" type="email" name=""
                  id="">
              </div>
              <div class="contact__row">
                <span class="contact__desc">Telefon *</span> <input class="contact__input" type="tel" name="" id="">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="contact__row">
                <span class="contact__desc contact__desc--textarea">Dodatkowa <br class="contact__linebreaker"> wiadomość</span>

                <textarea class="contact__textarea" name="" id="" cols="30" rows="7"></textarea>
              </div>
            </div>
          </div>

          <div class="contact__bottom-row row">
            <div class="contact__checkbox-box">
              <!-- Default unchecked -->
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultUnchecked1">
                <label class="custom-control-label" for="defaultUnchecked1">Zagadzam się na kontakt mailowy *</label>
              </div><!-- Default unchecked -->
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultUnchecked2">
                <label class="custom-control-label" for="defaultUnchecked2">Zagadzam się na kontakt telefoniczny
                  *</label>
              </div>
            </div>

            <p class="contact__text">
              Podając nam swoje dane kontaktowe, wyrażasz zgodę na kontakt ze strony należącej do TOYOTA Słupsk w
              sprawie zaaranżowania jazdy próbnej oraz na otrzymywanie od czasu do czasu informacji na temat firmy
              Infiniti.
              Zaznaczenie poszczególnego sposobu kontaktu oznacza zgodę na otrzymywanie informacji od TOYOTA Słupsk w
              ten sposób.
            </p>
          </div>

          <div class="contact__bottom-row row">
            <span class="contact__second-desc">* Pola wymagane</span>
            <input class="contact__submit" type="submit" value="WYŚLIJ">
          </div>
        </form>
      </div>
    </section>
  </main>