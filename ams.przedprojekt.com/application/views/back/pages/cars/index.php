<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container-fluid mb-5">
			<!-- Table with panel -->
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<input type="text" id="searchInput" class="form-control" onkeyup="searchFiltr(0)" placeholder="Wpisz nazwę..." title="Wpisz nazwę">
			    </div>
			    
			    <a href="" class="white-text mx-3">Samochody</a>

                  <div class="text-right" style="width: 181px;">
                      <a href="<?php echo base_url(); ?>panel/otomoto_cron/login">
                          <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2">
                              <i class="fas fa-sync-alt mt-0"></i>
                          </button>
                      </a>
			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4">

			    <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
				            <th style="width: 10%;"></th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 40%;">
				              Tytuł
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableInt(1)" style="width: 20%;">
				              Cena
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(2)" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 10%;"></th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->

				        <tbody>
						  	<?php foreach ($rows as $key): ?>
						  		<tr>
                                    <th scope="row" class="align-middle"></th>
						            <td class="align-middle"><?php echo $key->title; ?></td>
						            <td class="align-middle">
						            <?php 
						            	setlocale(LC_MONETARY, 'pl_PL');
										echo money_format('%i', $key->price) . "\n";
									?></td>
						            <td class="align-middle"><?php echo $key->created_at; ?></td>
						            <td class="align-middle text-right">
						              <a href="<?php echo base_url(); ?>p/szczegoly/<?php echo $this->slugify_m->slugify($key->title); ?>/<?php echo $key->samochody_id; ?>" target="_blank">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="far fa-eye mt-0"></i>
									    </button>
									  </a>
								  	</td>
				            	</tr>
							<?php endforeach; ?>
				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
</main>
