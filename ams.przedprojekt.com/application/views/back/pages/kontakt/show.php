<main class="main-section water-logo">
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Sprawdź wiadomość #<?php echo $row->kontakt_id; ?></a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2" onclick="send_message(<?php echo $row->kontakt_id; ?>)">
                        Wyślij
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-6">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formFirstName" class="form-control" value="<?php echo $row->first_name; ?>" readonly>
                      <label for="formFirstName">Imię</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="formEmail" class="form-control" value="<?php echo $row->email; ?>" readonly>
                      <label for="formEmail">Adres E-mail</label>
                    </div>

                    <div class="md-form mb-5">
                        <input type="text" id="formPhone" class="form-control" value="<?php echo $row->phone; ?>" readonly>
                        <label for="formPhone">Telefon</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="formSubject" class="form-control" value="<?php echo $row->subject; ?>" readonly>
                      <label for="formSubject">Temat</label>
                    </div>

                    <div class="md-form">
                      <textarea id="formMessage" class="md-textarea form-control" rows="3" readonly><?php echo $row->message; ?></textarea>
                      <label for="formMessage">Treść wiadomości</label>
                    </div>
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="rodo" <?php if($row->rodo == 1){echo 'checked';} ?> readonly onclick="return false;">
                      <label class="form-check-label" for="rodo">Zgoda na przetwarzanie danych osobowych</label>
                    </div>
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="rodo2" <?php if($row->rodo2 == 1){echo 'checked';} ?> readonly onclick="return false;">
                      <label class="form-check-label" for="rodo2">Zgoda na otrzymywanie wiadomości marketingowych</label>
                    </div>

                </div>
                <div class="col-md-6">
                  <p id="message_request">Wyślij odpowiedź do klienta bezpośrednio z panelu</p>
                    <div class="md-form mb-5">
                      <input type="text" id="formSubjectSend" class="form-control" name="subject">
                      <label for="formSubjectSend">Temat</label>
                    </div>

                    <div class="md-form">
                      <textarea name="content" id="editor" placeholder="Treść wiadomości"></textarea>
                    </div>
                  </div>
               </div>
            </div>
        </div>
    </section>
</main>

    <script type="text/javascript">
      function send_message(id)
      {
        var field = field;
        var email = document.getElementById('formEmail').value;
        var subject = document.getElementById('formSubjectSend').value;
        var message = document.getElementById('editor').value;
        //alert(message);
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/send", 
             data: {id:id, email:email, subject:subject, message:message}, 
             cache: false,
             complete:function(html)
             {
                 console.log(html);
                 document.getElementById('message_request').innerHTML = '<span class="text-succes"><i class="fas fa-check"></i> Wiadomość została wysłana</span>';
                 document.getElementById('formSubjectSend').value ="";
                 document.getElementById('formMessageSend').value ="";
             }  
        });  
      }
    </script>