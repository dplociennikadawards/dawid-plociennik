<main class="main-section water-logo">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-2 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	<section>
		<div class="container-fluid px-md-5 mt-5">
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Lista dokumentów</a>

			    <div class="text-right" style="width: 181px;">
				      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
				        Zapisz
				      </button>
			    </div>

			  </div>
			   <div class="row px-5">
			   	<div class="col-md-7">
			   			<?php  
			   			$table = $this->uri->segment(4);
			   			$id = $this->uri->segment(5);
			   			$galeria = $this->base_m->get_gallery($table,$id);
			   			?>
			   		<div class="row" id="table_photos">
			   			<?php foreach (array_reverse($galeria) as $row): ?>              
		                        <div class="pl-md-0 px-3 col-md-12 mg-t--1 mg-md-t-0 mb-2 " id="box_<?php echo $row->gallery_id;  ?>">
		                        	<div style="height: auto;cursor: pointer;">
			                           	
			                           	<a href="<?php echo base_url(); ?>uploads/gallery/<?php echo $row->img ?>"><i class="fas fa-file-download"></i> <?php echo $row->img ?></a>
			                   		</div>
			                   		<div class="row">

				                   		<div class="md-form pr-md-0 col-md-8 px-0">
				                            <div class="form-group bd-t-0-force bd-l-0-force bd-r-0-force bd-b-0-force mt-2 pl-4">
				                                <input class="form-control" type="text" id="<?php echo $row->gallery_id ?>" name="img_alt" 
				                                value="<?php echo $row->img_alt ?>"  placeholder="Alternatywny tekst">
				                              
				                              </div>
				                        </div><!-- col-12 -->
				                       <div class="pr-md-0 col-md-4 px-0 mt-1" id="box_alt<?php echo $row->gallery_id ?>">
				                          <div class="form-group bd-t-0-force bd-l-0-force bd-r-0-force bd-b-0-force ml-1" 
				                          	id="spinner_alt_<?php echo $row->gallery_id ?>">
				                            <button type="submit" class="btn btn-outline-dark btn-sm px-2" style="cursor: pointer;" onclick="update_alt(<?php echo $row->gallery_id ?>)">
				                            	Zapisz
				                            </button>
				                            <button onclick="delete_photo(<?php echo $row->gallery_id ?>)" type="submit" class="btn btn-outline-danger btn-sm px-2" style="cursor: pointer;">
				                            	Usuń
				                            </button>
				                            
				                          </div>
				                      </div><!-- col-12 -->

				                   </div>
		                        </div>
	                      <?php endforeach; ?>
			   		</div>
			   	</div>
			   	<div class="col-md-5">
			   		<?php echo validation_errors(); ?>
					<div class="md-form">
					  <input type="text" id="formAlt" class="form-control" name="alt">
					  <input type="hidden" id="table_name" class="form-control" name="table_name" value="<?php echo $this->uri->segment(4) ?>">
					  <input type="hidden" id="item_id" class="form-control" name="item_id" value="<?php echo $this->uri->segment(5) ?>">
					  <label for="formAlt">Nazwa dokumentu</label>
					</div>
					<div class="md-form">
					  <div class="file-field">
					  	
					    <div class="btn btn-primary btn-sm float-left gold-gradient">
					      <span>Wybierz dokument</span>
					      <input type="file" name="photo" id="photo">
					    </div>
					    <div class="file-path-wrapper">
					      <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz dokument" required readonly>
					    </div>
					  </div>
					</div>
				</div>
			   </div>
			</div>
		</div>
	</section>
	</form>
</main>

   <!-- Update tekstu alternatywnego -->
<script type="text/javascript">
	
	function update_alt(id){
		element = document.getElementById(id).value;
		     $.ajax({
                type: "post",
                url: "<?php echo base_url() ?>panel/gallery/update_alt/"+id+"",
                data: {element:element},
                cache: false,
                 	beforeSend: function() { //przed dodaniem
                      document.getElementById('spinner_alt_'+id+'').innerHTML = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>';
                     },
                     success:function(data)  //success po dodaniu
                     {  
                        
                     },
                     complete:function(data) { 
                     	 setTimeout(function(){ 
                     	 document.getElementById('spinner_alt_'+id+'').innerHTML = '<button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2" style="cursor: pointer;" onclick="update_alt('+id+')">Zapisz</button><button onclick="delete_photo('+id+')" type="submit" class="btn btn-outline-danger btn-sm px-2" style="cursor: pointer;">Usuń</button>';}, 1234);
                     },
                     

                                         
      

                   	 });

	}
</script>


<script type="text/javascript">
	
	function delete_photo(id){
		     $.ajax({
                type: "post",
                url: "<?php echo base_url() ?>panel/gallery/delete/"+id+"",
                cache: false,
                 	beforeSend: function() { //przed dodaniem
                      document.getElementById('box_alt'+id+'').innerHTML = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>';
                     },
                     success:function(data)  //success po dodaniu
                     {  
                        
                     },
                     complete:function(data) { 
                     		document.getElementById("box_"+id+"").remove();
                     },
                     

                                         
      

                   	 });

	}
</script>