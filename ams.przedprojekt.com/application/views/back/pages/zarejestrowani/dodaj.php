<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Dodaj </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="container-fluid">
                
                    <?php echo validation_errors(); ?>
                   <div class="row px-5">
                      <div class="col-md-12">
                           <div class="md-form mb-5">
                               <select name="rola" class="form-control" style="display: block !important;">
                                 <option value="" selected>Wybierz rolę</option>
                                 <option value="admin">Administrator</option>
                                 <option value="redaktor">Redaktor</option>
                               </select>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <input type="text" id="login" class="form-control" name="login" required>
                               <label for="login">Login<span class="text-danger">*</span></label>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <input type="text" id="email" class="form-control" name="email" required>
                               <label for="email">E-mail<span class="text-danger">*</span></label>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <input type="text" id="first_name" class="form-control" name="first_name" required>
                               <label for="first_name">Imię<span class="text-danger">*</span></label>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <input type="text" id="last_name" class="form-control" name="last_name" required>
                               <label for="last_name">Nazwisko<span class="text-danger">*</span></label>
                           </div>
                       </div>
                       <div class="col-md-12">
                           <div class="md-form mb-5">
                               <input type="text" id="password" class="form-control" name="password" required>
                               <label for="password">Hasło<span class="text-danger">*</span></label>
                           </div>
                       </div>
                   </div>

        </div>
    </section>
    </form>
</main>
