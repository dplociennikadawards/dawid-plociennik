<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj dane #<?php echo $row->finansowanie_id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-12">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                      <label for="formTitle">Tytuł <span class="text-danger">*</span></label>
                    </div>
                </div>

                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <textarea name="content" id="editor"><?php echo $row->content; ?></textarea>
                    </div>
                    <div class="col-md-6">
                      <textarea name="content2" id="editor"><?php echo $row->content2; ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mt-4">
                  <textarea name="content3" id="editor"><?php echo $row->content3; ?></textarea>
                </div>

               </div>
            </div>
        </div>
    </section>
    </form>
</main>
