    <!--Footer-->
    <footer>
        <!-- SCRIPTS -->
	    <!-- JQuery -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/jquery-3.3.1.min.js"></script>
	    <!-- Bootstrap tooltips -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/popper.min.js"></script>
	    <!-- Bootstrap core JavaScript -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/bootstrap.min.js"></script>
	    <!-- MDB core JavaScript -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/mdb.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    	<script src="<?php echo base_url(); ?>assets/back/dist/bootstrap-tagsinput.min.js"></script>
    	<script src="<?php echo base_url(); ?>assets/back/dist/bootstrap-tagsinput/bootstrap-tagsinput-angular.min.js"></script>
    	<script src="https://cdn.tiny.cloud/1/jslf2irttimrhb50rrfk7qbxql93f9ywgr0b7su6w4wcq50j/tinymce/5/tinymce.min.js"></script>
	    <script type="text/javascript">
	    	$('.date').datepicker({ 
	    		multidate: true,
	    		format: 'yyyy-mm-dd',
    		 }); 
	    </script>
	    <script type="text/javascript">
	    	$( "#toggle-menu" ).click(function() {
			   $("#menu").toggleClass("show-menu");
			   $("#bg-opacity").toggleClass("d-none");
			});
	    	$( "#bg-opacity" ).click(function() {
			   $("#menu").toggleClass("show-menu");
			   $("#bg-opacity").toggleClass("d-none");
			});
	    </script>

    	<script>
        tinymce.init({ 
            selector:'textarea',
            paste_as_text: true,
            plugins : 'advlist autolink link image lists charmap print preview code paste anchor code textpattern table',
            advlist_bullet_styles: "square",
  			toolbar: "numlist bullist anchor image code table textpattern format",
            valid_elements : '*[*]',
            height: 400,
              setup: function (editor) {
              editor.on('change', function (e) {
              editor.save();
              });
            }
            });
    	</script>

	    <script>
		function searchFiltr(nr) {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("searchInput");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("filtrableTable");
		  tr = table.getElementsByTagName("tr");
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[nr];
		    if (td) {
		      txtValue = td.textContent || td.innerText;
		      if (txtValue.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    }       
		  }
		}
		</script>
<script type="text/javascript">
  $('#specialform').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
</script>
		<script type="text/javascript">
			$(function () {
			$('[data-toggle="popover-hover"]').popover();
			});
		</script>

   	</footer>
    <!--Footer-->
  </body>
</html>