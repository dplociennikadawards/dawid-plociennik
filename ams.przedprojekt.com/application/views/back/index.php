<main class="main-section water-logo">
  <section>
    <div class="container-fluid">

          <div class="row p-3">

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/glowna">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/home.svg');"></div>Strona główna
                </button>
              </a>
            </div>

            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/slider">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/image.svg');"></div>Slider
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/podstrony">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/folder.svg');"></div>Podstrony
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/zarejestrowani">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/teamwork.svg');"></div>Zarejestrowani
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/cars">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/car(1).svg');"></div>Samochody
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/otomoto">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/otomoto.png');"></div>Otomoto
                </button>
              </a>
            </div>
            <?php endif; ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/finansowanie">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/money-bag.svg');"></div>Finansowanie
                </button>
              </a>
            </div>

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/serwis">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/car.svg');"></div>Serwis
                </button>
            </a>
            </div>  

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/lakiernia">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/spray-gun.svg');"></div>Lakiernia/blacharnia
                </button>
            </a>
            </div>

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/ubezpieczenia">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/car-insurance.svg');"></div>Ubezpieczenia
                </button>
            </a>
            </div> 

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/salony">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/enterprise.svg');"></div>Nasze salony
                </button>
              </a>
            </div>

            <!-- <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/gallery/dodaj/galeria/1">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/factory.svg');"></div>{{Tworzenie galerii}}
                </button>
              </a>
            </div> -->


<!--             <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/newsletter">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/newsletter.svg');"></div>Newsletter
                </button>
            </a>
            </div> -->
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/kontakt">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/email.svg');"></div>Kontakt
                </button>
            </a>
            </div>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/ustawienia">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/settings.svg');"></div>Ustawienia
                </button>
            </a>
            </div>
            <?php endif; ?>

          </div>

        </div>
  </section>
</main>