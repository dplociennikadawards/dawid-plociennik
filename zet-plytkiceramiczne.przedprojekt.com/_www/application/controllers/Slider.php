<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

public function index()
	{	
		
		$data['data'] = $this->users_m->get('slider');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/slider/main_page',$data);
		$this->load->view('back/blocks/footer');

	}

	public function add()
	{	
		
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/slider/add');
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
		$save = $this->input->post('save');
		if(isset($save))
		{

			$insert['header'] = $this->input->post('header');
			$insert['alt'] = $this->input->post('alt');
			$insert['text'] = $this->input->post('text');
			$insert['text'] = str_replace("<p>","",$insert['text']);
			$insert['text'] = str_replace("</p>","<br>",$insert['text']);
			$insert['text'] = str_replace('font-size: 0.875rem;',"",$insert['text']);
			$insert['img'] = $this->input->post('img');
			$insert['img'] = str_replace(" ","_",$insert['img']);

			$this->users_m->insert('slider', $insert);


			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś slajd!</p>');
				
			redirect('slider');

		}

	}

	public function edit($id)
	{	
		
		$data['data'] = $this->users_m->get_element('slider',$id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/slider/edit',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
		$save = $this->input->post('save');
		if(isset($save))
		{

			$update['header'] = $this->input->post('header');
			$insert['alt'] = $this->input->post('alt');
			$update['text'] = $this->input->post('text');
			$update['text'] = str_replace("<p>","",$update['text']);
			$update['text'] = str_replace("</p>","<br>",$update['text']);
			$update['text'] = str_replace('font-size: 0.875rem;',"",$update['text']);
			$update['img'] = $this->input->post('img');
			$update['img'] = str_replace(" ","_",$update['img']);

			$this->db->where('slider_id', $id);
			$this->db->update('slider', $update);

			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie wprowadziłeś zmiany!</p>');
				
			redirect('slider');

		}

	}


	public function delete($id)
	{

		$id = $this->uri->segment(3);
		$where = array('slider_id' => $id);
		$this->users_m->delete('slider' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś slajd!</p>');
		redirect('slider');
	}

}
