<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_upload extends CI_Controller {

public function index()  
      {  
           if(isset($_FILES["image_file"]["name"]))  
           {  
                $config['upload_path'] = './upload/'; //sciezka do folderu 
                $config['allowed_types'] = '*'; //dopuszczalne typy
                $config['max_width']  = '*'; //dopuszczalna szerokosc
                $config['max_height']  = '*'; //dopuszczalna wysokosc
                $new_name = time().$_FILES["image_file"]['name']; //unikalna nazwa pliku data+nazwa_pliku
                $config['file_name'] = $new_name;  //unikalna nazwa pliku data+nazwa_pliku

                $this->load->library('upload', $config);  
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('image_file'))  
                {  
                     echo $this->upload->display_errors();
                }  
                else  //wysylanie danych do widoku nazwa pliku, rozdzielczosc i podglad zdjęcia
                {  
                     $data = $this->upload->data();  
                     $data["file_name"] = str_replace(" ","",$data["file_name"]);
                      
                     echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="100%" height="180" class="img-thumbnail" />';      
                     echo '<input id="name_photo" class="home_class_photo" type="hidden" value="' . $data["file_name"] . '">';  
                     echo '<input id="send_resolution" class="galeria" type="hidden" value="' . $data["image_width"] . 'x' . $data["image_height"] . '">'; 
                }  
           }  
      }  



}
