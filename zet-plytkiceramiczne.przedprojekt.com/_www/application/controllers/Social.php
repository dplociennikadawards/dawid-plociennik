<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		if(!isset($_SESSION['login']))
		{
			redirect('panel/login');
		}
	}
	
	public function index()
	{

		$data['data'] = $this->users_m->get_element('social', '1');

		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/social/main_page', $data);
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$update['facebook'] = $this->input->post('facebook');
				$update['twitter'] = $this->input->post('twitter');
				$update['youtube'] = $this->input->post('youtube');
				
				

				$this->db->where('social_id', '1');
				$this->db->update('social', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('social');

			}

	}
	
}
