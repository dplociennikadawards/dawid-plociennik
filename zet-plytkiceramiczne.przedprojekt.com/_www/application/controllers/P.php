<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/header.php');
		$this->load->view('front/index.php');
		$this->load->view('front/blocks/footer.php');
	}




	public function ofirmie()
	{	
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/ofirmie.php');
		$this->load->view('front/blocks/footer.php');
	}



	public function produkty()
	{	
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/produkty.php');
		$this->load->view('front/blocks/footer.php');
	}


		public function realizacje()
	{	
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/realizacje.php');
		$this->load->view('front/blocks/footer.php');
	}



		public function dlaprojektanta()
	{	
		$data['miasta'] = $this->users_m->get('miasta');

		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/dlaprojektanta.php', $data);
		$this->load->view('front/blocks/footer.php');
	}


		public function kontakt()
	{	

		$data['kontakt'] = $this->users_m->get_element('kontakt',1);

		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/kontakt.php', $data);
		$this->load->view('front/blocks/footer.php');
	}


		public function produkt()
	{	
		$this->load->view('front/blocks/head.php');
		$this->load->view('front/blocks/subheader.php');
		$this->load->view('front/produkt.php');
		$this->load->view('front/blocks/footer.php');
	}

}
	