<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



		public function index()
	{
			$data['galeria'] = $this->users_m->get('galeria');

			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/galeria/main_page',$data);
			$this->load->view('back/blocks/javascript');
			$this->load->view('back/blocks/footer');
			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$insert['img'] = $this->input->post('img');
				$insert['img'] = str_replace(" ", "_", $insert['img']);
				$insert['rozmiar'] = $this->input->post('rozmiar');

				
				

				$this->users_m->insert('galeria', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś zdjęcie!</p>');
					
				redirect('galeria');


			}

	}


	public function delete($id)
	{

		$id = $this->uri->segment(3);
		$where = array('galeria_id' => $id);
		$this->users_m->delete('galeria' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś zdjęcie!</p>');

		redirect('galeria');
	}


 

	
}
