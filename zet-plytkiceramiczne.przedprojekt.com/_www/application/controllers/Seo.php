<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	
	public function ustawienia_strony()
	{

		$data['data'] = $this->users_m->get_element('ustawienia_strony', '1');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/seo/main_page', $data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$update['description'] = $this->input->post('description');
				$update['keywords'] = $this->input->post('keywords');
				$update['title'] = $this->input->post('title');
				$update['polityka'] = $this->input->post('img');
				$update['polityka'] = str_replace(" ", "_", $update['polityka']);

				
				
				

				$this->db->where('ustawienia_strony_id', '1');
				$this->db->update('ustawienia_strony', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('seo/ustawienia_strony');

			}

	}



	public function keys()
	{

		$data['data'] = $this->users_m->get_element('keys', '1');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/seo/keys', $data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$update['console'] = $this->input->post('console');
				$update['analytics'] = $this->input->post('analytics');

				
				
				

				$this->db->where('keys_id', '1');
				$this->db->update('keys', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('seo/keys');

			}

	}


		public function robots()
	{

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/seo/robots');
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$insert['robots'] = $this->input->post('robots');
				$f = $this->input->post('robots');
				
               	$my_file = './robots.txt';
				$save = fopen($my_file, 'w') ;
				fwrite($save, $f);
				fclose($save);
				
				

				$this->users_m->insert('robots', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('seo/robots');

			}

	}



		public function links()
	{


		$data['data'] = $this->users_m->get('site_links');


		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/seo/links',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$insert['link'] = $this->input->post('link');
				$insert['priority'] = $this->input->post('priority');
				$insert['changefreq'] = $this->input->post('changefreq');
				
               
				

				$this->users_m->insert('site_links', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('seo/links');

			}

	}

		public function delete_link()
	{

		$id = $this->uri->segment(3);
		$where = array('id' => $id);
		$this->users_m->delete('site_links' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś link!</p>');
		redirect('seo/links');


	}

		public function sitemap() {

		header('Content-type: text/xml');
		$data['links'] = $this->users_m->get('site_links');
		$sitemap = '';


		//WIADOMOSCI
		foreach ($data['links'] as $row) {
			$link = $this->front_model->slugify_link($row->link);
			$sitemap .= '<url>'."\n";
			$sitemap .= '<loc>'.base_url().''.$link.'</loc>'."\n";
			$sitemap .= '<lastmod>'.date("Y-m-d", strtotime($row->date)).'</lastmod>'."\n";
			$sitemap .= '<changefreq>'.$row->changefreq.'</changefreq>'."\n";
			$sitemap .= '<priority>'.$row->priority.'</priority>'."\n";
			$sitemap .= '</url>'."\n";
		}

		
		$sitemap .= '</urlset>'."\n";

		echo $sitemap;
		//$plik = fopen('/text.txt', 'w');
		$file = 'sitemap.xml';
		$current = file_get_contents($file);
		// Append a new person to the file
		$current = $sitemap;
		// Write the contents back to the file
		file_put_contents($file, $current);

		redirect('seo/links');

	}


	
}
