<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oferta extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		if(!isset($_SESSION['login']))
		{
			redirect('panel/login');
		}
	}
	



	public function index()
	{	
		
		$data['data'] = $this->users_m->get('produkty');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/main_page',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

	}

	public function add()
	{	


		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/add');
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
		$save = $this->input->post('save');
		if(isset($save))
		{


			$insert['name'] = $this->input->post('name');
			$insert['img'] = $this->input->post('img');
			$insert['img'] = str_replace(" ", "_", $insert['img']);
			$insert['alt'] = $this->input->post('alt');

			
			$this->users_m->insert('produkty', $insert);


			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś podstronę!</p>');
				
			redirect('oferta');

		}

	}

	public function edit($id)
	{	
		
		$data['data'] = $this->users_m->get_element('produkty',$id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/edit',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
		$save = $this->input->post('save');
		if(isset($save))
		{


			$update['name'] = $this->input->post('name');
			$update['img'] = $this->input->post('img');
			$update['img'] = str_replace(" ", "_", $update['img']);
			$update['alt'] = $this->input->post('alt');

			$this->db->where('produkty_id', $id);
			$this->db->update('produkty', $update);

			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie wprowadziłeś zmiany!</p>');
				
			redirect('oferta');

		}

	}



	public function delete($id)
	{

		$id = $this->uri->segment(3);
		$where = array('produkty_id' => $id);
		$this->users_m->delete('produkty' , $where);

		$where_add = array('oferta_id' => $id);
		$this->users_m->delete('komponenty' , $where_add);

		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś podstronę!</p>');
		redirect('oferta');
	}









	public function tresc($id)
	{	
		
		$data['tresc'] = $this->users_m->get_dane($id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/main_page_dane.php',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

	}

	public function add_tresc()
	{	
		$id = $this->uri->segment(3);
		$typ = $this->uri->segment(4);
		if($typ!=4){
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			if($typ == 1){
			$this->load->view('back/components/uniwersalne/text/add');
			}
			if($typ == 2){
			$this->load->view('back/components/uniwersalne/img/add');
			}
			if($typ == 3){
			$this->load->view('back/components/uniwersalne/img_text/add');
			}
			$this->load->view('back/blocks/javascript');
			$this->load->view('back/blocks/footer');
			$save = $this->input->post('save');
			if(isset($save))
			{

				$insert['typ'] = $typ;
				$insert['oferta_id'] = $id;
				$insert['img'] = $this->input->post('img');
				$insert['alt'] = $this->input->post('alt');
				$insert['text'] = $this->input->post('text');
				$insert['text'] = str_replace("<p>","",$insert['text']);
				$insert['text'] = str_replace("</p>","<br>",$insert['text']);
				$insert['text'] = str_replace('font-size: 0.875rem;',"",$insert['text']);

				$query = $this->users_m->get_max_of_priority('komponenty',$id);
				$position = $query->priority;
				$position++;
				$insert['priority'] = $position;

				$text = $this->input->post('text');
				$img = $this->input->post('img');
				if ($text != '' || $img != '' ) {
				$this->users_m->insert('komponenty', $insert);


				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś treść!</p>');
				$redirect = 'oferta/tresc/'.$id.'';
				redirect($redirect);
				}else{
				$this->session->set_flashdata('success', '<p class="text-danger font-weight-bold">Nie wprowadziłeś żadnych danych! Spróbuj jeszcze raz!</p>');
				$redirect = 'oferta/tresc/'.$id.'';
				redirect($redirect);
				}

			}
		}else{
			$insert['typ'] = $typ;
			$insert['oferta_id'] = $id;
			$query = $this->users_m->get_max_of_priority('komponenty',$id);
			$position = $query->priority;
			$position++;
			$insert['priority'] = $position;

			$this->users_m->insert('komponenty', $insert);


			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś treść!</p>');
			$redirect = 'oferta/tresc/'.$id.'';
			redirect($redirect);
		}

	}


		public function add_photo()
	{		
			$galeria_id = $this->uri->segment(3); //id galerii
			$tresc_id = $this->uri->segment(4); // id głownej treści

			$data['galeria'] = $this->users_m->get_galeria($galeria_id);

			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/components/uniwersalne/galeria/main_page',$data);
			$this->load->view('back/blocks/javascript');
			$this->load->view('back/blocks/footer');
			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$insert['galeria_id'] = $galeria_id;
				$insert['alt'] = $this->input->post('alt');
				$insert['img'] = $this->input->post('img');
				$insert['img'] = str_replace(" ", "_", $insert['img']);
				$insert['rozmiar'] = $this->input->post('rozmiar');

				
				

				$this->users_m->insert('komponenty_galeria', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie dodałeś zdjęcie!</p>');
					
				redirect('oferta/add_photo/'.$galeria_id.'/'.$tresc_id.'');


			}

	}

		public function delete_photo($id)
	{

		$galeria_id = $this->uri->segment(3); //id galerii
		$tresc_id = $this->uri->segment(4); // id głownej treści
		$id = $this->uri->segment(5); // id do usunięcia
		$where = array('id' => $id);
		$this->users_m->delete('komponenty_galeria' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś zdjęcie!</p>');

		redirect('oferta/add_photo/'.$galeria_id.'/'.$tresc_id.'');
	}

	public function edit_tresc()
	{	
		$id = $this->uri->segment(4);
		$oferta = $this->uri->segment(3);
		$data['dane'] = $this->users_m->getelement('komponenty',$id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/edit_dane',$data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
		$save = $this->input->post('save');
		if(isset($save))
		{


			$update['img'] = $this->input->post('img');
			$update['alt'] = $this->input->post('alt');
			$update['text'] = $this->input->post('text');
			$update['text'] = str_replace("<p>","",$update['text']);
			$update['text'] = str_replace("</p>","<br>",$update['text']);
			$update['text'] = str_replace('font-size: 0.875rem;',"",$update['text']);


			$text = $this->input->post('text');
			$img = $this->input->post('img');
			if ($text != '' || $img != '' ) {
			$this->db->where('id', $id);
			$this->db->update('komponenty', $update);


			$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zmieniłeś treść!</p>');
			$redirect = 'oferta/tresc/'.$oferta.'';
			redirect($redirect);
			}else{
			$this->session->set_flashdata('success', '<p class="text-danger font-weight-bold">Nie wprowadziłeś żadnych danych! Spróbuj jeszcze raz!</p>');
			$redirect = 'oferta/tresc/'.$oferta.'';
			redirect($redirect);
			}

		}

	}


	public function update_alt($id)
	{	
		
		
			$update['alt'] = $this->input->post('element');

			$this->db->where('id', $id);
			$this->db->update('komponenty_galeria', $update);


		}


	public function upPriority_tresc()  
      {  
		$data['row'] = $this->users_m->getelement('komponenty', $_POST['id']);
		$data['check'] = $this->users_m->get_sort_up_komponenty('komponenty',$_POST['oferta_id']);
		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority < $key->priority)				

				{
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('komponenty', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('komponenty', $update2);			


      }  
	public function downPriority_tresc()  
      {  
		$data['row'] = $this->users_m->getelement('komponenty', $_POST['id']);
		$data['check'] = $this->users_m->get_sort_down_komponenty('komponenty',$_POST['oferta_id']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority > $key->priority)				

				{
					$old_priority = $new_priority;
					$old_id = $key->id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where('id', $_POST['id']);
				$this->db->update('komponenty', $update);		
		$update2['priority'] = $old_priority;
				$this->db->where('id', $old_id);
				$this->db->update('komponenty', $update2);			

      }



	public function delete_tresc($id)
	{

		$id = $this->uri->segment(4);
		$powrot = $this->uri->segment(3);
		$where = array('id' => $id);
		$this->users_m->delete('komponenty' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś treść!</p>');
		$redirect = 'oferta/tresc/'.$powrot.'';
		redirect($redirect);
	}



	
	


	
}
