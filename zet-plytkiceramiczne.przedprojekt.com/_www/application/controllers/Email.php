	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	
	public function index()
	{
		
		$data['data'] = $this->users_m->get('email');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/email/main_page', $data);
		$this->load->view('back/blocks/footer');

	}
	

	public function display($id)
	{
		
		$data['row'] = $this->users_m->get_element('email',$id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/email/display', $data);
		$this->load->view('back/blocks/footer');

	}


	public function delete($id)
	{

		$id = $this->uri->segment(3);
		$where = array('email_id' => $id);
		$this->users_m->delete('email' , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś formularz!</p>');
		redirect('email');
	}

}
?>