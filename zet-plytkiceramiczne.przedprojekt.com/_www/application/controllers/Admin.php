<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {

		$this->load->view('back/blocks/session'); //blok odpowiedzialny za sesję ( czy zalogowany)
		$this->load->view('back/blocks/head'); //linki meta itd
		$this->load->view('back/blocks/header'); //menu
		$this->load->view('back/main/main_page'); //glowne cialo
		$this->load->view('back/blocks/footer'); //stopka
	}

public function delete($table,$id) //uniwersalne usuwanie 1-parametr to tabela a 2-parametr to id które usuwamy. !!!!WAŻNE pole klucza w atbeli musi mieć nazwę: nazwa_tabeli_id!!!
	{
		$where = array($table.'id' => $id);
		$this->users_m->delete($table , $where);
		$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś dane!</p>');
		redirect($table);
	}
}
