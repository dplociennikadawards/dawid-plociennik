<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	
	public function index($id)
	{

		$data['data'] = $this->users_m->get_element('ustawienia', $id);

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/ustawienia/main_page', $data);
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$update['telefon'] = $this->input->post('telefon');
				$update['telefon_2'] = $this->input->post('telefon_2');
				$update['email'] = $this->input->post('email');
				$update['email_k'] = $this->input->post('email_k');
				$update['adres'] = $this->input->post('adres');

				
				

				$this->db->where('ustawienia_id', $id);
				$this->db->update('ustawienia', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('ustawienia/index/'.$id.'');

			}

	}
	
}
