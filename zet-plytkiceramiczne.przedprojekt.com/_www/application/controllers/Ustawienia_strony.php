<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia_strony extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	
	public function index()
	{

		$data['data'] = $this->users_m->get_element('ustawienia_strony', '1');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/ustawienia_strony/main_page', $data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');

			$save = $this->input->post('save');
			
			if(isset($save))
			{

				
				$update['description'] = $this->input->post('description');
				$update['keywords'] = $this->input->post('keywords');
				$update['title'] = $this->input->post('title');
				$update['polityka'] = $this->input->post('img');
				$update['polityka'] = str_replace(" ", "_", $update['polityka']);

				
				
				

				$this->db->where('ustawienia_strony_id', '1');
				$this->db->update('ustawienia_strony', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Pomyślnie zaktualizowałeś dane!</p>');
					
				redirect('ustawienia_strony');

			}

	}
	
}
