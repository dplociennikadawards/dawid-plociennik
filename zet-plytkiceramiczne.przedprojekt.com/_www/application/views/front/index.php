



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo base_url(); ?>p/produkty" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            Zapoznaj się z ofertą
          </a>
        </h4>  
        <div class="offer__row">

          <div class="offer__maincell">
            <div class="offer__title-box">
              <h4 class="offer__desc">płytki ŁAZIENKOWE I PODłOGOWE</h4>

              <a class="offer__link" href="#">
                <button class="offer__btn offer__btn--maincell">CZYTAJ więcej</button>
              </a>
            </div>
          </div>
          <div class="offer__vertical-col">
            <div class="offer__cell-vertical offer__cell-vertical--margin-bottom offer__cell-vertical--img1">

              <div class="offer__layer">
                <h5 class="offer__desc">chemia budowlana</h5>
                <span class="offer__separator"></span>
                <p class="offer__paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore ...</p>
                <a class="offer__link" href="#">
                  <button class="offer__btn">czytaj więcej</button>
                </a>
              </div>

              <h4 class="offer__desc offer__desc--small">chemia budowlana</h4>
            </div>
            <div class="offer__cell-vertical offer__cell-vertical--img2">

              <div class="offer__layer">
                <h5 class="offer__desc">płytki na schody</h5>
                <span class="offer__separator"></span>
                <p class="offer__paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore ...</p>
                <a class="offer__link" href="#">
                  <button class="offer__btn">czytaj więcej</button>
                </a>
              </div>

              <h4 class="offer__desc offer__desc--small">płytki na schody</h4>
            </div>
          </div>
        </div>
        <div class="offer__row">

          <div class="offer__smcell offer__smcell--img3">
            <div class="offer__layer">
              <h5 class="offer__desc">ceramika sanitarna</h5>
              <span class="offer__separator"></span>
              <p class="offer__paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore ...</p>
              <a class="offer__link" href="#">
                <button class="offer__btn">czytaj więcej</button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small">ceramika sanitarna</h4>
          </div>

          <div class="offer__smcell offer__smcell--img4">
            <div class="offer__layer">
              <h5 class="offer__desc">baterie</h5>
              <span class="offer__separator"></span>
              <p class="offer__paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore ...</p>
              <a class="offer__link" href="#">
                <button class="offer__btn">czytaj więcej</button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small">baterie</h4>
          </div>

          <div class="offer__smcell offer__smcell--img5">
            <div class="offer__layer">
              <h5 class="offer__desc">kabiny prysznicowe</h5>
              <span class="offer__separator"></span>
              <p class="offer__paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore ...</p>
              <a class="offer__link" href="#">
                <button class="offer__btn">czytaj więcej</button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small">kabinY prysznicowe</h4>
          </div>

        </div>
      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">poznaj</span> naszą firmę</h4>
        <span class="standard-sect__separator"></span>

        <div class="row">
          <div class="col-lg-6">
            <p class="standard-sect__paragraph">
              <b>Od początku lat 90-tych zajmujemy sie branżą cermiczną, import z Włoch, Hiszpani, Niemiec ,Czech i
                innych
                krajów.</b>
              <br>
              <br>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur.
            </p>
          </div>
          <div class="col-lg-6">
            <p class="standard-sect__paragraph">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam.
              <br>
              <br>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
              ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur.
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>