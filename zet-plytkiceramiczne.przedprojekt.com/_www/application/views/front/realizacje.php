



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo base_url(); ?>p/produkty" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            Zapoznaj się z ofertą
          </a>
        </h4>        

        <h4 class="standard-sect__title"><span class="standard-sect__bold">Nasze realizacje</span></h4>
        <span class="standard-sect__separator"></span>
      </div>
      <div class="container-fluid">
        
      <div id="mdb-lightbox-ui"></div>


          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="container">


                  <!-- lightbox start -->
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="standard-sect__title"><span class="standard-sect__bold">Beton</span></h4>
                        <div class="mdb-lightbox">

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid">
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                        </div>

                      </div>
                    </div>
                  <!-- lightbox end -->


                </div>
              </div>
              <div class="carousel-item">
                


<div class="container">
  <!-- lightbox start -->
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="standard-sect__title"><span class="standard-sect__bold">Kamień</span></h4>
                        <div class="mdb-lightbox">

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid">
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                        </div>

                      </div>
                    </div>
                  <!-- lightbox end -->
</div>




              </div>
              <div class="carousel-item">
                



                <div class="container">
  <!-- lightbox start -->
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="standard-sect__title"><span class="standard-sect__bold">Drewno</span></h4>
                        <div class="mdb-lightbox">

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/184/1/colour_bathroom_1_mp,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid">
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/176/1/soft_romantic_mp_1_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/178/1/calm_organic_bathroom_contemporary_1_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/179/1/arctic_storm_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/180/1/mysticcemento_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                          <figure class="col-md-4">
                            <a href="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" data-size="1600x1067">
                              <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/181/1/mystic_cemento_bathroom_mp_small,rIKK6menpVrZqcjaWqSZ.jpg" class="img-fluid" />
                            </a>
                          </figure>

                        </div>

                      </div>
                    </div>
                  <!-- lightbox end -->
</div>



              </div>
            </div>
            <a class="carousel-control-prev rgba-green-strong" href="#carouselExampleControls" role="button" data-slide="prev" style="width: 10%;">
              <span class="carousel-control-prev-icon" aria-hidden="true" style="height: 50px; width: 50px;"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next rgba-green-strong" href="#carouselExampleControls" role="button" data-slide="next" style="width: 10%;">
              <span class="carousel-control-next-icon" aria-hidden="true" style="height: 50px; width: 50px;"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>