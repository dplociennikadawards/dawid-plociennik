



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo base_url(); ?>p/produkty" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            Zapoznaj się z ofertą
          </a>
        </h4>  
        <div class="offer__row">
          <div class="container">
            <h4 class="standard-sect__title"><span class="standard-sect__bold">poznaj</span> naszą firmę</h4>
            <span class="standard-sect__separator"></span>

            <div class="row">
              <div class="col-lg-6 bg-photo border-right" style="background-image: url(<?php echo base_url(); ?>assets/front/img/cell1.jpg);">
              </div>
              <div class="col-lg-6 pt-3">
                <p class="standard-sect__paragraph">
                  <b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam.</b>
                  <br>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                  fugiat nulla pariatur.
                </p>
              </div>
              <div class="col-lg-6 pt-3">
                <p class="standard-sect__paragraph">
                  <b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam.</b>
                  <br>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                  fugiat nulla pariatur.
                </p>
              </div>
              <div class="col-lg-6 bg-photo  border-left" style="background-image: url(<?php echo base_url(); ?>assets/front/img/cell4.jpg);">
              </div>
              <div class="col-lg-6 bg-photo border-right-2" style="background-image: url(<?php echo base_url(); ?>assets/front/img/cell3.jpg);">
              </div>
              <div class="col-lg-6 pt-3">
                <p class="standard-sect__paragraph">
                  <b>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam.</b>
                  <br>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                  fugiat nulla pariatur.
                </p>
              </div>
              <div class="col-lg-12 my-3 bg-photo" style="background-image: url(<?php echo base_url(); ?>assets/front/img/TŁO.jpg);">
              </div>
              <div class="col-lg-12 mb-3">
                <p class="standard-sect__paragraph">
                  <b>Od początku lat 90-tych zajmujemy sie branżą cermiczną, import z Włoch, Hiszpani, Niemiec ,Czech i
                    innych
                    krajów.</b>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam.
                  <br>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                  fugiat nulla pariatur.
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam.
                  <br>
                  <br>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                  fugiat nulla pariatur.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="standard-sect">
      <div class="container-fluid">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">Nasze ostatnie realizacje</span></h4>
        <span class="standard-sect__separator"></span>
        <!-- <div class="owl-carousel owl-theme">
          <div class="items">
            <div class="row">
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
            </div>
          </div>
        </div> -->


<div id="test" class="owl-carousel owl-theme owl-carousel-2">
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell5.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell4.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell5.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell5.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
    <div class="item">
      <div style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg'); height: 250px; margin-left: 20px; margin-right: 20px;"></div>
    </div>
</div>


      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>


