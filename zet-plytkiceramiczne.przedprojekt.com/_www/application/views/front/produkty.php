



  <main>
<!-- Section: Blog v.3 -->
<section class="my-5">
<div class="container">
  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">Produkty firmy Zet</h2>
  <!-- Section description -->
  <p class="text-center dark-grey-text mx-auto mb-5">Zet jest znaną i cenioną marką wyposażenia łazienek nie tylko w Polsce, ale i za granicą.  Jako jedyna oferuje swoim Klientom tak szeroki asortyment i gotowe, kompleksowe rozwiązania do łazienek, łączące w sobie funkcjonalność, wygodę, wysoką jakość i dobry styl. Zet korzysta z najnowszej wiedzy technologicznej i przekłada ją na innowacyjne rozwiązania, które dostarcza swoim konsumentom. W ramach oferowanych kolekcji wprowadza szereg pomysłów funkcjonalnych, które ułatwiają utrzymanie czystości i zapewniają higienę na najwyższym poziomie. Występuje w roli doradcy, który podpowiada jak urządzić wnętrze w optymalny sposób i proponuje rozwiązania, które będą przyjazne zarówno na etapie urządzania, jak i użytkowania.</p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url('https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,sX2P6mqgpVrZqcjaWqSZ.jpg');">
        <a href="<?php echo base_url(); ?>p/produkt">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/produkt" class="text-dark">STAY CLASSY</a></strong></h3>
      <h3 class="mb-3">Błyszcząca klasyka</h3>
      <!-- Excerpt -->
      <p class="dark-grey-text">Stay Classy to kolekcja prezentująca nowoczesne oblicze szlachetnego marmuru. Kunsztowne odwzorowanie naturalnego kamienia na płytce idealnie wpisuje się w najnowsze trendy wnętrzarskie, w których odwoływanie się do elementów ze świata natury odgrywa kluczową rolę.</p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/produkt" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url('https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/175/1/touchme_cersanit_bathroom_contemporary_1_mp_small,sX2P6mqgpVrZqcjaWqSZ.jpg');">
        <a href="<?php echo base_url(); ?>p/produkt">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong><a href="<?php echo base_url(); ?>p/produkt" class="text-dark">TOUCH ME</a></strong></h3>
      <h3 class="mb-3">Efekt minimalistycznej elegancji w 3D</h3>
      <!-- Excerpt -->
      <p class="dark-grey-text">Wyjątkowa kolekcja Touch Me to propozycja, którą bez reszty pokochają zwolennicy nowoczesnej elegancji. Subtelne wzory uwypuklone na delikatnej satynie płytek pozwolą stworzyć minimalistyczne wnętrze pełne harmonii i spokoju, które otuli nas aksamitną taflą i pozwoli na chwile pełne relaksu.</p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/produkt" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url('https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/177/1/lovely_white_mp_small,sX2P6mqgpVrZqcjaWqSZ.jpg');">
        <a href="<?php echo base_url(); ?>p/produkt">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong><a href="<?php echo base_url(); ?>p/produkt" class="text-dark">LOVELY WHITE</a></strong></h3>
      <h3 class="mb-3">Biel w geometrycznym wydaniu</h3>
      <!-- Excerpt -->
      <p class="dark-grey-text">Awangardowy wystrój łazienki nawiązujący do naturalnych materiałów? Z kolekcją Lovely White stworzysz niebanalne wnętrze doskonale wpisujące się w najnowsze trendy.</p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/produkt" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->
</div>
</section>
<!-- Section: Blog v.3 -->

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>