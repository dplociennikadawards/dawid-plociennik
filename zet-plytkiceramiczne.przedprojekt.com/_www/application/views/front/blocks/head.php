<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>ZET Płytki ceramiczne</title>
  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans|Rubik:700" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- owl carousel -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/assets/owl.theme.default.min.css">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/front/css/mediaqueries.css" rel="stylesheet" type="text/css">


  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.carousel.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.theme.default.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.theme.green.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/core.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/animate.scss">


</head>
<body>