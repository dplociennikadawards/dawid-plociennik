  <header style="border-bottom: 3px solid #7BA543;">


    <nav class="navbar navbar-expand-lg navbar-light">

      <div class="navbar__container container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img class="navbar__brand" src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="logo"></a>

        <div class="navbar__socialmedia-box navbar__socialmedia-box--mobile">
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="facebook">
          </a>
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/twitter.png" alt="twitter">
          </a>
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/instagram.png" alt="instagram">
          </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link">Home</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>">home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">O firmie</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/ofirmie">o firmie</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Produkty</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/produkty">produkty</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">realizacje</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/realizacje">realizacje</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Dla projektanta</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/dlaprojektanta">dla projektanta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">kontakt</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
            </li>
          </ul>
        </div>
        <div class="navbar__socialmedia-box navbar__socialmedia-box--desktop">
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="facebook">
          </a>
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/twitter.png" alt="twitter">
          </a>
          <a class="navbar__sociallink" href="#">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/instagram.png" alt="instagram">
          </a>
        </div>
      </div>

    </nav>

<style type="text/css">
	.item__background {
		height: 500px;
	}
  .owl-nav {
    display: none;
  }
  .owl-dots {
    display: none;
  }
</style>

    <div class="">

      <div class="">
        <div class="item item__background item__background--1">

          <div class="item__container container">
            <div class="item__content-box">
              <h4 class="item__title">SZEROKA OFERTA</h4>
              <h4 class="item__subtitle">PŁYTEK CERAMICZNYCH</h4>

              <p class="item__paragraph mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor
                incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation.</p>
            </div>
          </div>

        </div>
      </div>
    </div>


  </header>