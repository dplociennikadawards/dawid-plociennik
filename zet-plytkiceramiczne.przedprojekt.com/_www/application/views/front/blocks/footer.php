
  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 footer__col">
          <div><img class="footer__logo" src="<?php echo base_url(); ?>assets/front/img/ft-logo.png" alt="logo"></div>

          <p class="footer__paragraph">
            ZET Płytki Ceramiczne<br>
            Ul. Ceramiczna 25/6 Warszawa 65-000
          </p>
        </div>
        <div class="col-lg-8 footer__col">
          <ul class="footer__links">
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">Home</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">home</a>
            </li>
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">O firmie</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">o firmie</a>
            </li>
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">Produkty</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">produkty</a>
            </li>
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">realizacje</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">realizacje</a>
            </li>
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">Dla projektanta</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">dla projektanta</a>
            </li>
            <li class="footer__item">
              <a class="footer__link footer__link--hidden">kontakt</a> <!-- <-- hidden -->
              <a class="footer__link" href="#">kontakt</a>
            </li>
          </ul>

          <div class="footer__newsletter d-flex justify-content-end align-items-center">
            <p class="footer__paragraph">
              Chcesz wiedzieć więcej?<br>
              <span class="footer__bold">Zapisz się do Newslettera</span>
            </p>


            <form class="footer__form" action="" method="get">
              <input class="footer__input" type="email" name="" id="" placeholder="Podaj adres e-mail">
              <input class="footer__submit" type="submit" value="wyślij">
            </form>
          </div>

        </div>
      </div>

      <div class="footer__copyrights-bar">
        <span class="footer__copyright">
          2019 © Kompleks home Wszystkie prawa zastrzeżone
        </span>

        <span class="footer__copyright">
          Projekt i wdrożenie <a class="copyrights__link" href="#">AdAwards</a>
        </span>
      </div>
    </div>
  </footer>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
  <!-- owl carousel -->
  <script src="<?php echo base_url(); ?>assets/front/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/owl-init.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.carousel.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.animate.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autoheight.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autoplay.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autorefresh.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.hash.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.lazyload.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.navigation.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.support.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.video.js"></script>



<script type="text/javascript">

$(function () {
$("#mdb-lightbox-ui").load("<?php echo base_url() ?>assets/front/mdb-addons/mdb-lightbox-ui.html");
});

</script>

<script type="text/javascript">
    var mapsAPI = "AIzaSyCJ4dpcfW41zqwNu_lJ4EcT7ON-SNXDZrA";
  $.getScript('https://maps.google.com/maps/api/js?key=' + mapsAPI).done(function() {
    initMap();
  });

  function initMap() {
  <?php foreach ($miasta as $row) : ?>
  var miasto<?php echo $row->miasta_id; ?> = new google.maps.LatLng(<?php echo $row->lat; ?>,<?php echo $row->lng; ?> );
   // var   pointB = new google.maps.LatLng(52.919438, 19.145136);
  <?php endforeach; ?>
  <?php foreach ($miasta as $row) : ?>
    myOptions = {
      zoom: 5,
      center: miasto109

    },
    <?php endforeach; ?>
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
     <?php foreach ($miasta as $row) : ?>
    miasto<?php echo $row->miasta_id; ?> = new google.maps.Marker({
      position: miasto<?php echo $row->miasta_id; ?>,
      title: "<?php echo $row->nazwa; ?>",
      label: "",
      map: map
    });
    <?php endforeach; ?>
    //  markerB = new google.maps.Marker({
    //   position: pointB,
    //   title: "Marker B",
    //   label: "B",
    //   map: map
    // });
    <?php foreach ($miasta as $row) : ?>
  miasto<?php echo $row->miasta_id ?>.addListener('click', function(e) {
    map.setCenter(this.position);
    window.location.href = "<?php echo base_url(); ?>p/transport_zmarlych?miasto=<?php echo $row->lokalizacja; ?>";
  });
   <?php endforeach; ?>
}
</script>


</body>

</html>