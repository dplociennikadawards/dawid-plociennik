



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title"><img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">Zapoznaj się z
          ofertą</h4>
            <h4 class="standard-sect__title"><span class="standard-sect__bold">Mapa</span> naszych projektantów</h4>
            <span class="standard-sect__separator"></span>
        <div class="offer__row">

          <div class="col-md-12 px-0">
            <div id="map-canvas" style="width: 100%;height: 500px;"></div>
          </div>

        </div>

        <div class="offer__row mt-5">

          <div class="col-md-4 bg-photo"  style="background-image: url(<?php echo base_url(); ?>assets/front/img/cell4.jpg);">
          </div>

          <div class="col-md-8">
            <p class="standard-sect__paragraph">
              <b>Wykonujemy projekty łazienek , salonów i kuchni drogą elektroniczną. Koszt wykonania projektu ustalamy indywidualnie. Ceny są bardzo atrakcyjne i są podawane za metr kwadratowy powierzchni podłogi łazienki. Projekt obejmuje płytki oraz inne elementy wyposazenia zgodnie z Państwa sugestiami i życzeniami.</b>
              <br>
              <br>
              Otrzymacie Państwo wykaz asortymentu płytek zgodnie z projektem oraz wycenę płytek w naszym sklepie internetowym wykonanym przez naszych najlepszych projektantów.
            </p>
          </div>

        </div>

      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>