



  <main>

    <div class="container">
      
<!-- Section: Blog v.4 -->
<section class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Card -->
      <div class="card card-cascade wider reverse">

        <!-- Card image -->
        <div class="view view-cascade overlay bg-photo" style="background-image: url('https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,sX2P6mqgpVrZqcjaWqSZ.jpg'); height: 500px;">
            <div class="mask rgba-white-slight"></div>
        </div>

        <!-- Card content -->
        <div class="card-body card-body-cascade text-center">

          <!-- Title -->
          <h2 class="font-weight-bold">STAY CLASSY</h2>
          <h2>Błyszcząca klasyka</h2>


        </div>
        <!-- Card content -->

      </div>
      <!-- Card -->

      <!-- Excerpt -->
      <div class="row mt-5">
        <div class="col-md-12">
          <p class="font-weight-bold">
            Stay Classy to kolekcja prezentująca nowoczesne oblicze szlachetnego marmuru. Kunsztowne odwzorowanie naturalnego kamienia na płytce idealnie wpisuje się w najnowsze trendy wnętrzarskie, w których odwoływanie się do elementów ze świata natury odgrywa kluczową rolę.
          </p>
          <p>
            W kolekcji znajdziemy przepięknie żyłkowane, perfekcyjnie oddające rysunek luksusowego marmuru płytki bazowe, które dodadzą naszemu wnętrzu szyku i ponadczasowej elegancji. Dostępne w dwóch wariantach - gładkim oraz strukturalnym - będą idealnie współgrały z listwą w głębokim, miedzianym kolorze, również dostępną w kolekcji. Marmurowy total look uzyskamy wykorzystując w aranżacji płytkę podłogową w formacie 42x42, z grafiką identyczną jak ta widoczna na płytce bazowej. Surowe oblicze kamienia ociepli zaś płytka gresowa inspirowana naturalnym drewnem, która wprowadzi do wnętrza nieco przytulności.
          </p>
        </div>
        <div class="col-md-4 bg-photo">
          <img src="https://www.cersanit.com.pl/gfx/opoczno/_thumbs/pl/produktyaranzacje/174/1/stay_classy_bathroom_mp_small,sX2P6mqgpVrZqcjaWqSZ.jpg" class="img-fluid">
        </div>
        <div class="col-md-8">
          <p class="font-weight-bold">
            Stay Classy to kolekcja prezentująca nowoczesne oblicze szlachetnego marmuru. Kunsztowne odwzorowanie naturalnego kamienia na płytce idealnie wpisuje się w najnowsze trendy wnętrzarskie, w których odwoływanie się do elementów ze świata natury odgrywa kluczową rolę.
          </p>
          <p>
            W kolekcji znajdziemy przepięknie żyłkowane, perfekcyjnie oddające rysunek luksusowego marmuru płytki bazowe, które dodadzą naszemu wnętrzu szyku i ponadczasowej elegancji. Dostępne w dwóch wariantach - gładkim oraz strukturalnym - będą idealnie współgrały z listwą w głębokim, miedzianym kolorze, również dostępną w kolekcji. Marmurowy total look uzyskamy wykorzystując w aranżacji płytkę podłogową w formacie 42x42, z grafiką identyczną jak ta widoczna na płytce bazowej. Surowe oblicze kamienia ociepli zaś płytka gresowa inspirowana naturalnym drewnem, która wprowadzi do wnętrza nieco przytulności.
          </p>
        </div>
      </div>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->
        <div class="owl-carousel owl-theme">
          <div class="items">
            <div class="row">
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell5.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell4.jpg');"></h4>
              </div>
            </div>
          </div>
        </div>
</section>
<!-- Section: Blog v.4 -->


    </div>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">producenci</span></h4>
        <span class="standard-sect__separator"></span>

        <div class="standard-sect__row">
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/1280px-Hitachi_inspire_the_next-Logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/Steinel_(Unternehmen)_logo.svg.png" alt=""></div>
          <div><img class="standard-sect__brand" src="<?php echo base_url(); ?>assets/front/img/2000px-Dremel_logo.svg.png" alt=""></div>
        </div>
      </div>
    </section>
  </main>