<script type="text/javascript"> //edytor tekstu po dodaniu klasy css ['summernote'] będzie aktywny
$( document ).ready(function() {
$($(".summernote").summernote({
      height: 250,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        },
                  toolbar: [
                ["style", ["style"]],
                ["font", ["bold", "underline", "clear"]],
                //["fontname", ["fontname"]],
                ['fontsize', ['fontsize']],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["table", ["table"]],
                ["insert", ["link", "picture", "video"]],
                ["view", ["fullscreen", "codeview", "help"]]
            ],
    }));
});
</script>


<script type="text/javascript">//dodawanie zdjęcia przez ajaxa do kontrolera Ajax_upload.php
 $(document).ready(function(){  
      $('#upload_form').change('#image_file', function(e){  
           e.preventDefault();  
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/index",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { //przed dodaniem
                      document.getElementById('spinner').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  //success po dodaniu
                     {  
                        $('#uploaded_image').html(data);  
                     },
                    complete:function(data) { //zakonczenie funkcji
                        document.getElementById('rozdzielczosc').value = document.getElementById('send_resolution').value;
                        document.getElementById('zdjecie').value = document.getElementById('name_photo').value;
                        document.getElementById('spinner').innerHTML = '';
                     }     
                });  
      });  
 });  

</script>





<!-- reszta upload formów -->

<script type="text/javascript">
 $(document).ready(function(){  
      $('#upload_form_gallery').change('#image_file', function(e){  
           e.preventDefault();  
                $.ajax({  
                     url:"<?php echo base_url(); ?>ajax_upload/index",   
                     //base_url() = http://localhost/tutorial/codeigniter  
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     beforeSend: function() { 
                      document.getElementById('spinner').innerHTML = '<div class="sk-folding-cube mb-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                     success:function(data)  
                     {  
                        $('#uploaded_image').html(data);  
                        
                     },
                    complete:function(data) {
                        document.getElementById('rozdzielczosc').value = document.getElementById('send_resolution').value;
                        document.getElementById('zdjecie').value = document.getElementById('name_photo').value;
                        document.getElementById('spinner').innerHTML = '';
                     }     
                });  
      });  
 });  

</script>
<!-- reszta upload formów -->







<script type="text/javascript">

  function formatDate()
    {  
      var date = document.getElementsByName('date')[0].value;
      var mouth = date.substring(0, 2);
      var day = date.substring(3, 5);
      var year = date.substring(6, 10);
      document.getElementsByName('date')[0].value = day + '.' + mouth + '.' + year;
    }


</script>


<script type="text/javascript">
    function delete_photo()//usuwanie zdjęcia z inputa - czyszczenie go!
  {
    document.getElementById('uploaded_image').innerHTML = '';
    document.getElementById('zdjecie').value = '';
  }
</script>