<body class="">

<div id="preloader">
    <div id="status">
<!--       <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
      </div> -->
<!-- <div class="sk-rotating-plane bg-gray-800"></div> -->
    <div class="sk-double-bounce">
        <div class="sk-child sk-double-bounce1 bg-gray-800"></div>
        <div class="sk-child sk-double-bounce2 bg-gray-800"></div>
    </div>    
  </div>
</div>

    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo">
      <a href="<?php echo base_url(); ?>" class="mx-auto">
        <img src="<?php echo base_url(); ?>assets/back/img/logoAdAwards.png" width="75">
      </a>
    </div>
    <div class="br-sideleft overflow-y-auto green-dark-bg">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Menu</label>
      <!-- br-sideleft-menu -->
      <ul class="br-sideleft-menu">

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>admin" class="br-menu-link <?php if($this->uri->segment(1)=="admin"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-home tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Home</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub show-sub <?php if($this->uri->segment(1)=="seo"){echo "active";}?>">
            <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
            <span class="menu-item-label">&nbsp;Strona główna</span>
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub">
            <li class="sub-item"><a href="<?php echo base_url(); ?>slider" class="sub-link <?php if($this->uri->segment(1)=="slider"){echo "active";}?>">Slider</a></li>
          </ul>
          </ul>
        </li>

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>oferta" class="br-menu-link <?php if($this->uri->segment(1)=="oferta"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-cog tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Mini zarządzenie treścią</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>social" class="br-menu-link <?php if($this->uri->segment(1)=="social"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-cog tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Social Media</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>email" class="br-menu-link <?php if($this->uri->segment(1)=="email"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-cog tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Formularze kontaktowe</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>ustawienia/index/1" class="br-menu-link <?php if($this->uri->segment(1)=="ustawienia"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-cog tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Ustawienia</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub show-sub <?php if($this->uri->segment(1)=="seo"){echo "active";}?>">
            <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
            <span class="menu-item-label">&nbsp;SEO</span>
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub">
            <li class="sub-item"><a href="<?php echo base_url(); ?>seo/ustawienia_strony" class="sub-link <?php if($this->uri->segment(2)=="ustawienia_strony"){echo "active";}?>">Podstawowe ustawienia</a></li>
            <li class="sub-item"><a href="<?php echo base_url(); ?>seo/keys" class="sub-link <?php if($this->uri->segment(1)=="slider"){echo "active";}?>">Klucze do Google</a></li>
            <li class="sub-item"><a href="<?php echo base_url(); ?>seo/robots" class="sub-link <?php if($this->uri->segment(1)=="slider"){echo "active";}?>">robots.txt</a></li>
            <li class="sub-item"><a href="<?php echo base_url(); ?>seo/links" class="sub-link <?php if($this->uri->segment(1)=="links"){echo "active";}?>">Sitemap</a></li>
          </ul>
          </ul>
        </li>

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/logout" class="br-menu-link">
            <i class="menu-item-icon icon ion-power tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Wyloguj</span>
          </a>
        </li><!-- br-menu-item -->

      </ul><!-- br-sideleft-menu -->

      <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->


    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>

      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo $_SESSION['name']; ?></span>
              <img src="<?php echo $ico; ?>" class="wd-32 rounded" alt="">
              <span class="square-10 bg-success"></span>
            </a>
          </div>
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->