<!-- <?php echo base_url(); ?>application/views/template/<!DOCTYPE html> -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title><?php echo $page_title; ?> - Panel administracyjny</title>

    <!-- vendor css -->
    <link href="<?php echo base_url() ?>/assets/back/template/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/back/template/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/back/template/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/back/template/css/bracket.css">
  </head>

  <body>

    <form action="" method="post">
      <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

        <div class="login-wrapper wd-300 wd-xs-400 pd-25 pd-xs-40 bg-white rounded shadow-base">
          <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">[</span> AD <span class="tx-info">Panel</span> <span class="tx-normal">]</span></div><br>
          <?php echo validation_errors(); ?>
          <div class="form-group">
            <input name="name" type="text" class="form-control" placeholder="Podaj swoję imię" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input name="login" type="text" class="form-control" placeholder="Wprowadź swój login" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input name="email" type="email" class="form-control" placeholder="Wprowadź swój E-mail" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Wprowadź swoje hasło" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input name="confirm_password" type="password" class="form-control" placeholder="Potwierdź swoje hasło" required>
          </div><!-- form-group -->


          <button type="submit" class="btn btn-info btn-block" style="cursor: pointer;">Zarejestruj się</button>


        </div><!-- login-wrapper -->
      </div><!-- d-flex -->
    </form>

    <script src="<?php echo base_url() ?>/assets/back/template/lib/jquery/jquery.js"></script>
    <script src="<?php echo base_url() ?>/assets/back/template/lib/popper.js/popper.js"></script>
    <script src="<?php echo base_url() ?>/assets/back/template/lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/back/template/lib/select2/js/select2.min.js"></script>


  </body>
</html>
