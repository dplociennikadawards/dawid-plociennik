<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'ustawienia_strony') {
  $page = 'SEO';
  $subpage = 'Ustawienia strony';
  $icon = 'fa fa-rss';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
       
        <div>
          <h4> <i class=" <?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">


                  <input class="form-control" type="hidden" name="img" id="zdjecie" value="<?php echo $data->polityka; ?>" required>

                  <div class="row no-gutters">
                     <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group  ">
                      <label class="form-control-label">Tytuł strony: <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="title" value="<?php echo $data->title; ?>" placeholder="Tytuł strony" required>
                      
                    </div>
                    </div><!-- col-4 -->
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group bd-t-0-force">
                      <label class="form-control-label">Opis strony: <span class="tx-danger">*</span></label>
                      <textarea class="form-control summernote" type="text" name="description"  placeholder="Opis strony" required><?php echo $data->description; ?></textarea>
                      
                    </div>
                    </div><!-- col-4 -->
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group bd-t-0-force">
                      <label class="form-control-label">Słowa kluczowe: <span class="tx-danger">*</span></label>
                      <textarea class="form-control" type="text" name="keywords"  placeholder="Słowa kluczowe" rows="5"><?php echo $data->keywords; ?></textarea>
                      
                    </div>
                    </div><!-- col-4 -->
                    
                    
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->

                    <div class="col-md-5">
                      <div class="pl-md-0 px-0 col-md-12 mg-t--1 mg-md-t-0" style="height: 100%;">
                        <div class="form-group mg-md-l--1 m--   m-0">

                          <form method="post" id="upload_form" enctype="multipart/form-data">
                              <div id="spinner"></div>
                              <label class="form-control-label">Polityka:   <span class="tx-danger">*</span></label>

                              <div id="uploaded_image">
                                <?php if(!empty($data->polityka)): ?>
                                  <img src="<?php echo base_url(); ?>assets/images/<?php echo $data->polityka; ?>" width="180" height="180" class="img-thumbnail" onclick="delete_photo()" title="Usuń zdjęcie">
                                <?php endif; ?>
                              </div>  
                          
                              <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                            <br>
                          </form>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->




    <!-- ########## END: MAIN PANEL ########## -->
