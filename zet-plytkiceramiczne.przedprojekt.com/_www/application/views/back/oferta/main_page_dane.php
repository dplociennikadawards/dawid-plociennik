<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'tresc') {
  $page = 'Produkty';
  $subpage = 'Treści';
  $icon = 'fa fa-cog';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
       
        <div class="col-md-6">
          <h4><i class="<?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>

        <div class="col-md-4 text-right">
              <a class="btn btn-primary" data-toggle="modal" data-target="#centralModalSmz" style="cursor: pointer;color: white;">Dodaj treść</a>
        </div>

                <div class="modal fade" id="centralModalSmz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="width: 100%;">

                      <div class="modal-dialog modal-lg" role="document">


                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title w-100" id="myModalLabel">Dodawanie treści</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            
                            <a href="<?php echo base_url(); ?>oferta/add_tresc/<?php  echo $this->uri->segment(3); ?>/1"><button type="button" class="btn btn-success btn-md">Tekst</button></a>
                            <a href="<?php echo base_url(); ?>oferta/add_tresc/<?php  echo $this->uri->segment(3); ?>/2"><button type="button" class="btn btn-success btn-md">Zdjęcie</button></a>
                            <a href="<?php echo base_url(); ?>oferta/add_tresc/<?php  echo $this->uri->segment(3); ?>/3"><button type="button" class="btn btn-success btn-md">Tekst + zdjęcie</button></a>
                            <a href="<?php echo base_url(); ?>oferta/add_tresc/<?php  echo $this->uri->segment(3); ?>/4"><button type="button" class="btn btn-success btn-md">Galeria</button></a>
                          </div>
                          <div class="modal-footer ">
                            <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Zamknij</button>
                          </div>
                        </div>
                      </div>
                    </div>


      </div>

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <span id="refreshTable">
          <table class="table table-hover">
            <thead>
              <th class="wd-10p">L.p.</th>
              <th class="wd-10p">Pozycja</th>
              <th class="wd-10p">Typ treści</th>
              <th class="wd-40p">Treść</th>
            </thead>
            <tbody>
              <?php $i=1; foreach($tresc as $row): ?>
                <tr>
                  <td  class="align-middle"><?php echo $i; ?>.</td>
                   <td  class="align-middle"><?php echo $row->priority; ?> 
                      <span onclick="upPriority(<?php echo $row->id; ?>,<?php echo $row->oferta_id; ?>)" style="cursor: pointer;">
                        <i class="fas fa-arrow-down"></i>
                      </span>
                      <span onclick="downPriority(<?php echo $row->id; ?>,<?php echo $row->oferta_id; ?>)" style="cursor: pointer;">
                        <i class="fas fa-arrow-up"></i>
                      </span>
                  </td>
                  <td  class="align-middle">
                    <?php if ($row->typ == 1) {echo "Tekst";} ?>
                    <?php if ($row->typ == 2) {echo "Zdjęcie";} ?>
                    <?php if ($row->typ == 3) {echo "Tekst + zdjęcie";} ?>
                    <?php if ($row->typ == 4) {echo "Galeria";} ?>
                  </td>
                  <td  class="align-middle">
                    <?php if ($row->typ == 1) { ?><?php echo substr($row->text,0,300); } ?>
                    <?php if ($row->typ == 2) {?><img src="<?php echo base_url(); ?>upload/<?php echo $row->img ?>" width='220px'><?php } ?>
                    <?php if ($row->typ == 3) {?><img src="<?php echo base_url(); ?>upload/<?php echo $row->img ?>" width='220px' class="mr-2"><br><?php echo substr($row->text,0,300); ?><?php } ?>
                    <?php if ($row->typ == 4) {?>
                      <a href="<?php echo base_url(); ?>oferta/add_photo/<?php echo $row->id; ?>/<?php  echo $this->uri->segment(3); ?>"><button type="button" class="btn btn-info btn-md">Galeria</button></a>
                    <?php } ?>
                  </td>
                  <td class="align-middle">
                  <!-- Usuwanie -->
                  <a style="cursor: pointer;" data-toggle="modal" data-target="#centralModalSm<?php echo $i; ?>" class="ad_button"> Usuń </a>
                    <div class="modal fade" id="centralModalSm<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%;">

                          <div class="modal-dialog modal-lg" role="document">


                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title w-100" id="myModalLabel">Czy na pewno chcesz usunąć?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <?php if ($row->typ == 1) {echo "Tekst";} ?>
                                <?php if ($row->typ == 2) {echo "Zdjęcie";} ?>
                                <?php if ($row->typ == 3) {echo "Tekst + zdjęcie";} ?>
                                <?php if ($row->typ == 4) {echo "Galerię";} ?>
                              </div>
                              <div class="modal-footer ">
                                <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Nie</button>
                                <a href="<?php echo base_url(); ?>oferta/delete_tresc/<?php  echo $this->uri->segment(3); ?>/<?php echo $row->id; ?>"><button type="button" class="btn btn-danger btn-md">Tak</button></a>
                              </div>
                            </div>
                          </div>
                        </div>
                    <!-- Usuwanie -->
                    <?php if ($row->typ !=4) {?>
                    <a href="<?php echo base_url(); ?>oferta/edit_tresc/<?php  echo $this->uri->segment(3); ?>/<?php echo $row->id; ?>" class="ad_button">| Edytuj</a>
                    <?php } ?>
                  </td>
                </tr>
              <?php $i++;  endforeach; ?>
            </tbody>
          </table>
        </span>

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script type="text/javascript">
  
  function upPriority(id,oferta_id)
  {
    var id = id;
    var oferta_id = oferta_id;
    $.ajax({  
         type: "post", 
         url:"http://localhost/work/panel_start/oferta/upPriority_tresc", 
         data: {id:id,oferta_id:oferta_id}, 
         cache: false,  
                     beforeSend: function() { 
                      document.getElementById('refreshTable').innerHTML = '<div class="sk-folding-cube mb-5 mt-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                    complete:function(data) {
                      setTimeout(function(){ $('#refreshTable').load(document.URL +  ' #refreshTable'); }, 1000);
                     
                     } 
         
    });  
  }  

  function downPriority(id,oferta_id)
  {
    var id = id;
    var oferta_id = oferta_id;
    $.ajax({  
         type: "post", 
         url:"http://localhost/work/panel_start/oferta/downPriority_tresc", 
         data: {id:id,oferta_id:oferta_id}, 
         cache: false,  
                    beforeSend: function() { 
                      document.getElementById('refreshTable').innerHTML = '<div class="sk-folding-cube mb-5 mt-5"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>';
                     },
                    complete:function(data) {
                      setTimeout(function(){ $('#refreshTable').load(document.URL +  ' #refreshTable'); }, 1000);
                     } 
       
    });  
  }
</script>