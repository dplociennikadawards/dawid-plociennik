<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'edit_tresc') {
  $page = 'Treści';
  $subpage = 'Edycja';
  $icon = 'fa fa-circle';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <div>
          <h4><i class="<?php echo $icon; ?>"></i><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
       <div class="form-layout form-layout-2">
            <div class="row no-gutters">

               
              <div class="pr-md-0  <?php if ($dane->typ == '3') {?> col-md-8 <?php } ?> <?php if ($dane->typ == '1') {?> col-md-12 <?php } ?> <?php if ($dane->typ == '2') {?> col-md-8 <?php } ?> ">
              	 <?php echo form_open(); ?>
                    <input class="form-control" type="hidden" name="img" id="zdjecie" value="<?php echo $dane->img; ?>" >
                    <input class="form-control" type="hidden" name="rozmiar" id="rozdzielczosc" value="">
                    
                   <?php if ($dane->typ == '1' || $dane->typ == '3') {?>
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group  ">
                      <label class="form-control-label">Tekst: <span class="tx-danger">*</span></label>
                      <textarea class="form-control summernote" type="text" name="text"  placeholder="Wprowadź tekst" required>
                     <?php echo $dane->text; ?>
                      </textarea>
                      
                    </div>
                    </div><!-- col-4 -->
                   <?php } ?>

	                <?php if ($dane->typ == '2' || $dane->typ == '3') {?>
                     <div class="pr-md-0 col-md-12 px-0 ">
                        <div class="form-group  ">
                          <label class="form-control-label">Alternatywny tekst zdjęcia: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="alt" value="<?php echo $dane->alt; ?>"  placeholder="Alternatywny tekst zdjęcia" required>
                          
                        </div>
                    </div><!-- col-12 -->
	                <?php } ?>
	                <div class="pr-md-0 col-md-12 px-0">
	                    <div class="form-group bd-t-0-force">
	                      <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
	                    </div>
                	</div><!-- col-4 -->
                </div><!-- col-8 -->
                <?php echo form_close(); ?>
				
                
                <?php if ($dane->typ == '2' || $dane->typ == '3') {?>
                  <div class="pl-md-0 px-0 <?php if ($dane->typ == '2') {?> col-md-4 <?php }else{ ?>col-md-4 <?php } ?> mg-t--1 mg-md-t-0">
                  <div class="form-group mg-md-l--1 m--  m-0">

                    <form method="post" id="upload_form" enctype="multipart/form-data">



                      <label class="form-control-label">Zdjęcie:   <span class="tx-danger">*</span></label>

            
                        <div id="spinner"></div>
                      <div id="uploaded_image">
                      	<?php if(!empty($dane->img)): ?>
                              	<div class="delete_photo" style="height: auto;width:fit-content;">
                            		<img src="<?php echo base_url(); ?>upload/<?php echo $dane->img; ?>" width="180" height="180" class="img-thumbnail" onclick="delete_photo()" title="Usuń zdjęcie">
                            	</div>
                      	<?php endif; ?>
                      </div>  
                    

                      <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      <br>
                    </form>
                  </div>
                </div><!-- col-4 -->
               <?php } ?>
            </div><!-- row -->

      </div><!-- form-layout -->

            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->



