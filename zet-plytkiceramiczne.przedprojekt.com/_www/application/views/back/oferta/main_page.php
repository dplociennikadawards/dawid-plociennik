<?php 
$main_page = '';
$page = '';
if($this->uri->segment(1) == 'oferta') {
  $page = 'Produkty';
  $icon = 'fa fa-circle';
  $subpage = '';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
       <div class="br-pagetitle">
        
        <div class="col-md-6">
          <h4><i class="<?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
        <div class="col-md-6 text-right"><a href="<?php echo base_url(); ?>oferta/add"><button type="button" class="btn btn-primary" style="cursor: pointer;">Dodaj produkt</button></a></div>
      </div>

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-10p">Lp.</th>
            <th class="wd-55p">Nagłówek</th>
          </thead>
          <tbody>
            <?php $i=1; foreach ($data as $key) :?>
              <tr>
                <td class="align-middle"><?php echo $i; ?></td>
                <td class="align-middle"><?php echo $key->name; ?></td>
                 <td class="align-middle">
                  <a href="<?php echo base_url(); ?>oferta/tresc/<?php echo $key->produkty_id; ?>" style="cursor: pointer;"><button type="button" class="btn btn-primary">Treści</button></a>&nbsp;
                  <a style="cursor: pointer;" data-toggle="modal" data-target="#centralModalSm<?php echo $i; ?>" class="ad_button"> Usuń</a> | 

                  <!-- Usuwanie -->
                    <div class="modal fade" id="centralModalSm<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%;"> 

                          <div class="modal-dialog modal-lg" role="document">


                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title w-100" id="myModalLabel">Czy na pewno chcesz usunąć?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Produkt <span class="font-weight-bold"><?php echo $key->name ?></span>
                              </div>
                              <div class="modal-footer ">
                                <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Nie</button>
                                <a href="<?php echo base_url(); ?>oferta/delete/<?php echo $key->produkty_id; ?>"><button type="button" class="btn btn-danger btn-md">Tak</button></a>
                              </div>
                            </div>
                          </div>
                        </div>
                    <!-- Usuwanie -->
                <a href="<?php echo base_url(); ?>oferta/edit/<?php echo $key->produkty_id; ?>" class="ad_button">Edycja</a>
              </td>
              </tr>
          <?php $i++; endforeach; ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->