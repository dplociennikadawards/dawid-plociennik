<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'robots') {
  $page = 'SEO';
  $subpage = 'robots.txt';
  $icon = 'fa fa-rss';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
       
        <div>
          <h4> <i class=" <?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-12">
                <form action="" method="POST">

               <?php 
               	$my_file = './robots.txt';
				$handle = fopen($my_file, 'r+') ;
                ?>

                  <div class="row no-gutters">
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group ">
                      <label class="form-control-label">robots.txt: </label>
                      <textarea class="form-control" type="text" name="robots"  placeholder="robots.txt" rows="10"><?php 
                      while (!feof($handle)) {
					   $line = fgets($handle);
					   echo $line;
					} ?></textarea>
                      
                    </div>

                    </div><!-- col-4 -->
                    
                    
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->


              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->




    <!-- ########## END: MAIN PANEL ########## -->
