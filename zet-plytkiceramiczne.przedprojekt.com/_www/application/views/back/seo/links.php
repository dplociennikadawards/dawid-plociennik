<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'links') {
  $page = 'SEO';
  $subpage = 'Sitemap links';
  $icon = 'fa fa-rss';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
       
        <div>
          <h4> <i class=" <?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7 pr-2 pl-2">
		        <table class="table table-hover">
		          <thead>
		            <th class="wd-10p">Lp.</th>
		            <th class="wd-25p">Link</th>
		          </thead>
		          <tbody>
		            <?php $i=1; foreach ($data as $key) :?>
		              <tr>
		                <td class="align-middle"><?php echo $i; ?></td>
		                <td class="align-middle"><?php echo $key->link; ?></td>
		                <td class="align-middle">
		                  <a href="<?php echo base_url(); ?>seo/delete_link/<?php echo $key->id; ?>" class="ad_button">Usuń</a>
		                </td>
		              </tr>
		          <?php $i++; endforeach; ?>
		          </tbody>
		        </table>
              </div>
              <div class="col-md-5">
                <form action="" method="POST">



                  <div class="row no-gutters">
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group">
                      <label class="form-control-label">Link: <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="link"   placeholder="Link do sitemapy (sam kontroler z funkcją!) np.: p/oferta" required>
                      
                    </div>
                    </div><!-- col-4 -->
                    <div class="pr-md-0 col-md-12 px-0 ">
                    <div class="form-group bd-t-0-force">
                      <label class="form-control-label">Priorytet (0-1): <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="priority"   placeholder="Priorytet" required>
                      
                    </div>
                    </div><!-- col-4 -->
                      <div class="pr-md-0 px-0 col-md-12 mg-t--1 mg-md-t-0">
                        <div class="form-group bd-t-0-force">
                          <select name="changefreq" class="form-control" required>
                            <option disabled selected>Częstość</option>
                            <option value="daily">daily</option>
                            <option value="weekly">weekly</option>
                            <option value="monthly">monthly</option>
                            <option value="yearly">yearly</option>
                          </select>
                        </div>
                      </div><!-- col-4 -->
                    
                    
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                        </div>
                      </div><!-- col-4 -->
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <a href="<?php echo base_url() ?>seo/sitemap"  class="btn btn-primary" style="cursor: pointer;">Twórz sitemapę</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->




    <!-- ########## END: MAIN PANEL ########## -->
