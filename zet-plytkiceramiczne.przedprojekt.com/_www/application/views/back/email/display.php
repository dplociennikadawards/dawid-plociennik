
<?php 
$main_page = '';
$page = '';
if($this->uri->segment(1) == 'email') {
  $page = 'Formularze kontaktowe';
  $icon = 'fab fa-cirle';
  $subpage = 'Podgląd';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
         <table class="table ">
            <tbody>
              <tr>
                <td class="align-middle"> <h4 class="br-section-label mt-0">Imię: </h4></td>
                <td class="align-middle"><?php echo $row->name; ?></td>
              </tr>
              <tr>
                <td class="align-middle"><h4 class="br-section-label mt-0">Email: </h4></td>
                <td class="align-middle"><?php echo $row->email; ?></td>
              </tr>
              <tr>
                <td class="align-middle"><h4 class="br-section-label mt-0">RODO: </h4></td>
                <td class="align-middle"><?php echo $row->rodo; ?></td>
              </tr>
              <tr>
                <td class="align-middle"><h4 class="br-section-label mt-0">Temat: </h4></td>
                <td class="align-middle"><?php echo $row->subject; ?></td>
              </tr>
              <tr>
                <td colspan="2"><h4 class="br-section-label mt-0">Wiadomość: </h4></td>
              </tr>
              <tr style="background-color: white!important;border-top-color:#fff!important;" >
                <td colspan="2" style="background-color: white!important;border-top-color:#fff!important;"><?php echo $row->message; ?></td>
              </tr>
            </tbody>
          </table>

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    
