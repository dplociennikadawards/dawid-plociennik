<?php 
$main_page = '';
$page = '';
if($this->uri->segment(1) == 'admin') {
  $page = 'Strona główna';
  $icon = 'fas fa-home';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" ><?php echo $page_title; ?></a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagebody">

        <div class="row">
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>ustawienia">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-circle"></i><br>Ustawienia
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>ustawienia_strony">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-circle"></i><br>Ustawienia strony
              </button>
            </a>
          </div>   
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>email">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-circle"></i><br>Formularze kontaktowe
              </button>
            </a>
          </div>
        </div>  



        <div class="row mt-5">
          <div class="col-md-2"></div>
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>social">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-box-open"></i><br>Social Media
              </button>
            </a>
          </div>    
          <div class="col-md-4 col-6 text-center">
            <a href="<?php echo base_url(); ?>panel/login/logout">
              <button class="zoom navigations-blocks text-center p-5 mx-auto rounded tx-20 border-0 box-shadow" style="cursor: pointer;">
                <i class="tx-80 fas fa-power-off"></i><br>Wyloguj
              </button>
            </a>
          </div> 
        </div> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->