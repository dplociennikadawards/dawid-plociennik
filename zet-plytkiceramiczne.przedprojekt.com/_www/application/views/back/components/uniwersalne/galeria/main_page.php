<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'add_photo') {
  $page = 'Treści';
  $subpage = 'Galeria';
  $icon = 'fa fa-circle';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
       
        <div class="col-md-6">
          <h4> <i class="<?php echo $icon; ?>"></i> <?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>

        <div class="col-md-4 text-right">
              <a class="btn btn-primary" href="<?php echo base_url() ?>oferta/tresc/<?php  echo $this->uri->segment(4); ?>">Powrót</a>
        </div>

      </div>

      <div class="br-pagebody">
      	<?php echo validation_errors() ?>
	      <div class="form-layout form-layout-2">
	             
	            <div class="pl-md-0 col-md-12 col-12">
	              <div class="row">
	                <div class="pl-md-0 px-0 col-md-8 mg-t--1 mg-md-t-0">
	                  <label class="form-control-label">Zdjęcia już dodane: </label>
	                    <div class="row p-3">
	                      <?php foreach ($galeria as $row): ?>              
		                        <div class="pl-md-0 px-3 col-md-6 mg-t--1 mg-md-t-0 mb-2 ">
		                        	<div class="delete_photo" style="height: auto;cursor: pointer;">
		                      		 	<a href="<?php echo base_url(); ?>oferta/delete_photo/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>/<?php echo $row->id ?>"> 
			                           		 <img src="<?php echo base_url(); ?>upload/<?php echo $row->img ?>" width="100%"  class="mydanger m-auto">
			                   			 </a>
			                   		</div>
			                   		<div class="row" >

				                   		<div class="pr-md-0 col-md-8 px-0 ">
				                            <div class="form-group bd-t-0-force bd-l-0-force bd-r-0-force bd-b-0-force">
				                                <label class="form-control-label">Tekst alternatywny (SEO): <span class="tx-danger">*</span></label>
				                                <input class="form-control" type="text" id="<?php echo $row->id ?>" name="alt" value="<?php echo $row->alt ?>"  placeholder="Alternatywny tekst" required>
				                              
				                              </div>
				                        </div><!-- col-12 -->
				                        
				                       <div class="pr-md-0 col-md-4 px-0" >
				                          <div class="form-group bd-t-0-force bd-l-0-force bd-r-0-force bd-b-0-force" id="spinner_alt<?php echo $row->id ?>">
				                            <button type="submit" class="btn btn-primary" style="cursor: pointer;" onclick="update_alt(<?php echo $row->id ?>)">Zapisz</button>
				                            
				                          </div>
				                      </div><!-- col-12 -->

				                    </div>
		                        </div>
	                      <?php endforeach; ?>
	                    </div>
	                
	                </div><!-- col-4 -->
	 
	                <div class="pr-md-0 col-md-4 px-0">
	                    <div class="form-group mg-md-l--1 m--  m-0" style="height: auto;">
	                    
	                    <form method="post" id="upload_form_gallery" enctype="multipart/form-data">



	                      <label class="form-control-label">Dodaj zdjęcie do galerii:   <span class="tx-danger">*</span></label>
	                      <div id="spinner"></div>
	                      <div id="uploaded_image">

	                      </div>  

	                      <input class="form-control mt-2" type="file" id="image_file" name="image_file">
	                      <br>
	                    </form>
	                  </div>

        				<?php echo form_open(); ?>
       					<div class="row no-gutters">
	     
	                      <input class="form-control" type="hidden" name="img" id="zdjecie">
	                      <input class="form-control" type="hidden" name="rozmiar" id="rozdzielczosc">
	                    <div class="pr-md-0 col-md-12 px-0 ">
                        <div class="form-group bd-t-0-force">
	                          <label class="form-control-label">Tekst alternatywny (SEO): <span class="tx-danger">*</span></label>
	                          <input class="form-control" type="text" name="alt"  placeholder="Alternatywny tekst" required>
                          
	                        </div>
	                    </div><!-- col-12 -->
	             		</div>   
	                    <div class="form-group bd-t-0-force" style="height: auto;">
	                      <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
	                    </div>

        				<?php echo form_close(); ?>
	                  </div><!-- col-4 -->
	            </div>
	           </div>
	          </div><!-- form-layout -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
<script type="text/javascript">
	
	function update_alt(id){
		element = document.getElementById(id).value;
		     $.ajax({
                type: "post",
                url: "<?php echo base_url() ?>oferta/update_alt/"+id+"",
                data: {element:element},
                cache: false,
                 	beforeSend: function() { //przed dodaniem
                      document.getElementById('spinner_alt'+id+'').innerHTML = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>';
                     },
                     success:function(data)  //success po dodaniu
                     {  
                        
                     },
                     complete:function(data) { 
                     	 setTimeout(function(){ 
                     	 document.getElementById('spinner_alt'+id+'').innerHTML = ' <button type="submit" class="btn btn-primary" style="cursor: pointer;" onclick="update_alt('+id+')">Zapisz</button>';
                     	}, 1234);
                     },
                     

                                         
      

                   	 });

	}
</script>


