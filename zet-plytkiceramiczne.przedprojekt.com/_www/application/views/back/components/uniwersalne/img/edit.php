<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'edit_tresc') {
  $page = 'Treści';
  $subpage = 'Edycja';
  $icon = 'fa fa-circle';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <div>
          <h4><i class="<?php echo $icon; ?>"></i><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
           <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-12">
                <div class="pl-md-0 px-0 col-md-12 mg-t--1 mg-md-t-0" style="height: 100%;">
                  <div class="form-group mg-md-l--1 m--  m-0">

                    <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner"></div>
                        <label class="form-control-label">Zdjęcie:   <span class="tx-danger">*</span></label>

                        <div id="uploaded_image">
                          <?php if(!empty($data->img)): ?>
                            <img src="<?php echo base_url(); ?>assets/images/<?php echo $data->img; ?>" width="180" height="180" class="img-thumbnail" onclick="delete_photo()" title="Usuń zdjęcie">
                          <?php endif; ?>
                        </div>  
                    
                        <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      <br>
                    </form>
                  </div>
                </div><!-- col-4 -->
              </div><!-- col-md-5 -->
              <div class="col-md-12">
                <form action="" method="POST">
                  <div class="row no-gutters">
                         <input class="form-control" type="hidden" name="img" id="zdjecie" value="<?php echo $data->img; ?>" required>
                     <div class="pr-md-0 col-md-12 px-0" >
                        <div class="form-group bd-t-0-force  ">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          
                        </div>
                    </div><!-- col-12 -->
                    </div><!-- row no-gutters -->
                  </form>
              </div><!-- col-md-12 -->
              </div>
            </div><!-- row --> 

            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->



