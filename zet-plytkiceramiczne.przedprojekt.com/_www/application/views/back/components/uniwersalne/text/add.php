<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'add_tresc') {
  $page = 'Treści';
  $subpage = 'Dodawanie';
  $icon = 'fa fa-circle';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div>

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-12">
                <form action="" method="POST">
                  <div class="row no-gutters">

                     <div class="pr-md-0 col-md-12 px-0 ">
                        <div class="form-group bd-b-0-force">
                          <label class="form-control-label">Tekst: <span class="tx-danger">*</span></label>
                          <textarea class="form-control summernote" type="text" name="text"  placeholder="Tekst" required></textarea>
                          
                        </div>
                    </div><!-- col-12 -->
                     <div class="pr-md-0 col-md-12 px-0" >
                        <div class="form-group bd-t-0-force  ">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          
                        </div>
                    </div><!-- col-12 -->
                    </div><!-- row no-gutters -->
                  </form>
              </div><!-- col-md-7 -->
              </div>
            </div><!-- row --> 

            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->



