    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Front_model extends CI_Model
{
   public function __construct() {
        parent::__construct();
    }

	public function slugify($str) {
	    $str = mb_strtolower($str,'utf-8');
	    $search = array('ą', 'ć', 'ś', 'ó', 'ż', 'ź', 'ę', 'ł', 'ń',' ', '?', '!', '(', ')', '.', ',', '/');
	    $replace = array('a', 'c', 's', 'o', 'z', 'z', 'e', 'l', 'n','-','','','','','','','','');
	    $str = str_ireplace($search, $replace, strtolower(trim($str)));
	    $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
	    return $str;

		}

	public function slugify_link($str) {
	    $str = mb_strtolower($str,'utf-8');
	    $search = array('ą', 'ć', 'ś', 'ó', 'ż', 'ź', 'ę', 'ł', 'ń',' ', '?', '!', '(', ')', '.', ',');
	    $replace = array('a', 'c', 's', 'o', 'z', 'z', 'e', 'l', 'n','-','','','','','','','');
	    $str = str_ireplace($search, $replace, strtolower(trim($str)));
	    return $str;

		}
}  
?>