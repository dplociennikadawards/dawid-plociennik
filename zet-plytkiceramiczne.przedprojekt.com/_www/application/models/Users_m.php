    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model
{
   public function __construct() {
        parent::__construct();
    }

    public function get($table) {
        $query = $this->db->get($table);
        
        return $query->result();            
    }

    public function get_element($table,$where) {
        $this->db->where([$table.'_id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }

    public function getelement($table,$where) {
        $this->db->where(['id' => $where]);
        $query = $this->db->get($table);
        
        return $query->row();            
    }




      //produkty

    public function get_galeria($where)
    {
        $this->db->where(['galeria_id' => $where]);
        $query = $this->db->get('komponenty_galeria');
        
        return $query->result();            
    }

     public function get_dane($where)
    {
        $this->db->where(['oferta_id' => $where]);
        $this->db->order_by("priority", "asc");
        $query = $this->db->get('komponenty');
        
        return $query->result();            
    }

      public function get_sort_up_komponenty($table,$where)
    {
        $this->db->where(['oferta_id' => $where]);
        $this->db->order_by('priority', 'ASC');
        $query = $this->db->get($table);        

        return $query->result();            
    }

    public function get_sort_down_komponenty($table,$where)
    {
        $this->db->where(['oferta_id' => $where]);
        $this->db->order_by('priority', 'DESC');
        $query = $this->db->get($table);        

        return $query->result();            
    }

     public function get_max_of_priority($table,$where)
    {
        $this->db->select_max('priority');
        $this->db->where(['oferta_id' => $where]);
        $query = $this->db->get($table);
        return $query->row();          
    }

    //produkty

    public function insert($table, $data) {
        $this->db->insert($table,$data);
    }

    public function delete($table,$where) {
        $this->db->where($where);
        $this->db->delete($table);
    }

}  
?>