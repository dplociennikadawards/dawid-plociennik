<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rozpakowanie Archiwa Lista</title>
</head>

<body>
<?
$root=getcwd()."/";

if (isset($_GET['unzip']) and is_file($root.$_GET['unzip'])):
  $unzip = zip_open ($_GET['unzip']);
  while ($plik = zip_read($unzip)):
	  $rozmiar=zip_entry_filesize($plik);
	  $nazwa=zip_entry_name($plik);
	  if ((strrpos($nazwa,"/")+1)==strlen($nazwa)):
		  if(!is_dir($nazwa)):
			  mkdir($root.substr($nazwa,0,-1));
			  echo $nazwa. " katalog utworzony<br />\n";
		  endif;
	  else:
		  if (!is_dir(dirname($root.$nazwa))):
			  mkdir(dirname($root.$nazwa),0777,true);
			  echo $nazwa. " katalog utworzony<br />\n";
		  endif;
		  $rozpak=fopen($root.$nazwa,"x+");
		  if (fwrite($rozpak,zip_entry_read($plik,$rozmiar))):
			  echo $nazwa. " plik utworzony<br />\n";
		  endif;
		  fclose($rozpak);
	  endif;
  endwhile;

else:
  $dir=opendir($root);
  while (false !== ($item = readdir($dir))):
	  if (strpos($item,".zip")>0):
		  echo $item." <a href=\"".basename(__FILE__)."?unzip=".$item."\">rozpakuj</a><br />\n";
	  endif;
  endwhile;
endif;
?>
</body>
</html>