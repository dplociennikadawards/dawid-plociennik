<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	public function index(){
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		// $data['podstrony'] = $this->users_m->get_page('podstrony', 'index');
		$data['producenci'] = $this->users_m->get('producenci');

		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);
		$data['onas'] = $this->users_m->get_element('glowna',33);
		$data['slider'] = $this->users_m->get_slide('glowna' , 'slider');
		// $data['onas_glowna'] = $this->users_m->get_slide('glowna' , 'onas');
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');

		// $data['oferta'] = $this->users_m->get_sortByDate('oferta');
		// $data['polecane'] = $this->users_m->get_promoted('oferta');
		// $data['onas'] = $this->users_m->get('onas');

		$this->load->view('front/blocks/head.php', $data);
		$this->load->view('front/blocks/header.php', $data);
		$this->load->view('front/index.php', $data);
		$this->load->view('front/blocks/footer.php', $data);
	}

	public function ofirmie(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get('onas');
		$data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'onas');
		$data['onas'] = $this->users_m->get_element('onas',1);
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		// $data['onas_wpis'] = $this->users_m->get_element('onas',2);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/ofirmie.php' , $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function produkty(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['rows'] = $this->users_m->get_sortByDate('promocje');
		$data['produkt'] = $this->users_m->get_element('promocje',1);
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);
		// $data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'oferta');
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/produkty.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function kategoria(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['rows'] = $this->users_m->get_sortByDateWhere('promocje', $this->uri->segment(3));
		$data['produkt'] = $this->users_m->get_text_category('glowna', $this->uri->segment(3));
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);
		// $data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'oferta');
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/kategoria.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function realizacje(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['rows'] = $this->users_m->get('galeria');
		$data['category'] = $this->users_m->get('galeria_cat');
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);
		// $data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'galeria');
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/realizacje.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function promocje(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['rows'] = $this->users_m->get_sortByDate('promocje2');
		// $data['promocje'] = $this->users_m->get_element('promocje',1);
		$data['promocja'] = $this->users_m->get_element('promocje2',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		// $data['galeria_zdjec'] = $this->users_m->get_sortByPriority('gallery', 'promocje');
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');
		$data['miasta'] = $this->users_m->get('miasta');
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);

		if(isset($_GET['miasto'])) {
			$data['row'] = $this->users_m->get_city('miasta', $_GET['miasto']);	
		} else {
			$data['row1'] = $this->users_m->get_element('miasta', 1);	
		}


		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/dlaprojektanta.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function kontakt(){
		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['podstrony'] = $this->users_m->get_page('podstrony', $this->uri->segment(2));
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);
		// $data['stopka'] = $this->users_m->get_slide('glowna' , 'stopka');
		// $data['faq'] = $this->users_m->get('faq');

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader.php' , $data);
		$this->load->view('front/kontakt.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}


	public function produkt($slug, $id) {	

		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['produkt'] = $this->users_m->get_element('promocje',$id);
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader2.php' , $data);
		$this->load->view('front/produkt.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}

	public function promocja($slug, $id) {	

		$data['ustawienia'] = $this->users_m->get_element('ustawienia',1);
		$data['kontakt'] = $this->users_m->get_element('kontakt',1);
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['produkt'] = $this->users_m->get_element('promocje2',$id);
		$data['producenci'] = $this->users_m->get('producenci');
		$data['poznaj'] = $this->users_m->get_element('glowna',26);
		$data['oferta'] = $this->users_m->get_element('glowna',27);
		$data['oferta1'] = $this->users_m->get_element('glowna',28);
		$data['oferta2'] = $this->users_m->get_element('glowna',29);
		$data['oferta3'] = $this->users_m->get_element('glowna',30);
		$data['oferta4'] = $this->users_m->get_element('glowna',31);
		$data['oferta5'] = $this->users_m->get_element('glowna',32);

		$this->load->view('front/blocks/head.php' , $data);
		$this->load->view('front/blocks/subheader2.php' , $data);
		$this->load->view('front/produkt.php', $data);
		$this->load->view('front/blocks/footer.php' , $data);
	}
	public function newsletter()
	{
		$this->form_validation->set_rules('e-mail', 'email', 'valid_email|min_length[2]|trim|is_unique[newsletter.email]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	$this->form_validation->set_message('valid_email', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->session->set_flashdata('false', '<p class="text-danger font-weight-bold">Został podany niepoprawny adres e-mail lub dany adres jest już użyciu!</p>');
				redirect('p#newsletter');
			}
		else
			{
				$insert['email'] = $this->input->post('e-mail');
				$this->users_m->insert('newsletter', $insert);
				$this->session->set_flashdata('false', '<p class="text-dark font-weight-bold">Zostałeś zapisany do naszego newsletter. Dziękujemy!</p>');
				redirect('p#newsletter');
			}
	}

}
