<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_upload extends CI_Controller {

public function index()  
      {  
           if(isset($_FILES["image_file"]["name"]))  
           {  
                $config['upload_path'] = './upload/';  
                $config['allowed_types'] = '*';
                $config['max_width']  = '*';
                $config['max_height']  = '*';
                $new_name = time().$_FILES["image_file"]['name'];
                $config['file_name'] = $new_name;  

                $this->load->library('upload', $config);  
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('image_file'))  
                {  
                     echo $this->upload->display_errors();
                }  
                else  
                {  
                     $data = $this->upload->data();  
                     $data["file_name"] = str_replace(" ","",$data["file_name"]);
                      
                     echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="100%" height="180" class="img-thumbnail" />';      
                     echo '<input id="name_photo" class="home_class_photo" type="hidden" value="' . $data["file_name"] . '">';  
                     echo '<input id="send_resolution" class="galeria" type="hidden" value="' . $data["image_width"] . 'x' . $data["image_height"] . '">'; 
                }  
           }  
      }  


public function photo1()  
      {  
           if(isset($_FILES["image_file"]["name"]))  
           {  
                $config['upload_path'] = './upload/';  
                $config['allowed_types'] = '*';
                $config['max_width']  = '*';
                $config['max_height']  = '*';
                $new_name = time().$_FILES["image_file"]['name'];
                $config['file_name'] = $new_name;  

                $this->load->library('upload', $config);  
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('image_file'))  
                {  
                     echo $this->upload->display_errors();
                }  
                else  
                {  
                     $data = $this->upload->data();  
                     $data["file_name"] = str_replace(" ","",$data["file_name"]);
                      
                     echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="100%" height="180" class="img-thumbnail" />';      
                     echo '<input id="name_photo1" class="home_class_photo" type="hidden" value="' . $data["file_name"] . '">';  
                     echo '<input id="send_resolution1" class="galeria" type="hidden" value="' . $data["image_width"] . 'x' . $data["image_height"] . '">'; 
                }  
           }  
      } 
public function photo2()  
      {  
           if(isset($_FILES["image_file"]["name"]))  
           {  
                $config['upload_path'] = './upload/';  
                $config['allowed_types'] = '*';
                $config['max_width']  = '*';
                $config['max_height']  = '*';
                $new_name = time().$_FILES["image_file"]['name'];
                $config['file_name'] = $new_name;  

                $this->load->library('upload', $config);  
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('image_file'))  
                {  
                     echo $this->upload->display_errors();
                }  
                else  
                {  
                     $data = $this->upload->data();  
                     $data["file_name"] = str_replace(" ","",$data["file_name"]);
                      
                     echo '<img src="'.base_url().'upload/'.$data["file_name"].'" width="100%" height="180" class="img-thumbnail" />';      
                     echo '<input id="name_photo2" class="home_class_photo" type="hidden" value="' . $data["file_name"] . '">';  
                     echo '<input id="send_resolution1" class="galeria" type="hidden" value="' . $data["image_width"] . 'x' . $data["image_height"] . '">'; 
                }  
           }  
      } 

public function pdf()  
      {  
           if(isset($_FILES["pdf_file"]["name"]))  
           {  
                $config['upload_path'] = './upload/';  
                $config['allowed_types'] = '*';
                $config['max_width']  = '*';
                $config['max_height']  = '*';
                $new_name = time().$_FILES["pdf_file"]['name'];
                $config['file_name'] = $new_name;  

                $this->load->library('upload', $config);  
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('pdf_file'))  
                {  
                     echo $this->upload->display_errors();
                }  
                else  
                {  
                     $data = $this->upload->data();  
                     $data["file_name"] = str_replace(" ","",$data["file_name"]);
                         
                     echo '<input id="pdf_file" class="home_class_photo" type="hidden" value="' . $data["file_name"] . '">';  
                }  
           }  
      } 
}
