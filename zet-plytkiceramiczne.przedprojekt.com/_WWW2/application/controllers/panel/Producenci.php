<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producenci extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get('producenci');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/producenci/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[producenci.title]');
		// $this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/producenci/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('photo') != null) {
					$insert['photo'] = $this->input->post('photo');
					$insert['resolution'] = $this->input->post('resolution');
				}

				$insert['title'] = $this->input->post('title');
				$insert['link'] = $this->input->post('link');


				$this->users_m->insert('producenci', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/producenci');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('producenci', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');
		// $this->form_validation->set_rules('photo', 'zdjęcie', 'trim|required');
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/producenci/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				$update['photo'] = $this->input->post('photo');
				$update['resolution'] = $this->input->post('resolution');
				$update['title'] = $this->input->post('title');
				$update['link'] = $this->input->post('link');

				$this->db->where('producenci_id', $id);
				$this->db->update('producenci', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/producenci');
			}

	}

}
