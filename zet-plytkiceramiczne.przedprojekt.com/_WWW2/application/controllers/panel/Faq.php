<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get('faq');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/faq/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[faq.title]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/faq/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				$insert['question'] = $this->input->post('question');
				$insert['answer'] = $this->input->post('answer');

				$this->users_m->insert('faq', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/faq');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('faq', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/faq/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['question'] = $this->input->post('question');
				$update['answer'] = $this->input->post('answer');

				$this->db->where('faq_id', $id);
				$this->db->update('faq', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/faq');
			}

	}

}
