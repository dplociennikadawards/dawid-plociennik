<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get_sortByDate('galeria');
		$data['gallery'] = $this->users_m->get_sortByPriority('gallery', $this->uri->segment(2));

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/galeria_page/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$data['rows'] = $this->users_m->get('galeria_cat');
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/galeria_page/add', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('photo') != null) {
					$insert['title'] = $this->input->post('title');
					$insert['photo'] = $this->input->post('photo');
					$insert['resolution'] = $this->input->post('resolution');
				}

				$this->users_m->insert('galeria', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/galeria');
			}

	}


public function edit($id) {
		$data['rows'] = $this->users_m->get('galeria_cat');

		$data['row'] = $this->users_m->get_element('galeria', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/galeria_page/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('photo') != null) {
					$update['title'] = $this->input->post('title');
					$update['photo'] = $this->input->post('photo');
					$update['resolution'] = $this->input->post('resolution');
				}
				$this->db->where('galeria_id', $id);
				$this->db->update('galeria', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/galeria');
			}

	}


public function add_cat() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[galeria_cat.category]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/galeria_page/add_cat');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('title') != null) {
					$insert['category'] = $this->input->post('title');
				}

				$this->users_m->insert('galeria_cat', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/galeria');
			}

	}

}
