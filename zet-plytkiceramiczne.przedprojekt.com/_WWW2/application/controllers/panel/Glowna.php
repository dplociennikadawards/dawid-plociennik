<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Glowna extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get('glowna');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/glowna/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add($part) {
		$this->form_validation->set_rules('value1', 'Tytuł', 'min_length[2]|trim');    
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/glowna/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{


				if($this->input->post('value1') != null) {
					$insert['value1'] = $this->input->post('value1');
					$insert['value1'] = $this->input->post('value1');
					$insert['value1'] = str_replace('<p>', '', $insert['value1']);
					$insert['value1'] = str_replace('</p>', '<br>', $insert['value1']);
				}
				
					$insert['value2'] = $this->input->post('value2');
					$insert['value2'] = $this->input->post('value2');
					$insert['value2'] = str_replace('<p>', '', $insert['value2']);
					$insert['value2'] = str_replace('</p>', '<br>', $insert['value2']);
				
				if($this->input->post('value3') != null) {
					$insert['value3'] = $this->input->post('value3');
					$insert['value3'] = $this->input->post('value3');
					$insert['value3'] = str_replace('<p>', '', $insert['value3']);
					$insert['value3'] = str_replace('</p>', '<br>', $insert['value3']);
				}
				if($this->input->post('value4') != null) {
					$insert['value4'] = $this->input->post('value4');
					$insert['value4'] = $this->input->post('value4');
					$insert['value4'] = str_replace('<p>', '', $insert['value4']);
					$insert['value4'] = str_replace('</p>', '<br>', $insert['value4']);
				}
				if($this->input->post('value5') != null) {
					$insert['value5'] = $this->input->post('value5');
					$insert['value5'] = $this->input->post('value5');
					$insert['value5'] = str_replace('<p>', '', $insert['value5']);
					$insert['value5'] = str_replace('</p>', '<br>', $insert['value5']);
				}
				if($this->input->post('value6') != null) {
					$insert['value6'] = $this->input->post('value6');
					$insert['value6'] = $this->input->post('value6');
					$insert['value6'] = str_replace('<p>', '', $insert['value6']);
					$insert['value6'] = str_replace('</p>', '<br>', $insert['value6']);
				}
				$insert['part'] = $part;
				$insert['part_pl'] = $part;

				$this->users_m->insert('glowna', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/glowna');
			}

	}

public function add2($part) {
		$this->form_validation->set_rules('value1', 'Tytuł', 'min_length[2]|trim');    
    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');

    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/glowna/add-stopka');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{


				if($this->input->post('value1') != null) {
					$insert['value1'] = $this->input->post('value1');
					$insert['value1'] = $this->input->post('value1');
					$insert['value1'] = str_replace('<p>', '', $insert['value1']);
					$insert['value1'] = str_replace('</p>', '<br>', $insert['value1']);
				}
				
					$insert['value2'] = $this->input->post('value2');
					$insert['value2'] = $this->input->post('value2');
					$insert['value2'] = str_replace('<p>', '', $insert['value2']);
					$insert['value2'] = str_replace('</p>', '<br>', $insert['value2']);
				
				if($this->input->post('value3') != null) {
					$insert['value3'] = $this->input->post('value3');
					$insert['value3'] = $this->input->post('value3');
					$insert['value3'] = str_replace('<p>', '', $insert['value3']);
					$insert['value3'] = str_replace('</p>', '<br>', $insert['value3']);
				}
				if($this->input->post('value4') != null) {
					$insert['value4'] = $this->input->post('value4');
					$insert['value4'] = $this->input->post('value4');
					$insert['value4'] = str_replace('<p>', '', $insert['value4']);
					$insert['value4'] = str_replace('</p>', '<br>', $insert['value4']);
				}
				if($this->input->post('value5') != null) {
					$insert['value5'] = $this->input->post('value5');
					$insert['value5'] = $this->input->post('value5');
					$insert['value5'] = str_replace('<p>', '', $insert['value5']);
					$insert['value5'] = str_replace('</p>', '<br>', $insert['value5']);
				}
				if($this->input->post('value6') != null) {
					$insert['value6'] = $this->input->post('value6');
					$insert['value6'] = $this->input->post('value6');
					$insert['value6'] = str_replace('<p>', '', $insert['value6']);
					$insert['value6'] = str_replace('</p>', '<br>', $insert['value6']);
				}
				$insert['part'] = $part;
				$insert['part_pl'] = $part;

				$this->users_m->insert('glowna', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/glowna');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('glowna', $id);

		$this->form_validation->set_rules('value1', 'Tytuł', 'min_length[2]|trim');    	
		$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('required', '<p style="color: red;">Pole %s jest wymagane</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				if($data['row']->part == 'poznaj') {
					$this->load->view('back/glowna/edit-poznaj', $data);	
				}
				if($data['row']->part == 'zapoznaj_oferta') {
					$this->load->view('back/glowna/edit-warto', $data);	
				}
				if($data['row']->part == 'oferta') {
					$this->load->view('back/glowna/edit-onas', $data);	
				}
				if($data['row']->part == 'stopka') {
					$this->load->view('back/glowna/edit-stopka', $data);	
				}
				if($data['row']->part == 'slider') {
					$this->load->view('back/glowna/edit', $data);	
				}
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				if($this->input->post('value1') != null) {
					$update['value1'] = $this->input->post('value1');
					$update['value1'] = $this->input->post('value1');
					$update['value1'] = $this->input->post('value1');
					$update['value1'] = str_replace('<p>', '', $update['value1']);
					$update['value1'] = str_replace('</p>', '<br>', $update['value1']);
				}
				if($this->input->post('value1') != null && $data['row']->part == 'oferta') {
				  $slug = $this->input->post('value1');
			      $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
			      $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
			      $update['value2'] = strtolower(str_replace($cenzura, $zamiana, $slug));
				} else {
					$update['value2'] = $this->input->post('value2');
					$update['value2'] = $this->input->post('value2');
					$update['value2'] = $this->input->post('value2');
					$update['value2'] = str_replace('<p>', '', $update['value2']);
					$update['value2'] = str_replace('</p>', '<br>', $update['value2']);
				}
				
				if($this->input->post('value3') != null) {
					$update['value3'] = $this->input->post('value3');
					$update['value3'] = $this->input->post('value3');
					$update['value3'] = $this->input->post('value3');
					$update['value3'] = str_replace('<p>', '', $update['value3']);
					$update['value3'] = str_replace('</p>', '<br>', $update['value3']);
				}
				if($this->input->post('value4') != null) {
					$update['value4'] = $this->input->post('value4');
					$update['value4'] = $this->input->post('value4');
					$update['value4'] = $this->input->post('value4');
					$update['value4'] = str_replace('<p>', '', $update['value4']);
					$update['value4'] = str_replace('</p>', '<br>', $update['value4']);
				}
				if($this->input->post('value5') != null) {
					$update['value5'] = $this->input->post('value5');
					$update['value5'] = $this->input->post('value5');
					$update['value5'] = $this->input->post('value5');
					$update['value5'] = str_replace('<p>', '', $update['value5']);
					$update['value5'] = str_replace('</p>', '<br>', $update['value5']);
				}
				if($this->input->post('value6') != null) {
					$update['value6'] = $this->input->post('value6');
					$update['value6'] = $this->input->post('value6');
					$update['value6'] = $this->input->post('value6');
					$update['value6'] = str_replace('<p>', '', $update['value6']);
					$update['value6'] = str_replace('</p>', '<br>', $update['value6']);
				}
				if($this->input->post('value7') != null) {
					$update['value7'] = $this->input->post('value7');
					$update['value7'] = $this->input->post('value7');
					$update['value7'] = $this->input->post('value7');
					$update['value7'] = str_replace('<p>', '', $update['value7']);
					$update['value7'] = str_replace('</p>', '<br>', $update['value7']);
				}


				$this->db->where('glowna_id', $id);
				$this->db->update('glowna', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/glowna');
			}

	}

}
