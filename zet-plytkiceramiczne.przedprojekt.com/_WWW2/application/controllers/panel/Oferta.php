<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oferta extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get_sortByDate('oferta');
		$data['gallery'] = $this->users_m->get_sortByPriority('gallery', $this->uri->segment(2));

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/oferta/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|is_unique[oferta.title]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podany %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/oferta/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{

				if($this->input->post('photo') != null) {
					$insert['photo'] = $this->input->post('photo');
					$insert['resolution'] = $this->input->post('resolution');
				}

				if($this->input->post('header_photo') != null) {
					$insert['header_photo'] = $this->input->post('header_photo');
				}

				if($this->input->post('icon') != null) {
					$insert['icon'] = $this->input->post('icon');
				}

				$insert['title'] = $this->input->post('title');

				if($this->input->post('subtitle') != null) {
					$insert['subtitle'] = $this->input->post('subtitle');
				}

				if($this->input->post('content') != null) {
					$insert['content'] = $this->input->post('content');
					$insert['content'] = str_replace('<p>', '', $insert['content']);
					$insert['content'] = str_replace('</p>', '<br>', $insert['content']);
				}

				if($this->input->post('content2') != null) {
					$insert['content2'] = $this->input->post('content2');
					$insert['content2'] = str_replace('<p>', '', $insert['content2']);
					$insert['content2'] = str_replace('</p>', '<br>', $insert['content2']);
				}

				if($this->input->post('selected_data') != null) {
					$insert['selected_data'] = date('Y-m-d', strtotime($this->input->post('selected_data')));					
				} else {
					$insert['selected_data'] = date('Y-m-d');
				}


				$this->users_m->insert('oferta', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/oferta');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('oferta', $id);

		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/oferta/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['header_photo'] = $this->input->post('header_photo');
				$update['photo'] = $this->input->post('photo');
				$update['icon'] = $this->input->post('icon');
				$update['resolution'] = $this->input->post('resolution');
				$update['title'] = $this->input->post('title');
				$update['subtitle'] = $this->input->post('subtitle');
				$update['content'] = $this->input->post('content');
				$update['content'] = str_replace('<p>', '', $update['content']);
				$update['content'] = str_replace('</p>', '<br>', $update['content']);

				$update['content2'] = $this->input->post('content2');
				$update['content2'] = str_replace('<p>', '', $update['content2']);
				$update['content2'] = str_replace('</p>', '<br>', $update['content2']);

				if($this->input->post('selected_data') != null) {
					$update['selected_data'] = date('Y-m-d', strtotime($this->input->post('selected_data')));					
				} else {
					$update['selected_data'] = date('Y-m-d');
				}

				$this->db->where('oferta_id', $id);
				$this->db->update('oferta', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/oferta');
			}

	}

}
