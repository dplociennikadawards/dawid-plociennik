<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Miasta extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['rows'] = $this->users_m->get('miasta');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/miasta/index', $data);
		$this->load->view('back/blocks/footer');
	}

public function add() {
		$this->form_validation->set_rules('nazwa', 'Miasto', 'min_length[2]|trim|is_unique[miasta.nazwa]');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podane %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/miasta/add');
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$insert['nazwa'] = $this->input->post('nazwa');
				$insert['lokalizacja'] = $this->input->post('lokalizacja');
				$insert['lat'] = $this->input->post('lat');
				$insert['lng'] = $this->input->post('lng');
				if($this->input->post('header_photo') != null) {
					$insert['header_photo'] = $this->input->post('header_photo');	
				}
				if($this->input->post('photo') != null) {
					$insert['photo'] = $this->input->post('photo');	
				}
				$insert['worker'] = $this->input->post('worker');
				if($this->input->post('stanowisko') != null) {
					$insert['stanowisko'] = $this->input->post('stanowisko');	
				}
				if($this->input->post('phone') != null) {
					$insert['phone'] = $this->input->post('phone');	
				}
				if($this->input->post('mobile') != null) {
					$insert['mobile'] = $this->input->post('mobile');	
				}
				if($this->input->post('email') != null) {
					$insert['email'] = $this->input->post('email');	
				}
				if($this->input->post('text') != null) {
					$insert['text'] = $this->input->post('text');
					$insert['text'] = str_replace('<p>', '', $insert['text']);
					$insert['text'] = str_replace('</p>', '<br>', $insert['text']);
					$insert['text'] = str_replace('<div>', '', $insert['text']);
					$insert['text'] = str_replace('</div>', '', $insert['text']);
				}
				$this->users_m->insert('miasta', $insert);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/miasta');
			}

	}


public function edit($id) {

		$data['row'] = $this->users_m->get_element('miasta', $id);

		$this->form_validation->set_rules('nazwa', 'Miasto', 'min_length[2]|trim');

    	$this->form_validation->set_message('min_length', '<p style="color: red;">Pole %s ma za mało znaków</p>');
    	$this->form_validation->set_message('is_unique', '<p style="color: red;">Podane %s jest już w użyciu</p>');
    	
    	if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('back/blocks/session');
				$this->load->view('back/blocks/head');
				$this->load->view('back/blocks/header');
				$this->load->view('back/miasta/edit', $data);
				$this->load->view('back/blocks/javascript');
				$this->load->view('back/blocks/footer');
			}
		else
			{
				$update['nazwa'] = $this->input->post('nazwa');
				$update['lokalizacja'] = $this->input->post('lokalizacja');
				$update['lat'] = $this->input->post('lat');
				$update['lng'] = $this->input->post('lng');
				if($this->input->post('header_photo') != null) {
					$update['header_photo'] = $this->input->post('header_photo');	
				}
				if($this->input->post('photo') != null) {
					$update['photo'] = $this->input->post('photo');	
				}
				$update['worker'] = $this->input->post('worker');
				if($this->input->post('stanowisko') != null) {
					$update['stanowisko'] = $this->input->post('stanowisko');	
				}
				if($this->input->post('phone') != null) {
					$update['phone'] = $this->input->post('phone');	
				}
				if($this->input->post('mobile') != null) {
					$update['mobile'] = $this->input->post('mobile');	
				}
				if($this->input->post('email') != null) {
					$update['email'] = $this->input->post('email');	
				}
					$update['text'] = $this->input->post('text');
					$update['text'] = str_replace('<p>', '', $update['text']);
					$update['text'] = str_replace('</p>', '<br>', $update['text']);
					$update['text'] = str_replace('<div>', '', $update['text']);
					$update['text'] = str_replace('</div>', '', $update['text']);
			
				$this->db->where('miasta_id', $id);
				$this->db->update('miasta', $update);

				$this->session->set_flashdata('success', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');

        		redirect('panel/miasta');
			}

	}

}
