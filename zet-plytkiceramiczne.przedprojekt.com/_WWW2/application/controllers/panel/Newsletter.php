<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

public function __construct() { parent::__construct(); }

public function index() {
		$data['settings'] = $this->users_m->get_element('newsletter',1);
		$data['rows'] = $this->users_m->get('newsletter');

		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/newsletter/index', $data);
		$this->load->view('back/blocks/javascript');
		$this->load->view('back/blocks/footer');
	}

public function send() {

		header('Location: '.base_url().'mailerNewsletter/thankyou.php?title='.$_POST["title"].'&content='.$_POST["content"].'&liame='.$_POST["liame"].'');
	}

}
