<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'newsletter') {
  $page = 'Newsletter';
  $icon = 'fas fa-share';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-6">
                  <div class="row no-gutters">
                  	<?php foreach ($rows as $row): ?>
                      <div class="pr-md-0 col-6">
                        <div class="form-group">
                          <label class="form-control-label">Adres e-mail:</label>
                          <input class="form-control" type="text" value="<?php echo $row->email; ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-6">
                        <div class="form-group">
                          <label class="form-control-label">Data wysłania wiadomości:</label>
                          <input class="form-control" type="text" value="<?php echo date('H:i d.m.Y', strtotime($row->date)); ?>" readonly>
                        </div>
                      </div>
                  	<?php endforeach; ?>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <a href="<?php echo base_url(); ?>panel/<?php echo strtolower($page); ?>" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                </div><!-- col-md-7 -->
                <div class="col-md-6">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
	                      <div class="col-12">
                    			<p id="answer">Napisz Newsletter</p>
	                        <div class="form-group">
	                          <label class="form-control-label">Tytuł wiadomości:</label>
	                          <input class="form-control" type="text" name="title">
                            <input class="form-control" type="hidden" name="liame" value="nean12.bg@gmail.com">
	                        </div>
	                      </div>
	                      <div class="col-12">
	                        <div class="form-group bd-t-0-force">
	                          <label class="form-control-label">Treść wiadomości:</label>
	                          <textarea class="form-control summernote" name="content"></textarea>
	                        </div>
	                      </div>
	                      <div class="col-md-12">
	                        <div class="form-group bd-t-0-force">
	                          <button type="button" onclick="send_message();" class="btn btn-primary cursor">Wyślij</button>
	                        </div>
	                      </div><!-- col-4 -->
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->


    <script type="text/javascript">
      function send_message()
      {
        var liame = document.getElementsByName('liame')[0].value;
        var title = document.getElementsByName('title')[0].value;
        var content = document.getElementsByName('content')[0].value;

        alert("Tytuł: " + title + "<br>" + content);
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>mailerNewsletter/thankyou.php", 
             data: {liame:liame, title:title, content:content}, 
             cache: false,
             success: function(html) {
              alert('success');
             },
             complete:function(html)
             {
                document.getElementById('answer').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Odpowiedź została wysłana!</span>';
                
             }  
        });  
      }
    </script>