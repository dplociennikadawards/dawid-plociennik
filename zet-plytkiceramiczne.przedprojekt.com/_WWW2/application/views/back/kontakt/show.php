<?php 
$main_page = '';
$page = '';
if($this->uri->segment(3) == 'show') {
  $page = 'Kontakt';
  $subpage = 'Wiadomość od użykownika - ' . $row->name;
  $icon = 'fas fa-mail-bulk';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-6">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-12">
                        <div class="form-group">
                          <label class="form-control-label">Imię: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" value="<?php echo $row->name; ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Adres e-mail:</label>
                          <input class="form-control" type="text" value="<?php echo $row->email; ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Zgoda RODO:</label>
                          <input class="form-control" type="text" value="<?php if($row->zgoda == 0) {echo 'Nie zaakceptowane';} else { echo 'Zaaakceptowane'; } ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Zgoda telefoniczna:</label>
                          <input class="form-control" type="text" value="<?php if($row->zgoda2 == 0) {echo 'Nie zaakceptowane';} else { echo 'Zaaakceptowane'; } ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł wiadomości:</label>
                          <input class="form-control" type="text" value="<?php echo $row->subject; ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wiadomości:</label>
                          <textarea class="form-control" rows="5" readonly><?php echo $row->content; ?></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Data wysłania wiadomości:</label>
                          <input class="form-control" type="text" value="<?php echo date('H:i d.m.Y', strtotime($row->date)); ?>" readonly>
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <a href="<?php echo base_url(); ?>panel/admin/delete/kontakt/<?php echo $row->kontakt_id; ?>" type="submit" name="save" class="btn btn-danger" style="cursor: pointer;">Usuń wiadomość</a>
                          <a href="<?php echo base_url(); ?>panel/<?php echo strtolower($page); ?>" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                </div><!-- col-md-7 -->
                <div class="col-md-6">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
	                      <div class="pr-md-0 col-12">
	                      	<?php if($row->odpowiedz == 0): ?>
                    			<p id="answer">ODPOWIEDŹ</p>
                    		<?php else: ?>
                    			<p id="answer"><span class="text-success"><i class="fas fa-check"></i> Odpowiedź została wysłana!</span></p>
                    		<?php endif; ?>
	                        <div class="form-group">
	                          <label class="form-control-label">Tytuł wiadomości:</label>
	                          <input class="form-control" type="text" name="title">
	                          <input class="form-control" type="hidden" name="liame" value="<?php echo $row->email; ?>">
	                        </div>
	                      </div>
	                      <div class="pr-md-0 col-12">
	                        <div class="form-group bd-t-0-force">
	                          <label class="form-control-label">Treść wiadomości:</label>
	                          <textarea class="form-control summernote" name="content"></textarea>
	                        </div>
	                      </div>
	                      <div class="col-md-12">
	                        <div class="form-group bd-t-0-force">
	                          <button type="button" onclick="send_message(<?php echo $row->kontakt_id; ?>);" class="btn btn-primary cursor">Wyślij</button>
	                        </div>
	                      </div><!-- col-4 -->
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->


    <script type="text/javascript">
      function send_message(id)
      {
        var id = id;
        var title = document.getElementsByName('title')[0].value;
        var content = document.getElementsByName('content')[0].value;
        var liame = document.getElementsByName('liame')[0].value;
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/send", 
             data: {id:id, title:title, content:content, liame:liame}, 
             cache: false,
             complete:function(html)
             {
                document.getElementById('answer').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Odpowiedź została wysłana!</span>';
                
             }  
        });  
      }
    </script>