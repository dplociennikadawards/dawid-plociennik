<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'promocje') {
  $page = 'Produkty';
  $subpage = 'Edytuj wpis - ' . $row->title;
  $icon = 'fas fa-boxes';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-12">
                        <div class="form-group">
                          <label class="form-control-label">Kategoria: <span class="tx-danger">*</span></label>
                            <select name="kategoria" class="form-control select2-show-search" required>
                              <option selected disabled value="">
                                Wybierz kategorię
                              </option>

                              <option <?php if($row->kategoria == $oferta->value1){echo 'selected';} ?> value="<?php echo $oferta->value1 ?>">
                                <?php echo $oferta->value1 ?>
                              </option>

                              <option <?php if($row->kategoria == $oferta1->value1){echo 'selected';} ?> value="<?php echo $oferta1->value1 ?>">
                                <?php echo $oferta1->value1 ?>
                              </option>

                              <option <?php if($row->kategoria == $oferta2->value1){echo 'selected';} ?> value="<?php echo $oferta2->value1 ?>">
                                <?php echo $oferta2->value1 ?>
                              </option>

                              <option <?php if($row->kategoria == $oferta3->value1){echo 'selected';} ?> value="<?php echo $oferta3->value1 ?>">
                                <?php echo $oferta3->value1 ?>
                              </option>

                              <option <?php if($row->kategoria == $oferta4->value1){echo 'selected';} ?> value="<?php echo $oferta4->value1 ?>">
                                <?php echo $oferta4->value1 ?>
                              </option>

                              <option <?php if($row->kategoria == $oferta5->value1){echo 'selected';} ?> value="<?php echo $oferta5->value1 ?>">
                                <?php echo $oferta5->value1 ?>
                              </option>

                            </select>
                        </div>
                      </div>
                    <?php if($row->promocje_id != 1): ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_title"  placeholder="Wprowadź tytuł w nagłówku (domyślnie tytuł wpisu)" value="<?php echo $row->header_title; ?>">
                          <input class="form-control" type="hidden" name="header_photo" id="header_zdjecie" required value="<?php echo $row->header_photo; ?>">
                          <input class="form-control" type="hidden" name="photo" id="zdjecie" required value="<?php echo $row->photo; ?>">
                          <input class="form-control" type="hidden" name="resolution" id="rozdzielczosc" required value="<?php echo $row->resolution; ?>">
                          <input class="form-control" type="hidden" name="icon" id="icon" value="<?php echo $row->icon; ?>" required>
                        </div>
                      </div>
                    <?php endif; ?>
                    <?php if($row->promocje_id != 1): ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_subtitle"  placeholder="Wprowadź podtytuł w nagłówku (opcjonalnie)" value="<?php echo $row->header_subtitle; ?>">
                        </div>
                      </div>
                    <?php endif; ?>
                    <?php if($row->promocje_id == -1): ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Typ galerii:</label>
                            <div class="row">
                              <div class="col-md-3">
                                <label class="ckbox">
                                  <input name="gallery_status" value="1" type="checkbox" <?php if($row->gallery_status == '1'){ echo 'checked'; } ?>>
                                  <span>Galeria zdjęć</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="rdiobox">
                                  <input name="type_gallery" type="radio" value="gallery" <?php if($row->type_gallery == 'gallery'){ echo 'checked'; } ?>>
                                  <span>Galeria zdjęć</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="rdiobox">
                                  <input name="type_gallery" type="radio" value="slider" <?php if($row->type_gallery == 'slider'){ echo 'checked'; } ?>>
                                  <span>Slajder</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="ckbox">
                                  <input name="active_gallery" value="1" type="checkbox" <?php if($row->active_gallery == '1'){ echo 'checked'; } ?>>
                                  <span>Czy zdjęcie ma być powiększane?</span>
                                </label>
                              </div>
                            </div>
                        </div>
                      </div>
                    <?php endif; ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group <?php if($row->promocje_id != 1): ?>bd-t-0-force<?php endif; ?>">
                          <label class="form-control-label">Tytuł wpisu: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="title"  placeholder="Wprowadź tytuł wpisu" required value="<?php echo $row->title; ?>">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł wpisu:</label>
                          <textarea class="form-control" name="subtitle" rows="6" placeholder="Wprowadź podtytuł wpisu (opcjonalnie)"><?php echo $row->subtitle; ?></textarea>
                        </div>
                      </div>
                    <?php if($row->promocje_id != 1): ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wpisu: <span class="tx-danger">*</span></label>
                          <textarea class="form-control summernote" placeholder="Wprowadź treść wpisu" name="content" required><?php echo $row->content; ?></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wpisu 2:</label>
                          <textarea class="form-control summernote" placeholder="Wprowadź treść wpisu" name="content2"><?php echo $row->content2; ?></textarea>
                        </div>
                      </div>
                    <?php endif; ?>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Data (domyślnie obecna data):</label>
                          <input class="form-control" type="date" name="selected_data" placeholder="DD.MM.YYYY" value="<?php echo $row->selected_data; ?>">
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/promocje" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
              <?php if($row->promocje_id != 1): ?>
                <div class="col-md-5">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
                      <form method="post" id="upload_form2" enctype="multipart/form-data">
                        <div id="spinner"></div>
                        <label class="form-control-label">Zdjęcie nagłówka (domyślnie zdjęcie ze strony głównej):</label>
                          <div id="uploaded_image2" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box(2);">
                            <?php if($row->header_photo != null): ?>
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->header_photo; ?>" width="100%" height="180" class="img-thumbnail" />
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="image_file2" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
              <?php endif; ?>
              <?php if($row->promocje_id != 1): ?>
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-t-0-force bd-l-0-force">
                      <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner2"></div>
                        <label class="form-control-label">Zdjęcie główne wpisu:</label>
                          <div id="uploaded_image" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box('');">
                            <?php if($row->photo != null): ?>
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" width="100%" height="180" class="img-thumbnail" />
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
              <?php endif; ?>
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
