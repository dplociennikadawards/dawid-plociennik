<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'oferta') {
  $page = 'Produkty';
  $icon = 'fas fa-boxes';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-10p">Polecane</th>
            <th class="wd-10p">Na głównej</th>
            <th class="wd-35p">Tytuł</th>
            <th class="wd-20p">Data</th>
            <th class="wd-30p text-center">
              <a href="oferta/add" class="add cursor"><i class="fas fa-plus"></i> Dodaj wpis</a>
              <a href="gallery/add/<?php echo str_replace(' ', '',strtolower($page)); ?>" class="add-alt cursor"><i class="fas fa-plus"></i> Galeria zdjęć</a>
            </th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
          <?php if($row->oferta_id == 1 || $row->oferta_id == 2): ?>
            <tr style="background: #000; color: white;" >
              <td  class="align-middle"><?php echo $i; ?>.</td>        
              <td class="align-middle">
                <?php if($row->oferta_id > 2): ?> 
                <label class="ckbox ckbox-primary">
                  <input id="active<?php echo $row->oferta_id; ?>" type="checkbox" onchange="active(<?php echo $row->oferta_id; ?>);" <?php if($row->promoted == 1){echo 'checked';} ?>>
                    <span></span>
                </label>
                <?php endif; ?>
              </td>        
              <td class="align-middle">
                <?php if($row->oferta_id > 2): ?> 
                <label class="ckbox ckbox-primary">
                  <input id="active_index<?php echo $row->oferta_id; ?>" type="checkbox" onchange="active_index(<?php echo $row->oferta_id; ?>);" <?php if($row->active_index == 1){echo 'checked';} ?>>
                    <span></span>
                </label>
                <?php endif; ?>
              </td>
              <td  class="align-middle"><?php echo $row->title; ?></td>
              <td  class="align-middle"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/oferta/edit/<?php echo $row->oferta_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <?php if ($row->oferta_id > 2): ?>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/oferta/<?php echo $row->oferta_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                <?php endif; ?>
              </td>
            </tr>
            <?php endif; ?>
          <?php endforeach ?>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
           <?php if($row->oferta_id > 2): ?>
            <tr>
              <td  class="align-middle"><?php echo $i; ?>.</td>        
              <td class="align-middle">
                <?php if($row->oferta_id > 2): ?> 
                <label class="ckbox ckbox-primary">
                  <input id="active<?php echo $row->oferta_id; ?>" type="checkbox" onchange="active(<?php echo $row->oferta_id; ?>);" <?php if($row->promoted == 1){echo 'checked';} ?>>
                    <span></span>
                </label>
                <?php endif; ?>
              </td>        
              <td class="align-middle">
                <?php if($row->oferta_id > 2): ?> 
                <label class="ckbox ckbox-primary">
                  <input id="active_index<?php echo $row->oferta_id; ?>" type="checkbox" onchange="active_index(<?php echo $row->oferta_id; ?>);" <?php if($row->active_index == 1){echo 'checked';} ?>>
                    <span></span>
                </label>
                <?php endif; ?>
              </td>
              <td  class="align-middle"><?php echo $row->title; ?></td>
              <td  class="align-middle"><?php echo date('d.m.Y', strtotime($row->selected_data)); ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/oferta/edit/<?php echo $row->oferta_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <?php if ($row->oferta_id > 2): ?>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/oferta/<?php echo $row->oferta_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endif; ?>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script type="text/javascript">
  function active(nr)
  {
      id = nr;

      value = document.getElementById('active'+nr).checked;
      if(value == true) { value = 1; } else { value = 0; }

        if (id!=0) {
                $.ajax({  
                     type: "post", 
                     url:"<?php echo base_url(); ?>set_active_promoted", 
                     data: {id:id, value:value}, 
                     cache: false,  
                     success:function(html)  
                     {  
                     }  
                });  
          }
  }
</script>
<script type="text/javascript">
  function active_index(nr)
  {
      id = nr;
      value = document.getElementById('active_index'+nr).checked;
      if(value == true) { value = 1; } else { value = 0; }

        if (id!=0) {
                $.ajax({  
                     type: "post", 
                     url:"<?php echo base_url(); ?>set_active_index", 
                     data: {id:id, value:value}, 
                     cache: false,  
                     success:function(html)  
                     {  
                     }  
                });  
          }
  }
</script>