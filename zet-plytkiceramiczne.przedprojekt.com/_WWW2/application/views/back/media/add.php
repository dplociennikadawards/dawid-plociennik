<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'media') {
  $page = 'Media';
  $subpage = 'Dodaj plik';
  $icon = 'fas fa-box-open';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-3"></div>
              <div class="col-md-2">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-r-0-force">
                          <input class="form-control" type="hidden" name="pdf" id="pdf" required>
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/<?php echo $page; ?>" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-4">
                  <div class="pl-md-0 col-12">
                    <div class="form-group">
                      <form method="post" id="upload_pdf" enctype="multipart/form-data">
                        <div id="spinnerPDF"></div>
                        <label class="form-control-label">Plik:</label>
                          <div id="uploaded_pdf" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_pdf();">
                          </div>  
                          <input class="form-control mt-2" type="file" id="pdf_file" name="pdf_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
