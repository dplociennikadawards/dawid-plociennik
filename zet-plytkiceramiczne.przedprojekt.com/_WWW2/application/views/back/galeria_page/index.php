<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'galeria') {
  $page = 'Galeria';
  $icon = 'fas fa-images';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html"><?php echo $page_title; ?></a>
          <span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $page; ?></span>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
          <?php 
          if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
          }
          ?>
        <table class="table table-hover">
          <thead>
            <th class="wd-5p">Np.</th>
            <th class="wd-20p">Kategoria</th>
            <th class="wd-35p">Zdjęcie</th>
            <th class="wd-20p">Data</th>
            <th class="wd-30p text-center">
              <a href="galeria/add" class="add cursor"><i class="fas fa-plus"></i> Dodaj</a>
              <a href="galeria/add_cat" class="add-alt cursor"><i class="fas fa-plus"></i> Dodaj kategorię</a>
            </th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($rows as $row): $i++; ?>
            <tr <?php if($row->galeria_id == 1 || $row->galeria_id == 2){ echo 'style="background: #000; color: white;"';} ?>>
              <td  class="align-middle"><?php echo $i; ?>.</td>
              <td  class="align-middle"><?php echo $row->title; ?></td>
              <td  class="align-middle">
                <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" width="100%" height="180" class="img-thumbnail" style="max-width: 200px;" />
              </td>
              <td  class="align-middle"><?php echo date('d.m.Y', strtotime($row->date)); ?></td>
              <td  class="align-middle text-center">
                <a href="<?php echo base_url(); ?>panel/galeria/edit/<?php echo $row->galeria_id; ?>"><i class="text-success far fa-edit tx-30  btn"></i></a>
                <?php if ($row->galeria_id > 2): ?>
                  <a href="<?php echo base_url(); ?>panel/admin/delete/galeria/<?php echo $row->galeria_id; ?>"><i class="text-danger far fa-trash-alt tx-30 btn"></i></a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table> 

      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->