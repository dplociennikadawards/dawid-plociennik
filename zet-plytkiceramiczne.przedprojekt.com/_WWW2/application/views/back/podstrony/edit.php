<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'podstrony') {
  $page = 'Podstrony';
  $subpage = 'Edytuj wpis - ' . $row->header_title;
  $icon = 'far fa-object-ungroup';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-12">
                        <div class="form-group">
                          <label class="form-control-label">Tytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_title"  placeholder="Wprowadź tytuł w nagłówku (domyślnie tytuł wpisu)" value="<?php echo $row->header_title; ?>">
                          <input class="form-control" type="hidden" name="header_photo" id="header_zdjecie" required value="<?php echo $row->header_photo; ?>">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_subtitle"  placeholder="Wprowadź podtytuł w nagłówku (opcjonalnie)" value="<?php echo $row->header_subtitle; ?>">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tekst w nagłówku:</label>
                          <textarea class="form-control" rows="6" name="header_content"  placeholder="Wprowadź tresc w nagłówku (opcjonalnie)"><?php echo $row->header_content; ?></textarea>
                        </div>
                      </div>
                      <?php if($row->page == 'o_nas' || $row->page == 'oferta' || $row->page == 'galeria' || $row->page == 'cennik' || $row->page == 'index'): ?>
                        <div class="pr-md-0 col-12">
                          <div class="form-group bd-t-0-force">
                            <label class="form-control-label">Typ galerii:</label>
                              <div class="row">
                                <?php if($row->page == 'o_nas' || $row->page == 'oferta' || $row->page == 'galeria' || $row->page == 'cennik'): ?>
                                <div class="col-md-3">
                                  <label class="ckbox">
                                    <input name="gallery_status" value="1" type="checkbox" <?php if($row->gallery_status == '1'){ echo 'checked'; } ?>>
                                    <span>Galeria zdjęć</span>
                                  </label>
                                </div>
                                  <div class="col-md-3">
                                    <label class="rdiobox">
                                      <input name="type_gallery" type="radio" value="gallery" <?php if($row->type_gallery == 'gallery'){ echo 'checked'; } ?>>
                                      <span>Galeria zdjęć</span>
                                    </label>
                                  </div>
                                  <div class="col-md-3">
                                    <label class="rdiobox">
                                      <input name="type_gallery" type="radio" value="slider" <?php if($row->type_gallery == 'slider'){ echo 'checked'; } ?>>
                                      <span>Slajder</span>
                                    </label>
                                  </div>
                                <?php endif; ?>
                                <div class="col-md-3">
                                  <label class="ckbox">
                                    <input name="active_gallery" value="1" type="checkbox" <?php if($row->active_gallery == '1'){ echo 'checked'; } ?>>
                                    <span>Czy zdjęcie ma być powiększane?</span>
                                  </label>
                                </div>
                              </div>
                          </div>
                        </div>
                      <?php endif; ?>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/uslugi" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-5">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
                      <form method="post" id="upload_form2" enctype="multipart/form-data">
                        <div id="spinner"></div>
                        <label class="form-control-label">Zdjęcie nagłówka:</label>
                          <div id="uploaded_image2" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box(2);">
                            <?php if($row->header_photo != null): ?>
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->header_photo; ?>" width="100%" height="180" class="img-thumbnail" />
                            <?php endif; ?>
                          </div>  
                          <input class="form-control mt-2" type="file" id="image_file2" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
