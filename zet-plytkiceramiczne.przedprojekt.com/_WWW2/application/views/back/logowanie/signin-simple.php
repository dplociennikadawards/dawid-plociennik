<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>AD Panel - <?php echo base_url(); ?></title>

    <!-- vendor css -->
    <link href="../assets/back/template/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/back/template/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../assets/back/template/css/bracket.css">
    <link rel="stylesheet" href="../assets/back/template/css/style.css">
  </head>

  <body>

    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

    <form method="POST" action="">
      <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">
            <img src="<?php echo base_url(); ?>assets/back/img/logoAdAwards.png"><br>
            <span class="tx-normal">[</span> AD <span class="tx-ad ">Panel</span> <span class="tx-normal">]</span>
        </div>
        <div class="tx-center mg-b-60">Panel zarządzania stroną <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a></div>
        <?php echo validation_errors(); 
        if($this->session->flashdata('logged_failed')) {
            echo $this->session->flashdata('logged_failed');
        }
        if($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
        }
        if(isset($_GET['recovery_password'])) {
            echo '<p class="text-success font-weight-bold">Na Twój adres E-mail została wysłana instrukcja przywracania hasła.</p>';
        }

        ?>
        <div class="form-group">
          <input name="login" type="text" class="form-control" placeholder="Wprowadź swój login" required>
        </div><!-- form-group -->
        <div class="form-group">
          <input name="password" type="password" class="form-control" placeholder="Wprowadź swoje hasło" required>
          <a href="login/recovery_password" class="tx-info tx-12 d-block mg-t-10">Zapomniałeś hasła?</a>
        </div><!-- form-group -->
        <button type="submit" class="btn btn-info btn-block">Zaloguj się</button>
      </div><!-- login-wrapper -->
    </form>

    </div><!-- d-flex -->

    <script src="../assets/back/template/lib/jquery/jquery.js"></script>
    <script src="../assets/back/template/lib/popper.js/popper.js"></script>
    <script src="../assets/back/template/lib/bootstrap/js/bootstrap.js"></script>

  </body>
</html>
