<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'promocje2') {
  $page = 'Promocje';
  $subpage = 'Dodaj wpis';
  $icon = 'fas fa-percent';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-12">
                        <div class="form-group">
                          <label class="form-control-label">Tytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_title"  placeholder="Wprowadź tytuł w nagłówku (domyślnie tytuł wpisu)">
                          <input class="form-control" type="hidden" name="header_photo" id="header_zdjecie" required>
                          <input class="form-control" type="hidden" name="photo" id="zdjecie" required>
                          <input class="form-control" type="hidden" name="resolution" id="rozdzielczosc" required>
                          <input class="form-control" type="hidden" name="icon" id="icon" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł w nagłówku:</label>
                          <input class="form-control" type="text" name="header_subtitle"  placeholder="Wprowadź podtytuł w nagłówku (opcjonalnie)">
                        </div>
                      </div>
                      <!-- <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Typ galerii:</label>
                            <div class="row">
                              <div class="col-md-3">
                                <label class="ckbox">
                                  <input name="gallery_status" value="1" type="checkbox">
                                  <span>Galeria zdjęć</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="rdiobox">
                                  <input name="type_gallery" type="radio" value="gallery" checked>
                                  <span>Galeria zdjęć</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="rdiobox">
                                  <input name="type_gallery" type="radio"  value="slider">
                                  <span>Slajder</span>
                                </label>
                              </div>
                              <div class="col-md-3">
                                <label class="ckbox">
                                  <input name="active_gallery" value="1" type="checkbox">
                                  <span>Czy zdjęcie ma być powiększane?</span>
                                </label>
                              </div>
                            </div>
                        </div>
                      </div> -->
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Tytuł wpisu: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="title"  placeholder="Wprowadź tytuł wpisu" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Podtytuł wpisu:</label>
                          <input class="form-control" type="text" name="subtitle"  placeholder="Wprowadź podtytuł wpisu (opcjonalnie)">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wpisu: <span class="tx-danger">*</span></label>
                          <textarea class="form-control summernote" placeholder="Wprowadź treść wpisu" name="content" required></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść wpisu 2:</label>
                          <textarea class="form-control summernote" placeholder="Wprowadź treść wpisu" name="content2"></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Data (domyślnie obecna data):</label>
                          <input class="form-control" type="date" name="selected_data"  placeholder="DD.MM.YYYY">
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/promocje2" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-5">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
                      <form method="post" id="upload_form2" enctype="multipart/form-data">
                        <div id="spinner"></div>
                        <label class="form-control-label">Zdjęcie nagłówka (domyślnie zdjęcie ze strony głównej):</label>
                          <div id="uploaded_image2" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box(2);"></div>  
                          <input class="form-control mt-2" type="file" id="image_file2" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-t-0-force bd-l-0-force">
                      <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner2"></div>
                        <label class="form-control-label">Zdjęcie główne wpisu:</label>
                          <div id="uploaded_image" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box('');"></div>  
                          <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      </form>
                    </div>
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->
