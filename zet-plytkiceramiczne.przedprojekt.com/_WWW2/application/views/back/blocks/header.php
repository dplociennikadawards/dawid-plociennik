<body class="collapsed-menu">

<div id="preloader">
    <div id="status">
<!--       <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
      </div> -->
<!-- <div class="sk-rotating-plane bg-gray-800"></div> -->
		<div class="sk-double-bounce">
		    <div class="sk-child sk-double-bounce1 bg-gray-800"></div>
		    <div class="sk-child sk-double-bounce2 bg-gray-800"></div>
		</div>    
	</div>
</div>

    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo">
      <a href="<?php echo base_url(); ?>" class="mx-auto">
        <img src="<?php echo base_url(); ?>assets/back/img/logoAdAwards.png" width="75">
      </a>
    </div>
    <div class="br-sideleft overflow-y-auto green-dark-bg">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Menu</label>
      <!-- br-sideleft-menu -->
      <ul class="br-sideleft-menu">


        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/admin" class="br-menu-link <?php if($this->uri->segment(2)=="admin"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-home tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Strona główna</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/podstrony" class="br-menu-link <?php if($this->uri->segment(2)=="podstrony"){echo "active";}?>">
            <i class="menu-item-icon icon far fa-object-ungroup tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Podstrony</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/glowna" class="br-menu-link <?php if($this->uri->segment(2)=="glowna"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-file-invoice tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Ustawienia strony głównej</span>
          </a>
        </li><!-- br-menu-item -->

        <!-- <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/faq" class="br-menu-link <?php if($this->uri->segment(2)=="faq"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-question tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;FAQ</span>
          </a>
        </li> --><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/onas" class="br-menu-link <?php if($this->uri->segment(2)=="onas"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-users tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;O firmie</span>
          </a>
        </li><!-- br-menu-item -->

        <!-- <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/oferta" class="br-menu-link <?php if($this->uri->segment(2)=="oferta"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-project-diagram tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Oferta</span>
          </a>
        </li> --><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/promocje" class="br-menu-link <?php if($this->uri->segment(2)=="promocje"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-boxes tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Produkty</span>
          </a>
        </li> <!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/galeria" class="br-menu-link <?php if($this->uri->segment(2)=="galeria" || $this->uri->segment(5)=="uslugi_pogrzebowe"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-images tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Realizacje</span>
          </a>
        </li><!-- br-menu-item -->

        <!-- <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/cennik" class="br-menu-link <?php if($this->uri->segment(2)=="cennik"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-hand-holding-usd tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Cennik</span>
          </a>
        </li> --><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/promocje2" class="br-menu-link <?php if($this->uri->segment(2)=="promocje2" || $this->uri->segment(5)=="uslugi_pogrzebowe"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-map-pin tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Promocje i wyprzedaże</span>
          </a>
        </li>

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/kontakt" class="br-menu-link <?php if($this->uri->segment(2)=="kontakt"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-mail-bulk tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Kontakt</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/media" class="br-menu-link <?php if($this->uri->segment(2)=="media"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-box-open tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Media</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/producenci" class="br-menu-link <?php if($this->uri->segment(2)=="producenci"){echo "active";}?>">
            <i class="menu-item-icon icon fab fa-product-hunt tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Producenci</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/ustawienia" class="br-menu-link <?php if($this->uri->segment(2)=="ustawienia"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-cogs tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Ustawienia</span>
          </a>
        </li><!-- br-menu-item -->

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/newsletter" class="br-menu-link <?php if($this->uri->segment(2)=="media"){echo "active";}?>">
            <i class="menu-item-icon icon fas fa-envelope-open-text tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Newsletter</span>
          </a>
        </li>

        <li class="br-menu-item">
          <a href="<?php echo base_url(); ?>panel/login/logout" class="br-menu-link">
            <i class="menu-item-icon icon ion-power tx-20 text-white"></i><span class="menu-item-label menu-text">&nbsp;Wyloguj</span>
          </a>
        </li><!-- br-menu-item -->

      </ul><!-- br-sideleft-menu -->

      <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->


    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>

      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo $_SESSION['first_name']; ?></span>
              <img src="<?php echo $ico; ?>" class="wd-32 rounded" alt="">
              <span class="square-10 bg-success"></span>
            </a>
          </div>
        </nav>
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->