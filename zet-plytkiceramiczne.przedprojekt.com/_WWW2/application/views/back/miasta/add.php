<?php 
$main_page = '';
$page = '';
if($this->uri->segment(2) == 'miasta') {
  $page = 'Transport zmarłych';
  $subpage = 'Dodaj miejsowość';
  $icon = 'fas fa-shuttle-van';
  $main_page = 'active';
}
?>

<!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="index.html">Krematorium.pl</a>
          <span class="breadcrumb-item"><?php echo $page; ?></span>
          <?php if($subpage != ''): ?><span class="breadcrumb-item <?php echo $main_page; ?>"><?php echo $subpage; ?></span><?php endif; ?>
        </nav>
      </div><!-- br-pageheader -->
      <div class="br-pagetitle">
        <i class="icon <?php echo $icon; ?>"></i>
        <div>
          <h4><?php echo $page; ?> <?php if($subpage != ''){ echo '- ' . $subpage; } ?></h4>
          <p class="mg-b-0">Panel zarządzania stroną <a href="<?php echo base_url(); ?>" class="text-gold"><?php echo $page_title; ?></a></p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <?php echo validation_errors() ?>
          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-7">
                <form action="" method="POST">
                  <div class="row no-gutters">
                      <div class="pr-md-0 col-12">
                        <div class="form-group">
                          <label class="form-control-label">Nazwa na mapie: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="nazwa"  placeholder="Wprowadź nazwe wyświetlaną na mapie" required>
                          <input class="form-control" type="hidden" name="photo" id="zdjecie" required>
                          <input class="form-control" type="hidden" name="resolution" id="rozdzielczosc" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Lokalizacja <span class="tx-danger">*</span></label>
                          <input class="form-control complete" type="text" name="lokalizacja" placeholder="Wprowadź lokalizację miejsowości" required>
                          <input class="form-control complete" type="hidden" name="lat">
                          <input class="form-control complete" type="hidden" name="lng">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Imię i nazwisko pracownika: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="worker" placeholder="Wprowadź imię i nazwisko pracownika" required>
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Stanowisko:</label>
                          <input class="form-control" type="text" name="stanowisko" placeholder="Wprowadź stanowisko pracownika" >
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Telefon:</label>
                          <input class="form-control" type="tel" name="phone" placeholder="Wprowadź numer telefonu pracownika (opcjonalnie)" >
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Telefon komórkowy:</label>
                          <input class="form-control" type="tel" name="mobile" placeholder="Wprowadź numer komórkowy pracownika (opcjonalnie)">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Adres e-mail:</label>
                          <input class="form-control" type="email" name="email" placeholder="Wprowadź adres e-mail (opcjonalnie)">
                        </div>
                      </div>
                      <div class="pr-md-0 col-12">
                        <div class="form-group bd-t-0-force">
                          <label class="form-control-label">Treść: <span class="tx-danger">*</span></label>
                          <textarea class="form-control summernote" placeholder="Wprowadź treść" name="text"></textarea>
                        </div>
                      </div>
                      <div class="pr-md-0 col-md-12 px-0">
                        <div class="form-group bd-t-0-force">
                          <button type="submit" name="save" class="btn btn-primary" style="cursor: pointer;">Zapisz</button>
                          <a href="<?php echo base_url(); ?>panel/onas" class="btn btn-secondary" style="cursor: pointer;">Anuluj</a>
                        </div>
                      </div><!-- col-4 -->
                    </div><!-- row no-gutters -->
                  </form>
                </div><!-- col-md-7 -->
                <div class="col-md-5">
                  <div class="pl-md-0 col-12">
                    <div class="form-group bd-l-0-force">
                      <form method="post" id="upload_form" enctype="multipart/form-data">
                        <div id="spinner2"></div>
                        <label class="form-control-label">Zdjęcie pracownika:</label>
                          <div id="uploaded_image" class="delete_photo cursor" title="Usuń zdjęcie" onclick="clear_box('');"></div>  
                          <input class="form-control mt-2" type="file" id="image_file" name="image_file">
                      </form>
                    </div>
                  </div><!-- col-12 -->
                </div><!-- col-md-5 -->
              </div><!-- row --> 
            </div><!-- div form -->
      </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->



    <!-- ########## END: MAIN PANEL ########## -->

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAyg6xrHbL5io0jxM_jCJhgB4wLPx6LWBc&libraries=places"></script>

<script src="<?php echo base_url(); ?>assets/back/scriptMapa/jquery.geocomplete.js"></script>
<script type="text/javascript">

  // Call Geo Complete
  $(".complete").geocomplete({ details: "form" });


</script>