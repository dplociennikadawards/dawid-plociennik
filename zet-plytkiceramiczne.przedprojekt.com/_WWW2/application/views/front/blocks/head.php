<?php 
if($this->uri->segment(2) == 'ofirmie') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'produkty') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'realizacje') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'dlaprojektanta') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'kontakt') {
$first = $podstrony->header_title . ' - ';
$second = $ustawienia->page_title;
}
elseif($this->uri->segment(2) == 'produkt') {
$first = $produkt->title . ' - ';
$second = $ustawienia->page_title;
}
else {
$first = $ustawienia->page_title; 
$second = '';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $first . ' ' . $second; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>upload/<?php echo $ustawienia->icon; ?>">

  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans|Rubik:700" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- owl carousel -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/assets/owl.theme.default.min.css">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/front/css/mediaqueries.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.carousel.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.theme.default.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/owl.theme.green.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/core.scss">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owl/scss/animate.scss">


</head>
<body>