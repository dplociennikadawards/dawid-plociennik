  <header style="border-bottom: 3px solid #7BA543;">


    <nav class="navbar navbar-expand-lg navbar-light">

      <div class="navbar__container container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img class="navbar__brand" src="<?php echo base_url(); ?>upload/<?php echo $ustawienia->photo; ?>" alt="logo"></a>

        <div class="navbar__socialmedia-box navbar__socialmedia-box--mobile">
          <?php if($ustawienia->facebook != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->facebook; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="facebook">
          </a>
          <?php endif; ?>
          <?php if($ustawienia->twitter != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->twitter; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/twitter.png" alt="twitter">
          </a>
          <?php endif; ?>
          <?php if($ustawienia->instagram != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->instagram; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/instagram.png" alt="instagram">
          </a>
          <?php endif; ?>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link">Home</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>">home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">O firmie</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/ofirmie">o firmie</a>
            </li>
            <li id="showMenu" class="nav-item">
              <a class="nav-link">Produkty</a>
              <!-- <a class="navbar__navlink" href="<?php echo base_url(); ?>p/produkty">produkty</a> -->
              
                <a class="navbar__navlink" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Produkty
                </a>
                <div id="dropdownMenu" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/produkty">Wszystkie produkty</a>
  <?php $slug = $oferta->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta->value1; ?></a>

  <?php $slug = $oferta1->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta1->value1; ?></a>

  <?php $slug = $oferta2->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta2->value1; ?></a>

  <?php $slug = $oferta3->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta3->value1; ?></a>

  <?php $slug = $oferta4->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta4->value1; ?></a>

  <?php $slug = $oferta5->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>"><?php echo $oferta5->value1; ?></a>
                </div>

            </li>
            <li class="nav-item">
              <a class="nav-link">realizacje</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/realizacje">realizacje</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">Promocje i wyprzedaże</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/promocje">Promocje i wyprzedaże</a>
            </li>
            <li class="nav-item">
              <a class="nav-link">kontakt</a> <!-- <-- hidden -->
              <a class="navbar__navlink" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
            </li>
          </ul>
        </div>
        <div class="navbar__socialmedia-box navbar__socialmedia-box--desktop">
          <?php if($ustawienia->facebook != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->facebook; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="facebook">
          </a>
          <?php endif; ?>
          <?php if($ustawienia->twitter != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->twitter; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/twitter.png" alt="twitter">
          </a>
          <?php endif; ?>
          <?php if($ustawienia->instagram != null): ?>
          <a class="navbar__sociallink" href="<?php echo $ustawienia->instagram; ?>">
            <img class="navbar__socialicon" src="<?php echo base_url(); ?>assets/front/img/instagram.png" alt="instagram">
          </a>
          <?php endif; ?>
        </div>
      </div>

    </nav>

<!--Carousel Wrapper-->
<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
        <?php $i=0; foreach ($slider as $row): $i++; ?>
        <div class="item item__background item__background--1 carousel-item <?php if($i == 1){echo 'active';} ?>" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $row->value4; ?>);">

          <div class="item__container container">
            <div class="item__content-box">
              <h4 class="item__title"><?php echo $row->value1; ?></h4>
              <h4 class="item__subtitle"><?php echo $row->value2; ?></h4>

              <p class="item__paragraph"><?php echo $row->value3; ?></p>

              <a class="item__link" href="<?php echo $row->value6; ?>">
                <button class="item__btn"><?php echo $row->value5; ?></button>
              </a>
            </div>
          </div>

        </div>
        <?php endforeach; ?>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->


  </header>

