



  <main>
<!-- Section: Blog v.3 -->
<section class="my-5">
<div class="container">
  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5"><?php echo $produkt->value1; ?></h2>
  <!-- Section description -->
  <p class="text-center dark-grey-text mx-auto mb-5"><?php echo $produkt->value3; ?></p>

  <hr class="my-5">


  <!-- Grid row -->
  <div class="row">
      <?php foreach ($rows as $value): ?>
      <?php $slug = $value->title;
      $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
      $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
      $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>
    <div class="col-md-4 mb-3">
        <!-- Grid column -->
        <div class="col-lg-12 col-xl-12 mb-3">

          <!-- Featured image -->
          <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $value->photo; ?>);">
            <a href="<?php echo base_url(); ?>p/produkt/<?php echo $slug; ?>/<?php echo $value->promocje_id; ?>">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-12 col-xl-12">

          <!-- Post title -->
          <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/produkt/<?php echo $slug; ?>/<?php echo $value->promocje_id; ?>" class="text-dark"><?php echo $value->title; ?></a></strong></h3>
          <!-- Excerpt -->
          <p class="dark-grey-text"><?php echo $value->subtitle; ?></p>
          <!-- Read more button -->
          <a href="<?php echo base_url(); ?>p/produkt/<?php echo $slug; ?>/<?php echo $value->promocje_id; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

        </div>
        <!-- Grid column -->
    </div>
      <?php endforeach; ?>
  </div>
  <!-- Grid row -->

  <hr class="my-5">

</div>
</section>
<!-- Section: Blog v.3 -->

  </main>