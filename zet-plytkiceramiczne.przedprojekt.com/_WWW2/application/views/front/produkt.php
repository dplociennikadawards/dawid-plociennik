<style type="text/css">
  img {
    margin-right: 20px;
    margin-left: 20px;
  }
</style>



  <main>

    <div class="container">
      
<!-- Section: Blog v.4 -->
<section class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Card -->
      <div class="card card-cascade wider reverse">

        <!-- Card image -->
        <div class="view view-cascade overlay bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $produkt->photo; ?>); height: 500px;">
            <div class="mask rgba-white-slight"></div>
        </div>

        <!-- Card content -->
        <div class="card-body card-body-cascade text-center">

          <!-- Title -->
          <h2 class="font-weight-bold"><?php echo $produkt->title; ?></h2>
          <h2><?php echo $produkt->header_subtitle; ?></h2>


        </div>
        <!-- Card content -->

      </div>
      <!-- Card -->

      <!-- Excerpt -->
      <div class="row mt-5">
        <div class="col-md-12 mb-3">
          <p>
            <?php echo $produkt->content; ?>
          </p>
        </div>
        <div class="col-md-12">
          <p>
            <?php echo $produkt->content2; ?>
          </p>
        </div>
      </div>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->
        <div class="owl-carousel owl-theme">
          <div class="items">
            <div class="row">
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell1.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell5.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell3.jpg');"></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 style="background-image: url('<?php echo base_url(); ?>assets/front/img/cell4.jpg');"></h4>
              </div>
            </div>
          </div>
        </div>
</section>
<!-- Section: Blog v.4 -->


    </div>

  </main>