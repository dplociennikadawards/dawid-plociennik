



  <main>
<!-- Section: Blog v.3 -->
<section class="my-5">
<div class="container">
  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5"><?php echo $promocja->title; ?></h2>
  <!-- Section description -->
  <p class="text-center dark-grey-text mx-auto mb-5"><?php echo $promocja->subtitle; ?></p>

  <hr class="my-5">
  <?php if(count($rows) <= 1){echo 'Chwilowo nie posiadamy żadnych promocji. Wróc poźniej.';} ?>
  <?php foreach ($rows as $row): ?>
  <?php if($row->promocje2_id > 1): ?>

  <?php $slug = $row->title;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '');
  $slug = str_replace($cenzura, $zamiana, $slug); ?>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>);">
        <a href="<?php echo base_url(); ?>p/promocja/<?php echo $slug; ?>/<?php echo $row->promocje2_id; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/promocja/<?php echo $slug; ?>/<?php echo $row->promocje2_id; ?>" class="text-dark"><?php echo $row->title; ?></a></strong></h3>
      <h3 class="mb-3"><?php echo $row->header_subtitle; ?></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $row->subtitle; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/promocja/<?php echo $slug; ?>/<?php echo $row->promocje2_id; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">
  <?php endif; ?>
  <?php endforeach; ?>

</div>
</section>
<!-- Section: Blog v.3 -->

  </main>