<style>
  li i:before {
    color: #7BA543;
  }
</style>



  <main>
    a
 <section class="bg-white p-md-5 py-3 py-md-0">

  <div class="row">
    <div class="col-md-4">
      <iframe src="<?php echo $kontakt->map; ?>" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-8">
       <!--Section heading-->
    <h4 class="standard-sect__title"><span class="standard-sect__bold"><?php echo $kontakt->title; ?></span></h4>
    <span class="standard-sect__separator mb-3"></span>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5"><?php echo $kontakt->subtitle; ?></p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <?php if(isset($_GET['mail_send'])): ?>
                <p class="text-success">Twoja wiadomość została wysłana. Wkrótce otrzymasz odpowiedź.</p>
            <?php endif; ?>
            <form id="contact-form" name="contact-form" action="<?php echo base_url(); ?>mailer/thankyou.php" method="POST"  onsubmit="return validateMyForm(0);">

                <!--Grid row-->
                <div class="row px-3 px-md-0">
                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <input type="hidden" name="my_email" value="<?php echo $kontakt->my_email; ?>" required>
                            <label for="name" class="">Twoje imię</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="email" id="email" name="liame" class="form-control" required>
                            <label for="email" class="">Twój adres e-mail</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control" required>
                            <label for="subject" class="">Temat</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea" required></textarea>
                            <label for="message">Twoja wiadomość</label>
                        </div>
                        <div class="form-check mb-3">
                            <input name="zgoda" value="1" type="checkbox" class="form-check-input" id="materialUnchecked">
                            <label class="form-check-label" for="materialUnchecked"><small>„Wyrażam zgodę na przetwarzanie przez danych osobowych podanych w formularzu. Podanie danych jest dobrowolne. Administratorem podanych przez Pana/ Panią danych osobowych jest Krematorium z siedzibą w Kowalska 8, Kowalowo, 82-133. Pana/Pani dane będą przetwarzane w celach związanych z udzieleniem odpowiedzi, przedstawieniem oferty usług Krematorium oraz świadczeniem usług przez administratora danych. Przysługuje Panu/Pani prawo dostępu do treści swoich danych oraz ich poprawiania.”</small></label>
                            <input name="zgoda2" value="1" type="checkbox" class="form-check-input" id="materialUnchecked2">
                            <label class="form-check-label" for="materialUnchecked2"><small>„Wyrażam zgodę na otrzymywanie informacji handlowych od Krematorium dotyczących jej oferty w szczególności poprzez połączenia telefoniczne lub sms z wykorzystaniem numeru telefonu podanego w formularzu, a także zgodę na przetwarzanie moich danych osobowych w tym celu przez Krematorium oraz w celach promocji, reklamy i badania rynku.”</small></label>
                        </div>
                    </div>
                </div>
                <center>
                <div class="g-recaptcha" data-sitekey="6Lfml5MUAAAAAHZejLidCrq6vt6c5Y4hFl67f7o7"></div>
              <span class="warning_captcha text-danger" class="text-danger"></span></center>
                <!--Grid row-->
                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-12">



                    </div>
                </div>
                <!--Grid row-->
            <div class="text-center text-md-left px-3 px-md-0">
                <button type="submit" class="btn mt-3 color-yellow btn-green text-shadow">Wyślij</button>
            </div>
            <div class="status"></div>
            </form>


        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x icon-color"></i>
                    <p><?php echo $kontakt->address; ?></p>
                </li>

                <li>
                    <a href="tel:<?php echo $kontakt->phone; ?>" class="text-dark">
                        <i class="fas fa-phone mt-4 fa-2x  icon-color"></i>
                        <p><?php echo $kontakt->phone; ?></p>
                    </a>
                </li>

                <li>
                    <a href="mailto:<?php echo $kontakt->my_email; ?>" class="text-dark">
                        <i class="fas fa-envelope mt-4 fa-2x  icon-color"></i>
                        <p><?php echo $kontakt->my_email; ?></p>
                    </a>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
    </div>
  </div>
 </section>
  </main>