



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo $poznaj->value2; ?>" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            <?php echo $poznaj->value1; ?>
          </a>
        </h4>  
        <div class="offer__row">
          <div class="container">
            <h4 class="standard-sect__title"><span class="standard-sect__bold">poznaj</span> naszą firmę</h4>
            <span class="standard-sect__separator"></span>

            <div class="row">
              <?php $i=0; foreach ($rows as $row): $i++; ?>
              <?php if($row->onas_id > 1): ?>
              <?php if($i % 2 != 0): ?>
              <div class="col-lg-6 bg-photo border-right" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>);">
              </div>
              <div class="col-lg-6 pt-3">
                <p class="standard-sect__paragraph">
                  <b><?php echo $row->title; ?></b>
                  <?php if($row->subtitle != null): ?>
                  <br>
                  <br>
                  <?php echo $row->subtitle; ?>
                  <?php endif; ?>
                  <br>
                  <br>
                  <?php echo $row->content; ?>
                </p>
              </div>
              <?php else: ?>
              <div class="col-lg-6 pt-3">
                <p class="standard-sect__paragraph">
                  <b><?php echo $row->title; ?></b>
                  <?php if($row->subtitle != null): ?>
                  <br>
                  <br>
                  <?php echo $row->subtitle; ?>
                  <?php endif; ?>
                  <br>
                  <br>
                  <?php echo $row->content; ?>
                </p>
              </div>
              <div class="col-lg-6 bg-photo  border-left" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>);">
              </div>
              <?php endif; ?>
              <?php endif; ?>
              <?php endforeach; ?>

              <div class="col-lg-12 my-3 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $onas->photo; ?>);">
              </div>
              <div class="col-lg-12 mb-3">
                <p class="standard-sect__paragraph">
                  <b><?php echo $onas->title; ?></b>
                  <?php if($onas->subtitle != null): ?>
                  <br>
                  <br>
                  <?php echo $onas->subtitle; ?>
                  <?php endif; ?>
                  <?php echo $onas->content; ?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="standard-sect">
      <div class="container-fluid">
        <h4 class="standard-sect__title"><span class="standard-sect__bold">Nasze ostatnie realizacje</span></h4>
        <span class="standard-sect__separator"></span>
        <!-- <div class="owl-carousel owl-theme">
          <div class="items">
            <div class="row">
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
              <div class="col-md-4 item-recomended px-3">
                <h4 ></h4>
              </div>
            </div>
          </div>
        </div> -->

<div id="mdb-lightbox-ui"></div>
<div id="test" class="owl-carousel owl-theme owl-carousel-2 mdb-lightbox">
  <?php foreach ($galeria_zdjec as $row): ?>
                <figure class="col-md-12 item">
                  <a href="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" class="d-block w-100" data-size="<?php echo $row->resolution ?>">
                    <!--  -->
                    <div class="photo-wpis" style="
                    height: 300px; 
                    background-image: url('<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>');
                    background-size: cover;">
                      <img src="<?php echo base_url(); ?>assets/front/img/invisible.png" class="img-fluid" style="height: 300px;">
                    </div>
                  </a>
                </figure>
  <?php endforeach; ?>
</div>


      </div>
    </section>

  </main>


