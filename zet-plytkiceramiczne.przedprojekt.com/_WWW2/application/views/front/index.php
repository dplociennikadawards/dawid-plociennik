



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo $poznaj->value2; ?>" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            <?php echo $poznaj->value1; ?>
          </a>
        </h4>  
        <div class="offer__row">

          <div class="offer__maincell" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta->value4 ?>);">
            <div class="offer__title-box">
              <h4 class="offer__desc"><?php echo $oferta->value1; ?></h4>

              <a class="offer__link" href="<?php echo $oferta->value7; ?>">
                <button class="offer__btn offer__btn--maincell"><?php echo $oferta->value6; ?></button>
              </a>
            </div>
          </div>
          <div class="offer__vertical-col">
            <div class="offer__cell-vertical offer__cell-vertical--margin-bottom offer__cell-vertical--img1" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta1->value4 ?>);">

              <div class="offer__layer">
                <h5 class="offer__desc"><?php echo $oferta1->value1; ?></h5>
                <hr style="background: #fff; width: 100%;">
                <p class="offer__paragraph"><?php echo $oferta1->value3; ?></p>
                <a class="offer__link" href="<?php echo $oferta1->value7; ?>">
                  <button class="offer__btn"><?php echo $oferta1->value6; ?></button>
                </a>
              </div>

              <h4 class="offer__desc offer__desc--small"><?php echo $oferta1->value1; ?></h4>
            </div>
            <div class="offer__cell-vertical offer__cell-vertical--margin-bottom offer__cell-vertical--img1" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta2->value4 ?>);">

              <div class="offer__layer">
                <h5 class="offer__desc"><?php echo $oferta2->value1; ?></h5>
                <hr style="background: #fff; width: 100%;">
                <p class="offer__paragraph"><?php echo $oferta2->value3; ?></p>
                <a class="offer__link" href="<?php echo $oferta2->value7; ?>">
                  <button class="offer__btn"><?php echo $oferta2->value6; ?></button>
                </a>
              </div>

              <h4 class="offer__desc offer__desc--small"><?php echo $oferta2->value1; ?></h4>
            </div>
          </div>
        </div>
        <div class="offer__row">

          <div class="offer__smcell offer__smcell--img3" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta3->value4 ?>);">
            <div class="offer__layer">
              <h5 class="offer__desc"><?php echo $oferta3->value1; ?></h5>
              <hr style="background: #fff; width: 100%;">
              <p class="offer__paragraph"><?php echo $oferta3->value3; ?></p>
              <a class="offer__link" href="<?php echo $oferta3->value7; ?>">
                <button class="offer__btn"><?php echo $oferta3->value6; ?></button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small"><?php echo $oferta3->value1; ?></h4>
          </div>

          <div class="offer__smcell offer__smcell--img3" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta4->value4 ?>);">
            <div class="offer__layer">
              <h5 class="offer__desc"><?php echo $oferta4->value1; ?></h5>
              <hr style="background: #fff; width: 100%;">
              <p class="offer__paragraph"><?php echo $oferta4->value3; ?></p>
              <a class="offer__link" href="<?php echo $oferta4->value7; ?>">
                <button class="offer__btn"><?php echo $oferta4->value6; ?></button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small"><?php echo $oferta4->value1; ?></h4>
          </div>

          <div class="offer__smcell offer__smcell--img3" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta5->value4 ?>);">
            <div class="offer__layer">
              <h5 class="offer__desc"><?php echo $oferta5->value1; ?></h5>
              <hr style="background: #fff; width: 100%;">
              <p class="offer__paragraph"><?php echo $oferta5->value3; ?></p>
              <a class="offer__link" href="<?php echo $oferta5->value7; ?>">
                <button class="offer__btn"><?php echo $oferta5->value6; ?></button>
              </a>
            </div>
            <h4 class="offer__desc offer__desc--small"><?php echo $oferta5->value1; ?></h4>
          </div>

        </div>
      </div>
    </section>

    <section class="standard-sect">
      <div class="container">
        <h4 class="standard-sect__title"><?php echo $onas->value1; ?></h4>
        <span class="standard-sect__separator"></span>

        <div class="row">
          <div class="col-lg-6 standard-sect__paragraph">
            <?php echo $onas->value3; ?>
          </div>
          <div class="col-lg-6 standard-sect__paragraph">
            <?php echo $onas->value4; ?>
          </div>
        </div>
      </div>
    </section>


  </main>

  