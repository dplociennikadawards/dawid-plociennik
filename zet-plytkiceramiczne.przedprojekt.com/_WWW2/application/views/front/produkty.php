



  <main>
<!-- Section: Blog v.3 -->
<section class="my-5">
<div class="container">
  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5"><?php echo $produkt->title; ?></h2>
  <!-- Section description -->
  <p class="text-center dark-grey-text mx-auto mb-5"><?php echo $produkt->subtitle; ?></p>

  <hr class="my-5">

  <?php $slug = $oferta->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>

  <!-- Grid row -->
  <div class="row">

    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
    </div>

  <?php $slug = $oferta1->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>


    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta1->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta1->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta1->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
  </div>

    <?php $slug = $oferta2->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>


    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta2->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta2->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta2->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
</div>

    <?php $slug = $oferta3->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>


    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta3->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta3->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta3->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
</div>

    <?php $slug = $oferta4->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>


    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta4->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta4->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta4->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
</div>

    <?php $slug = $oferta5->value1;
  $cenzura = array('ą', 'ć', 'ł', 'ó', 'ś', ' ', 'ę', 'ń', 'ż', 'ź', ',', '.', '!', '?', '(', ')', '%', 'Ą', 'Ć', 'Ł', 'Ó', 'Ś', ' ', 'Ę', 'Ń', 'Ż', 'Ź');
  $zamiana = array('a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z', '', '', '', '', '', '', '', 'a', 'c', 'l', 'o', 's', '_', 'e', 'n', 'z', 'z');
  $slug = strtolower(str_replace($cenzura, $zamiana, $slug)); ?>


    <div class="col-md-4 mb-3">
    <!-- Grid column -->
    <div class="col-md-12 mb-3">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4 bg-photo" style="background-image: url(<?php echo base_url(); ?>upload/<?php echo $oferta5->value4; ?>);">
        <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-12">

      <!-- Post title -->
      <h3 class="font-weight-bold"><strong><a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="text-dark"><?php echo $oferta5->value1; ?></a></strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text"><?php echo $oferta5->value3; ?></p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/kategoria/<?php echo $slug; ?>" class="btn btn-green btn-md">Czytaj więcej</a>

    </div>
    <!-- Grid column -->
</div>
  </div>
  <!-- Grid row -->

</div>
</section>
<!-- Section: Blog v.3 -->

  </main>