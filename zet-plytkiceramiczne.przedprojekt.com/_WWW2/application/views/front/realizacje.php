



  <main>
    <section class="offer">
      <div class="container">
        <h4 class="offer__title">
          <a href="<?php echo $poznaj->value2; ?>" class="text-dark">
            <img class="offer__arrow" src="<?php echo base_url(); ?>assets/front/img/green-arrow.png" alt="strzałka">
            <?php echo $poznaj->value1; ?>
          </a>
        </h4>       

        <h4 class="standard-sect__title"><span class="standard-sect__bold">Nasze realizacje</span></h4>
        <span class="standard-sect__separator"></span>
      </div>
      <div class="container-fluid">
        
      <div id="mdb-lightbox-ui"></div>


          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">

              <?php $i=0; foreach ($category as $cat): $i++; ?>
              <div class="carousel-item <?php if($i == 1){echo 'active';} ?>">
                <div class="container">


                  <!-- lightbox start -->
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="standard-sect__title"><span class="standard-sect__bold"><?php echo $cat->category; ?></span></h4>
                        <div class="mdb-lightbox">

                          <?php foreach ($rows as $row): ?>
                          <?php if($row->title == $cat->category): ?>
                          <figure class="col-md-4">
                            <a href="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" data-size="<?php echo $row->resolution; ?>">
                              <img src="<?php echo base_url(); ?>upload/<?php echo $row->photo; ?>" class="img-fluid">
                            </a>
                          </figure>
                          <?php endif; ?>
                          <?php endforeach; ?>

                        </div>

                      </div>
                    </div>
                  <!-- lightbox end -->


                </div>
              </div>
              <?php endforeach; ?>

            </div>
            <a class="carousel-control-prev rgba-green-strong" href="#carouselExampleControls" role="button" data-slide="prev" style="width: 10%;">
              <span class="carousel-control-prev-icon" aria-hidden="true" style="height: 50px; width: 50px;"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next rgba-green-strong" href="#carouselExampleControls" role="button" data-slide="next" style="width: 10%;">
              <span class="carousel-control-next-icon" aria-hidden="true" style="height: 50px; width: 50px;"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


      </div>
    </section>

  </main>