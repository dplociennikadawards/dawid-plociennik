-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 66062.m.tld.pl
-- Czas generowania: 08 Mar 2019, 13:31
-- Wersja serwera: 5.7.21-20-log
-- Wersja PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `baza66062_krematorium`
--
CREATE DATABASE IF NOT EXISTS `baza66062_krematorium` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `baza66062_krematorium`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `page` text COLLATE utf8_polish_ci NOT NULL,
  `priority` text COLLATE utf8_polish_ci NOT NULL,
  `active` int(11) NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `created_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `page`, `priority`, `active`, `photo`, `resolution`, `created_data`) VALUES
(12, 'krematorium', '1', 1, '1550946377przyklad1.jpg', '1684x1122', '2019-02-23 18:26:18'),
(13, 'krematorium', '2', 1, '1550946385przyklad2.jpg', '1728x1152', '2019-02-23 18:26:26'),
(14, 'krematorium', '3', 1, '1550946393przyklad1.jpg', '1684x1122', '2019-02-23 18:26:33'),
(15, 'krematorium', '4', 1, '1550946400przyklad2.jpg', '1728x1152', '2019-02-23 18:26:41'),
(16, 'krematorium', '5', 1, '1550946408przyklad1.jpg', '1684x1122', '2019-02-23 18:26:48'),
(20, 'onas', '3', 1, '1551090742przyklad1.jpg', '1684x1122', '2019-02-25 10:32:23'),
(21, 'onas', '2', 1, '1551090754przyklad2.jpg', '1728x1152', '2019-02-25 10:32:36'),
(22, 'onas', '1', 1, '1551090771przyklad1.jpg', '1684x1122', '2019-02-25 10:32:52');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_wpis`
--

CREATE TABLE `gallery_wpis` (
  `id` int(11) NOT NULL,
  `page` text COLLATE utf8_polish_ci NOT NULL,
  `wpis_id` int(11) NOT NULL,
  `priority` text COLLATE utf8_polish_ci NOT NULL,
  `active` int(11) NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `created_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `gallery_wpis`
--

INSERT INTO `gallery_wpis` (`id`, `page`, `wpis_id`, `priority`, `active`, `photo`, `resolution`, `created_data`) VALUES
(12, 'uslugi_pogrzebowe', 7, '1', 1, '1551094618przyklad1.jpg', '1684x1122', '2019-02-25 11:36:59'),
(13, 'uslugi_pogrzebowe', 7, '2', 1, '1551094626przyklad2.jpg', '1728x1152', '2019-02-25 11:37:08');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `glowna`
--

CREATE TABLE `glowna` (
  `glowna_id` int(11) NOT NULL,
  `part` text COLLATE utf8_polish_ci NOT NULL,
  `part_pl` text COLLATE utf8_polish_ci NOT NULL,
  `value1` text COLLATE utf8_polish_ci NOT NULL,
  `value2` text COLLATE utf8_polish_ci NOT NULL,
  `value3` text COLLATE utf8_polish_ci NOT NULL,
  `value4` text COLLATE utf8_polish_ci NOT NULL,
  `value5` text COLLATE utf8_polish_ci NOT NULL,
  `value6` text COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `glowna`
--

INSERT INTO `glowna` (`glowna_id`, `part`, `part_pl`, `value1`, `value2`, `value3`, `value4`, `value5`, `value6`, `date`) VALUES
(1, 'slider-button', 'Przycisk w nagłówku', 'Dowiedź się więcej', 'http://krematorium.przedprojekt.com/p/transport_zmarlych', '', '', '', '', '2019-02-24 19:13:04'),
(2, 'kafla', 'Kafelka pod banerem', 'Transport zmarłych', '', 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit ...\r\n<br>', '1551039774world.png', 'Transport zmarłych', 'http://krematorium.przedprojekt.com/p/transport_zmarlych', '2019-02-24 19:13:21'),
(3, 'kafla', 'Kafelka pod banerem', 'Usługi pogrzebowe', '', 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit ...<br>', '1551090352coffin.png', 'Usługi pogrzebowe', 'http://krematorium.przedprojekt.com/p/uslugi_pogrzebowe', '2019-02-24 19:13:30'),
(4, 'kafla', 'Kafelka pod banerem', 'Krematorium', '', 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit ...<br>', '1551090534statue.png', 'Krematorium', 'http://krematorium.przedprojekt.com/p/krematorium', '2019-02-24 19:13:30'),
(5, 'center', 'Tekst nad sliderem', 'Jak działamy?', 'Proin gravida nibh!', '', '', '', '', '2019-02-25 12:55:56'),
(6, 'contact', 'Sekcja kontaktowa', 'Zapewnij transport anenan sollicitudin, lorem quis bibendum!', 'Pozostaw dane, oddzwonimy!', 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.\r\n<br>', '1551039437yellow-background.png', '', '', '2019-02-24 19:14:07'),
(7, 'slider', 'Slajder', 'Kontakt z nami pod numerem tel. 123-456-789', '', 'Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. <br>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit ...<br>', '1551036993przyklad1.jpg', '1684x1122', '', '2019-02-24 19:13:48'),
(8, 'slider', 'Slajder', 'Kontakt z nami pod numerem', ' tel. 123-456-789', 'Po kontakcie pod numerem Proin gravida nibh vel velit auctor aliquet. <br>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit ...<br>', '', '', '', '2019-02-24 20:48:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakt`
--

CREATE TABLE `kontakt` (
  `kontakt_id` int(11) NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL,
  `zgoda` int(11) NOT NULL,
  `zgoda2` int(11) NOT NULL,
  `odpowiedz` int(11) NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `subject` text COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `map` text COLLATE utf8_polish_ci NOT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `address` text COLLATE utf8_polish_ci NOT NULL,
  `phone` text COLLATE utf8_polish_ci NOT NULL,
  `my_email` text COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `kontakt`
--

INSERT INTO `kontakt` (`kontakt_id`, `name`, `zgoda`, `zgoda2`, `odpowiedz`, `email`, `subject`, `content`, `map`, `title`, `subtitle`, `address`, `phone`, `my_email`, `date`) VALUES
(1, '', 0, 0, 0, '', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2503.221583164363!2d17.09134211522663!3d51.141264845228775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470fe8575ca855a9%3A0x2817e82580af4550!2sKowalska+8%2C+51-423+Wroc%C5%82aw!5e0!3m2!1spl!2spl!4v1549879448752', 'Kontakt', 'Masz jakieś pytania? Nie wahaj się skontaktować z nami bezpośrednio. Nasz zespół odpisze do Ciebie w ciągu kilku godzin, aby Ci pomóc.', 'ul. Kowalska 8, Kowalowo 82-133', '789 45 61 23', 'biuro@krematorium.pl', '2019-02-23 10:08:04'),
(10, 'Dawid Adawards', 1, 0, 1, 'nean12.bg@gmail.com', 'Temat', 'Treść', '', '', '', '', '', '', '2019-02-23 10:42:24'),
(11, 'Dawid Płóciennik', 0, 0, 1, 'dawid.plociennik13@gmail.com', 'dasd', 'asdasdasd', '', '', '', '', '', '', '2019-02-23 19:34:08'),
(12, 'Dawid Płóciennik', 0, 0, 0, 'dawid.plociennik13@gmail.com', 'Tes', 'dasdad', '', '', '', '', '', '', '2019-02-24 21:10:31'),
(13, 'Dawid Płóciennik', 0, 0, 0, 'dawid.plociennik13@gmail.com', 'dasd', 'asdasd', '', '', '', '', '', '', '2019-02-24 21:10:58'),
(14, 'fsdf', 0, 0, 1, 'fsdf', 'sdfsd', 'fsdfsdf', '', '', '', '', '', '', '2019-02-24 21:11:14'),
(15, 'Dawid Płóciennik', 1, 1, 1, 'dawid.plociennik13@gmail.com', 'dasd', 'asdasd', '', '', '', '', '', '', '2019-02-24 21:11:36'),
(16, 'dasd', 1, 1, 0, 'dsadad@dsadasd.pl', 'dasdasd', 'dsadasd', '', '', '', '', '', '', '2019-02-25 12:18:19'),
(17, 'dsada', 1, 1, 0, 'dsa@dsad', 'dasdas', 'dsadas', '', '', '', '', '', '', '2019-02-25 12:19:10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `krematorium`
--

CREATE TABLE `krematorium` (
  `krematorium_id` int(11) NOT NULL,
  `type_gallery` text COLLATE utf8_polish_ci NOT NULL,
  `active_gallery` int(11) NOT NULL,
  `header_photo` text COLLATE utf8_polish_ci NOT NULL,
  `header_title` text COLLATE utf8_polish_ci NOT NULL,
  `header_subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `selected_data` date NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `krematorium`
--

INSERT INTO `krematorium` (`krematorium_id`, `type_gallery`, `active_gallery`, `header_photo`, `header_title`, `header_subtitle`, `photo`, `resolution`, `title`, `subtitle`, `content`, `selected_data`, `date`) VALUES
(1, '', 0, 'header.png', 'Krematorium', '', '1550946360header.png', '1953x545', 'Krematorium', '', 'Dosyć, że teraz Napoleon, człek mądry a chłopi żegnali się, by stary Dąbrowskiego usłyszeć mazurek. biegał po łacinie. Mężczyznom dano jako wódz gospodarstwa obmyśla wypraw w twarz spójrzała, z łąk, i jak raz miała wysmukłą, kształtną, pierś powabną suknię materyjalną, różową, jedwabną gors wycięty, kołnierzyk z rozsądkiem wiedział, że pewnie na wciąż otwarta przechodniom ogłasza, Że nie puste, bo tak były świeżo polewane. Tuż i po zadzwonieniu na jutro solwuję i w domu i silni do stodoły a nam też nie był portret króla Stanisława.', '2019-02-25', '2019-02-22 11:13:53');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `media`
--

CREATE TABLE `media` (
  `media_id` int(11) NOT NULL,
  `link` text COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `media`
--

INSERT INTO `media` (`media_id`, `link`, `date`) VALUES
(3, 'http://krematorium.przedprojekt.com/_beta/upload/1551088378Zrzut_ekranu_z_2019-02-06_10-01-41.png', '2019-02-25 09:52:59');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miasta`
--

CREATE TABLE `miasta` (
  `miasta_id` int(11) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL,
  `lokalizacja` text COLLATE utf8_polish_ci NOT NULL,
  `lat` text COLLATE utf8_polish_ci NOT NULL,
  `lng` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `worker` text COLLATE utf8_polish_ci NOT NULL,
  `stanowisko` text COLLATE utf8_polish_ci NOT NULL,
  `phone` text COLLATE utf8_polish_ci NOT NULL,
  `mobile` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `miasta`
--

INSERT INTO `miasta` (`miasta_id`, `nazwa`, `lokalizacja`, `lat`, `lng`, `photo`, `worker`, `stanowisko`, `phone`, `mobile`, `email`, `text`, `date`) VALUES
(109, 'Transport zwłok Białystok', 'Białystok, Polska', '53.13248859999999', '23.168840300000056', '1551090835przyklad1.jpg', 'Jan Kowalski', 'Kierowca karawanu', '789456123', '312654987', 'j.kowalski@gmail.pl', '<b>Transport zwłok - co to?</b><br><span style=\"font-size: 0.875rem;\">Transport zwłok to proces, który ma miejsce każdorazowo po śmierci jednej z bliskich nam osób. Zorganizowanie transportu ciała zmarłego jest obowiązkiem członków najbliższej rodziny tej osoby Transport zwłok mogą przeprowadzać wyłącznie firmy, którym udało się uzyskać szereg pozwoleń i certyfikatów. Kwestie związane z transportem zwłok określa w sposób wyczerpujący i bardzo dokładny Rozporządzenie Ministra Zdrowia wydane w dniu 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.).  Jest to Rozporządzenie o wydawaniu pozwoleń oraz zaświadczeń, dzięki którym możliwe jest przewożenie szczątków i zwłok ludzkich. Do składnia wniosków o wydanie zaświadczeń oraz potrzebnych zezwoleń na przewiezienie zwłok uprawnieni są członkowie najbliższej rodziny zmarłego. Wniosek musi zawierać szereg ważnych informacji, takich jak data i miejsce śmierci, jego imię i nazwisko, miejsce pochówku itd.&nbsp;</span><br><br><br><b>Transport zwłok Białystok&nbsp;</b><br>Cena za zrealizowanie transportu zwłok do Białegostoku uzależniona jest od okoliczności śmierci zmarłego oraz regionu kraju. Z tego względu zapraszamy do kontaktu z naszymi konsultantami - udzielą oni Państwu dokładnych informacji na ten temat.&nbsp;<br><br><br><b>Skontaktuj się z nami</b><br>Nasi konsultanci są do Państwa dyspozycji przez 24 godziny na dobę i chętnie pomogą w kwestii zorganizowania transportu zwłok do Białegostoku.<br>', '2019-02-22 13:48:27'),
(112, 'Transport z Anglii', 'London, Wielka Brytania', '51.5073509', '-0.12775829999998223', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 12:04:22'),
(113, 'Transport z Austrii', 'Wiedeń, Austria', '48.2081743', '16.37381890000006', '', 'John Smith', 'Kierowca karawanu', '123 345 567 ', '', '', '', '2019-02-28 11:51:03'),
(114, 'Transport z Belgi', 'Bruksela, Belgia', '50.8503396', '4.351710300000036', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:51:50'),
(115, 'Transport z Chorwacji', 'Zagrzeb, Chorwacja', '45.8150108', '15.981918899999982', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:52:29'),
(116, 'Transport z Danii', 'Kopenhaga, Dania', '55.6760968', '12.568337199999974', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:52:54'),
(117, 'Transport z Francji', 'Paryż, Francja', '48.85661400000001', '2.3522219000000177', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:53:14'),
(118, 'Transport z Grecji', 'Ateny, Grecja', '37.9838096', '23.727538800000048', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:53:32'),
(119, 'Transport z Hiszpanii', 'Madrid, Hiszpania', '40.4167754', '-3.7037901999999576', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:53:52'),
(120, 'Transport z Holandii', 'Amsterdam, Holandia', '52.3679843', '4.903561399999944', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:54:11'),
(121, 'Transport z Niemiec', 'Berlin, Niemcy', '52.52000659999999', '13.404953999999975', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:54:45'),
(122, 'Transport z Norwegii', 'Oslo, Norwegia', '59.9138688', '10.752245399999993', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:55:14'),
(123, 'Transport z Szwecji', 'Sztokholm, Szwecja', '59.32932349999999', '18.068580800000063', '', 'John Smith', 'Kierowca karawanu', '', '', '', 'Zwiększa się liczba osób emigrujących z Polski do Szwecji. Co w przypadku kiedy nasz bliski wyjedzie i dowiemy się, że niestety zmarł na terenie Szwecji? Jakich formalności musimy dokonać i jak zorganizować transport zwłok ze Szwecji?<br><br><br>W pierwszej kolejności trzeba otrzymać zezwolenie na transport zwłok osoby zmarłej ze Szwecji do Polski w konsulacie. Najważniejsze dokumenty to: międzynarodowy akt zgonu, zezwolenie od starosty w Polsce na przewóz ciała zmarłego ze Szwecji do Polski, zaświadczenie lekarskie o zgonie, zaświadczenie o odpowiednim przygotowaniu zwłok do transportu (odpowiednia metalowa trumna przystosowana do transportu ciał, w której powinny być tylko zwłoki).<br><br><br>Bardzo ułatwia cały proces skorzystanie z usług polskiego zakładu pogrzebowego. W takim wypadku wystarczy podpisać specjalne upoważnienie dla zakładu pogrzebowego, który za nas zajmie się wszystkimi procedurami. Koszt takiej pełnej usługi wraz z przygotowaniem pogrzebu jest uzależniony od regionu z którego transportowane są zwłoki oraz wielu innych.<br><br><br>Dlatego prosimy o kontakt wszystkich zainteresowanych taką usługą. Wszystko wyjaśnimy i pomożemy kompleksowo dokonać całą procedurę. Rozumiemy, że jest to dla Państwa ciężki okres, dlatego staramy się jak najbardziej odciążyć osoby zainteresowane taką pomocą.<br>', '2019-02-28 11:55:39'),
(124, 'Transport z USA', 'Waszyngton, Stany Zjednoczone', '47.7510741', '-120.7401385', '', 'John Smith', 'Kierowca karawanu', '', '', '', '', '2019-02-28 11:55:59'),
(125, 'Transport z Włoch', 'Rzym, Włochy', '41.90278349999999', '12.496365500000024', '', 'John Smith', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - dokładna definicja na podstawie przepisów prawa\r\n</b><br>Transport zwłok to proces, do którego dochodzi po śmierci bliskiej nam osoby. Wówczas należy zorganizować transport ciała bądź szczątków zmarłej osoby. Do wykonywania transportu zwłok upoważnione są wyłącznie przedsiębiorstwa, którym udało się zdobyć szereg trudnych do uzyskania pozwoleń - jednym z nich jest nasza firma. Kwestie szczegółowe dotyczące transportu zwłok określa Rozporządzenie Ministra Zdrowia o wydawaniu pozwoleń i zaświadczeń na transport zwłok i szczątków wydane przez Ministra Zdrowia (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). <br><b><br></b><br><b>Aby móc przewieźć zwłoki z Włoch, należy zdobyć m.in. następujące dokumenty:\r\n</b><br><ul><li>odpowiednie zezwolenie na wywiezienie zwłok z terenu państwa włoskiego do Polski - otrzymuje się je od polskich władz,\r\n</li><li>oryginalny akt zgonu wydany przez włoski Urząd Stanu Cywilnego,\r\n</li><li>zaświadczenie lekarskie zawierające przyczyny zgonu.\r\n</li></ul><b><br></b><br><b>Ile kosztuje transport zwłok z Włoch do Polski?\r\n</b><br>Dokładna cena przewiezienia zwłok z Włoch do Polski uzależniona jest od okoliczności śmierci zmarłego oraz długości drogi do pokonania. \r\n<br>\r\n<br><b>Skontaktuj się z naszymi pracownikami \r\n</b><br>Aby uzyskać dokładne informacje o kosztach transportu zwłok, należy skontaktować się z naszymi konsultantami. Udzielą oni Państwu wszystkich niezbędnych informacji.&nbsp;<br><br>', '2019-02-28 11:56:20'),
(126, 'Transport zwłok Bydgoszcz', 'Bydgoszcz, Polska', '53.12348040000001', '18.008437800000024', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<span style=\"font-size: 0.875rem;\"><b>Czym jest transport zwłok?</b></span><br>Zaraz po odejściu bliskiej osoby, musimy zorganizować transport ciała zmarłej osoby. Takiego transportu mogą się podjąć jedynie firmy z odpowiednimi pozwoleniami, tak samo, jak jedynymi osobami, które mogą zadecydować o wydaniu pozwolenia na przewóz ciała zmarłego, jest najbliższa rodzina.<br><br><br><b>Transport zwłok do Bydgoszczy</b><br>Zaraz po otrzymaniu upoważnienia, firma załatwia wszelkie formalności związane z krajowym i międzynarodowym transportem zwłok. Uzyskanie wymaganych dokumentów w urzędach na terenie kraju, uzyskanie dokumentów za granicą, w tym zaświadczenia na przewóz zwłok. Do przewozu używane są specjalne pojazdy spełniające wszystkie wymagania sanitarne obowiązujące w Unii Europejskiej.<br>Koszty sprowadzenia zwłok różni się w zależności od przypadku zgonu oraz kraju, w którym osoba zmarła.<br><br><br>Nasza firma zajmuje się transportem zmarłych oraz prochów, najwyższa jakość usług w bardzo rozsądnej cenie. Oferujemy krajowy oraz międzynarodowy transport zwłok do Polski i profesjonalną obsługę przeszkoloną i doświadczoną w transporcie zwłok. Każde zlecenie odbierane jest indywidualnie, przede wszystkim dbamy, aby oferta spełniła oczekiwania klientów. Prosimy o kontakt w razie pytań lub jeśli zainteresowała Państwa nasza oferta.<br>&nbsp;<br>', '2019-02-28 11:57:31'),
(127, 'Transport zwłok Gdańsk', 'Gdańsk, Polska', '54.35202520000001', '18.64663840000003', '', 'John Smith', 'Kierowca karawanu', '', '', '', 'Każdego roku nasi rodacy wyjeżdżają za granicę. Wiele osób właśnie za granicą znajduję odpowiednią pracę i zakłada rodzinę. Rodzą się przez to nowe wyzwania, nie koniecznie przyjemne. Jednym z nich jest sprowadzenie zwłok z zagranicy. Chociaż zapewne nigdy się nad tym nie zastanawialiśmy, nie jest to łatwa sprawa. Musimy wziąć pod uwagę wiele kwestii, zanim zdecydujemy się na transport zwłok do naszego rodzinnego miasta.<br><br><br><b>Transport zwłok Gdańsk- co obowiązuje.&nbsp;</b><br>Aby sprowadzić zwłoki z zagranicy, niezbędne jest zdobycie wielu zaświadczeń oraz aktów. Ważnym dokumentem jest zaświadczenie lekarskie, które potwierdzi przyczynę zgonu. Istotne jest tu potwierdzenie, że nie ma żadnego zagrożenia chorobą zakaźną. Musimy mieć również oryginalne dokumenty potwierdzające zgodę na transport oraz pochówek w rodzinnym kraju.<br><br><br><b>Transport zwłok Gdańsk- wybór pewnej firmy.</b><br>Musimy pamiętać, że kwestią transportu zwłok do naszego kraju nie jesteśmy w stanie zająć się samodzielnie. Silne emocje oraz stres, który towarzyszy utracie bliskiej osoby, może utrudnić nam podejmowanie mądrych decyzji. Nasza firma pomoże Państwo załatwić wszystkie niezbędne formalności. Nasze doświadczenie w tej dziedzinie sprawi, że wszystko przebiegnie szybko i bez komplikacji. Oferujemy również przejrzysty wykaz kosztów oraz jasno sformułowaną umowę.<br>', '2019-02-28 11:57:52'),
(128, 'Transport zwłok Katowice', 'Katowice, Polska', '50.26489189999999', '19.02378150000004', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', 'Transport osób zmarłych wiąże się często z szeregiem formalności, jakich należy dokonać, by sprowadzić ciało bliskiej osoby w miejsce, w którym odbędzie się pochówek. Celem działania firmy, zajmującej się transportem zwłok jest kompleksowa realizacja usług, obejmująca załatwienie niezbędnych upoważnień, dokumentacji, transport zwłok w odpowiednich warunkach oraz niezawodność działania. Transport zwłok Katowice świadczy usługi z największym profesjonalizmem i wieloletnim doświadczeniem, pozwalającym realizować pracę solidnie, niezawodnie oraz z dużą dozą empatii dla najbliższych osoby zmarłej. Transport osoby zmarłej odbywa się z dbałością o zachowanie powagi, poufności oraz szacunku wobec osoby zmarłej oraz jej najbliższych. Sprawna organizacja procesu transportu zwłok oraz nowoczesna flota, przystosowana do tak wymagających zadań są gwarancją szybkiej, niezawodnej usługi na najwyższym poziomie. Doświadczeni pracownicy wpływają na profesjonalizm i czas realizacji zlecenia. W sytuacji, gdy potrzebny będzie transport zwłok Katowice, warto skontaktować się z nami telefonicznie. Podczas rozmowy postaramy się rozwiać Państwa wątpliwości, a po wykonaniu zadania potwierdzić swoją niezawodność i profesjonalizm.&nbsp;<br>', '2019-02-28 11:58:11'),
(129, 'Transport zwłok Kielce', 'Kielce, Polska', '50.8660773', '20.628567599999997', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - definicja na podstawie przepisów prawa</b><br>Z transportem zwłok mamy do czynienia, gdy dochodzi do śmierci jednego z naszych bliskich. Do wykonywania usługi przewozu zwłok uprawnione są wyłącznie firmy posiadające szereg odpowiednich zezwoleń i certyfikatów. Warto również wiedzieć, że dokładne wytyczne dotyczące wykonywania usługi transportu zwłok zawarte są w Rozporządzeniu Ministra Zdrowia z 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do złożenia wniosku o wydanie stosownych zezwoleń i certyfikatów uprawnieni są członkowie najbliższej rodziny osoby zmarłej.&nbsp;<br><br><br><b>Transport zwłok Kielce</b><br>Koszty transportu zwłok zza granicy do Kielc są bardzo zróżnicowane. Wszystko zależy od tego, w jakich okolicznościach zmarła dana osoba, oraz w jakim kraju miało to miejsce. Przeprowadzenie transportu wymaga również oczywiście znajomości wielu przepisów prawa międzynarodowego - my je znamy, dlatego warto skorzystać z usług naszej firmy. Oprócz tego udzielimy Państwu odpowiedzi na temat całkowitych kosztów transportu zwłok.&nbsp;<br><br><br><b>Skontaktuj się z naszymi pracownikami</b><br>Zapraszamy do skontaktowania się z naszymi pracownikami. Udzielą oni Państwu wszelkich niezbędnych informacji odnośnie kosztów transportu zwłok.&nbsp;<br>', '2019-02-28 11:58:29'),
(130, 'Transport zwłok Kraków', 'Kraków, Polska', '50.06465009999999', '19.94497990000002', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - co to takiego?</b><br><br>Transport zwłok to proces, który ma miejsce zawsze, gdy umrze bliska nam osoba. Warto wiedzieć, że do przewożenia zwłok i szczątków ludzkich uprawnione są wyłącznie firmy posiadające szereg odpowiednich zezwoleń i certyfikatów. Jedną z takich firm jesteśmy właśnie my. Dokładne regulacje prawne dotyczące przewożenia zwłok zawiera Rozporządzenie Ministra Zdrowia z dnia 27 grudnia 2007 roku. Do składania wniosków o wydanie niezbędnych zaświadczeń i pozwoleń umożliwiających przewiezienie ciała zmarłego upoważnieni są członkowie najbliższej rodziny osoby zmarłej. Wniosek musi zawierać szereg danych, takich jak między innymi datę i miejsce śmierci, nazwisko i imię osoby zmarłej, miejsce pochówku oraz środek transportu, którym będą przewożone zwłoki.<br>&nbsp;<br><br><br><b>Transport zwłok Kraków</b><br><br>Transport zwłok zza granicy do Krakowa to trudne zadanie do wykonania. Nasza firma specjalizuje się od wielu lat w wykonywaniu takich operacji. Dokładne koszty przewiezienia zwłok zza granicy do Krakowa zależą od okoliczności śmierci oraz kraju, w którym doszło do zgonu.<br><br><br><br><b>Skontaktuj się z nami</b><br><br>Zapraszamy do skontaktowania się z naszymi konsultantami - udzielą oni Państwu wszystkich informacji odnośnie kosztów transportu zwłok.<br>', '2019-02-28 11:58:46'),
(131, 'Transport zwłok Lublin', 'Lublin, Polska', '51.2464536', '22.568446300000005', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - co to takiego?</b><br>Transport zwłok to procedura, do której dochodzi każdorazowo po śmierci jednego z naszych bliskich. Warto wiedzieć więcej na ten temat, aby w razie straty bliskiej osoby wiedzieć, gdzie należy się zgłosić. Wszelkie kwestie związane z przeprowadzaniem transportu zwłok zawarte są w Rozporządzeniu Ministra Zdrowia wydanym w dniu 27 grudnia 2007 roku. Jest to rozporządzenie mówiące o warunkach wydawania pozwoleń i zaświadczeń na transport zwłok i szczątków ludzkich (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do przeprowadzania przewozu zwłok uprawnione są wyłącznie firmy, którym uda się zdobyć szereg zezwoleń i certyfikatów. Jedną z nich jest właśnie nasza firma.&nbsp;<br><br><br><b>Transport zwłok Lublin&nbsp;</b><br>Transport zwłok zza granicy może mieć różnorakie koszty. Wszystko zależy od tego, w jakich okolicznościach zmarła dana osoba, oraz w jakim kraju miał miejsce zgon. Z tego względu zapraszamy najpierw do kontaktu telefonicznego z naszymi konsultantami - udzielą oni Państwu wszystkich najważniejszych informacji.<br><br><br><b>Dowiedz się więcej</b><br>Aby uzyskać szczegółowe informacje odnośnie kosztu transportu zwłok do Lublina, zapraszamy do skontaktowania się z naszą obsługą. Pomożemy rozwiać wszelkie wątpliwości.&nbsp;<br>', '2019-02-28 11:59:03'),
(132, 'Transport zwłok Olsztyn', 'Olsztyn, Polska', '53.778422', '20.48011919999999', '', 'John Smith', 'Kierowca karawanu', '', '', '', '<b>Czym jest transport zwłok?</b><br>Transport zwłok ma miejsce zawsze, gdy dochodzi do śmierci jednej z bliskich nam osób. Zdajemy sobie sprawę z tego, jak trudny jest to dla Państwa czas. Warto wiedzieć, że dokładne wytyczne dotyczące wydawania pozwoleń oraz zaświadczeń na przewóz zwłok i szczątków ludzkich zawarte są w Rozporządzeniu Ministra Zdrowia z dnia 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Oprócz tego przypominamy, że do przewożenia zwłok uprawnione są wyłącznie te przedsiębiorstwa, którym udało się zebrać szereg pozwoleń i certyfikatów. My je posiadamy.&nbsp;<span style=\"font-size: 0.875rem;\">&nbsp;</span><br><br><br><b>Transport zwłok Olsztyn</b><br>Przetransportowanie zwłok zmarłego zza granicy do Olsztyna nie jest łatwe, ponieważ wymaga dopełnienia szeregu formalności. My to wszystko zrobimy za Państwa. Ponadto, nasi konsultanci są cały czas do Waszej dyspozycji. Koszt przetransportowania zwłok zza granicy do Olsztyna jest uzależniony od tego, w jakim kraju miał miejsce zgon oraz w jakich okolicznościach doszło do śmierci tej osoby.&nbsp;<span style=\"font-size: 0.875rem;\">&nbsp;</span><br><br><br><b>Skontaktuj się z naszą obsługą&nbsp;</b><br>Aby omówić szczegóły dotyczące przetransportowania zwłok, należy skontaktować się z naszymi pracownikami. Udzielą oni Państwu wszystkich najważniejszych informacji.<br><br><br>&nbsp;<br>', '2019-02-28 11:59:19'),
(133, 'Transport zwłok Opole', 'Opole, Polska', '50.6751067', '17.921297600000003', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - cechy charakterystyczne&nbsp;</b><br>Śmierć bliskich osób jest zawsze bardzo ciężkim przeżyciem. Aby móc szybko załatwić wszelkie formalności związane z pogrzebem oraz transportem zwłok, należy pozyskać fachową wiedzę na ten temat. Transport zwłok jest procedurą, która ma miejsce zawsze po śmierci naszych bliskich. Mogą go wykonywać wyłącznie firmy, które posiadają szereg stosownych zezwoleń i certyfikatów. Dokładne wytyczne dotyczące przeprowadzania transportu zawarte są w Rozporządzeniu Ministra Zdrowia z dnia 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do złożenia wniosku o wydanie zaświadczeń oraz pozwoleń na przewóz szczątków zmarłego uprawniona jest najbliższa rodzina.&nbsp;<br><br><br><b>Transport zwłok Opole</b><br>Ceny za przewiezienie zwłok zza granicy do Opola zależą od tego, w jakich okolicznościach doszło do zgonu oraz od kraju, w którym miało to miejsce. Nie muszą Państwo martwić się formalnościami - my wszystkie dokumenty pozyskamy w Państwa imieniu.&nbsp;<br><br><br><b>Skontaktuj się z nami&nbsp;</b><br>Aby uzyskać szczegółowe informacje dotyczące kosztów transportu zwłok zapraszamy do skontaktowania się z naszymi konsultantami. Udzielą oni Państwu wszystkich niezbędnych informacji i pomogą rozwiać pojawiające się wątpliwości.<br><br><br>&nbsp;<br>', '2019-02-28 11:59:32'),
(134, 'Transport zwłok Poznań', 'Poznań, Polska', '52.406374', '16.925168100000064', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '&nbsp; &nbsp; &nbsp;Gdy umiera bliska nam osoba, jedną z czynności, o które musimy zadbać, jest transport zwłok. Warto zdać się na wyspecjalizowaną firmę, ponieważ do przewożenia zwłok należy posiadać stosowne pozwolenie. Ponadto zwłoki wolno transportować jedynie w specjalnie przystosowanych do tego celu, oznakowanych pojazdach. Skorzystanie z usług profesjonalnej firmy zdecydowanie ułatwi całą procedurę.<br><br><br>&nbsp; &nbsp; &nbsp;Nasza firma oferuje państwu kompleksową obsługę przy transporcie zwłok. Przewieziemy ciało zmarłego na terenie Poznania, a także sprowadzimy zwłoki do miasta, jeżeli znajdują się poza naszym rejonem. Pomagamy w przeprowadzeniu wszystkich formalności, niezbędnych do przewiezienia zwłok. Przewozimy zmarłych we wskazane miejsca, odbierając zwłoki ze szpitali i z domów. Współpracujemy i polecamy najlepsze domy pogrzebowe w Poznaniu i okolicy. Posiadając doświadczenie w transporcie zwłok, staramy się zapewnić szybki i kompetentny przebieg całej procedury. Pracujemy rzetelnie i dyskretnie, zachowując szacunek do osoby zmarłej. Służymy wsparciem w trudnych chwilach, starając się zminimalizować stres towarzyszący transportowi zwłok. Posiadamy grono zadowolonych klientów. Jesteśmy do całodobowej dyspozycji. Zapraszamy do kontaktu i skorzystania z naszych usług.<br>', '2019-02-28 11:59:45'),
(135, 'Transport zwłok Rzeszów', 'Rzeszów, Polska', '50.0411867', '21.999119599999972', '', 'John Smith', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - pełna definicja prawna</b><br>Transport zwłok przeprowadza się, gdy dochodzi do zgonu jednego z naszych bliskich. Do przewożenia zwłok uprawnione są wyspecjalizowane przedsiębiorstwa posiadające szereg pozwoleń i certyfikatów. Jedną z takich firm jesteśmy my. Szereg ważnych kwestii określających procedury transportu zwłok zawarte są w Rozporządzeniu Ministra Zdrowia z dnia 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do składania wniosku o wydanie pozwoleń oraz stosownych zaświadczeń na przewóz ciała zmarłego uprawnieni są członkowie najbliższej rodziny. Wniosek musi zawierać m.in. datę i miejsce śmierci, miejsce pochówku, środek transportu, którym przetransportowane zostaną zwłoki.<br><br><br><b>Transport zwłok Rzeszów</b><br>Koszty związane z przetransportowaniem zwłok zza granicy do Rzeszowa są zróżnicowane. Wszystko zależy od tego, jakie były przyczyny zgonu oraz w jakim regionie/kraju miała miejsce śmierć naszego bliskiego.&nbsp;<br><br><br><b>Skontaktuj się z naszymi konsultantami&nbsp;</b><br>Aby uzyskać szczegółowe informacje dotyczące kosztów przetransportowania zwłok, skontaktuj się z naszym personelem. Udzieli on wszelkich niezbędnych informacji, dzięki którym będzie możliwy transport zwłok do Rzeszowa.&nbsp;&nbsp;<br><br><br>&nbsp;<br>', '2019-02-28 12:00:02'),
(136, 'Transport zwłok Toruń', 'Toruń, Polska', '53.0137902', '18.59844369999996', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - dokładna definicja pojęcia&nbsp;</b><br>Transport zwłok jest procedurą, którą należy przeprowadzać zawsze po śmierci jednej z bliskich nam osób. Transport zwłok mogą wykonywać wyłącznie firmy posiadające odpowiednie zezwolenia i certyfikaty - jedną z nich jest właśnie nasza firma. Dokładne regulacje dotyczące przewozu zwłok zawarte są w Rozporządzeniu Ministra Zdrowia z dnia 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do złożenia wniosku o wydanie potrzebnych pozwoleń i zaświadczeń uprawniona jest najbliższa rodzina osoby zmarłej. Wniosek musi zawierać informacje takie jak nazwisko i imię zmarłego, miejsce i datę śmierci, miejsce pochówku oraz środek transportu, którym będzie przewiezione ciało.<br><br><br><b>Transport zwłok Toruń</b><br>W przypadku transportu zwłok zza granicy do Torunia, to należy pamiętać o tym, że jest to skomplikowana procedura. Jego cena jest uzależniona od okoliczności śmierci danej osoby oraz kraju, w którym miał miejsce zgon.<br><br><br><b>Skontaktuj się z naszymi specjalistami</b><br>Zapraszamy do kontaktu z naszymi wykwalifikowanymi pracownikami. Udzielą oni Państwu wszelkich niezbędnych informacji, dzięki którym możliwe będzie zorganizowanie transportu zwłok zza granicy do Torunia.<br>', '2019-02-28 12:00:17'),
(137, 'Transport zwłok Warszawa', 'Warszawa, Polska', '52.2296756', '21.012228700000037', '', 'Jan Kowalski', 'Kierowca karawanu', '', '', '', 'Transport osoby zmarłej, której zgon nastąpił z dala od miejsca zamieszkania, jest dla najbliższych powodem stresu i niepewności, wiążących się z dezorientacją i trudnościami przy organizacji pochówku. firma transportowa, specjalizująca się w transporcie osób zmarłych kompleksowo organizuje przewóz zmarłego do miejsca, w którym nastąpi pochówek.&nbsp;<br><br><b>Jak działa firma, transportująca osoby zmarłe?</b>Transport zwłok Warszawa odbywa się przy użyciu nowoczesnych pojazdów, przystosowanych do transportu zmarłych. Jeżeli zgon nastąpił poza granicami kraju, zakres usług obejmuje organizację transportu drogą lądową, powietrzną lub morską oraz załatwienie formalności, niezbędnych do transportu zwłok. Nasi pracownicy, wyróżniający się doświadczeniem, empatią oraz wysoką kulturą osobistą, dokładają wszelkich starań, aby transport zwłok Warszawa odbywał się szybko i bez nieprzewidzianych problemów.<br><br><b>Formalności to nasze zadanie</b>W trudnej sytuacji śmierci bliskiej osoby, konieczne wymogi formalnoprawne stanowią znaczną trudność. Korzystając z usług naszej firmy, formalności można przenieść na naszych pracowników. Kontaktując się z naszą firmą, podejmują Państwo krok w celu kompleksowego transportu zmarłego do miejsca pogrzebu, korzystając z doświadczenia i rzetelności, które pozwolą bez dodatkowych stresów przygotować pochówek.&nbsp;', '2019-02-28 12:00:32'),
(138, 'Transport zwłok Zielona Góra', 'Zielona Góra, Polska', '51.9356214', '15.506186200000002', '', 'John Smith', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - co to takiego?</b><br><span style=\"font-size: 0.875rem;\">Transport zwłok to procedura, którą stosuje się każdorazowo po śmierci człowieka. Do transportowania zwłok uprawnione są wyłącznie te przedsiębiorstwa, które uzyskają szereg zezwoleń i certyfikatów. Dokładne kwestie dotyczące przewożenia zwłok reguluje Rozporządzenie wydane przez Ministra Zdrowia w dniu 27 grudnia 2007 roku. Jest to Rozporządzenie w sprawie wydawania zezwoleń i zaświadczeń na przewóz zwłok oraz szczątków ludzkich (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Do złożenia wniosku o wydanie pozwoleń i zaświadczeń na transport szczątków zmarłego uprawnieni są członkowie najbliższej rodziny.&nbsp;</span><br><br><br><br><b>Transport zwłok Zielona Góra&nbsp;</b><br>Jeśli chcą Państwo przewieźć zwłoki zza granicy do Zielonej Góry, to należy pamiętać o tym, że jest to skomplikowana procedura. Z tego względu zachęcamy do tego, by na bieżąco kontaktować się z naszymi konsultantami - oni udzielą Państwu odpowiedzi w razie jakichkolwiek pytań i wątpliwości. Koszty transportu zwłok są zróżnicowane - wszystko zależy od kraju, w którym miał miejsce zgon.&nbsp;<br><br><br><b>Skontaktuj się z nami&nbsp;</b><br>Zapraszamy do kontaktu z naszymi konsultantami. Udzielą oni Państwu wszystkich najważniejszych informacji dotyczących kosztów przewozu oraz formalności, jakie należy spełnić.<br>', '2019-02-28 12:00:48'),
(140, 'Transport z Irlandii', 'Dublin, Irlandia', '53.3498053', '-6.260309699999993', '', 'John Smith', 'Kierowca karawanu', '', '', '', '<b>Transport zwłok - definicja&nbsp;</b><br>Gdy przychodzi nam zmierzyć się ze śmiercią bliskiej osoby, jedną z rzeczy, jaką trzeba zorganizować, jest transport zwłok. Do przeprowadzania transportu zwłok upoważnione są wyłącznie te firmy, które posiadają szereg niezbędnych certyfikatów i zezwoleń. Szczegółowe kwestie dotyczące procedur związanych z transportem zwłok zawarte są w Rozporządzeniu Ministra Zdrowia z dnia 27 grudnia 2007 roku (Dz. U. Nr 249, poz. 1866 z 31 grudnia 2007 r.). Jest to Rozporządzenie ws. wydawania zaświadczeń i pozwoleń na przewożenie szczątków ludzkich i zwłok. Do składnia wniosku o przewóz zwłok upoważnione są osoby z najbliższej rodziny.&nbsp;<br><br><br><b>Transport zwłok z Irlandii&nbsp;</b><br>Transport zwłok z Irlandii to proces, którego cena uzależniona jest od tego, w jakich okolicznościach doszło do zgonu zmarłego oraz od odległości, jaka dzieli miejsce docelowe od miejsca, w którym aktualnie znajdują się zwłoki. W celu uzyskania informacji należy skontaktować się z naszymi pracownikami.<br><br><br><b>Skontaktuj się z naszymi konsultantami</b><br>Aby uzyskać szczegółowe informacje dotyczące kosztów związanych z transportem zwłok z Irlandii, należy skontaktować się z naszymi konsultantami. Udzielą oni Państwu wszystkich niezbędnych informacji.&nbsp;<br>', '2019-03-06 12:23:37'),
(141, 'Transport z Kanady', 'Ottawa, Ontario, Kanada', '45.4215296', '-75.69719309999999', '', 'John Smith', 'Kierowca karawanu', '', '', '', 'Coraz więcej naszych rodaków postanawia spędzić swoje życie w Kanadzie. Ma to swoje plusy i minusy, jednak pojawiają się i nowe wyzwania z tym związane. Jednym z bardzo przykrych wyzwań jest transport zwłok z Kanady. Być może nigdy się nad tym nie zastanawialiśmy, ale jest to bardzo skomplikowana sprawa. Dlaczego?<br><br><br>Musimy wziąć pod uwagę, że nie każda firma przewozowa ma odpowiednie kwalifikacje, aby przetransportować zwłoki. Często musimy wziąć pod uwagę transport samolotem. Nie możemy również zapomnieć o kwestiach prawnych z tym związanych. istotne jest tutaj co prawo Polskie mówi na temat sprowadzania zwłok z zagranicy, jak również prawo drugiego kraju. Transport zwłok z Kanady wymaga również wielu zezwoleń oraz upoważnień, na przykład zaświadczenie medyczne, które poświadczy, że nie obawy rozprzestrzenienia się chorób zakaźnych. Wymagane są oryginalne dokumenty, które potwierdzą zgodę na transport zwłok oraz pochówek. Nie rzadko jest to bardzo kosztowne i nie jesteśmy w stanie zadbać o wszystkie szczegóły samodzielnie, szczególnie gdy towarzyszą nam silne emocje i stres związany z utratą bliskiej osoby.<br><br><br>Warto zdecydować się na wybór firmy sprawdzonej i rzetelnej, która załatwi wszystkie formalności za ciebie. Nasza firma oferuje jasne warunki bez ukrytych kosztów oraz profesjonalne podejście do sprawy.&nbsp;<br>', '2019-03-06 12:57:52');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `onas`
--

CREATE TABLE `onas` (
  `onas_id` int(11) NOT NULL,
  `type_gallery` text COLLATE utf8_polish_ci NOT NULL,
  `active_gallery` int(11) NOT NULL,
  `header_photo` text COLLATE utf8_polish_ci NOT NULL,
  `header_title` text COLLATE utf8_polish_ci NOT NULL,
  `header_subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `selected_data` date NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `onas`
--

INSERT INTO `onas` (`onas_id`, `type_gallery`, `active_gallery`, `header_photo`, `header_title`, `header_subtitle`, `photo`, `resolution`, `title`, `subtitle`, `content`, `selected_data`, `date`) VALUES
(1, '', 0, '', '', '', '1550931402header.png', '1953x545', 'Nasze usługi', 'Jumbotron with image overlay', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur obcaecati vero aliquid libero doloribus ad, unde tempora maiores, ullam, modi qui quidem minima debitis perferendis vitae cumque et quo impedit.\r\n</p>', '2019-02-23', '2019-02-23 14:16:43'),
(6, 'gallery', 0, 'header.png', 'Tytuł w nagłówku 1', 'Podtytuł w nagłówku2', '1550931065przyklad2.jpg', '1728x1152', 'Tytuł wpisu3', 'Podtytuł wpisu4', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n</p><p>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n</p><p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n</p><p>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n</p><p>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n</p><p>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2019-02-22', '2019-02-22 11:13:53');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `podstrony`
--

CREATE TABLE `podstrony` (
  `podstrony_id` int(11) NOT NULL,
  `priority` text COLLATE utf8_polish_ci NOT NULL,
  `page` text COLLATE utf8_polish_ci NOT NULL,
  `header_photo` text COLLATE utf8_polish_ci NOT NULL,
  `header_title` text COLLATE utf8_polish_ci NOT NULL,
  `header_subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `gallery_status` int(11) NOT NULL,
  `type_gallery` text COLLATE utf8_polish_ci NOT NULL,
  `active_gallery` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `podstrony`
--

INSERT INTO `podstrony` (`podstrony_id`, `priority`, `page`, `header_photo`, `header_title`, `header_subtitle`, `gallery_status`, `type_gallery`, `active_gallery`) VALUES
(1, '1', 'index', '1551040012header.png', 'Międzynarodowy', 'Transport zmarłych', 0, '', 0),
(2, '2', 'o_nas', '1550926940header.png', 'O nas', '', 1, 'slider', 1),
(3, '3', 'transport_zmarlych', '1550927114header.png', 'Transport zmarłych', '', 0, '', 0),
(4, '4', 'uslugi_pogrzebowe', '1550927195header.png', 'Usługi pogrzebowe', '', 0, '', 0),
(5, '5', 'krematorium', '1550927252header.png', 'Krematorium', 'Galeria zdjęć', 1, 'gallery', 1),
(6, '6', 'poradnik', '1550927270header.png', 'Poradnik', '', 0, '', 0),
(7, '7', 'kontakt', '1550927301header.png', 'Kontakt', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `poradnik`
--

CREATE TABLE `poradnik` (
  `poradnik_id` int(11) NOT NULL,
  `type_gallery` text COLLATE utf8_polish_ci NOT NULL,
  `active_gallery` int(11) NOT NULL,
  `header_photo` text COLLATE utf8_polish_ci NOT NULL,
  `header_title` text COLLATE utf8_polish_ci NOT NULL,
  `header_subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `selected_data` date NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `poradnik`
--

INSERT INTO `poradnik` (`poradnik_id`, `type_gallery`, `active_gallery`, `header_photo`, `header_title`, `header_subtitle`, `photo`, `resolution`, `title`, `subtitle`, `content`, `selected_data`, `date`) VALUES
(6, 'gallery', 0, 'header.png', 'Tytuł w nagłówku 1', 'Podtytuł w nagłówku2', '1551095774przyklad1.jpg', '1684x1122', 'Jak zamówić transport?', 'Transport międzynarodowy', 'Wystarczy się z nami skontaktować na jeden z trzech sposobów, a jeden z naszych konsultantów ustali z Państwem dogodny termin. Wszystkimi formalnościami zajmuję się nasza firmą.<br><br><br>789 46 15 46<br>biuro@krematorium.pl<br>ul. Kowalska 8, Kowalowo 82-133<br>', '2019-02-08', '2019-02-22 11:13:53'),
(7, '', 0, '', '', '', '1551095758przyklad1.jpg', '1684x1122', 'Przygotowanie ceremonii', '', 'Ruski, teraz jeśli nasza młodzie wyjeżdża za dozorcę księdza, który teraz się rzucił kilku gośćmi poszedł do zdrowia powróciłaś cudem Gdy się kołem. W ślad gospodarza wszystko przepasane, jakby wstęgą, miedz zieloną, na swym dworze. Nikt go miało śmieszy to mówiąc, że się człowiek cudzy gdy z rana, bo tak się chlubi a wszystko się nie odrodził dobrze na spoczynek powraca. Już Sędzia sam wewnątrz siebie czuł się do sądów granicznych.', '2019-02-03', '2019-02-22 14:45:03'),
(8, '', 0, '', '', '', '1551096407przyklad2.jpg', '1728x1152', 'Cytat z książki Pan Tadeusz', 'Przykładowy wpis', 'Sędziego. Sędzia w tem roztargnieni na błoni i posępny obok Korsak, towarzysz jego puchar i panie słowem, ubiór galowy. szeptali niejedni, Że architekt był tytuł markiza. Jakoż, kiedy te przenosiny? Pan świata wie, jak Ołtarzyk złoty zawsze i wróciwszy w stodołę miał wielką, i obiegłszy dziedziniec zawrócił przed ganek wysiadł z Wereszczaką, Giedrojć z której już potraw ostatnich nie uszło baczności', '2019-02-25', '2019-02-25 12:06:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL,
  `login` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `first_name` text COLLATE utf8_polish_ci NOT NULL,
  `last_name` text COLLATE utf8_polish_ci NOT NULL,
  `avatar` text COLLATE utf8_polish_ci NOT NULL,
  `recovery_password` text COLLATE utf8_polish_ci NOT NULL,
  `role` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `created`, `active`, `login`, `email`, `password`, `first_name`, `last_name`, `avatar`, `recovery_password`, `role`) VALUES
(1, '2019-02-09 15:38:44', 1, 'admin', 'nean12.bg@gmail.com', '2e18bd02d91fa82f1689c15a57b81545', 'Jan', 'Nowak', '', '', 'administrator');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uslugi`
--

CREATE TABLE `uslugi` (
  `uslugi_id` int(11) NOT NULL,
  `gallery_status` int(11) NOT NULL,
  `type_gallery` text COLLATE utf8_polish_ci NOT NULL,
  `active_gallery` int(11) NOT NULL,
  `header_photo` text COLLATE utf8_polish_ci NOT NULL,
  `header_title` text COLLATE utf8_polish_ci NOT NULL,
  `header_subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `icon` text COLLATE utf8_polish_ci NOT NULL,
  `resolution` text COLLATE utf8_polish_ci NOT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `subtitle` text COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `selected_data` date NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uslugi`
--

INSERT INTO `uslugi` (`uslugi_id`, `gallery_status`, `type_gallery`, `active_gallery`, `header_photo`, `header_title`, `header_subtitle`, `photo`, `icon`, `resolution`, `title`, `subtitle`, `content`, `selected_data`, `date`) VALUES
(6, 1, 'gallery', 0, 'header.png', 'Usługi pogrzebowe', '', '1550952880przyklad1.jpg', '1551094302coffin.png', '1684x1122', 'Usługi pogrzebowe', 'Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.', 'Pańskiej cioci. Choć Sędzia spał. Więc Tadeusz przyglądał się Hreczecha, a pan Rejent na dzień za sznurek by nie dozwolę. Woźny! odwołaj sprawę na wzgórek z córkami. Młodzież poszła do żołnierki jedyni, w pogody lilia jeziór skroń ucałowawszy, uprzejmie pozdrowił. A na konikach małe dziecię, kiedy znidzie z kołka zdjęty do zdrowia powróciłaś cudem na tem, Że ta chwała należy chartu Sokołowi. Pytano zdania bo tak rzadka nowina! Ojcze Robaku ciszej rzekł Woźnemu, że posiadłość tam nie wyszli witać, ale razem ja powiem śmiało grzeczność nie odrodził dobrze na miejscu pustym oczy wkoło są architektury. Choć pani Telimena, i liczba żołnierza i obrok, i poplątane, w miechu. Starzy na siano. w ulicę się sam wewnątrz siebie czuł choroby zaród. Krzyczano na polu szukała kogoś posadzić na świadki pamiętam za nim w domu przyszłą urządza zabawę. Dał rozkaz ekonomom, wójtom i Waszeć z Rymszą, Rymsza z wojażu upodobał mury tłumacząc, że dziś nagodzi do Warszawy! He! Ojczyzna! Ja to mówiąc, że serce niewinne ale nigdzie nie uszło baczności, Że w Marengo, w purpurowe kwiaty na piersiach, przydawając zasłony sukience. Włos w tabakierę palcami ruch chartów przedziwnie udawał psy za pierwszym na samym końcu Wojskiej Hreczeszance. Tadeusz Telimenie, Asesor zaś Gotem. Dość, że przeniosłem stoły do swawoli. Z wieku może Białopiotrowiczowi samemu odmówił! Bo nie stracił, a starzy mówili myśliwi widząc, że tamuje progresy, że po kryjomu. Chłopiec, co jasnej bronisz.', '2019-02-22', '2019-02-22 11:13:53'),
(7, 1, 'gallery', 0, 'header.png', 'Krematorium', '', '1551094603przyklad2.jpg', '1551094383statue.png', '1728x1152', 'Krematorium', 'Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.', 'Tadeuszu bo tak gadać: Cóż złego, że serce mu bił głośno, i wznosi chmurę pyłu. dalej drzeć pazurami, a bij jak kity z las wiąże w ziemstwie i damy spały we łzach i obrok, i szukał komnaty gdzie nie po wielu kosztach i wysoką jego pamięć droga co dzień postrzegam, jak naoczne świadki. I przyjezdny gość, krewny albo sam ku drzwiom odprowadzał i jadł. wtem z kształtu, jeśli zechcesz, i Waszeć z cudzych krajó wtargnęli do włosów, włosy pozwijane w całej ozdobi widzę i swój kielich o jakie pół kroku Tak każe u progu rękę do zamku sień wielka, jeszcze kołyszą się od tylu brzemienna imionami rycerzy, od króla Stanisława. Ojcu Podkomorzego zdał się Hreczecha, a starzy mówili myśliwi młodzi tak rzadka nowina! Ojcze Robaku ciszej rzekł półgłosem: Przepraszam, musieliśmy siadać niepodobna wieczerzy będzie z tych pagórków leśnych, do rąk strzelby, którą do dworu. Tu Kościuszko w wiecznej wiośnie pachnące kwitną lasy. z liczby kopic, co pod bramę. We dworze jako wierzchołki drzewa powiązane społem gdy inni, których nie gadał lecz zagorzalec wysadził się cukier wytapia i z las i jakoby dwa charty w Litwę. nieraz na folwarku nie przeczym, że jacyś Francuzi wymowny zrobili wynalazek: iż ludzie są rowni. Choć o naszym wojsku wie Jegomość? Nic a co jasnej bronisz Częstochowy i nie lada czychał niby kamień z Warszaw mam list, to mówiąc, że przychodził już.<br>', '2019-02-09', '2019-02-22 14:25:40'),
(8, 1, 'gallery', 0, '1551094511header.png', 'Transport zmarłych', '', '1551094518przyklad2.jpg', '1551094532world.png', '1728x1152', 'Transport zmarłych', 'Recently, we added several exotic new dishes to the menu of our restaurant. They come from countries such as Mexico, Argentina, and Spain. Come to us, have a delicious wine and enjoy the juicy meals from around the world.', 'Rodułtowskim Obuchowicz Piotrowski, Obolewski, Rożycki, Janowicz, Mirzejewscy, Brochocki i wznosi chmurę pyłu. dalej drzeć pazurami, a my do Podkomorzanki. Nie zmienia jej ubiór galowy. szeptali niejedni, Że Bonapart figurka! Bez Suworowa to mówiąc, że oko nie mające kłów, rogów, pazurów zostawiano dla sług zapytać. Odemknął, wbiegł do swawoli. Z góry już byli z nim mówił widać było gorąca). wachlarz dla zabawki smycz i smuci, i wszystkich lekkim dotknieniem się ramieniu. Przeprosiwszy go czeladka ściskała zanosząc się damom, starcom i tam pogląda, gdzie panieńskim rumieńcem dzięcielina pała a bij jak gwiazdy, widać było ogrodniczki. Tylko co w zastępstwie gospodarza, gdy inni, których by stary Rejtan, gdyby na kształt ogrodowych grządek: Że wszyscy siedli i długie paznokcie przedstawiając dwa charty zostały i opisuję, bo tak nas starych więcej godni Wojewody ojca Podkomorzego, Mościwego Pana zastępuje i książki. Wszystko bieży ku północy, aż na Francuza. oj, ten zaszczyt należy. Idąc z dokumentów przekonywał o piękności metrykę nie zbłądzi i miłość dziecinna i w naukach mniej silnie, ale nigdzie nie daje czasu szukać mody odmianą się dawniej zdały. I wnet sierpy gromadnie dzwoniąc we zbożach i jadł. wtem z któremi się niedawno w Ojczyźnie Boga, przodków wiarę prawa i kłopotach, i hec! od lasu bawić się rąk muskała włosów pukle i z legiją Dunaj tam pogląda, gdzie w francuskiej gazecie. Podczaszyc, mimo całą rodzina pańska, jak w charta. Tak każe u tej.<br>', '2019-02-25', '2019-02-25 11:36:17');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ustawienia`
--

CREATE TABLE `ustawienia` (
  `ustawienia_id` int(11) NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `icon` text COLLATE utf8_polish_ci NOT NULL,
  `page_title` text COLLATE utf8_polish_ci NOT NULL,
  `pdf` text COLLATE utf8_polish_ci NOT NULL,
  `cookies` text COLLATE utf8_polish_ci NOT NULL,
  `kod_header` text COLLATE utf8_polish_ci NOT NULL,
  `kod_footer` text COLLATE utf8_polish_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ustawienia`
--

INSERT INTO `ustawienia` (`ustawienia_id`, `photo`, `icon`, `page_title`, `pdf`, `cookies`, `kod_header`, `kod_footer`, `date`) VALUES
(1, '1550948788logo.png', '1551099260statue.png', 'Krematorium - międzynarodowy transport zmarłych', '1550948784logo.png', '<p>Strona internetowa naszego krematorium korzysta z plików cookie. Dzięki temu możemy zapewnić naszym użytkownikom satysfakcjonujące wrażenia z przeglądania naszej witryny i jej prawidłowe funkcjonowanie.</p>', '', '', '2019-02-23 11:47:37');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gallery_wpis`
--
ALTER TABLE `gallery_wpis`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `glowna`
--
ALTER TABLE `glowna`
  ADD PRIMARY KEY (`glowna_id`);

--
-- Indeksy dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`kontakt_id`);

--
-- Indeksy dla tabeli `krematorium`
--
ALTER TABLE `krematorium`
  ADD PRIMARY KEY (`krematorium_id`);

--
-- Indeksy dla tabeli `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indeksy dla tabeli `miasta`
--
ALTER TABLE `miasta`
  ADD PRIMARY KEY (`miasta_id`);

--
-- Indeksy dla tabeli `onas`
--
ALTER TABLE `onas`
  ADD PRIMARY KEY (`onas_id`);

--
-- Indeksy dla tabeli `podstrony`
--
ALTER TABLE `podstrony`
  ADD PRIMARY KEY (`podstrony_id`);

--
-- Indeksy dla tabeli `poradnik`
--
ALTER TABLE `poradnik`
  ADD PRIMARY KEY (`poradnik_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeksy dla tabeli `uslugi`
--
ALTER TABLE `uslugi`
  ADD PRIMARY KEY (`uslugi_id`);

--
-- Indeksy dla tabeli `ustawienia`
--
ALTER TABLE `ustawienia`
  ADD PRIMARY KEY (`ustawienia_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `gallery_wpis`
--
ALTER TABLE `gallery_wpis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT dla tabeli `glowna`
--
ALTER TABLE `glowna`
  MODIFY `glowna_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `kontakt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `krematorium`
--
ALTER TABLE `krematorium`
  MODIFY `krematorium_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `miasta`
--
ALTER TABLE `miasta`
  MODIFY `miasta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT dla tabeli `onas`
--
ALTER TABLE `onas`
  MODIFY `onas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `podstrony`
--
ALTER TABLE `podstrony`
  MODIFY `podstrony_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `poradnik`
--
ALTER TABLE `poradnik`
  MODIFY `poradnik_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `uslugi`
--
ALTER TABLE `uslugi`
  MODIFY `uslugi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `ustawienia`
--
ALTER TABLE `ustawienia`
  MODIFY `ustawienia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
