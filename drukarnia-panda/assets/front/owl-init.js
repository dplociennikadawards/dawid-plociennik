$(document).ready(function () {
    //Init the carousel

    initSlider();
    function initSlider() {
        $(".owl-carousel-1").owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: true
        });
    }

    initSlider2();
    function initSlider2() {
        $(".owl-carousel-2").owlCarousel({
            items: 3,
            loop: true,
            autoplay: true,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: true
        });
    }

    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "100%",
            transition: "width 5000ms"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }
});