<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Produkty extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->get_sort('produkty');
		$data['media'] = $this->base_m->get('media');
		$data['row_1'] = $this->base_m->get_record('produkty',1);
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/produkty/index', $data);
		$this->load->view('back/blocks/footer');
		$this->load->view('back/blocks/ajax');
	}
	public function add() {
            echo $this->upload->display_errors(); 
            print_r($_POST);
		    $now = date('Y-m-d');
	        if (!is_dir('uploads/produkty/'.$now)) {
		    	mkdir('./uploads/produkty/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/produkty/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) { 
				$insert['alt'] = $_POST['alt'];
				$insert['photo'] = 'blad dodawania zdjecia';
				$this->base_m->insert('produkty', $insert);     
            } else {
            	$data = $this->upload->data();
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');;
				$insert['content'] = $_POST['content'];
				$insert['photo'] = $now.'/'.$data['file_name'];
				$this->base_m->insert('produkty', $insert);  	
            }
	}
	public function edit($id) {
            echo $this->upload->display_errors(); 
            print_r($_POST);
		    $now = date('Y-m-d');
	        if (!is_dir('uploads/produkty/'.$now)) {
		    	mkdir('./uploads/produkty/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/produkty/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) { 
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');
				$insert['content'] = $_POST['content'];
				$this->base_m->update('produkty', $insert, $id);  	 
            } else {
            	$data = $this->upload->data();
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');;
				$insert['content'] = $_POST['content'];
				$insert['photo'] = $now.'/'.$data['file_name'];
				$this->base_m->update('produkty', $insert, $id);  	
            }
	}
	public function update_change($id, $field) {
            
		$insert[$field] = $_POST['value'];
		$this->base_m->update('produkty', $insert, $id);  	
    }

}
