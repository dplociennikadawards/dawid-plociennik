<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->get_sort('newsletter');
		$data['media'] = $this->base_m->get('media');
		$data['row_1'] = $this->base_m->get_record('newsletter',1);
		$data['row_2'] = $this->base_m->get_record('newsletter',2);
		$data['row_3'] = $this->base_m->get_record('newsletter',3);
		$data['row_4'] = $this->base_m->get_record('newsletter',4);
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/newsletter/index', $data);
		$this->load->view('back/blocks/footer');
		$this->load->view('back/blocks/ajax');
	}
	public function add() {
            echo $this->upload->display_errors(); 
            print_r($_POST);
		    $now = date('Y-m-d');
	        if (!is_dir('uploads/newsletter/'.$now)) {
		    	mkdir('./uploads/newsletter/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/newsletter/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) { 
				$insert['alt'] = $_POST['alt'];
				$insert['photo'] = 'blad dodawania zdjecia';
				$this->base_m->insert('newsletter', $insert);     
            } else {
            	$data = $this->upload->data();
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');;
				$insert['content'] = $_POST['content'];
				$insert['photo'] = $now.'/'.$data['file_name'];
				$this->base_m->insert('newsletter', $insert);  	
            }
	}
	public function edit($id) {
            echo $this->upload->display_errors(); 
            print_r($_POST);
		    $now = date('Y-m-d');
	        if (!is_dir('uploads/newsletter/'.$now)) {
		    	mkdir('./uploads/newsletter/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/newsletter/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) { 
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');;
				$insert['content'] = $_POST['content'];
				$this->base_m->update('newsletter', $insert, $id);  	 
            } else {
            	$data = $this->upload->data();
				$insert['title'] = $_POST['title'];
				$insert['subtitle'] = $_POST['subtitle'];
				$insert['alt'] = $_POST['alt'];
				$insert['gallery'] = rtrim($_POST['gallery'],',');;
				$insert['content'] = $_POST['content'];
				$insert['photo'] = $now.'/'.$data['file_name'];
				$this->base_m->update('newsletter', $insert, $id);  	
            }
	}
	public function update_change($id, $field) {
            
		$insert[$field] = $_POST['value'];
		$this->base_m->update('newsletter', $insert, $id);  	
    }
    public function send_mail() {
        $data['newsletter'] = $this->base_m->get('newsletter');
        $this->load->library('email');

        /**
         *
         * config smtp
         *
         */

        $config = array(
            'smtp_host' =>"smtp.adawards.pl",
            'smtp_user' =>"no-reply-car@adawards.pl",
            'smtp_pass' =>"8lllRuWL4H",
            'smtp_port' =>"587",
            'smtp_timeout' =>"30",
            'mailtype'  =>"html",
            'charset'  =>"utf-8",
        );

        /**
         *
         * tworzenie maila
         *
         */

        $this->email->clear();

        $this->email->initialize($config);
        foreach ($data['newsletter'] as $item) {
            $this->email->to($item->liame);
        }


        $this->email->from('no-reply-car@adawards.pl', 'Nowy Newsletter ze strony Drukarnia Panda');

        $this->email->subject($_POST['title']);

        $this->email->message('Temat' . $_POST['title'] . '<br>Wiadomość:'.$_POST['message']);

        $this->email->send();
    }
}
