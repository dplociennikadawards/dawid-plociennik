<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
	
		$data['users'] = $this->base_m->get('users');

		$this->form_validation->set_rules('email', 'Login', 'min_length[2]|trim');
		$this->form_validation->set_rules('password', 'Hasło', 'min_length[6]|trim');

		$this->form_validation->set_message('min_length', '<p>Pole %s ma za mało znaków </p>');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('failed', validation_errors());
			$this->load->view('back/blocks/head');
			$this->load->view('back/login');
			$this->load->view('back/blocks/footer');
		} else {

			$options = ['cost' => 12];

			$login = strtolower($this->input->post('email'));
			$password = $this->input->post('password');
			$logged = false;
			foreach($data['users'] as $check):


				if (($login == $check->login || $login == $check->email) && (password_verify($password, $check->password)) && $check->active == 1):

					$session_data['first_name'] = $check->first_name;
					$session_data['email'] = $check->email;
					$session_data['name'] = $check->login;
					$session_data['id'] = $check->users_id;
					$session_data['unique_hash'] = $check->unique_hash;
					$session_data['login'] = TRUE;

					$this->session->set_userdata($session_data);
					$logged = true;
					
					$this->base_m->log('logowanie');
					redirect('/panel');
					break;
				else:
					$logged = false;
				endif;
			endforeach;

			if($logged == false)
			{
				$this->session->set_flashdata('message', 'Błędny login lub hasło');
				redirect('/login');
			}

		}
	}


}