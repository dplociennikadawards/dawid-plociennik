<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model  
{
  
    public function get($table) {
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_record($table,$id) {
        $this->db->where([$table.'_id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_email($table,$email) {
        $this->db->where(['email' => $email]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function insert($table,$data) {
        $insert_query = $this->db->insert($table, $data);
        return $insert_query;
    }

    public function new_password($table,$data,$hash_id) {
        $this->db->where(['unique_hash' => $hash_id]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }

    public function set_active($table,$data,$hash_id) {
        $this->db->where(['unique_hash' => $hash_id]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }

}