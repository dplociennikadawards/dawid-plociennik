<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_m extends CI_Model  
{


    public function get($table) {
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_record($table,$id) {
        $this->db->where([$table.'_id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_many($table,$limit) {
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_records_filter($table,$filter,$value) {
        $filters = explode(',', $filter);
        $values = explode(',', $value);
        $i=0;
        foreach ($filters as $key) {
            if($key != ''):
                $this->db->where([ $key => $values[$i] ]);
                $i++;
            endif;
        }
       
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_gallery($table,$item_id) {
        $this->db->where(['table_name' => $table]);
        $this->db->where(['item_id' => $item_id]);
        $query = $this->db->get('gallery');
        
        return $query->result();        
    }


}

