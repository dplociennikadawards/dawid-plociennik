<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_m extends CI_Model  
{
   public function get_comments($tablename,$item_id){

   	 	$this->db->where(['tablename' => $tablename]);
   	 	$this->db->where(['item_id' => $item_id]);
    	$query = $this->db->get('comments');

    	return $query->result();

   }

}