<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php echo form_open_multipart();?>
	  	<?php if($this->session->flashdata('flashdata')) {?>
	  		<div class="container">
				<div class="alert alert-success alert-dismissible fade show mt-2 mb-5 text-dark" role="alert">
				  		<?php echo $this->session->flashdata('flashdata'); ?>
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
	<?php } ?>
	</div>
	<section>
		<div class="container">
			<div class="card card-cascade narrower">
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
			    <div class="md-form my-0">
					<a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>
			    </div>
			    <div class="text-right" style="width: 181px;">
			    </div>
			  </div>
			  <div class="px-4">
			    <div class="table-wrapper">
			    	<span id="change"></span>
        <div class="row">
          <div class="col-md-6">
        <div class="md-form">
          <input type="text" id="title" class="form-control" name="title" value="<?php echo $row_1->title; ?>" required>
          <label for="title">Nazwa strony</label>
        </div>
        <div class="md-form">
          <input type="text" id="subtitle" class="form-control" name="subtitle" value="<?php echo $row_1->subtitle; ?>" required>
          <label for="subtitle">Słowa kluczowe</label>
        </div>
        <div class="md-form">
          <input type="text" id="alt" class="form-control" name="alt" value="<?php echo $row_1->alt; ?>" required>
          <label for="alt">Tekst alternatywny loga</label>
        </div>
        <div class="md-form">
          <div class="file-field">
            <div id="photo_show" class="text-center">
              <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/ustawienia/<?php echo $row_1->photo; ?>" width="250">
            </div>
              <div class="btn btn-primary btn-sm float-left gold-gradient">
                <span>Wybierz logo</span>
                <input type="file" name="photo" id="photo">
              </div>
            <div class="file-path-wrapper">
              <input id="name_photo" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" required readonly>
            </div>
          </div>
        </div>
        <div class="md-form">
          <div class="file-field">
            <div id="header_photo_show" class="text-center">
              <?php if($row_1->pdf != null): ?>
                <span class="text-success"><i class="fas fa-check"></i> Polityka prywatności została wgrana!</span>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="md-form">
          <button id="button_ajax" type="button" class="btn btn-primary gold-gradient" onclick="add_record();">Zapisz</button>
        </div>  
      </div>
      <div class="col-md-6">
        <div class="md-form mt-md-0">
          <textarea id="content" class="form-control specialArea" name="content"  required><?php echo $row_1->content; ?></textarea>
        </div>
      </div>
    </div>
			    </div>
			  </div>
			</div>
		</div>
	</section>
</main>


    <script type="text/javascript">
      function add_record()
      {
        var title = document.getElementById('title').value;
        var subtitle = document.getElementById('subtitle').value;
        var alt = document.getElementById('alt').value;
        var content = document.getElementById('content').value;
        var photo = document.getElementById('photo');
        var data=new FormData();
    data.append(photo.name,photo.files[0]);
    data.append('title',title);
    data.append('subtitle',subtitle);
    data.append('alt',alt);
    data.append('content',content);

        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/ustawienia/edit/"+1, 
             data: data, 
             cache: false,
           contentType: false,
           processData: false,
           beforeSend:function(data) 
           {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Ładowanie zmian.</span>';
           },
             success:function(data)  
             {  
             console.log(data);
             },
             complete:function(html)
             {
              console.log(html);
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
             }  
        });  
      }
    </script>