<main>
    <section class="default products">
      <div class="container">
        <div class="default__title-box">
          <h3 class="default__title"><?php echo $row_1->title; ?></h3>
          <p class="default__text"><?php echo $row_1->content; ?></p>
        </div>
      </div>

      <div class="container">
        <div class="products__row">
            <?php foreach ($rows as $key): if($key->produkty_id > 1) :?>
          <a href="<?php echo base_url(); ?>p/produkt/<?php echo $this->slugify_m->slugify($key->title) . '/' . $key->produkty_id; ?>" class="products__cell">
            <img class="products__img" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $key->photo; ?>">
            <img class="products__img products__img--hover" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $key->photo; ?>">
            <h5 class="products__title"><?php echo $key->title; ?></h5>
          </a>
            <?php endif; endforeach; ?>
        </div>
          <div class="text-center mt-3">
            <a href="<?php echo base_url(); ?>p/kontakt" class="products__btn mt-3 text-center">nie znalazłeś produktu? wyślij zapytanie</a>
          </div>
        
      </div>
    </section>


