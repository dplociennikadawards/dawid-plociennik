
<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#ff7401",
                    "text": "#fff"
                },
                "button": {
                    "background": "#000",
                    "text": "#fff"
                }
            },
            "type": "opt-out",
            "content": {
                "message": "Nasza strona internetowa korzysta z plików cookie. Dzięki temu możemy zapewnić naszym użytkownikom satysfakcjonujące wrażenia z przeglądania naszej witryny i jej prawidłowe funkcjonowanie.",
                "dismiss": "Rozumiem",
                "deny": "",
                "allow": "Rozumiem",
                "link": "Czytaj więcej...",
                "href": "<?php echo base_url(); ?>polityka-prywatnosci.pdf"
            }
        })});
</script>


<section class="default support" style="background-image: url('<?php echo base_url(); ?>uploads/media/<?php echo substr($home->gallery, 0, strpos($home->gallery, "|")); ?>')">
    <div class="default__title-box">
        <h3 class="default__title"><?php echo $home->title; ?></h3>
    </div>

    <div class="container">
        <p class="support__text"><span class="support__bold-text"><?php echo $home->content; ?>
            <img class="support__icon" src="<?php echo base_url(); ?>assets/front/img/miedz.png" alt="logo miedzi legnica">
        </p>
    </div>
</section>

<section class="mailing-list">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h3 class="default__title"><?php echo $newsletter->title; ?></h3>
                <p class="mailing-list__text"><?php echo $newsletter->content; ?></p>
            </div>
            <div class="col-lg-6 d-flex align-items-center">
                <div class="mailing-list__form" action="" method="get">
                    <span id="mailInfo"></span>
                    <input class="mailing-list__input" type="email" name="" id="liameNews" placeholder="Wpisz adres e-mail">
                    <input class="mailing-list__submit" type="submit" onclick="add_newsletter()" value="zapisz się">
                </div>
            </div>
        </div>
    </div>
</section>
</main>

<footer class="main-footer">
    <div class="footer-shape container">
      <div class="footer-shape__background"></div>
      <div class="footer-shape__title"><div>Dostawa na terenie całej Polski <span class="default__bold-text">całkowicie za darmo!</span></div></div>
      <div class="footer-shape__small-part"></div>
      <img class="footer-shape__scissors" src="<?php echo base_url(); ?>assets/front/img/scissors.png" alt="ikonka nożyczek">
    </div>

    <div class="main-footer__content-box">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 main-footer__column mobile-order-1">
            <h3 class="default__title">Napisz do nas</h3>

            <div class="main-footer__form" action="" method="post">
                <span id="mailInfo2"></span>
              <input class="main-footer__input" type="text" name="" id="nameMessage" placeholder="Imię i nazwisko">
              <input class="main-footer__input" type="email" name="" id="liameMessage" placeholder="E-mail">
              <select class="main-footer__input"  name="" id="selectMessage">
                  <?php foreach ($products as $item): ?>
                <option value="<?php echo $item->title; ?>"><?php echo $item->title; ?></option>
                  <?php endforeach; ?>
              </select>
              <textarea class="main-footer__textarea" name="" id="contentMessage" cols="30" rows="4" placeholder="Wiadomość"></textarea>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="rodoMessage" value="Zaakceptowane" name="rodo">
                    <label class="form-check-label" for="rodoMessage"><small>Zgoda na przetwarzanie danych osobowych</small></label>
                    <center>
                <div class="g-recaptcha my-3" data-sitekey="6LcMbagUAAAAAFlecQvLKf-Gh3Prm-fH79-lALqg"></div>
              <span class="warning_captcha text-danger" class="text-danger"></span></center>
                </div>

              <input class="main-footer__submit" onclick="send_message(1)"  type="submit" value="wyślij">
            </div>
          </div>
          <div class="col-lg-4 main-footer__column position-relative mobile-order-3">
            <h3 class="default__title">Kontakt</h3>

            <p class="default__text default__text--address">
                <?php echo $contact_4->content; ?>
                <a class="main-footer__contact-link" href="tel:<?php echo $contact_2->title; ?>">tel./fax <?php echo $contact_2->title; ?></a><br>
                <a class="main-footer__contact-link" href="tel:<?php echo $contact_2->subtitle; ?>">tel. kom. <?php echo $contact_2->subtitle; ?></a><br>
            </p>

            <a href="<?php echo base_url(); ?>p/kontakt" class="main-footer__button">zapytanie o wycenę on-line</a>

            <p class="main-footer__copyrights">Wszystkie prawa zastrzeżone.<br>Copyright by <span class="default__bold-text">Drukarnia Panda</span></p>
          </div>
          <div class="col-lg-4 main-footer__column mobile-order-2">
            <h3 class="default__title">Dla klientów</h3>

            <div class="main-footer__link-box">
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/produkty">Produkty</a><br>
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/studio">Studio graficzne</a><br>
                <a class="main-footer__link" href="<?php echo base_url(); ?>p/wyroznienia">Wyróżnienia</a><br>
              <a class="main-footer__link" href="<?php echo base_url(); ?>p/kontakt">Napisz wiadomość</a><br>
            </div>

            <div>
              <a href="https://www.facebook.com/PPHUPanda/"><img class="main-footer__icon" src="<?php echo base_url(); ?>assets/front/img/facebook.png" alt="ikona facebooka"></a>
<!--              <a href="#"><img class="main-footer__icon" src="--><?php //echo base_url(); ?><!--assets/front/img/instagram.png" alt="ikona instagrama"></a>-->

              <button class="main-footer__up-button">na górę</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- /Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
  <!-- owl carousel -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/owl.carousel.min.js"></script>
  <!-- my scripts -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/lightbox.js"></script>

    <script src="<?php echo base_url(); ?>assets/front/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl-init.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.animate.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autoheight.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autoplay.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.autorefresh.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.hash.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.lazyload.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.navigation.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.support.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/owl/js/owl.video.js"></script>

    <script type="text/javascript">
      function validateMyForm(nr)
      {
        if (grecaptcha.getResponse(nr) == ""){
            document.getElementsByClassName('warning_captcha')[nr].innerHTML = 'Potwierdź captche';
            return false;
        } else {
            return true;
        }
      }
    </script>
    
<script type="text/javascript">
    function add_newsletter()
    {
        var liame = document.getElementById('liameNews').value;
        if(liame != '' && liame.indexOf("@") != -1 && liame.indexOf(".") != -1) {
        $.ajax({
            type: "post",
            url:"<?php echo base_url(); ?>mail/add",
            data: {liame:liame},
            cache: false,
            beforeSend:function(data)
            {
                document.getElementById('mailInfo').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Zapisywanie do listy.</span>';
            },
            complete:function(html)
            {
                document.getElementById('mailInfo').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zostałeś pomyślnie zapisany!</span>';
            }
        });
        } else {
            document.getElementById('mailInfo').innerHTML = '<span class="text-danger"><i class="fas fa-times"></i> Pole zostało niepoprawnie wypełnione.</span>';
        }
    }
</script>

<script type="text/javascript">
    function send_message(nr)
    {
                if (grecaptcha.getResponse(nr) == ""){
            document.getElementById('mailInfo2').innerHTML = '<span class="text-danger"><i class="fas fa-times"></i> Potwierdź captche.</span>';
          } else {
        var name = document.getElementById('nameMessage').value;
        var liame = document.getElementById('liameMessage').value;
        var content = document.getElementById('contentMessage').value;
        var select = document.getElementById('selectMessage').value;
        var rodo = document.getElementById('rodoMessage').checked;
        if(rodo == true) { rodo = 'Zaakceptowane'; } else { rodo = 'Niezaakceptowane'; }
        //alert('name: ' + name + ' liame: ' + liame + ' content: ' + content + ' select: ' + select + ' rodo: ' + rodo);
        if(liame != '' && liame.indexOf("@") != -1 && liame.indexOf(".") != -1 && name != '' && content != '') {
            $.ajax({
                type: "post",
                url:"<?php echo base_url(); ?>mail/send",
                data: {name:name, liame:liame, content:content, select:select, rodo:rodo},
                cache: false,
                beforeSend:function(data)
                {
                    document.getElementById('mailInfo2').innerHTML = '<span class="text-success"><i class="fas fa-spinner fa-pulse"></i> Wysyłanie wiadomości.</span>';
                },
                success:function(data)
                {
                  console.log(data);
                },
                complete:function(data)
                {
                  console.log(data);
                    document.getElementById('mailInfo2').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Wiadomośc została wysłana!</span>';
                }
            });
        } else {
            document.getElementById('mailInfo2').innerHTML = '<span class="text-danger"><i class="fas fa-times"></i> Pola zostały niepoprawnie wypełnione.</span>';
        }
    }
  }
</script>

</body>

</html>