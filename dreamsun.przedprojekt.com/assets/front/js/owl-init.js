$(document).ready(function () {
    //Init the carousel

    initSlider();
    function initSlider() {
        $(".owl-carousel-1").owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: false,
            dots: false
        });
    }

    initSlider2();
    function initSlider2() {
        $(".owl-carousel-2").owlCarousel({
            items: 5,
            loop: true,
            autoplay: true,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: false,
            responsive:{
                0:{
                    items:1
                },
                540:{
                    items:2
                },
                767:{
                    items:3
                },
                992:{
                    items:3
                }

            }

        });
    }

    initSlider3();
    function initSlider3() {
        $(".owl-carousel-3").owlCarousel({
            items: 3,
            loop: false,
            autoplay: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: false,
            responsive:{
                0:{
                    items:1
                },
                540:{
                    items:2
                },
                767:{
                    items:3
                },
                992:{
                    items:3
                }

            }
        });
        

    }

    initSlider4();
    function initSlider4() {
        $(".owl-carousel-4").owlCarousel({
            items: 10,
            loop: false,
            autoplay: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: false,
            responsive:{
                0:{
                    items:2
                },
                540:{
                    items:3
                },
                767:{
                    items:6
                },
                992:{
                    items:10
                }

            }
        });
        

    }

    initSlider5();
    function initSlider5() {
        $(".owl-carousel-5").owlCarousel({
            items: 10,
            loop: false,
            autoplay: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            nav: false,
            responsive:{
                0:{
                    items:2
                },
                540:{
                    items:3
                },
                767:{
                    items:6
                },
                992:{
                    items:10
                }

            }
        });
        

    }

    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "100%",
            transition: "width 5000ms"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }
});