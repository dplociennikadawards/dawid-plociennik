<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj ofertę #<?php echo $row->oferta_id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="container-fluid">
                
                    <?php echo validation_errors(); ?>
                   <div class="row px-5">
                       <div class="col-md-6">

                           <div class="md-form mb-5">
                               <select name="kategorie" class="form-control mdb-select md-form" style="display: block !important;">
                                <option value="" disabled selected>Wybierz kategorię</option>
                                <?php foreach ($kate as $k): ?>
                                <option value="<?php echo $k->slug ?>" <?php if($row->category == $k->slug){echo 'selected';} ?>><?php echo $k->title ?></option>
                                <?php endforeach ?>
                              </select>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                               <label for="formTitle">Tytuł oferty<span class="text-danger">*</span></label>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formSubtitle" class="form-control" name="subtitle" value="<?php echo $row->subtitle; ?>" required>
                               <label for="formSubtitle">Podtytuł oferty<span class="text-danger">*</span></label>
                           </div>

                           <div class="md-form">
                               <textarea name="content" id="editor"><?php echo $row->content; ?></textarea>
                           </div>

                           <div class="md-form">
                               <textarea name="content2" id="editor2"><?php echo $row->content2; ?></textarea>
                           </div>


                       </div>

                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <div class="file-field">
                                   <div id="photo_show" class="text-center">
                                       <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/oferta/<?php echo $row->photo; ?>" width="250">
                                   </div>
                                   <div class="btn btn-primary btn-sm float-left gold-gradient">
                                       <span>Wybierz zdjęcie</span>
                                       <input type="file" name="photo" id="photo">
                                   </div>
                                   <div class="file-path-wrapper">
                                       <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo; ?>" required readonly>
                                   </div>
                               </div>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formAlt1" class="form-control" name="alt" value="<?php echo $row->alt; ?>">
                               <label for="formAlt1">Tekst alternatywny zdjęcia</label>
                           </div>
                           <div class="md-form mb-5">
                               <div class="file-field">
                                   <div id="header_photo_show" class="text-center">
                                       <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/oferta/<?php echo $row->header_photo; ?>" width="250">
                                   </div>
                                   <div class="btn btn-primary btn-sm float-left gold-gradient">
                                       <span>Wybierz ikone</span>
                                       <input type="file" name="header_photo" id="header_photo">
                                   </div>
                                   <div class="file-path-wrapper">
                                       <input name="header_name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->header_photo; ?>" required readonly>
                                   </div>
                               </div>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formAlt2" class="form-control" name="header_alt" value="<?php echo $row->header_alt; ?>">
                               <label for="formAlt2">Tekst alternatywny zdjęcia</label>
                           </div>
                        </div>
                   </div>

        </div>
    </section>
    </form>
</main>
