<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container-fluid mb-5">
			<!-- Table with panel -->
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<input type="text" id="searchInput" class="form-control" onkeyup="searchFiltr(0)" placeholder="Wpisz nazwę..." title="Wpisz nazwę">
			    </div>
			    
			    <a href="" class="white-text mx-3">Rezerwacje</a>

                  <div class="text-right" style="width: 181px;">

			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4">

			    <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 12%;">
				              Potwierdzenie rezerwacji
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 16.6666%;">
				              Rezerwujący
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(1)" style="width: 20%;">
				              E-mail / Telefon
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(2)" style="width: 16.6666%;">
				              Zabieg
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(3)" style="width: 16.6666%;">
				              Data rezerwacji
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(4)" style="width: 16.6666%;">
				              Pracownik
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->

				        <tbody>
						  	<?php foreach (array_reverse($rows) as $key): ?>
						  		<tr>
						            <td class="align-middle text-center">
						            	<?php if($key->active == 1) {echo '<i class="far fa-check-circle text-success" style="font-size: 1.5rem;"></i>';} else {echo '<i class="far fa-times-circle text-danger" style="font-size: 1.5rem;"></i>';}; ?>
						            </td>
						            <td class="align-middle">
						            	<?php echo $key->name; ?>
						            </td>
						            <td class="align-middle">
						            	<?php echo $key->email; ?> /<br> <?php echo $key->tel; ?>
						            </td>
						            <td class="align-middle">
						            	<?php echo $key->reservation; ?>
						            </td>
						            <td class="align-middle">
						            	<?php echo $key->reservation_date; ?>
						            </td>
						            <td class="align-middle">
						            	<?php echo $key->reservation_worker; ?>
						            </td>
				            	</tr>
							<?php endforeach; ?>
				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
</main>