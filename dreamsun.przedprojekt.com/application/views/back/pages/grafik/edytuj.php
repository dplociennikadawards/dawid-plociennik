<style type="text/css">
  .dropdown-menu {
    background-color: white;
    width: auto;
  }
</style>
<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Dodaj zabieg</a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="container-fluid">
                
                    <?php echo validation_errors(); ?>
                   <div class="row px-5">
                       <div class="col-md-12">

                           <div class="md-form mb-5">
                               <select name="oferta_id" class="form-control mdb-select md-form" style="display: block !important;">
                                <option value="" disabled selected>Wybierz ofertę</option>
                                <?php foreach ($produkty as $p): ?>
                                <option value="<?php echo $p->produkty_id ?>" <?php if($row->oferta_id == $p->produkty_id){echo 'selected';} ?>><?php echo $p->title ?></option>
                                <?php endforeach ?>
                              </select>
                           </div>

                           <div class="md-form mb-5">
                               <select name="pracownicy_id" class="form-control mdb-select md-form" style="display: block !important;">
                                <option value="" disabled selected>Wybierz pracownika</option>
                                <?php foreach ($pracownicy as $prac): ?>
                                <option value="<?php echo $prac->pracownicy_id ?>" <?php if($row->pracownicy_id == $prac->pracownicy_id){echo 'selected';} ?>><?php echo $prac->title ?></option>
                                <?php endforeach ?>
                              </select>
                           </div>

                           <div class="md-form">
                            <input type="text" id="date-picker-example" name="free_dates" value="<?php echo $row->free_dates ?>" class="form-control date"/>
                            <label for="date-picker-example">Wybierz termin</label>
                          </div>

                       </div>

                   </div>

        </div>
    </section>
    </form>
</main>
