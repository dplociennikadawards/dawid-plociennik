<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj stronę główną #<?php echo $row->glowna_id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-12">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                      <label for="formTitle">Tytuł<span class="text-danger">*</span></label>
                    </div>
                    <div class="md-form mb-5">
                        <input type="text" id="formSubtitle" class="form-control" name="subtitle" value="<?php echo $row->subtitle; ?>" required>
                        <label for="formSubtitle">Podtytuł<span class="text-danger">*</span></label>
                    </div>
                </div>
              </div>
                 <div class="row px-5">
                      <div class="col-4">
                        <div class="md-form">
                      <input type="text" id="formTitle1" class="form-control" name="title1" value="<?php echo $row->title1; ?>" required>
                      <label for="formTitle1">Tytuł<span class="text-danger">*</span></label>
                      </div>
                        <div class="md-form">
                      <div class="file-field">
                        <div id="photo_show" class="text-center">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/<?php echo $row->photo; ?>" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie</span>
                          <input type="file" name="photo" id="photo">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo; ?>" required readonly>
                        </div>
                      </div>
                    </div>

                    <div class="md-form">
                    <input type="text" id="formAlt" class="form-control" name="alt" value="<?php echo $row->alt; ?>">
                    <label for="formAlt">Tekst alternatywny zdjęcia</label>
                    </div>

                    <div class="md-form">
                        <textarea name="content" id="editor1"><?php echo $row->content; ?></textarea>
                        </div>
                        

                    

                      </div>

                      <div class="col-4">
                        <div class="md-form">
                      <input type="text" id="formTitle2" class="form-control" name="title2" value="<?php echo $row->title2; ?>" required>
                      <label for="formTitle2">Tytuł<span class="text-danger">*</span></label>
                    </div>
                      <div class="md-form">
                      <div class="file-field">
                        <div id="photo_show1" class="text-center">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/<?php echo $row->photo1; ?>" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie</span>
                          <input type="file" name="photo1" id="photo1">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo1" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo1; ?>" required readonly>
                        </div>
                      </div>
                    </div>

                    <div class="md-form">
                    <input type="text" id="formAlt1" class="form-control" name="alt1" value="<?php echo $row->alt1; ?>">
                    <label for="formAlt1">Tekst alternatywny zdjęcia</label>
                    </div>
                        <div class="md-form">
                        <textarea name="content1" id="editor2"><?php echo $row->content1; ?></textarea>
                        </div>
                        
                      </div>

                      <div class="col-4">
                        <div class="md-form mb-5">
                      <input type="text" id="formTitle3" class="form-control" name="title3" value="<?php echo $row->title3; ?>" required>
                      <label for="formTitle3">Tytuł<span class="text-danger">*</span></label>
                    </div>
                        <div class="md-form">
                      <div class="file-field">
                        <div id="photo_show2" class="text-center">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/<?php echo $row->photo2; ?>" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie</span>
                          <input type="file" name="photo2" id="photo2">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo2" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo2; ?>" required readonly>
                        </div>
                      </div>
                    </div>

                    <div class="md-form">
                    <input type="text" id="formAlt2" class="form-control" name="alt2" value="<?php echo $row->alt2; ?>">
                    <label for="formAlt2">Tekst alternatywny zdjęcia</label>
                    </div>

                    <div class="md-form">
                        <textarea name="content2" id="editor3"><?php echo $row->content2; ?></textarea>
                        </div>
                        
                      </div>
                    </div>
            </div>
        </div>
    </section>
    </form>
</main>
