<script>
  //single photo
window.addEventListener('load', function() {
  document.getElementById('photo').addEventListener('change', function() {
    document.getElementById('photo_show').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_show').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});

  //second photo
window.addEventListener('load', function() {
  document.getElementById('photo1').addEventListener('change', function() {
    document.getElementById('photo_show1').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_show1').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});

  //third photo
window.addEventListener('load', function() {
  document.getElementById('photo2').addEventListener('change', function() {
    document.getElementById('photo_show2').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('photo_show2').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});


//single header
window.addEventListener('load', function() {
  document.getElementById('header_photo').addEventListener('change', function() {
    document.getElementById('header_photo_show').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';
    
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          setTimeout(function(){
          img.onload = imageIsLoaded; // optional onload event listener
            document.getElementById('header_photo_show').innerHTML = '<img class="img-fluid img-thumbnail" src="'+img.src+'" width=250>';
          }, 500);
      }
  });
});

function imageIsLoaded(e) { }
</script>

<script type="text/javascript">
//multiple gallery
window.addEventListener('load', function() {
  document.getElementById('gallery').addEventListener('change', function() {
    document.getElementById('gallery_show').innerHTML = '<i class="fas fa-spinner fa-pulse loader"></i>';

      if (this.files && this.files[0]) {
          var filesLength = this.files.length;
          alert(filesLength);
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          img.onload = imageIsLoaded; // optional onload event listener

          var photos = '';
          for (var i = 0; i < filesLength; i++) {
            var photos =+ '<div class="col-md-4"><img class="img-fluid img-thumbnail" src="'+img.src+'" width=250></div>';
          }
          document.getElementById('gallery_show').innerHTML = '<div class="row">'+photos+'</div>';

      }
  });
});

function imageIsLoaded(e) { }
</script>

<script>
  ClassicEditor
      .create( document.querySelector( '#editor' ) );
</script>
<script>
  ClassicEditor
      .create( document.querySelector( '#editor1' ) );
</script>
<script>
  ClassicEditor
      .create( document.querySelector( '#editor2' ) );
</script>
<script>
  ClassicEditor
      .create( document.querySelector( '#editor3' ) );
</script>

<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("filtrableTable");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc"; 
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++; 
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

<script>
function sortTableData(n) {
  var table, rows, switching, i, shouldSwitch, dir, switchcount = 0;
  var x = 0;
  var y = 0;
  table = document.getElementById("filtrableTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      x = Date.parse(x.innerHTML.toLowerCase());
      y = Date.parse(y.innerHTML.toLowerCase());
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x > y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x < y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

<script type="text/javascript">

  function upPriority(id,table) {
    $.ajax({  
         type: "post", 
	       url:"<?php echo base_url(); ?>panel/p/upPriority/", 
         data: {id:id, table:table}, 
         cache: false,  
         success:function(html)  
         { 
          $('#refreshTable').load(document.URL +  ' #refreshTable');
         },
         complete:function(html){
          console.log(html);
         }
    });  
  }
  
  function downPriority(id,table) {
    $.ajax({  
         type: "post", 
	       url:"<?php echo base_url(); ?>panel/p/downPriority/", 
         data: {id:id, table:table}, 
         cache: false,  
         success:function(html)  
         { 
         	console.log(html);
          $('#refreshTable').load(document.URL +  ' #refreshTable');
         },
         complete:function(html){
          console.log(html);
         }  
    });  
  }

  function active(id,table) {
    value = document.getElementById('checkbox'+id).checked;
    if(value == true) { value = 1;} else {value = 0;}

	  $.ajax({  
	       type: "post", 
	       url:"<?php echo base_url(); ?>panel/p/active", 
	       data: {id:id, value:value, table:table}, 
	       cache: false,  
	       success:function(html)  
	       {  
	       	console.log(html);
	       }  
	  });  
  }
</script>
<script type="text/javascript">
  function showIcon(){
    icon = document.getElementById('formIcon').value;
    document.getElementById('show_icon').innerHTML = '<i class="'+icon+'" style="font-size: 2rem;"></i>';
  }
</script>