    <!--Footer-->
    <footer>
        <!-- SCRIPTS -->
	    <!-- JQuery -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/jquery-3.3.1.min.js"></script>
	    <!-- Bootstrap tooltips -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/popper.min.js"></script>
	    <!-- Bootstrap core JavaScript -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/bootstrap.min.js"></script>
	    <!-- MDB core JavaScript -->
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/back/js/mdb.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	    <script type="text/javascript">
	    	$('.date').datepicker({ 
	    		multidate: true,
	    		format: 'yyyy-mm-dd',
    		 }); 
	    </script>
	    <script type="text/javascript">
	    	$( "#toggle-menu" ).click(function() {
			   $("#menu").toggleClass("show-menu");
			   $("#bg-opacity").toggleClass("d-none");
			});
	    	$( "#bg-opacity" ).click(function() {
			   $("#menu").toggleClass("show-menu");
			   $("#bg-opacity").toggleClass("d-none");
			});
	    </script>

	    <script>
		function searchFiltr(nr) {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("searchInput");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("filtrableTable");
		  tr = table.getElementsByTagName("tr");
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[nr];
		    if (td) {
		      txtValue = td.textContent || td.innerText;
		      if (txtValue.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    }       
		  }
		}
		</script>

		<script type="text/javascript">
			$(function () {
			$('[data-toggle="popover-hover"]').popover();
			});
		</script>

   	</footer>
    <!--Footer-->
  </body>
</html>