      <!--Double navigation-->
    <header>
      <!-- Navbar -->
      <nav class="navbar navbar-toggleable-md navbar-expand-lg double-nav py-0">
        <!-- SideNav slide-out button -->
        <div class="float-left py-2">
          <a href="#" id="toggle-menu" class="button-collapse">
          	<i class="fas fa-bars"></i>
          </a>
        </div>
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
          <li class="nav-item py-2 pl-2">
            <a href="<?php echo base_url(); ?>logout" class="nav-link header-font-size">
            	<i class="fas fa-power-off"></i><span class="clearfix d-none d-sm-inline-block"></span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.Navbar -->

      <!-- Sidebar navigation -->
      <div id="menu" class="side-nav slide-margin">
        <ul class="custom-scrollbar">
          <!-- Side navigation links -->
          <li>
            <ul class="collapsible collapsible-accordion">
              <li>
              	<a href="<?php echo base_url(); ?>panel/glowna" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/home.svg" class="img-fluid menu-icon"> Strona głowna
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/slider" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/image.svg" class="img-fluid menu-icon"> Slider
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/podstrony" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/folder.svg" class="img-fluid menu-icon"> Podstrony
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/o_nas" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/teamwork.svg" class="img-fluid menu-icon"> O nas
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/news" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/tiles.svg" class="img-fluid menu-icon"> Aktualności
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/oferta" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/online-shop.svg" class="img-fluid menu-icon"> Oferta
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/produkty" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/tiles.svg" class="img-fluid menu-icon"> Produkty
                </a>
              </li>
              <li>
              	<a href="<?php echo base_url(); ?>panel/gallery/dodaj/galeria/1" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/factory.svg" class="img-fluid menu-icon"> Urządzenia
              	</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/kontakt" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/email.svg" class="img-fluid menu-icon"> Kontakt
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>panel/ustawienia" class="collapsible-header my-2">
                  <img src="<?php echo base_url(); ?>assets/back/img/icons/settings.svg" class="img-fluid menu-icon"> Ustawienia
                </a>
              </li>
            </ul>
            <ul>
              <!-- Logo -->
              	<li>
                  <div class="logo-wrapper">
                    <a href="https://agencjamedialna.pro/"><img src="<?php echo base_url(); ?>assets/back/img/adawards.png" class="img-fluid flex-center"></a>
                  </div>
                </li>
                  <!--/. Logo -->
            </ul>
          </li>
          <!--/. Side navigation links -->
          
        </ul>
        <div class="sidenav-bg"></div>
      </div>
      <!--/. Sidebar navigation -->

    </header>
    <!--/.Double navigation-->
    <?php if($this->uri->segment(1) != ''): ?>
      <div class="navi-header pl-md-3">
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php echo ucfirst($this->uri->segment(1)); ?></a>
      <?php if($this->uri->segment(2) != ''): ?>
          <a href="<?php echo base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2); ?>">/ <?php echo ucfirst($this->uri->segment(2)); ?></a>
      <?php endif; ?>
      <?php if($this->uri->segment(3) != ''): ?>
          <a href="<?php echo base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3); ?>">/ <?php echo ucfirst($this->uri->segment(3)); ?></a>
      <?php endif; ?>
      <?php if($this->uri->segment(4) != ''): ?>
          <a href="<?php echo base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4); ?>">/ <?php echo ucfirst($this->uri->segment(4)); ?></a>
      <?php endif; ?>
      <?php if($this->uri->segment(5) != ''): ?>
          <a href="<?php echo base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5); ?>">/ <?php echo ucfirst($this->uri->segment(5)); ?></a>
      <?php endif; ?>
      </div>
    <?php endif; ?>