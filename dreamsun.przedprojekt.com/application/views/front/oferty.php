<main>
	
	<div class="container">
		<!-- Section: Blog v.3 -->
<section class="my-5">

<?php foreach ($rows as $row): ?>
  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
        <img class="img-fluid" src="<?php echo base_url(); ?>uploads/oferta/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
        <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->oferta_id; ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold mb-3">
      	<a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->oferta_id; ?>" class="text-dark">
      		<strong><?php echo $row->title; ?></strong>
      	</a>
      </h3>
      <!-- Excerpt -->
      <p class="dark-grey-text mb-5"><?php echo substr($row->content, 0, 300); ?>...</p>
      <!-- Read more button -->
      <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->oferta_id; ?>" class="main-header__btn border mt-5" style="cursor: pointer;">Czytaj więcej</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">
<?php endforeach ?>


</section>
<!-- Section: Blog v.3 -->
	</div>
</main>