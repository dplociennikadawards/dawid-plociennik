  <style type="text/css">
  	html {
  		scroll-behavior: smooth;
  	}
  	.backup {
  		position: fixed;
  		bottom: 5%;
  		right: 2%;
  		background-color: rgba(0,0,0, 0.3);
  		height: 50px;
  		width: 50px;
  		text-align: center;
  		line-height: 50px;
  		font-size: 1.5rem;
  		border-radius: 50%;
  		z-index: 2;
  		}
  </style>
  <main id="backUp">
  	<div class="backup">
  		<a href="#backUp" class="text-white"><i class="fas fa-chevron-up"></i></a>
  	</div>
  	<div class="container">
  		<!-- Section: Blog v.1 -->
		<section class="my-5">

		  <!-- Grid row -->
		  <div class="row">
		  	<?php if($row->photo): ?>
			    <!-- Grid column -->
			    <div class="col-lg-5 mb-2">

			      <!-- Featured image -->
			      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
			        <img class="img-fluid w-100" src="<?php echo base_url(); ?>uploads/news/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
			        <a>
			          <div class="mask rgba-white-slight"></div>
			        </a>
			      </div>

			    </div>
			    <!-- Grid column -->
				<?php endif; ?>

			    <!-- Grid column -->
			    <div id="<?php echo $this->slugify_m->slugify($row->title); ?>" class="col-lg-7 <?php if($row->photo == ''){echo 'col-lg-12';} ?> mb-2">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong><?php echo $row->title; ?></strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p><?php echo $row->content; ?>
				    </p>
			    </div>
			    <!-- Grid column -->

		    
		  </div>
		  <!-- Grid row -->

		</section>

		<hr class="my-5">
		<!-- Section: Blog v.1 -->
  	</div>

  </main>
