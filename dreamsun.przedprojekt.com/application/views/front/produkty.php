  <main>

  <div class="container my-5">
    <span class="about__separator my-5 w-100"></span>
      <div class="row shadow">

        <?php $i=0; foreach ($rows as $row): $i++; ?>
        <div class="col-lg-3 col-md-4 col-6 box-offer <?php if($i%2!=0){echo'bg-orange';}else{echo'bg-dark';} ?>">
          <a href="<?php echo base_url(); ?>p/wpis/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->produkty_id; ?>">
            <div class="box-img">
              <img src="<?php echo base_url(); ?>uploads/produkty/<?php echo $row->header_photo; ?>" width="200" class="img-offer invert">
            </div>
            <div>
              <h5 class="slide__title text-white text-center text-uppercase"><?php echo $row->title; ?></h5>
            </div>
          </a>
        </div>
        <?php endforeach ?>

      </div>
    <span class="about__separator my-5 w-100"></span>
    </div>

  </main>