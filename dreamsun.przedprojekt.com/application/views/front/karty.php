  <style type="text/css">
  	html {
  		scroll-behavior: smooth;
  	}
  	.backup {
  		position: fixed;
  		bottom: 5%;
  		right: 2%;
  		background-color: rgba(0,0,0, 0.3);
  		height: 50px;
  		width: 50px;
  		text-align: center;
  		line-height: 50px;
  		font-size: 1.5rem;
  		border-radius: 50%;
  		z-index: 2;
  		}
  </style>
  <main id="backUp">
  	<div class="backup">
  		<a href="#backUp" class="text-white"><i class="fas fa-chevron-up"></i></a>
  	</div>
  	<div class="container">
  		<!-- Section: Blog v.1 -->
		<section class="my-5">

		  <!-- Grid row karta dream sun -->
		  <div class="row" id="dreamsun">
			    <!-- Grid column -->
			    <div class="col-lg-5 mb-2">

			      <!-- Featured image -->
			      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
			        <img class="img-fluid m-auto" src="<?php echo base_url(); ?>uploads/karta_klienta_1l.jpg" alt="zdj">
			        <a>
			          <div class="mask rgba-white-slight"></div>
			        </a>
			      </div>

			    </div>
			    <!-- Grid column -->

			    <!-- Grid column -->
			    <div class="col-lg-7 mb-2">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong>Karta Dream Sun</strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p>1. Kartę stałego klienta "Dream Sun" otrzymyjemy w momencie zakupu karnetu na wybrane usługi.<br><br>

					2. Karta stałego klienta "Dream Sun" jest kartą imienną. Oznacza to, że tylko włąściciel karty może korzystać z jej przywilejów.<br><br>

					3. Posiadacz karty "Dream Sun" uprawniony jest do zakupów w cenach rabatowych, odpowiednio do posiadanej karty (rabaty od - 10% do - 40%).<br><br>

					4. Zakupując kolejne karnety na usługi, naliczane są punkty na konto włąściciela karty. Po uzbieraniu odpowiedniej ilości punktów właściciel karty otrzymuje "Złotą kartę VIP", dzięki której może korzystać z większych rabatów oraz dodatkowych przywilejów, promocji.
					<br><br>
					System punktowy za poszczególne usługi:

					(1x collarium = 3pkt, 1xVacuMaxx= 3pkt, 1x Platforma= 2pkt, 1xFitness= 1pkt, 1xsauna= 4pkt, 1xTermoSpace= 3 pkt, 1xRollmasaż= 2 pkt)
					<br><br>
					5. Informację dotyczącą ilość posiadanych punktów w ramach programu "Dream Sun" można uzyskać bezpośrednio w salonie.
				    </p>
			    </div>
			    <!-- Grid column -->

		    
		  </div>
		  <!-- Grid row -->


		  <!-- Grid row złota karta vip -->
		  <div class="row" id="vip">
			    <!-- Grid column -->
			    <div class="col-lg-5 mb-2">

			      <!-- Featured image -->
			      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
			        <img class="img-fluid m-auto" src="<?php echo base_url(); ?>uploads/zlota_karta_vip.jpg" alt="zdj">
			        <a>
			          <div class="mask rgba-white-slight"></div>
			        </a>
			      </div>

			    </div>
			    <!-- Grid column -->

			    <!-- Grid column -->
			    <div class="col-lg-7 mb-2">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong>Złota Karta VIP</strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p>1. W programie "Złota Karta VIP" może uczestniczyć osoba posiadająca "Kartę Dream Sun". Zakupując karnety na wybrane usługi naliczane są punkty odpowiednio do przeliczników. Np. nabywając karnet na 11 zabiegów w saunie i 6 zabiegów w kolarium uzyskujemy odpowiednio: (11 x 4 pkt) + (6 x 3 pkt) = 62 pkt. Bieżącą informację odnośnie posiadanych punktów otrzymają Państwo bezpośrednio w salonie
			      	<br><br>
					2. "Złotą Kartę VIP" otrzymujemy w momencie zgromadzenia odpowiednio:
<br><br>
					a. do końca roku 2010 - 250 pkt.
<br><br>
					b. do końca czerwca 2011 - 300 pkt.
<br><br>
					3. Posiadacz "Złotej Karty VIP" jest uprawniony do:
<br><br>
					a. aktualnych cen rabatowych (rabaty od - 20% do - 50%)
<br><br>
					b. otrzymuje w dniu urodzin bon na usługi w salonie w wysokości 30,00 pln do wykorzystania w ciągu 3 dni roboczych
<br><br>
					c. każde kolejne 100 pkt może wymienić na dowolny karnet w cenie 50 % ceny podstawowej (nie obejmuje cen promocyjnych)
<br><br>
					d. może przekazać posiadaną "Kartę Dream Sun" innej osobie, która wówczas jest uprawniona do zakupów w cenach rabatowych. W innym przypadku należy zwrócić posiadaną kartę do salonu
<br><br>
					e. jest uprawniony do korzystania z innych atrakcji, promocji na bieżąco ogłaszanych przez Salon Dream Sun
				    </p>
			    </div>
			    <!-- Grid column -->

		    
		  </div>
		  <!-- Grid row -->


		  	  <!-- Grid row karty multi sport itd -->
		  <div class="row" id="pracownicy">
			    <!-- Grid column -->
			    <div class="col-lg-5 mb-2">

			      <!-- Featured image -->
			      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
			        <img class="img-fluid m-auto" src="<?php echo base_url(); ?>uploads/dreamsun_karty.jpg" alt="zdj">
			        <a>
			          <div class="mask rgba-white-slight"></div>
			        </a>
			      </div>

			    </div>
			    <!-- Grid column -->

			    <!-- Grid column -->
			    <div class="col-lg-7 mb-2">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong>Dobrze motywuj swoich praconików!</strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p><strong>Dobry pracownik, to pracownik dobrze zmotywowany.</strong><br><br>
					Zapewnij swoim pracownikom dobre samopoczucie poprzez zmiejszenie stresu, poprawy stanu zdrowia, lepszą sprawność fizyczną i zwiększenie atrakcyjności.<br><br>

					Pracownik pozytywnie nastawiony do życia lepiej i efektywniej pracuje. Jest bardziej zaangażowany w swoją pracę i czuje się bardziej zintegrowany z firmą.<br><br>

					Mając to wszystko na uwadze, przygotowaliśmy specjalną ofertę dla firm.<br><br>

					Skontaktuj się z nami. Umówimy się na spotkanie i wspólnie omówimy potrzeby Twoich pracowników.
				    </p>
			    </div>
			    <!-- Grid column -->

		    
		  </div>
		  <!-- Grid row -->

		</section>

		<hr class="my-5">
		<!-- Section: Blog v.1 -->
  	</div>

  </main>
