  <style type="text/css">
  	html {
  		scroll-behavior: smooth;
  	}
  	.backup {
  		position: fixed;
  		bottom: 5%;
  		right: 2%;
  		background-color: rgba(0,0,0, 0.3);
  		height: 50px;
  		width: 50px;
  		text-align: center;
  		line-height: 50px;
  		font-size: 1.5rem;
  		border-radius: 50%;
  		z-index: 2;
  		}
  </style>
  <main id="backUp">
  	<div class="backup">
  		<a href="#backUp" class="text-white"><i class="fas fa-chevron-up"></i></a>
  	</div>
  	<div class="container">
  		<!-- Section: Blog v.1 -->
		<section class="my-5">

		  <!-- Grid row -->
		  <div class="row">
		  	<?php foreach ($news as $row): ?>
		  	<?php if((strpos($row->photo, 'pdf') === false)){ ?>
			    <!-- Grid column -->
			    <?php if($row->photo != ''): ?>
				    <div class="col-lg-5 mb-2">

				      <!-- Featured image -->
				      <div class="view overlay rounded z-depth-2  mb-4">
				        <img class="img-fluid w-100" src="<?php echo base_url(); ?>uploads/news/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
				        <a>
				          <div class="mask rgba-white-slight"></div>
				        </a>
				      </div>

				    </div>
				<?php endif; ?>
			    <!-- Grid column -->


			    <!-- Grid column -->
			    <div id="<?php echo $this->slugify_m->slugify($row->title); ?>" class="<?php if($row->photo != ''){echo 'col-md-7';}else{echo 'col-md-12';} ?> mb-4">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong><?php echo $row->title; ?></strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p><?php echo substr($row->content, 0,300); ?>
				    </p>
				    <a href="<?php echo base_url() ?>p/aktualnosc/<?php echo $this->slugify_m->slugify($row->title); ?>/<?php echo $row->news_id; ?>" class="btn btn-lg text-dark">Czytaj więcej</a>
			    </div>
			    <!-- Grid column -->
				<?php }else{ ?>

			    <!-- Grid column -->
			    <div id="<?php echo $this->slugify_m->slugify($row->title); ?>" class="col-md-12 mb-4">

			      <!-- Post title -->
			      <h3 class="font-weight-bold"><strong><?php echo $row->title; ?></strong></h3>
				  <span class="main-header__shape1 mb-4 w-100"></span>
			      <!-- Excerpt -->
			      <p><?php echo substr($row->content, 0,300); ?>
				    </p>
				    <a href="<?php echo base_url(); ?>uploads/news/<?php echo $row->photo; ?>" target="_blank" class="btn btn-lg text-dark">Czytaj więcej</a>
			    </div>
			    <!-- Grid column -->
				<?php } ?>
  			<?php endforeach ?>
		    
		  </div>
		  <!-- Grid row -->

		</section>

		<hr class="my-5">
		<!-- Section: Blog v.1 -->
  	</div>

  </main>
