<?php if($this->session->flashdata('flashdata')) {
    echo '<div class="container">' . $this->session->flashdata('flashdata') . '</div>';
} ?>
  <main>
    <section class="offer">
      <div class="offer__background-text offer__background-text--part1"><img class="offer__background-img"
          src="<?php echo base_url(); ?>assets/front/img/OFERTA.png" alt="fragment tła z napisem oferta"></div>
      <div class="offer__background-text offer__background-text--part2"><img class="offer__background-img"
          src="<?php echo base_url(); ?>assets/front/img/OFERTA2.png" alt="fragment tła z napisem oferta"></div>

      <div class="container">
        <div class="row">
          <?php foreach ($category as $row): ?>
          <div class="offer__col col-lg-6">
            <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->kategorie_id; ?>" class="offer__img-cell">
              <h4 class="offer__title offer__title--sun"><?php echo $row->title; ?></h4>
              <img class="offer__img" src="<?php echo base_url(); ?>uploads/kategorie/<?php echo $row->photo2; ?>" alt="<?php echo $row->alt2; ?>">
            </a>
          </div>
          <?php endforeach ?>
        </div>
<!-- 
        <a href="<?php echo base_url(); ?>p/oferty" class="offer__btn">pełna oferta</a> -->
      </div>
    </section>

    <section class="about">
      <div class="container">
        <h3 class="about__header"><?php echo $about->title; ?></h3>
        <div class="row">
          <div class="col-lg-6 about__col">
            <img class="about__img" src="<?php echo base_url(); ?>uploads/o_nas/<?php echo $about->photo; ?>" alt="<?php echo $about->alt; ?>">
          </div>
          <div class="col-lg-6 about__col about__col--right">
            <p class="about__text">
              <?php echo $about->content; ?>
            </p>
          </div>
        </div>
      </div>
    </section>
  </main>
