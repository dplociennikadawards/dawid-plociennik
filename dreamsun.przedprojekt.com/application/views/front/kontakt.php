  <main>
  	<div class="container-fluid">
  		<div class="row">
	  		<div class="col-md-4 px-0">
	  			<iframe style="min-height: 450px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2499.9496746980194!2d16.148348315758728!3d51.20157897958608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470f12893f6d05a3%3A0x4a1741ccfd7bd18f!2sDREAM+SUN.+Salon+odnowy+biologicznej.+Solarium%2C+sauna%2C+fitness%2C+spa.+Legnica.!5e0!3m2!1spl!2spl!4v1560779505245!5m2!1spl!2spl" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
	  		</div>
	  		<div class="col-md-8">
	  			<!-- Section: Contact v.2 -->
				<section class="my-5">

				  <!-- Section heading -->
				  <h2 class="h1-responsive font-weight-bold text-center my-5">Skontaktuj się z nami</h2>
				  <!-- Section description -->
				  <p class="text-center w-responsive mx-auto mb-5">Masz jakieś pytania? Nie wahaj się skontaktować z nami bezpośrednio. Nasz zespół odpisze do Ciebie w ciągu kilku godzin, aby Ci pomóc.</p>

				  <!-- Grid row -->
				  <div class="row">
				  	<?php if(isset($_SESSION['success'])) { echo $_SESSION['success'];} ?>
				    <!-- Grid column -->
				    <div class="col-md-9 mb-md-0 mb-5">

				      <form method="post" action="<?php echo base_url() ?>mail/send">

				        <!-- Grid row -->
				        <div class="row">

				          <!-- Grid column -->
				          <div class="col-md-6">
				            <div class="md-form mb-0">
				              <input type="text" id="contact-name" class="form-control" name="firstname" required="">
				              <label for="contact-name" class="">Twoje imię</label>
				            </div>
				          </div>
				          <!-- Grid column -->

				          <!-- Grid column -->
				          <div class="col-md-6">
				            <div class="md-form mb-0">
				              <input type="email" id="contact-email" class="form-control" name="email" required="">
				              <label for="contact-email" class="">Twój adres e-mail</label>
				            </div>
				          </div>
				          <!-- Grid column -->

				        </div>
				        <!-- Grid row -->

				        <!-- Grid row -->
				        <div class="row">

				          <!-- Grid column -->
				          <div class="col-md-12">
				            <div class="md-form mb-0">
				              <input type="text" id="contact-Subject" class="form-control" name="subject" required="">
				              <label for="contact-Subject" class="">Temat</label>
				            </div>
				          </div>
				          <!-- Grid column -->

				        </div>
				        <!-- Grid row -->

				        <!-- Grid row -->
				        <div class="row">

				          <!-- Grid column -->
				          <div class="col-md-12">
				            <div class="md-form">
				              <textarea id="contact-message" class="form-control md-textarea" rows="3" name="message" required=""></textarea>
				              <label for="contact-message">Twoja wiadomość</label>
				            </div>
				          </div>
				          <!-- Grid column -->

				        </div>
				        <!-- Grid row -->

				        <!-- Grid row -->
				        <div class="row">

				          <!-- Grid column -->
				          <div class="col-md-12">
				            <div class="md-form mt-0">
				              <input type="checkbox" class="form-check-input" id="rodo1" name="rodo" value="1">
    						  <label class="form-check-label" for="rodo1">
    						  	<small>„Wyrażam zgodę na przetwarzanie danych osobowych podanych w formularzu. Podanie danych jest dobrowolne. Administratorem podanych przez Pana/ Panią danych osobowych jest Dream Sun z siedzibą w Złotoryjska 108/1a, Legnica, 59-220. Pana/Pani dane będą przetwarzane w celach związanych z udzieleniem odpowiedzi, przedstawieniem oferty usług Dream Sun oraz świadczeniem usług przez administratora danych. Przysługuje Panu/Pani prawo dostępu do treści swoich danych oraz ich poprawiania.”</small>
    						  </label>
				            </div>
				          </div>
				          <!-- Grid column -->

				        </div>
				        <!-- Grid row -->

				        <!-- Grid row -->
				        <div class="row">

				          <!-- Grid column -->
				          <div class="col-md-12">
				            <div class="md-form mt-0 mb-5">
				              <input type="checkbox" class="form-check-input" id="rodo2" name="rodo2" value="1">
    						  <label class="form-check-label" for="rodo2">
    						  	<small>„Wyrażam zgodę na otrzymywanie informacji handlowych od Dream Sun dotyczących jej oferty w szczególności poprzez połączenia telefoniczne lub sms z wykorzystaniem numeru telefonu podanego w formularzu, a także zgodę na przetwarzanie moich danych osobowych w tym celu przez Dream Sun oraz w celach promocji, reklamy i badania rynku.”</small>
    						  </label>
				            </div>
				          </div>
				          <!-- Grid column -->

				        </div>
				        <!-- Grid row -->
						<center>
							<div class="g-recaptcha" data-sitekey="6LeHN5gUAAAAALga6MyyDCjhTIDKq4opVcvHzcSM"></div>
							<span class="warning_captcha text-danger" class="text-danger"></span>
						</center>
					      <div class="text-center text-md-left mt-3">
					        <button type="submit" class="main-header__btn border" style="cursor: pointer;">Wyślij</button>
					      </div>
				      </form>

				    </div>
				    <!-- Grid column -->

				    <!-- Grid column -->
				    <div class="col-md-3 text-center">
				      <ul class="list-unstyled mb-0">
				        <li>
				          <i class="fas fa-map-marker-alt fa-2x tx-orange"></i>
				          <p><?php echo $contact->address; ?><br><?php echo $contact->city; ?></p>
				        </li>
				        <li>
				          <i class="fas fa-phone fa-2x mt-4 tx-orange"></i>
				          <p><?php echo $contact->phone; ?></p>
				        </li>
				        <li>
				          <i class="fas fa-envelope fa-2x mt-4 tx-orange"></i>
				          <p class="mb-0"><?php echo $contact->email; ?></p>
				        </li>
				      </ul>
				    </div>
				    <!-- Grid column -->

				  </div>
				  <!-- Grid row -->

				</section>
				<!-- Section: Contact v.2 -->
	  		</div>
	  	</div>
  	</div>
  </main>