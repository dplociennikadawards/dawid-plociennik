  <style type="text/css">
  	html {
  		scroll-behavior: smooth;
  	}
  	.backup {
  		position: fixed;
  		bottom: 5%;
  		right: 2%;
  		background-color: rgba(0,0,0, 0.3);
  		height: 50px;
  		width: 50px;
  		text-align: center;
  		line-height: 50px;
  		font-size: 1.5rem;
  		border-radius: 50%;
  		z-index: 2;
  		}
  </style>
  <main id="backUp">
  	<div class="backup">
  		<a href="#backUp" class="text-white"><i class="fas fa-chevron-up"></i></a>
  	</div>
  	<div class="container">
  		<!-- Section heading --><!-- 
		  	<h3 class="my-5">
		  		<div class="d-flex align-items-center">
		  			<img class="navbar__dropdown-icon" src="<?php echo base_url(); ?>uploads/kategorie/<?php echo $category_title->header_photo; ?>" alt="<?php echo $category_title->alt; ?>">
                    <h2 class="navbar__link-title">
                    	<?php echo $category_title->title; ?>
                    </h2>
                </div>
		  	</h3> -->
		  	<?php 
		  	$numItems = count($rows);
		  	$i = 0;
		  	?>
		  	<?php foreach ($rows as $row): ?>
                    <span>
                    	<a href="#<?php echo $this->slugify_m->slugify($oferta->title); ?>" class="text-dark"><?php echo $row->title; ?></a>
                    	<?php if(++$i != $numItems){ echo '<i class="fas fa-sun"></i>';} ?>
                    </span>
		  	<?php endforeach ?>
  		<!-- Section: Blog v.1 -->
		<section class="my-5">

		  <!-- Grid row -->
		  <div class="row">

		  	<?php if($oferta->photo != ''): ?>
		    <!-- Grid column -->
		    <div class="col-lg-7">

		      <!-- Featured image -->
		      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
		        <img class="img-fluid w-100" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $oferta->photo; ?>" alt="<?php echo $oferta->alt; ?>">
		        <a>
		          <div class="mask rgba-white-slight"></div>
		        </a>
		      </div>

		    </div>
		    <!-- Grid column -->
			<?php endif; ?>

		    <!-- Grid column -->
		    <div id="<?php echo $this->slugify_m->slugify($oferta->title); ?>" class="col-lg-5 <?php if($oferta->photo == ''){echo 'col-lg-12';} ?>">

		      <!-- Post title -->
		      <h3 class="font-weight-bold"><strong><?php echo $oferta->title; ?></strong></h3>
		      <p class="mb-3"><?php echo $oferta->subtitle; ?></p>
			  <span class="main-header__shape1 mb-4 w-100"></span>
		      <!-- Excerpt -->
		      <p><?php echo $oferta->content; ?>
			    </p>
		    </div>
		    <!-- Grid column -->

		    <!-- Grid column -->
		    <div class="col-lg-12 mt-5">
		      <!-- Excerpt -->
		      <p><?php echo $oferta->content2; ?>
			    </p>
		    </div>
		    <!-- Grid column -->
			   <div class="col-12 px-0 mb-5">

				<div class="owl-carousel owl-carousel-3">
					<?php foreach ($gallery as $gal): if($gal->item_id == $oferta->produkty_id):?>
						<a href="<?php echo base_url(); ?>uploads/gallery/<?php echo $gal->img; ?>" class="elem">
						 <div class="mx-3 recommended-offer" style="background-image: url('<?php echo base_url(); ?>uploads/gallery/<?php echo $gal->img; ?>')"> 

						 </div>
						</a>
					<?php endif; endforeach ?>
				</div>

			   </div>

		    
		  </div>
		  <!-- Grid row -->

		</section>

		<hr class="my-5">
		<!-- Section: Blog v.1 -->
  		<div class="col-12">
		    <!-- Grid column -->
				<h3 class="mt-5"><strong class="font-weight-bold">Polecane</strong> oferty</h3>
				<span class="main-header__shape1 mb-5 w-100"></span>

			    <!-- Grid column -->
			    <div class="col-12 px-0">

					<div class="owl-carousel owl-carousel-3">
						<?php foreach ($category as $row): ?>
						  <div class="mx-3 recommended-offer" style="background-image: url('<?php echo base_url(); ?>uploads/kategorie/<?php echo $row->header_photo; ?>')"> 
						  	<a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->kategorie_id; ?>" class="recommended-link text-center">
						  		<span class="m-auto font-weight-bold px-2"><?php echo $row->title; ?></span>
						  	</a>
						  </div>
						<?php endforeach ?>
					</div>

			    </div>
			    <!-- Grid column -->
			</div>
  	</div>

  </main>
