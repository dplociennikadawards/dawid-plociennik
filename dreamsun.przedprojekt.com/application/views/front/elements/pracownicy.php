<div class="owl-carousel owl-carousel-5">
	<div id="<?php echo $this->slugify_m->slugify($pracownik->title); ?>" class="item px-2" style="padding-top: 15px; padding-bottom: 1px;" onclick="choose_worker('<?php echo $this->slugify_m->slugify($pracownik->title); ?>', '<?php echo $pracownik->title ?>')">
		<img src="<?php echo base_url(); ?>uploads/pracownicy/<?php echo $pracownik->photo; ?>">
		<p class="text-center"><?php echo $pracownik->title ?></p>
	</div>
</div>