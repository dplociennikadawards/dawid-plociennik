<div id="step2" class="owl-carousel owl-carousel-4">
	<?php $free_dates = explode(",",$grafik->free_dates); 
	$week['Mon'] = 'Pon';
	$week['Tue'] = 'Wt';
	$week['Wed'] = 'Śr';
	$week['Thu'] = 'Czw';
	$week['Fri'] = 'Pt';
	$week['Sat'] = 'Sob';
	$week['Sun'] = 'Nd';
	$fullWeek['Mon'] = 'Poniedziałek';
	$fullWeek['Tue'] = 'Wtorek';
	$fullWeek['Wed'] = 'Środa';
	$fullWeek['Thu'] = 'Czwartek';
	$fullWeek['Fri'] = 'Piątek';
	$fullWeek['Sat'] = 'Sobota';
	$fullWeek['Sun'] = 'Niedziela';
	foreach ($free_dates as $date): ?>
	<div class="item" style="height: 120px;">
		<input type="checkbox" class="form-check-input" id="date1">
		<label class="form-check-label mt-4" for="date1" onclick="
			choose_date('<?php echo $week[date('D', strtotime($date))].date('dm', strtotime($date)); ?>', '<?php echo $fullWeek[date('D', strtotime($date))]; ?>, <?php echo date('d.m', strtotime($date)); ?>');
			loadNext2(<?php echo $this->uri->segment(4); ?>,'<?php echo $date; ?>');
			">
			<div id="<?php echo $week[date('D', strtotime($date))].date('dm', strtotime($date)); ?>" class="date_ball">
				<p class="centered_ball"><b><?php echo $week[date('D', strtotime($date))]; ?>.</b><br><?php echo date('d.m', strtotime($date)); ?></p>
			</div>
		</label>
	</div>
	<?php endforeach; ?>
</div>