<style type="text/css">
	.h-300px {
		height: 300px;
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}
</style>
  <main>

	<div class="container py-5">
		<h3 class="title text-center">
        	<span class="underscore">Urządzenia</span>
        </h3>
  		<div class="row">
  			<?php foreach ($rows as $row): ?>
  			<a href="<?php echo base_url(); ?>uploads/gallery/<?php echo $row->img; ?>" class="elem col-md-4 " data-lcl-thumb="<?php echo base_url(); ?>uploads/gallery/<?php echo $row->img; ?>">
		    <div class="mb-3 h-300px" style="background-image: url('<?php echo base_url(); ?>uploads/gallery/<?php echo $row->img; ?>');">
		        
		    </div>
		        </a>
  			<?php endforeach ?>
		</div>
	</div>

  </main>