  <main>
	<section class="about">
      <div class="container">
        <h3 class="title text-center">
        	<span class="underscore"><?php echo $about->title; ?></span>
        </h3>
        <div class="row">
          <div class="col-lg-6 about__col">
            <img class="about__img" src="<?php echo base_url(); ?>uploads/o_nas/<?php echo $about->photo; ?>" alt="<?php echo $about->alt; ?>">
          </div>
          <div class="col-lg-6 about__col about__col--right">
            <p class="about__text">
              <?php echo $about->content; ?>
            </p>
          </div>
        </div>
      </div>
    </section>

<?php $i=0; foreach ($rows as $row): if($row->o_nas_id > 1):  $i++; ?>
    <section class="about bg-about" <?php if($i==1):?>style="background-image: url('<?php echo base_url(); ?>assets/front/img/hero-white.jpg');"<?php endif; ?>>
      <div class="container">
        <h3 class="title <?php if($row->photo == ''){echo 'text-left';}else {echo 'text-center';} ?>">
        	<span class="underscore"><?php echo $row->title; ?></span>
        </h3>
        <div class="row">
          <?php if($row->photo == ''): ?>
          <div class="col-lg-12 about__col about__col--right">
            <p class="about__text">
              <?php echo $row->content; ?>
            </p>
          </div>
          <?php else: ?>
            <div class="col-lg-6 about__col">
              <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                  <img class="img-fluid" src="<?php echo base_url(); ?>uploads/o_nas/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
            </div>
            <div class="col-lg-6 about__col about__col--right">
              <p class="about__text">
                <?php echo $row->content; ?>
              </p>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </section>
<?php endif; endforeach; ?>


    <section class="about">
      <div class="container">        
      	<h3 class="title text-left">
        	<span class="underscore">ODWIEDŹ</span>
        </h3>
        <div class="row">
          <?php foreach ($category as $cat): ?>
          <div class="col-md-6 mb-3">
          	<div class="d-flex align-items-center">
              <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($cat->title) . '/' . $cat->kategorie_id; ?>" class="text-dark">
          		<img class="navbar__dropdown-icon" src="<?php echo base_url(); ?>uploads/kategorie/<?php echo $cat->header_photo; ?>" alt="<?php echo $cat->alt; ?>">
                <h2 class="navbar__link-title"><?php echo $cat->title; ?></h2>
              </a>
            </div>
          </div>
          <?php endforeach ?>
        </div>
      </div>
    </section>

  </main>
