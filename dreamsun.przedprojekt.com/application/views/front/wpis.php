  <main>

  	<div class="container">
  		<!-- Section: Blog v.1 -->
		<section class="my-5">

		  <!-- Section heading -->
		  	<h3 class="mb-5">
		  		<div class="d-flex align-items-center">
		  			<img class="navbar__dropdown-icon brightness-dark" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $row->header_photo; ?>" alt="<?php echo $row->header_alt; ?>" width="100">
                    <h2 class="navbar__link-title"><?php echo $row->title; ?></h2>
                </div>
		  	</h3>

		  <!-- Grid row -->
		  <div class="row">

		    <!-- Grid column -->
		    <div class="col-lg-7">

		      <!-- Featured image -->
		      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
		        <img class="img-fluid m-auto" src="<?php echo base_url(); ?>uploads/produkty/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
		        <a>
		          <div class="mask rgba-white-slight"></div>
		        </a>
		      </div>

		    </div>
		    <!-- Grid column -->

		    <!-- Grid column -->
		    <div class="col-lg-5">

		      <!-- Post title -->
		      <h3 class="font-weight-bold mb-3"><strong><?php echo $row->subtitle; ?></strong></h3>
			  <span class="main-header__shape1 mb-4 w-100"></span>
		      <!-- Excerpt -->
		      <p><?php echo $row->content; ?></p>
		    </div>
		    <!-- Grid column -->

		    <!-- Grid column -->
		    <div class="col-lg-12 my-5">
		      <!-- Excerpt -->
		      <p><?php echo $row->content2; ?></p>
		    </div>
		    <!-- Grid column -->

		    <div class="col-12 px-0">

				<div class="owl-carousel owl-carousel-2">
				  <?php foreach ($gallery as $gal): ?>
						<a href="<?php echo base_url(); ?>uploads/gallery/<?php echo $gal->img; ?>" class="elem">
						 <div class="mx-3 recommended-offer" style="background-image: url('<?php echo base_url(); ?>uploads/gallery/<?php echo $gal->img; ?>')"> 

						 </div>
						</a>
					<?php endforeach ?>
				</div>

			</div>
		    <div class="col-12">
		    <!-- Grid column -->
				<h3 class="mt-5"><strong class="font-weight-bold">Polecane</strong> produkty</h3>
				<span class="main-header__shape1 mb-5 w-100"></span>

			    <!-- Grid column -->
			    <div class="col-12 px-0">

					<div class="owl-carousel owl-carousel-2">
					  <?php foreach ($product as $row): ?>
						  <div class="mx-3 recommended-offer" style="background-image: url('<?php echo base_url(); ?>uploads/produkty/<?php echo $row->header_photo; ?>')"> 
						  	<a href="<?php echo base_url(); ?>p/produkty/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->produkty_id; ?>" class="recommended-link text-center">
						  		<span class="m-auto font-weight-bold px-2"><?php echo $row->title; ?></span>
						  	</a>
						  </div>
						<?php endforeach ?>
					</div>

			    </div>
			    <!-- Grid column -->
			</div>
		  </div>
		  <!-- Grid row -->

		</section>
		<!-- Section: Blog v.1 -->
  	</div>

  </main>
