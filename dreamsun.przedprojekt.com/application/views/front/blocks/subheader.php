  <!-- Start your project here-->
  <header class="main-header">

    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container h-100">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
          <img class="navbar__logo" src="<?php echo base_url(); ?>uploads/settings/<?php echo $settings->logo; ?>" width="213" alt="<?php echo $settings->meta_title; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/o_nas">O nas</a>
            </li>
            <li class="nav-item navbar__dropdown">
              <a class="nav-link">oferta <i class="navbar__arrow-icon fas fa-sort-down"></i></a>
              <div class="navbar__dropdown-menu">
                <?php $i=0; foreach ($category as $row): $i++; ?>
                  <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->kategorie_id; ?>" class="navbar__dropdown-link navbar__dropdown-link--gray" style="background-image: url(<?php echo base_url(); ?>uploads/kategorie/<?php echo $row->photo; ?>)">
                    <div class="d-flex align-items-center">
                      <img class="navbar__dropdown-icon" src="<?php echo base_url(); ?>uploads/kategorie/<?php echo $row->header_photo; ?>" alt="<?php echo $row->alt; ?>">
                      <h2 class="navbar__link-title"><?php echo $row->title; ?></h2>
                    </div>
                  </a>           
                <?php endforeach; ?>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/produkty">Produkty</a>
            </li><!-- 
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/grafik">grafik</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/aktualnosci">news</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/urzadzenia">urządzenia</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
            </li>
          </ul>
        </div>

        <div class="navbar__phone-box">
          <a href="tel:<?php echo $contact->phone; ?>" class="navbar__phone-link"><img class="navbar__phone-icon" src="<?php echo base_url(); ?>assets/front/img/phone.png"
              alt="ikona telefonu"> <?php echo $contact->phone; ?></a>
        </div>
      </div>
    </nav>


    <div class="main-header__slider">


      <!--Carousel Wrapper-->
      <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
          <!--First slide-->
          <div class="carousel-item active">
            <div class="main-header__background">
              <div class="container d-flex align-items-center h-100">
                <div class="main-header__box-content">
                  <img class="main-header__image" src="<?php echo base_url(); ?>uploads/podstrony/<?php echo $podstrona->photo; ?>" alt="<?php echo $podstrona->alt2; ?>">
                  <h2 class="main-header__title"><?php echo $podstrona->title; ?></h2>
                  <h4 class="main-header__subtitle"><span class="main-header__shape1"></span><?php echo $podstrona->subtitle; ?></h4>
                  <p class="main-header__text"><?php echo $podstrona->content; ?></p>
                </div>
              </div>
            </div>
          </div>
          <!--/First slide-->
        </div>
        <!--/.Slides-->
      </div>
      <!--/.Carousel Wrapper-->
    </div>

  </header>