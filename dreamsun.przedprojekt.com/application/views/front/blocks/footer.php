  <footer class="main-footer">
    <div class="container">
      <div class="main-footer__row row">
        <div class="main-footer__col col-md-3 col-lg-3">
          <img class="main-footer__logo" src="<?php echo base_url(); ?>uploads/settings/<?php echo $settings->logo; ?>" alt="<?php echo $settings->meta_title; ?>" width="213">
        </div>
        <div class="main-footer__col col-md-3 col-lg-3">
          <h5 class="main-footer__title">oferta</h5>
          <?php foreach ($category as $row): ?>
            <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->kategorie_id; ?>"><?php echo $row->title; ?></a></div>
          <?php endforeach ?>
        </div>
        <div class="main-footer__col col-md-3 col-lg-3">
          <h5 class="main-footer__title">na skróty</h5>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/o_nas">O NAS</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/produkty">PRODUKTY</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/aktualnosci">NEWS</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/urzadzenia">URZĄDZENIA</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/kontakt">KONTAKT</a></div>
          <h5 class="main-footer__title">Karty</h5>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/karty#dreamsun">Dream Sun</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/karty#vip">VIP</a></div>
          <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/karty#pracownicy">Dla pracowników</a></div>
        </div>
        <div class="main-footer__col col-md-3 col-lg-3">
          <h5 class="main-footer__title">produkty</h5>
          <?php foreach ($product as $row): ?>
            <div><a class="main-footer__link" href="<?php echo base_url(); ?>p/usluga/<?php echo $this->slugify_m->slugify($row->title) . '/' . $row->produkty_id; ?>"><?php echo $row->title; ?></a></div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
    <div class="main-footer__copyrights-bar">
      <div class="container d-flex justify-content-between align-items-center">
        <span>Copyright 2019. Dream Sun | <a href="<?php echo base_url(); ?>uploads/settings/<?php echo $settings->pdf; ?>" target="_blank">Polityka prywatności</a></span>
        <span>Created by <a href="https://agencjamedialna.pro/" target="_blank">AD Awards</a></span>
      </div>
    </div>
  </footer>
  <!-- /Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
  <script type="text/javascript">new WOW().init();</script>
  <script src="<?php echo base_url(); ?>assets/front/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/owl-init.js"></script>
  <!-- Stepper JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/addons-pro/stepper.js"></script>
  <!-- Stepper JavaScript - minified -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/addons-pro/stepper.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(e) {
     
    // live handler
    lc_lightbox('.elem', {
      wrap_class: 'lcl_fade_oc',
      gallery : true, 
      thumb_attr: 'data-lcl-thumb', 
      
      skin: 'minimal',
      radius: 0,
      padding : 0,
      border_w: 0,
    }); 

  });
  </script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#ffb610",
        "text": "#fff"
      },
      "button": {
        "background": "#000",
        "text": "#fff"
      }
    },
    "type": "opt-out",
    "content": {
      "message": "<?php echo $settings->cookies; ?>",
      "dismiss": "Rozumiem",
      "deny": "",
      "allow": "Rozumiem",
      "link": "Czytaj więcej...",
      "href": "<?php echo base_url(); ?>uploads/settings/<?php echo $settings->pdf; ?>"
    }
  })});
  </script>
  <script type="text/javascript">
      function validateMyForm(nr)
      {
        if (grecaptcha.getResponse(nr) == ""){
            document.getElementsByClassName('warning_captcha')[nr].innerHTML = 'Potwierdź captche';
            return false;
        } else {
            return true;
        }
      }
    </script>
    <script type="text/javascript">
      $(document).ready(function () {
      $('.stepper').mdbStepper();
      })

      function someFunction21() {
      setTimeout(function () {
      $('#horizontal-stepper').nextStep();
      }, 2000);
      }
    </script>
</body>

</html>