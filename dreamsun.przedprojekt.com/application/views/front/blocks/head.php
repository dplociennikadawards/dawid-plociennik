<?php 
if($this->uri->segment(2) == 'o_nas') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'oferty') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'produkty') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'grafik') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'urzadzenia') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'kontakt') {
$first = $podstrona->title . ' - ';
$second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'oferta') {
  
    $first = $category_title->title . ' - ';
  $second = $settings->meta_title;
}
elseif($this->uri->segment(2) == 'wpis') {
$first = $row->title . ' - ';
$second = $settings->meta_title;
}
else {
$first = $settings->meta_title; 
$second = ' - Salon kosmetyczny';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php echo $settings->description; ?>">
  <meta name="keywords" content="<?php echo $settings->meta_title; ?>">
  <meta name="author" content="Dawid Płóciennik">
  <title><?php echo $first . ' ' . $second; ?></title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,700&display=swap" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/css/mediaqueries.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/owlcarousel/owl.theme.default.min.css">


  <script src="<?php echo base_url(); ?>assets/front/lib/jquery.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/front/js/lc_lightbox.lite.js" type="text/javascript"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/lc_lightbox.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/skins/minimal.css" />
  <script src="<?php echo base_url(); ?>assets/front/lib/AlloyFinger/alloy_finger.min.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <!-- Stepper CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/addons-pro/steppers.css" rel="stylesheet">
  <!-- Stepper CSS - minified-->
  <link href="<?php echo base_url(); ?>assets/front/css/addons-pro/steppers.min.css" rel="stylesheet">
</head>

<body>