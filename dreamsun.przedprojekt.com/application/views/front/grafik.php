<style type="text/css">
	.form-check-input[type="checkbox"] + label::before, .form-check-input[type="checkbox"]:not(.filled-in) + label::after, label.btn input[type="checkbox"] + label::before, label.btn input[type="checkbox"]:not(.filled-in) + label::after {
		display: none;
	}
	.list-group-item {
		height: 100px;
	}
</style>
<main>
	<div id="all_success" class="container">
		<ul class="stepper horizontal" id="horizontal-stepper">
		  <li id="step1" class="step active">
		    <div class="step-title waves-effect waves-dark">Wybierz zabieg</div>
		    <div class="step-new-content">
		      <div class="row">
				<div class="list-group-flush">
					<?php foreach ($grafik as $row): 
						$title = $this->base_m->get_record('produkty', $row->oferta_id)->title;
						$time = $this->base_m->get_record('produkty', $row->oferta_id)->subtitle;
						$slug = $this->slugify_m->slugify($this->base_m->get_record('produkty', $row->oferta_id)->title);
						$icon = $this->base_m->get_record('produkty', $row->oferta_id)->header_photo;
						?>
				  	<div id="<?php echo $slug; ?>" class="list-group-item cursor">
					    <input type="checkbox" class="form-check-input" id="<?php echo $slug; ?>1">
					    <label class="form-check-label cursor" for="<?php echo $slug; ?>1" onclick="
					    	choose('<?php echo $slug; ?>', '<?php echo $title; ?>');
					    	loadNext(<?php echo $row->oferta_id; ?>);
					    	">
					    	<div class="row">
					    		<div class="col-4 align-self-center">
					    			<img src="<?php echo base_url(); ?>uploads/produkty/<?php echo $icon; ?>" class="mr-4" width="70">
					    		</div>
					    		<div class="col-8 align-self-center">
					    			<?php echo $title; ?> - <small class="text-muted"><?php echo $time ?> min</small><br>
					    			<small><a href="<?php echo base_url(); ?>p/wpis/<?php echo $slug . '/' . $row->oferta_id; ?>" class="px-0">szczegóły</a></small>
					    		</div>
					    	</div>
					    </label>
				  	</div>
					<?php endforeach; ?>
		      </div>
		    </div>
		      <div class="step-actions">
		      	<input type="checkbox" id="valid_1" name="valid_1" title="Wybierz jeden z zabiegów, aby przejść dalej" required>
		        <button class="waves-effect waves-dark btn btn-sm bg-orange next-step" data-feedback="someFunction21">Dalej</button>
		      </div>
		  </li>


		  <li class="step">
		    <div class="step-title waves-effect waves-dark">Wybierz termin</div>
		    <div class="step-new-content">
		      <div class="row">
		      	<div class="col-12"><div id="treatment">Proszę wybrać zabieg</div></div>
					<div id="step2" class="col-12 align-self-center">
			      	</div>
			  </div>
		      <div class="step-actions">
		      	<input type="checkbox" id="valid_2" name="valid_2" title="Wybierz date zabiegu" required>
		        <button class="waves-effect waves-dark btn btn-sm bg-orange next-step" data-feedback="someFunction21">Dalej</button>
		        <button class="waves-effect waves-dark btn btn-sm btn-dark previous-step">Powrót</button>
		      </div>
		    </div>
		  </li>


		  <li class="step">
		    <div class="step-title waves-effect waves-dark">Wybierz pracownika</div>
		    <div class="step-new-content">
		      <div class="row">
		      	<div class="col-12"><div id="treatment"></div></div>
					<div id="step3" class="col-12 align-self-center">
			      		
			      	</div>
		      <div class="step-actions">
		      	<input type="checkbox" id="valid_3" name="valid_3" title="Proszę wybrać pracownika, który ma wykonać zabieg" required>
		        <button class="waves-effect waves-dark btn btn-sm bg-orange next-step" data-feedback="someFunction21">Dalej</button>
		        <button class="waves-effect waves-dark btn btn-sm btn-dark previous-step">Powrót</button>
		      </div>
		    </div>
		  </li>

		  <li class="step">
		    <div class="step-title waves-effect waves-dark">Podsumowanie</div>
		    <div class="step-new-content">
		      	<div class="row">
		      		<div class="col-12" id="zabieg">Nic nie wybrałeś</div>
		      		<div class="col-12" id="termin">Nic nie wybrałeś</div>
		      		<div class="col-12" id="pracownik">Nic nie wybrałeś</div>
		      		<div class="col-12 my-4">Na adres e-mail zostanie wysłana wiadomość potwierdzająca wizytę<br>oraz link aktywujący rezerwację.</div>
		      		<div class="col-md-6">
		      			<div class="md-form">
						  <input type="text" id="name" class="form-control" required>
						  <label for="name">Wprowadź swoje imię i nazwisko <span class="text-danger">*</span></label>
						</div>
		      			<div class="md-form">
						  <input type="email" id="email" class="form-control" required>
						  <label for="email">Wprowadź swój adres e-mail <span class="text-danger">*</span></label>
						</div>
		      			<div class="md-form">
						  <input type="tel" id="tel" class="form-control" required>
						  <label for="tel">Wprowadź swój numer telefonu <span class="text-danger">*</span></label>
						</div>
		      		</div>
			  	</div>
						<div class="g-recaptcha" data-sitekey="6LeHN5gUAAAAALga6MyyDCjhTIDKq4opVcvHzcSM"></div>
						<span id="valid_4" class="warning_captcha text-danger" class="text-danger"></span>
		      	<div class="step-actions">
		        	<button onclick="send_reserv()" class="waves-effect waves-dark btn-sm btn bg-orange m-0 mt-4" type="button">Zarezewuj wizytę</button>
		      	</div>
		    </div>
		  </li>
		</ul>
	</div>

	<input type="hidden" id="reservation" name="" required>
	<input type="hidden" id="reservation_date" name="" required>
	<input type="hidden" id="reservation_worker" name="" required>

</main>

<script type="text/javascript">
	document.getElementById('valid_1').checked = false;
  	document.getElementById('valid_2').checked = false;
  	document.getElementById('valid_3').checked = false;
	document.getElementById('reservation').value = '';
  	document.getElementById('reservation_date').value = '';
  	document.getElementById('reservation_worker').value = '';
	document.getElementById('name').value = '';
  	document.getElementById('email').value = '';
  	document.getElementById('tel').value = '';
	function choose(slug, name) {
		$(".list-group-flush div").removeClass("picked");
 		var toggle = document.getElementById(slug);
  		toggle.classList.toggle("picked");
  		document.getElementById('reservation').value = name;
  		document.getElementById('valid_1').checked = true;
  		document.getElementById('treatment').innerHTML = 'Wolne terminy na <strong>' + name + '</strong>';
  		document.getElementById('zabieg').innerHTML = 'Zabieg: <strong>' + name + '</strong>';
	}
	function choose_date(slug, name) {
		$(".date_ball").removeClass("picked_ball");
 		var toggle = document.getElementById(slug);
  		toggle.classList.toggle("picked_ball");
  		document.getElementById('reservation_date').value = name;
  		document.getElementById('valid_2').checked = true;
  		document.getElementById('termin').innerHTML = 'Termin: <strong>' + name + '</strong>';
	}
	function choose_worker(slug, name) {
		$(".owl-carousel div").removeClass("picked");
 		var toggle = document.getElementById(slug);
  		toggle.classList.toggle("picked");
  		document.getElementById('reservation_worker').value = name;
  		document.getElementById('valid_3').checked = true;
  		document.getElementById('pracownik').innerHTML = 'Pracownik: <strong>' + name + '</strong>';
	}
	function send_reserv() {
		var reservation = document.getElementById('reservation').value;
		var reservation_date = document.getElementById('reservation_date').value;
		var reservation_worker = document.getElementById('reservation_worker').value;
		var name = document.getElementById('name').value;
		var email = document.getElementById('email').value;
		var tel = document.getElementById('tel').value;

		console.log(reservation + '<br>' + reservation_date + '<br>' + reservation_worker + '<br>' + name + '<br>' + email + '<br>' + tel + '<br>');

		if(reservation != '' && reservation_date != '' && reservation_worker != '' && name != '' && email != '' && tel != '') {

			$.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>reservation",
                data: {reservation:reservation, reservation_date:reservation_date, reservation_worker:reservation_worker, name:name, email:email, tel:tel},
                cache: false,
                beforeSend:function(data) {
                    console.log(data);
                    console.log('<?php echo base_url(); ?>reservation/index');
					document.getElementById('all_success').innerHTML = '<div class="info_page"><i class="fas fa-circle-notch fa-spin big-tx"></i><br>Wysyłanie formularza</div>';
                },
                complete:function(html) {
					document.getElementById('all_success').innerHTML = '<div class="info_page"><i class="fas fa-check-circle big-tx"></i><br>Formularz został wysłany pomyślnie!<br>Proszę zatwierdź swoją wizytę na e-mailu</div>'; 
                }
            });
		} else {
			document.getElementById('valid_4').innerHTML = 'Formularz nie został poprawnie wypełniony.';
		}
		
	}

	function loadNext(id) {
		$("#step2").load("<?php echo base_url() ?>elements/loadelements/terminy/"+id); 
		setTimeout(function(){ initSlider4(); }, 500);
	}
	function loadNext2(zabieg_id, date) {
		$("#step3").load("<?php echo base_url() ?>elements/loadelements/pracownicy/"+zabieg_id+"/"+date); 
		setTimeout(function(){ initSlider5(); }, 500);
	}
    function initSlider4() {
        $(".owl-carousel-4").owlCarousel({
            items: 10,
            loop: false,
            autoplay: false,
            nav: false,
            responsive:{
                0:{
                    items:2
                },
                540:{
                    items:3
                },
                767:{
                    items:6
                },
                992:{
                    items:10
                }

            }
        });
        

    }
    function initSlider5() {
        $(".owl-carousel-5").owlCarousel({
            items: 10,
            loop: false,
            autoplay: false,
            nav: false,
            responsive:{
                0:{
                    items:2
                },
                540:{
                    items:3
                },
                767:{
                    items:6
                },
                992:{
                    items:10
                }
            }
        });
    }
</script>
