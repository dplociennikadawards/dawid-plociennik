<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loadelements extends CI_Controller {

	public function terminy($zabieg_id) {
		$data['grafik'] = $this->base_m->get_terminy($zabieg_id);
		$this->load->view('front/elements/terminy.php',$data);
	}
	
	public function pracownicy($zabieg_id, $date) {
		$data['pracownicy'] = $this->base_m->get_pracownicy($zabieg_id, $date);
		$data['pracownik'] = $this->base_m->get_record('pracownicy', $data['pracownicy']->pracownicy_id);
		$this->load->view('front/elements/pracownicy.php',$data);
	}
}
