<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {


    public function index()
    {

        $this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim|is_unique[users.login]');
        $this->form_validation->set_rules('email', 'E-mail', 'valid_email|trim|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[1]');
        $this->form_validation->set_rules('confirm_password', 'Hasło', 'min_length[1]|trim|matches[password]');

        $this->form_validation->set_message('valid_email', 'Niepoprawny E-Mail');
        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('matches', 'Hasła róznią się od siebie');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');

        if($this->form_validation->run() == FALSE) 
        {
            $info = '<p class="text-danger font-weight-bold">'.validation_errors().'</p>';
            echo $this->session->set_flashdata('failed', $info);

            redirect('/');

        } else {

            $options = ['cost' => 12];
            $encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
            $unique_hash = md5($this->input->post('login'));

            $insert['login'] = strtolower($this->input->post('login'));
            $insert['email'] = strtolower($this->input->post('email'));
            $insert['password'] = $encripted_pass;
            $insert['first_name'] = $this->input->post('first_name');
            $insert['unique_hash'] = $unique_hash;
            $insert['rola'] = 'user';

            $this->load->library('email');          

            $config = array(
                'smtp_host' =>"smtp.adawards.pl",
                'smtp_user' =>"no-reply-car@adawards.pl",
                'smtp_pass' =>"8lllRuWL4H",
                'smtp_port' =>"587",
                'smtp_timeout' =>"30",
                'mailtype'  =>"html",
            );

            $this->email->initialize($config);


            $this->email->from('no-reply-car@adawards.pl', 'TMOD');

            $this->email->to($insert['email']);

            $this->email->subject('Twoje konto zostało zarejestrowane! - TMOD');

            $this->email->message('Twoje konto zostało pomyślnie zarejestrowane.<br> 
                                    Prosimy o aktywacje konta klikająć w poniższy link.<br>
                                    <a href="'.base_url().'register/active_account/'.$unique_hash.'">'.base_url().'register/active_account/'.$unique_hash.'</a>');

            if ($this->email->send())
            {
                $this->base_m->insert('users', $insert);
                $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Twoje konto zostało zarejestrowane! Prosimy o aktywacje konta, szczegóły wysłaliśmy w wiadomości e-mail.</p>');
                redirect('/');
            }

                    
        }

    }

    public function active_account($unique_hash) {
        $update['active'] = 1;
        $this->user_m->set_active('users', $update, $unique_hash);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Twoje konto zostało aktywowane. Teraz możesz zalogować się do serwisu.</p>');
        redirect('/');
    }



    public function reset_password() {

            function randomPassword() {
                $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 8; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
                $pass[] = 1;
                return implode($pass); //turn the array into a string
            }

            $new_password = randomPassword();
            $email = $this->input->post('email');
            $data['row'] = $this->user_m->get_email('users', $email);
            $unique_hash = $data['row']->unique_hash;
            $this->load->library('email');          

            $config = array(
                'smtp_host' =>"smtp.adawards.pl",
                'smtp_user' =>"no-reply-car@adawards.pl",
                'smtp_pass' =>"8lllRuWL4H",
                'smtp_port' =>"587",
                'smtp_timeout' =>"30",
                'mailtype'  =>"html",
            );

            $this->email->initialize($config);


            $this->email->from('no-reply-car@adawards.pl', 'TMOD');

            $this->email->to($email);

            $this->email->subject('Twoje nowe hasło! - TMOD');

            $this->email->message('Twoje nowe hasło to:<br><strong>'.$new_password.'</strong><br>
                                    Potwierdź swoje nowe hasło klikając w ten link:<br>
                                    <a href="'.base_url().'register/new_password/'.$unique_hash.'/'.$new_password.'">'.base_url().'register/new_password/'.$unique_hash.'/'.$new_password.'</a>');

            if ($this->email->send())
            {
                $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wysłaliśmy na Twój adres nowe hasło. Potwierdź swoje nowe hasło klikając na link w wiadomości</p>');
                redirect('/');
            }
    }

    public function new_password($unique_hash,$encripted_pass) {
        $options = ['cost' => 12];
        $encripted_pass = password_hash($encripted_pass, PASSWORD_BCRYPT, $options);
        $update['password'] = $encripted_pass;
        $this->user_m->set_active('users', $update, $unique_hash);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Nowe hasło zostało zatwierdzone. Teraz możesz sie zalogować do serwisu.</p>');
        redirect('/');
    }

}

?>