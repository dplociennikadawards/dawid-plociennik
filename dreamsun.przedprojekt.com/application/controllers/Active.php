<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Active extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function reservation() {
    $liame = $this->uri->segment(3);
      $data['active'] = 1;
        $this->base_m->active_reserv('rezerwacje', $liame, $data);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Twoja wizyta została zweryfikowana. Dziękujemy za skorzystanie z naszych usług.</p>');
            redirect('p');
    }

}







	