<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {

     public function change() {

        $this->form_validation->set_rules('email', 'E-mail', 'valid_email|trim|is_unique[users.email]');

        $this->form_validation->set_message('valid_email', 'Niepoprawny E-Mail');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        if($this->form_validation->run() == FALSE) 
        {

            $this->session->set_flashdata('failed', '<p class="text-danger font-weight-bold">Podany email już jest w użyciu!</p>');

            redirect('/');

        }else{

            $update['first_name'] = $this->input->post('first_name');
            $update['email'] = $this->input->post('email');
            $unique_hash = $_SESSION['unique_hash'];
            $this->user_m->set_active('users', $update, $unique_hash);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Dane zostały zaaktualizowane!</p>');
            $_SESSION['first_name'] = $this->input->post('first_name');
            $_SESSION['email'] = $this->input->post('email');

            $this->base_m->log('zmiana danych');
            redirect('/');

            
        }
    }


    public function change_password() {


        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[1]');
        $this->form_validation->set_rules('confirm_password', 'Hasło', 'min_length[1]|trim|matches[password]');

         if($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('failed', '<p class="text-danger font-weight-bold">Hasła się rożnią!</p>');

            redirect('/');

         }else{
            $old_password = $this->input->post('password_old');
            $new_password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');

            $data['row'] = $this->user_m->get_email('users', $_SESSION['email']);

                if(password_verify($old_password, $data['row']->password)){
                    $options = ['cost' => 12];
                    $encripted_pass = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
                    $update['password'] = $encripted_pass;

                    $this->user_m->set_active('users', $update, $_SESSION['unique_hash']);
                    $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Zmieniłeś hasło!</p>');

                    $this->base_m->log('zmiana hasła');
                    redirect('/');
                }else{

                    $this->session->set_flashdata('failed', '<p class="text-danger font-weight-bold">To nie jest twoje stare hasło! </p>');

                    $this->base_m->log('nieudana próba zmiany hasła');
                    redirect('/');

                }

         }

        
    }

  }

?>