<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->get('slider');
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/slider/index', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}
	public function dodaj() {
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|is_unique[slider.photo]|required');
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|required');
		$this->form_validation->set_rules('subtitle', 'podtytuł', 'min_length[2]|trim|required');
		$this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/slider/dodaj');
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/slider/'.$now)) {
		    	mkdir('./uploads/slider/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/slider/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
             if (!$this->upload->do_upload('photo')) {
				$this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
				redirect('panel/slider/dodaj');            
            } else {
	            $data = $this->upload->data();
            	$insert['photo'] = $now.'/'.$data['file_name'];
	            if (!$this->upload->do_upload('header_photo')) {
					$this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
					redirect('panel/slider/dodaj');            
	            } else {
	            $data = $this->upload->data();
				$insert['header_photo'] = $now.'/'.$data['file_name'];

				$insert['title'] = $this->input->post('title');
				$insert['subtitle'] = $this->input->post('subtitle');
				$insert['content'] = $this->input->post('content');
				$insert['alt'] = $this->input->post('alt');
				$insert['alt1'] = $this->input->post('alt1');
				$insert['button'] = $this->input->post('button');
				$insert['button_link'] = $this->input->post('button_link');

				$this->base_m->insert('slider', $insert);
				$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
				redirect('panel/slider');
            }
		}
	}
}
	public function edytuj($id) {
		$data['row'] = $this->base_m->get_record('slider', $id);
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/slider/edytuj', $data);
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/slider/'.$now)) {
		    	mkdir('./uploads/slider/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/slider/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $update['photo'] = $now.'/'.$data['file_name'];        
                }
            }

            if($this->input->post('name_header_photo') != null) {
                if ($this->upload->do_upload('header_photo')) {
                    $data = $this->upload->data();
                    $update['header_photo'] = $now.'/'.$data['file_name'];         
                }
            }
    		
			$update['title'] = $this->input->post('title');
			$update['subtitle'] = $this->input->post('subtitle');
			$update['content'] = $this->input->post('content');
			$update['alt'] = $this->input->post('alt');
			$update['alt1'] = $this->input->post('alt1');
			$update['button'] = $this->input->post('button');
			$update['button_link'] = $this->input->post('button_link');


			$this->base_m->update('slider', $update, $id);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
			redirect('panel/slider');
		}
	}
}


