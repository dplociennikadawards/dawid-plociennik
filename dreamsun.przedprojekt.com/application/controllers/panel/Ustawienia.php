<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia extends CI_Controller {
	public function index() {
		$data['row'] = $this->base_m->get_record('ustawienia', 1);
		$this->form_validation->set_rules('meta_title', 'Tytuł strony', 'trim|required');
		$this->form_validation->set_rules('name_header_photo', 'Logo', 'trim|required');
		$this->form_validation->set_rules('content', 'Wiadomości cookies', 'trim|required');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/ustawienia/index', $data);
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/settings/'.$now)) {
		    	mkdir('./uploads/settings/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/settings/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
    		if($this->input->post('name_photo') != null) {
	            if ($this->upload->do_upload('photo')) {
		            $data = $this->upload->data();
	            	$update['logo'] = $now.'/'.$data['file_name'];        
	            }
    		}

    		if($this->input->post('name_header_photo') != null) {
		        if ($this->upload->do_upload('header_photo')) {
		            $data = $this->upload->data();
	            	$update['icon'] = $now.'/'.$data['file_name'];         
		        }
			}

    		if($this->input->post('name_pdf') != null) {
		        if ($this->upload->do_upload('pdf')) {
		            $data = $this->upload->data();
	            	$update['pdf'] = $now.'/'.$data['file_name'];         
		        }
			}

			$update['meta_title'] = $this->input->post('meta_title');
			$update['cookies'] = $this->input->post('content');
			$update['fb_link'] = $this->input->post('fb_link');
			$update['inst_link'] = $this->input->post('inst_link');
			$update['yt_link'] = $this->input->post('yt_link');
			if($this->input->post('description') != null) {
                $update['description'] = $this->input->post('description');
                $update['description'] = str_replace('<p>', '', $update['description']);
                $update['description'] = str_replace('</p>', '<br>', $update['description']);
                $update['description'] = str_replace('<div>', '', $update['description']);
                $update['description'] = str_replace('</div>', '', $update['description']);
            }
			

			$this->base_m->update('ustawienia', $update, 1);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Ustawienia zostały zaktualizowane!</p>');
			redirect('panel/ustawienia');
		}
	}
}
