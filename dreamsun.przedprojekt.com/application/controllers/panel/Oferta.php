<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Oferta extends CI_Controller {

public function index() {
    $data['rows'] = $this->base_m->get('oferta');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/oferta/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $data['kate'] = $this->base_m->get('kategorie');
    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/oferta/dodaj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/oferta/'.$now)) {
            mkdir('./uploads/oferta/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/oferta/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }
            }

            if($this->input->post('header_name_photo') != null) {
                if ($this->upload->do_upload('header_photo')) {
                    $data = $this->upload->data();
                    $insert['header_photo'] = $now.'/'.$data['file_name'];        
                }
            }

            $insert['title'] = $this->input->post('title');
            if($this->input->post('content') != null) {
                $insert['content'] = $this->input->post('content');
                $insert['content'] = str_replace('<p>', '', $insert['content']);
                $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
                $insert['content'] = str_replace('<div>', '', $insert['content']);
                $insert['content'] = str_replace('</div>', '', $insert['content']);
            }

            $insert['subtitle'] = $this->input->post('subtitle');
            if($this->input->post('content') != null) {
                $insert['content2'] = $this->input->post('content2');
                $insert['content2'] = str_replace('<p>', '', $insert['content2']);
                $insert['content2'] = str_replace('</p>', '<br>', $insert['content2']);
                $insert['content2'] = str_replace('<div>', '', $insert['content2']);
                $insert['content2'] = str_replace('</div>', '', $insert['content2']);
            }

            $insert['category'] = $this->input->post('kategorie');
            $insert['alt'] = $this->input->post('alt');
            $insert['header_alt'] = $this->input->post('header_alt');

            $this->base_m->insert('oferta', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
            redirect('panel/oferta');
        }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('oferta', $id);
    $data['kate'] = $this->base_m->get('kategorie');

    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/oferta/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/oferta/'.$now)) {
            mkdir('./uploads/oferta/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/oferta/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if($this->input->post('name_photo') != null) {
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $insert['photo'] = $now.'/'.$data['file_name'];        
            }
        }

        if($this->input->post('header_name_photo') != null) {
            if ($this->upload->do_upload('header_photo')) {
                $data = $this->upload->data();
                $insert['header_photo'] = $now.'/'.$data['file_name'];        
            }
        }

        $insert['title'] = $this->input->post('title');
        if($this->input->post('content') != null) {
            $insert['content'] = $this->input->post('content');
            $insert['content'] = str_replace('<p>', '', $insert['content']);
            $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
            $insert['content'] = str_replace('<div>', '', $insert['content']);
            $insert['content'] = str_replace('</div>', '', $insert['content']);
        }

        $insert['subtitle'] = $this->input->post('subtitle');
        if($this->input->post('content') != null) {
            $insert['content2'] = $this->input->post('content2');
            $insert['content2'] = str_replace('<p>', '', $insert['content2']);
            $insert['content2'] = str_replace('</p>', '<br>', $insert['content2']);
            $insert['content2'] = str_replace('<div>', '', $insert['content2']);
            $insert['content2'] = str_replace('</div>', '', $insert['content2']);
        }

        $insert['category'] = $this->input->post('kategorie');
        $insert['alt'] = $this->input->post('alt');
        $insert['header_alt'] = $this->input->post('header_alt');


        $this->base_m->update('oferta', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/oferta');
    }
}
}
