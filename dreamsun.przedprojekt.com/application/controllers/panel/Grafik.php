<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

public function index() {
    $data['rows'] = $this->base_m->get('grafik');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/grafik/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function reserv() {
    $data['rows'] = $this->base_m->get('rezerwacje');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/grafik/reserv', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $data['produkty'] = $this->base_m->get('produkty');
    $data['pracownicy'] = $this->base_m->get('pracownicy');
    $this->form_validation->set_rules('free_dates', 'Termin', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/grafik/dodaj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        
        $insert['free_dates'] = $this->input->post('free_dates');
        $insert['oferta_id'] = $this->input->post('oferta_id');
        $insert['pracownicy_id'] = $this->input->post('pracownicy_id');
        $this->base_m->insert('grafik', $insert);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
        redirect('panel/grafik');
    }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('grafik', $id);
    $data['produkty'] = $this->base_m->get('produkty');
    $data['pracownicy'] = $this->base_m->get('pracownicy');
    $this->form_validation->set_rules('free_dates', 'Termin', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/grafik/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        
        $insert['free_dates'] = $this->input->post('free_dates');
        $insert['oferta_id'] = $this->input->post('oferta_id');
        $insert['pracownicy_id'] = $this->input->post('pracownicy_id');
        $this->base_m->update('grafik', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/grafik');
    }
}

public function pracownicy() {
    $data['pracownicy'] = $this->base_m->get('pracownicy');
    $this->form_validation->set_rules('title', 'Imię i nazwisko', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/grafik/pracownicy', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/pracownicy/'.$now)) {
            mkdir('./uploads/pracownicy/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/pracownicy/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }
            }
        $insert['title'] = $this->input->post('title');
        $insert['content'] = $this->input->post('content');
        $insert['alt'] = $this->input->post('alt');
        $this->base_m->insert('pracownicy', $insert);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Pracownik został dodany!</p>');
        redirect('panel/grafik/pracownicy');
    }
}
}
