<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Realizacje extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->priority_DESC('realizacje');
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/realizacje/index', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}
	public function dodaj() {
		$this->form_validation->set_rules('alt', 'Tekst alternatywny', 'min_length[2]|trim|required');
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/realizacje/dodaj');
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/realizacje/'.$now)) {
		    	mkdir('./uploads/realizacje/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/realizacje/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) {
				$this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
				redirect('panel/realizacje');            
            } else {
	            $data = $this->upload->data();
				$data['rows'] = $this->base_m->priority_ASC('realizacje');
	            foreach($data['rows'] as $key) {
	            	$priority = $key->priority;
	            }
            	$insert['priority'] = $priority + 1;
            	$insert['photo'] = $now.'/'.$data['file_name'];
				$insert['alt'] = $this->input->post('alt');

				$this->base_m->insert('realizacje', $insert);
				$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
				redirect('panel/realizacje');
            }
		}
	}
}
