<?php defined('BASEPATH') OR exit('No direct script access allowed');

class O_nas extends CI_Controller {

public function index() {
    $data['rows'] = $this->base_m->get('o_nas');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/o_nas/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/o_nas/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/o_nas/'.$now)) {
            mkdir('./uploads/o_nas/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/o_nas/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];    
                    $insert['alt'] = $this->input->post('alt');    
                }
            }

            $insert['title'] = $this->input->post('title');
            if($this->input->post('content') != null) {
                $insert['content'] = $this->input->post('content');
                $insert['content'] = str_replace('<p>', '', $insert['content']);
                $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
                $insert['content'] = str_replace('<div>', '', $insert['content']);
                $insert['content'] = str_replace('</div>', '', $insert['content']);
            }

            $this->base_m->insert('o_nas', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
            redirect('panel/o_nas');
        }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('o_nas', $id);

    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/o_nas/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $now = date('Y-m-d');
        if (!is_dir('uploads/o_nas/'.$now)) {
            mkdir('./uploads/o_nas/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/o_nas/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
    
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if($this->input->post('name_photo') != null) {
            if ($this->upload->do_upload('photo')) {
                $data = $this->upload->data();
                $update['photo'] = $now.'/'.$data['file_name'];  
                $update['alt'] = $this->input->post('alt');      
            }
        }
            
        $update['title'] = $this->input->post('title');
        if($this->input->post('content') != null) {
            $update['content'] = $this->input->post('content');
            $update['content'] = str_replace('<p>', '', $update['content']);
            $update['content'] = str_replace('</p>', '<br>', $update['content']);
            $update['content'] = str_replace('<div>', '', $update['content']);
            $update['content'] = str_replace('</div>', '', $update['content']);
        }


        $this->base_m->update('o_nas', $update, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/o_nas');
    }
}
}
