<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Glowna extends CI_Controller
{

    public function index()
    {
        $data['rows'] = $this->base_m->get('glowna');
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/glowna/index', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    }

    public function edytuj($id)
    {

        if ($id == 1) {

            $data['row'] = $this->base_m->get_record('glowna', $id);
            $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
            $this->form_validation->set_rules('subtitle', 'Podtytuł', 'min_length[2]|trim|required');
            $this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|required');

            $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
            $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
            $this->form_validation->set_message('required', 'Pole %s jest wymagane');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('back/blocks/session');
                $this->load->view('back/blocks/head');
                $this->load->view('back/blocks/header');
                $this->load->view('back/pages/glowna/edit_content', $data);
                $this->load->view('back/blocks/ajax');
                $this->load->view('back/blocks/footer');

            } else {
                $now = date('Y-m-d');
                if (!is_dir('uploads/' . $now)) {
                    mkdir('./uploads/' . $now, 0777, TRUE);
                }
                $config['upload_path'] = './uploads/' . $now;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 0;
                $config['max_width'] = 0;
                $config['max_height'] = 0;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->input->post('name_photo') != null) {
                    if ($this->upload->do_upload('photo')) {
                        $data = $this->upload->data();
                        $update['photo'] = $now . '/' . $data['file_name'];
                    }
                }
                if ($this->input->post('name_photo1') != null) {
                    if ($this->upload->do_upload('photo1')) {
                        $data = $this->upload->data();
                        $update['photo1'] = $now . '/' . $data['file_name'];
                    }
                }
                if ($this->input->post('name_photo2') != null) {
                    if ($this->upload->do_upload('photo2')) {
                        $data = $this->upload->data();
                        $update['photo2'] = $now . '/' . $data['file_name'];
                    }
                }

                $update['title'] = $this->input->post('title');
                $update['title1'] = $this->input->post('title1');
                $update['title2'] = $this->input->post('title2');
                $update['title3'] = $this->input->post('title3');
                $update['subtitle'] = $this->input->post('subtitle');
                $update['alt'] = $this->input->post('alt');
                $update['alt1'] = $this->input->post('alt1');
                $update['alt2'] = $this->input->post('alt2');
                if($this->input->post('content') != null) {
                    $update['content'] = $this->input->post('content');
                    $update['content'] = str_replace('<p>', '', $update['content']);
                    $update['content'] = str_replace('</p>', '<br>', $update['content']);
                    $update['content'] = str_replace('<div>', '', $update['content']);
                    $update['content'] = str_replace('</div>', '', $update['content']);
                }
                if($this->input->post('content1') != null) {
                    $update['content1'] = $this->input->post('content1');
                    $update['content1'] = str_replace('<p>', '', $update['content1']);
                    $update['content1'] = str_replace('</p>', '<br>', $update['content1']);
                    $update['content1'] = str_replace('<div>', '', $update['content1']);
                    $update['content1'] = str_replace('</div>', '', $update['content1']);
                }
                if($this->input->post('content2') != null) {
                    $update['content2'] = $this->input->post('content2');
                    $update['content2'] = str_replace('<p>', '', $update['content2']);
                    $update['content2'] = str_replace('</p>', '<br>', $update['content2']);
                    $update['content2'] = str_replace('<div>', '', $update['content2']);
                    $update['content2'] = str_replace('</div>', '', $update['content2']);
                }

                $this->base_m->update('glowna', $update, $id);
                $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
                redirect('panel/glowna');

            }
        }



    }
	
}
