<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	public function index() {

		$data['slider'] = $this->base_m->get('slider');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['about'] = $this->base_m->get_record('o_nas', 1);
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/index', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function o_nas() {

		$data['rows'] = $this->base_m->get('o_nas');
		$data['about'] = $this->base_m->get_record('o_nas', 1);
		$data['podstrona'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/onas', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function oferty() {

		$data['rows'] = $this->base_m->get('oferta');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'oferta');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/oferty', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function produkty() {

		$data['rows'] = $this->base_m->get('produkty');
		$data['podstrona'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/produkty', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function grafik() {

		$data['rows'] = $this->base_m->get('grafik');
		$data['podstrona'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['grafik'] = $this->base_m->get_active('grafik');
		$data['pracownicy'] = $this->base_m->get_active('pracownicy');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/grafik', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function urzadzenia() {

		$data['rows'] = $this->base_m->get_photos();
		$data['podstrona'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/urzadzenia', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function kontakt() {

		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['podstrona'] = $this->base_m->get_page('podstrony', $this->uri->segment(2));
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/kontakt', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function wpis($slug, $id) {

		$data['row'] = $this->base_m->get_record('produkty', $id);
		$data['gallery'] = $this->base_m->get_gallery('produkty',$id);
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'produkty');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/wpis', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function oferta($slug, $id) {

		$data['rows'] = $this->base_m->get_offers('oferta', $slug);
		$data['category_title'] = $this->base_m->category_title($slug, $id);
		$data['gallery'] = $this->base_m->get('gallery');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'oferta');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/oferta', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function aktualnosci() {

		$data['rows'] = $this->base_m->get_offers('oferta', 'aktualnosci');
		$data['category_title'] = $this->base_m->category_title('aktualnosci', '1');
		$data['gallery'] = $this->base_m->get('gallery');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'aktualnosci');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');
		$data['news'] = $this->base_m->get('news');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/aktualnosci', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function karty() {

		$data['rows'] = $this->base_m->get_offers('oferta', 'aktualnosci');
		$data['category_title'] = $this->base_m->category_title('aktualnosci', '1');
		$data['gallery'] = $this->base_m->get('gallery');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'karty');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');
		$data['news'] = $this->base_m->get('news');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/karty', $data);
		$this->load->view('front/blocks/footer', $data);
	}


		public function aktualnosc($slug,$id) {

		$data['rows'] = $this->base_m->get_offers('oferta', 'aktualnosci');
		$data['category_title'] = $this->base_m->category_title('aktualnosci', '1');
		$data['gallery'] = $this->base_m->get('gallery');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'aktualnosci');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');
		$data['row'] = $this->base_m->get_record('news', $id);


		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/aktualnosc', $data);
		$this->load->view('front/blocks/footer', $data);
	}
		public function usluga($slug, $id) {

		$data['rows'] = $this->base_m->get_offers('oferta', $slug);
		$data['category_title'] = $this->base_m->category_title_offer($id);
		$data['oferta'] = $this->base_m->category_title_offer($id);
		$data['gallery'] = $this->base_m->get('gallery');
		$data['podstrona'] = $this->base_m->get_page('podstrony', 'oferta');
		$data['offer'] = $this->base_m->get_active('oferta');
		$data['product'] = $this->base_m->get_active('produkty');
		$data['settings'] = $this->base_m->get_record('ustawienia', 1);
		$data['contact'] = $this->base_m->get_record('contact_settings', 1);
		$data['category'] = $this->base_m->get('kategorie');


		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/usluga', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	
}
