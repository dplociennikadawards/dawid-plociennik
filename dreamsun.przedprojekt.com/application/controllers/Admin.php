<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
public function __construct() {
        parent::__construct();
        	if(!($_SESSION['login'] == TRUE) && !($_SESSION['role'] == 'admin'))//sprawdza czy użytkownik jest zalogowany i czy admin
        
            redirect('/');
        }
//public function __construct() { parent::__construct(); }
//uniwersalne usuwanie 1-parametr to tabela a 2-parametr to id które usuwamy. !!!!WAŻNE pole klucza w atbeli musi mieć nazwę: nazwa_tabeli_id!!!
public function delete() 
	{
		$table = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		$this->base_m->log("usunięcie danych z tabeli ".$table." o id ".$id."");
		$this->delete_m->drop_record($table , $id);
	}
}
