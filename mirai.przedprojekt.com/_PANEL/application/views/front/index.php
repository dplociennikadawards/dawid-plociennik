<?php 
$nadwozie['city-car'] = 'Auto miejskie'; 
$nadwozie['combi'] = 'Kombi'; 
$nadwozie['compact'] = 'Kompakt'; 
$nadwozie['minivan'] = 'Minivan'; 
$nadwozie['panel-van'] = 'Furgon'; 
$nadwozie['sedan'] = 'Sedan'; 
$nadwozie['suv'] = 'SUV'; 
$nadwozie['vans'] = 'Samochód dostawczy'; 

$paliwo['diesel'] = 'Diesel'; 
$paliwo['petrol'] = 'Benzyna'; 
?>

<?php if($this->session->flashdata('flashdata')) {
	echo '<div class="info">'.$this->session->flashdata('flashdata').'</div>';
} ?>
  <main>
    <section id="offers" class="search">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="search__text-box">
              <h4 class="search__light-title"><?php echo $glowna->content; ?></h4>
            </div>
          </div>
          <div class="col-lg-8">
            <form action="" method="post">
              <div class="search__form-row">
                <select class="search__top-select" name="makeInput" id="makeInput" onchange="loadRecords(); updateFilters('make');">
                  <option value="">Marka</option>
                  <?php foreach ($make as $value): ?>
                  <option value="<?php echo $value->make; ?>"><?php echo ucfirst($value->make); ?></option>
                  <?php endforeach ?>
                </select>
                <select class="search__top-select" name="modelInput" id="modelInput" onchange="loadRecords()">
                  <option value="">model</option>
                  <?php foreach ($model as $value): ?>
                  <option value="<?php echo $value->model; ?>"><?php echo ucfirst($value->model); ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="search__form-row">
                <span class="search__text">ROCZNIK</span>
                <select class="search__down-select" name="rok_odInput" id="rok_odInput" onchange="loadRecords(); updateFilters('rok_odInput');">
                  <option selected value="">od</option>
                  <?php foreach ($year as $value): ?>
                  <option value="<?php echo $value->year; ?>"><?php echo $value->year; ?></option>
                  <?php endforeach ?>
                </select>
                <select class="search__down-select" name="rok_doInput" id="rok_doInput" onchange="loadRecords()">
                  <option selected value="">do</option>
                  <?php foreach ($year as $value): ?>
                  		<option value="<?php echo $value->year; ?>"><?php echo $value->year; ?></option>
                  <?php endforeach ?>
                </select>
                <select class="search__down-select" name="body_typeInput" id="body_typeInput" onchange="loadRecords()">
                  <option value="">typ nadwozia</option>
                  <?php foreach ($body_type as $value): ?>
                  <option value="<?php echo $value->body_type; ?>"><?php echo $nadwozie[$value->body_type]; ?></option>
                  <?php endforeach ?>
                </select>
                <select class="search__down-select" name="fuel_typeInput" id="fuel_typeInput" onchange="loadRecords()">
                  <option value="">rodzaj paliwa</option>
                  <?php foreach ($fuel_type as $value): ?>
                  <option value="<?php echo $value->fuel_type; ?>"><?php echo $paliwo[$value->fuel_type]; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="search__form-row">
                <a class="search__clear-link text-white" onclick=" location.reload(); ">wyczyść filtrowanie</a>
                <!-- <input class="search__submit" type="submit" value="filtruj"> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <section class="list">
      <div class="container">
        <div class="list__nav-bar">
          <div class="list__sort">
            sortuj:
            <!-- Basic dropdown -->
            <!-- <button class="list__dropdown" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">Najnowsze <i class="fas fa-angle-down"></i></button>

            <div class="dropdown-menu">
              <a class="dropdown-item" onclick="loadRecords('created_at', 'ASC')">Najstarsze</a>
              <a class="dropdown-item">Cena od najniższej</a>
              <a class="dropdown-item">Cena od najwyższej</a>
            </div> -->
            <!-- Basic dropdown -->
            <select id="sort" class="list__dropdown" style="display: inline !important;" onchange="loadRecords();">
              <option value="created_at|DESC" selected>Najnowsze</option>
              <option value="created_at|ASC">Najstarsze</option>
              <option value="price|ASC">Cena od najniższej</option>
              <option value="price|DESC">Cena od najwyższej</option>
            </select>
          </div>
          <div class="list__pagination-box">
            <nav aria-label="navigation">
              <ul id="pagin_page1" class="pagination">
                <!-- <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/«.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/‹.png"></a>
                </li> -->

                <?php for ($i = 0; $i < (ceil(count($rows)/$ustawienia->cars_per_page)); $i++):  ?>
                <li id="activePagination<?php echo ($i*$ustawienia->cars_per_page); ?>" class="list__pagination-item <?php if($i==0): ?>list__pagination-item--active<?php endif; ?> page-item">
                  <a id="linkPagination<?php echo ($i*$ustawienia->cars_per_page); ?>" class="list__pagination-link <?php if($i==0): ?>list__pagination-link--active<?php endif; ?> page-link" onclick="pagination(<?php echo ($i*$ustawienia->cars_per_page); ?>)">
                    <?php echo ($i+1); ?>
                  </a>
                </li>
                <?php endfor ?>

                <!-- <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/›.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/».png"></a>
                </li> -->
              </ul>
            </nav>
          </div>
        </div>


<!-- LOAD CARS -->
        <div id="records" class="list__box">

        </div>
<!-- LOAD CARS -->


        <div class="list__nav-bar list__nav-bar--down">


          <div class="list__pagination-box">
            <nav aria-label="navigation">
              <ul id="pagin_page2" class="pagination">
                <!-- <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/«.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link" tabindex="-1"><img src="<?php echo base_url() ?>assets/front/img/‹.png"></a>
                </li> -->
                <?php for ($i = 0; $i < (ceil(count($rows)/$ustawienia->cars_per_page)); $i++):  ?>
                <li id="activePaginationF<?php echo ($i*$ustawienia->cars_per_page); ?>" class="list__pagination-item <?php if($i==0): ?>list__pagination-item--active<?php endif; ?> page-item">
                  <a id="linkPaginationF<?php echo ($i*$ustawienia->cars_per_page); ?>" class="list__pagination-link <?php if($i==0): ?>list__pagination-link--active<?php endif; ?> page-link" onclick="pagination(<?php echo ($i*$ustawienia->cars_per_page); ?>)">
                    <?php echo ($i+1); ?>
                  </a>
                </li>
                <?php endfor ?>
                <!-- <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/›.png"></a>
                </li>
                <li class="page-item">
                  <a class="list__pagination-arrow page-link"><img src="<?php echo base_url() ?>assets/front/img/».png"></a>
                </li> -->
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </section>

    <section class="contact">
      <div class="container">
        <h3 class="contact__title">
          ZAPRASZAMY DO KONTAKTU
        </h3>
        <p class="contact__subtitle">Wypełnij formularz, skontaktujemy się z Tobą najszybciej jak to będzie możliwe.</p>

        <form action="<?php echo base_url() ?>mail/send" method="post" onsubmit="return validateMyForm(0);">
          <!-- <div class="row">
            <div class="col-lg-6">
              <div class="contact__row contact__row--top">
                <span class="contact__desc">Oddział *</span> <select class="contact__select" name="" id="">
                  <option value=""></option>
                  <option value=""></option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-lg-6">
              <div class="contact__row">
                <span class="contact__desc">Imie *</span> <input class="contact__input" type="text" name="first_name" id="" required>
              </div>
              <div class="contact__row">
                <span class="contact__desc">Nazwisko *</span> <input class="contact__input" type="text" name="last_name" id="" required>
              </div>
              <div class="contact__row">
                <span class="contact__desc">Adres e-mail *</span> <input class="contact__input" type="email" name="email"
                  id="" required>
              </div>
              <div class="contact__row">
                <span class="contact__desc">Telefon *</span> <input class="contact__input" type="tel" name="phone" id="" required>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="contact__row">
                <span class="contact__desc contact__desc--textarea">Dodatkowa <br class="contact__linebreaker"> wiadomość</span>

                <textarea class="contact__textarea" name="message" id="" cols="30" rows="7" required></textarea>
              </div>
            </div>
          </div>

          <div class="contact__bottom-row row">
            <div class="contact__checkbox-box">
              <!-- Default unchecked -->
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="rodo" name="rodo" value="1">
                <label class="custom-control-label" for="rodo"><small>Wyrażam zgodę na przetwarzanie przez danych osobowych podanych w formularzu. Podanie danych jest dobrowolne. Administratorem podanych przez Pana/ Panią danych osobowych jest <?php echo $kontakt->company; ?> z siedzibą w <?php echo $kontakt->address; ?>, <?php echo $kontakt->city; ?>. Pana/Pani dane będą przetwarzane w celach związanych z udzieleniem odpowiedzi, przedstawieniem oferty usług <?php echo $kontakt->company; ?> oraz świadczeniem usług przez administratora danych. Przysługuje Panu/Pani prawo dostępu do treści swoich danych oraz ich poprawiania.</small></label>
              </div><!-- Default unchecked -->
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="rodo2" name="rodo2" value="1">
                <label class="custom-control-label" for="rodo2"><small>Wyrażam zgodę na otrzymywanie informacji handlowych od <?php echo $kontakt->company; ?> dotyczących jej oferty w szczególności poprzez połączenia telefoniczne lub sms z wykorzystaniem numeru telefonu podanego w formularzu, a także zgodę na przetwarzanie moich danych osobowych w tym celu przez Kenaz oraz w celach promocji, reklamy i badania rynku.</small></label>
              </div><br>
              <div class="custom-control custom-checkbox">
                <label class="font-weight-bold" b><small class="font-weight-bold">Podając nam swoje dane kontaktowe, wyrażasz zgodę na kontakt ze strony należącej do <?php echo $kontakt->company; ?> w
              sprawie zaaranżowania jazdy próbnej oraz na otrzymywanie od czasu do czasu informacji na temat firmy
              <?php echo $kontakt->company; ?>.
              Zaznaczenie poszczególnego sposobu kontaktu oznacza zgodę na otrzymywanie informacji od <?php echo $kontakt->company; ?> w
              ten sposób.</small></label>
              </div>
            </div>

            <p class="contact__text">
              
            </p>
          </div>
                <center>
                  <div class="g-recaptcha mb-3" data-sitekey="6LeGuq0UAAAAAAHZswDRLeirZX6_mkycqmOriDXN"></div>
                  <span class="warning_captcha text-danger" class="text-danger"></span>
                </center>
          <div class="contact__bottom-row row">
            <span class="contact__second-desc">* Pola wymagane</span>
            <input class="contact__submit" type="submit" value="WYŚLIJ">
          </div>
        </form>
      </div>
    </section>
  </main>