<?php 
$nadwozie['city-car'] = 'Auto miejskie'; 
$nadwozie['combi'] = 'Kombi'; 
$nadwozie['compact'] = 'Kompakt'; 
$nadwozie['minivan'] = 'Minivan'; 
$nadwozie['panel-van'] = 'Furgon'; 
$nadwozie['sedan'] = 'Sedan'; 
$nadwozie['suv'] = 'SUV'; 
$nadwozie['vans'] = 'Samochód dostawczy'; 

$paliwo['diesel'] = 'Diesel'; 
$paliwo['petrol'] = 'Benzyna'; 

$skrzynia['manual'] = 'Manualna'; 
$skrzynia['automatic'] = 'Automatyczna'; 
$skrzynia['am'] = 'Amracing'; 
$skrzynia['cvt'] = 'Bezstopniowa'; 

$osoba['business'] = 'Osoba biznesowa'; 

$naped['front-wheel'] = 'Przednie koła'; 
$naped['all-wheel-permanent'] = 'Wszystkie koła';
$naped['back-wheel'] = 'Tylne koła'; 

$kolor['grey'] = 'Szary'; 
$kolor['black'] = 'Czarny';
$kolor['red'] = 'Czerwony'; 
$kolor['white'] = 'Biały'; 
$kolor['gold'] = 'Złoty'; 
$kolor['blue'] = 'Niebieski'; 
$kolor['silver'] = 'Srebrny'; 
$kolor['green'] = 'Zielony'; 
$kolor['brown'] = 'Brązowy'; 
$kolor['other'] = 'Inny'; 
$kolor['yellow-gold'] = 'Złoty'; 

$wyposazenie['abs'] = 'ABS'; 
$wyposazenie['front-electric-windows'] = 'Przednie szyby elektryczne'; 
$wyposazenie['onboard-computer'] = 'Komputer pokładowy'; 
$wyposazenie['electronic-rearview-mirrors'] = 'Elektryczne lusterka'; 
$wyposazenie['electronic-immobiliser'] = 'Elektryczny immobilizer'; 
$wyposazenie['rear-parking-sensors'] = 'Czujniki parkowania z tyłu'; 
$wyposazenie['steering-whell-comands'] = 'Kierownica wielofunkcyjna'; 
$wyposazenie['bluetooth'] = 'Bluetooth'; 
$wyposazenie['rear-passenger-airbags'] = 'Poduszki powietrzne z tyłu'; 
$wyposazenie['front-airbags'] = 'Poduszki powietrzne z przodu'; 
$wyposazenie['dual-air-conditioning'] = 'Klimatyzacja dwustrefowa'; 
$wyposazenie['automatic-wipers'] = 'Automatyczne wycieraczki'; 
$wyposazenie['asr'] = 'ASR'; 
$wyposazenie['fog-lights'] = 'Światła przeciwmgielne'; 
$wyposazenie['assisted-steering'] = 'Wspomaganie kierowania'; 
$wyposazenie['isofix'] = 'Isofix'; 
$wyposazenie['side-window-airbags'] = 'Boczne poduszki powietrzne'; 
$wyposazenie['front-side-airbags'] = 'Przednie boczne poduszki powietrzne'; 
$wyposazenie['central-lock'] = 'Centralny zamek'; 
$wyposazenie['rear-electric-windows'] = 'Szyby elektryczne tylne'; 
$wyposazenie['roof-bars'] = 'Bagażnik dachowy'; 
$wyposazenie['cruise-control'] = 'Tempomat'; 
$wyposazenie['automatic-lights'] = 'Światła automatyczne'; 
$wyposazenie['speed-limiter'] = 'Termostat'; 
$wyposazenie['front-passenger-airbags'] = 'Poduszki powietrzne pasażera z przodu'; 
$wyposazenie['automatic-air-conditioning'] = 'Klimatyzacja automatyczna'; 
$wyposazenie['gps'] = 'GPS'; 
$wyposazenie['heated-rearview-mirrors'] = 'Podgrzewane lusterka'; 
$wyposazenie['esp'] = 'ESP'; 
$wyposazenie['usb-socket'] = 'Port USB'; 
$wyposazenie['daytime-lights'] = 'Światła dzienne'; 
$wyposazenie['mp3'] = 'Odtwarzacz MP3'; 
$wyposazenie['leather-interior'] = 'Skórzane wnętrze'; 
$wyposazenie['alarm'] = 'Alarm'; 
$wyposazenie['front-heated-seats'] = 'Podgrzewane przednie siedzenia'; 
$wyposazenie['electric-interior-mirror'] = 'Elektryczne lusterko wewnętrzne'; 
$wyposazenie['adjustable-suspension'] = 'Regulowane zawieszenie'; 
$wyposazenie['both-parking-sensors'] = 'Dwa czuniki parkowania'; 
$wyposazenie['shift-paddles'] = 'Łopatki zmiany biegów'; 
$wyposazenie['original-radio'] = 'Oryginalne radio'; 
$wyposazenie['alloy-wheels'] = 'Felgi aluminiowe'; 
$wyposazenie['tinted-windows'] = 'Przyciemniane szyby'; 
$wyposazenie['rearview-camera'] = 'Kamera cofania'; 
$wyposazenie['leds'] = 'Światła LED'; 
$wyposazenie['electric-adjustable-seats'] = 'Elektrycznie regulowane fotele'; 
$wyposazenie['cd'] = 'Odtwarzacz CD'; 
$wyposazenie['air-conditioning'] = 'Klimatyzacja'; 
$wyposazenie['system-start-stop'] = 'System start/stop'; 
$wyposazenie['blind-spot-sensor'] = 'Czujnik martwego pola'; 
$wyposazenie['aux-in'] = 'Wejście AUX'; 
$wyposazenie['heated-windshield'] = 'Podgrzewana szyba przednia'; 
$wyposazenie['lane-assist'] = 'Asystent pasa ruchu'; 
?>

<div class="container-fluid pading_header pb-md-3">

	<div class="row">
		<div class="col-md-9">
            <span class="header_title">
              <?php echo ucfirst($row->make); ?>
              <?php echo ucfirst($row->model); ?>
              <?php echo ucfirst($row->version); ?>
            </span>
            <!-- <span class="header_subtitle ml-3"><?php echo $row->year; ?></span>&bull;
            <span class="header_subtitle"><?php echo str_replace(',', ' ', number_format($row->mileage)); ?> km</span>&bull;
            <span class="header_subtitle"><?php echo $paliwo[$row->fuel_type]; ?></span> -->
		</div>
		<div class="col-md-3">
			<h3 class="cell__price"><?php echo str_replace(',', ' ', number_format($row->price)); ?> <span class="cell__light-text">PLN</span></h3>
		</div>
		<div class="col-md-8 d-none d-md-block">
            <a href="tel:<?php echo $kontakt->phone; ?>" class="btn btn-danger text-white">
            	<i class="fas fa-phone"></i> <?php echo $kontakt->phone; ?>
        	</a>
            <a href="mailto:<?php echo $kontakt->email; ?>" class="btn btn-danger text-white">
            	<i class="fas fa-envelope-open-text"></i> <?php echo $kontakt->email; ?>
        	</a>
		</div>
		<div class="col-md-4">
			<span class="cell__desc header_subtitle">Cena Brutto, <?php if($row->vat == 1){echo 'Faktura VAT';} ?></span>
		</div>
		<div class="col-6 mobile-button d-md-none mt-3">
			<a href="tel:<?php echo $kontakt->phone; ?>" class="text-white"><i class="fas fa-phone"></i></a>
		</div>
		<div class="col-6 mobile-button alt-mobile d-md-none mt-3">
			<a href="mailto:<?php echo $kontakt->email; ?>" class="text-white"><i class="fas fa-envelope-open-text"></i></a>
		</div>
	</div>
	
</div>

<?php $gallery = explode(",|,", $row->gallery); ?>
<div id="mdb-lightbox-ui"></div>
<div class="container mt-5">
	<div class="row">
		<div class="col-md-7 col-lg-8">

			<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">

			  <div class="carousel-inner mdb-lightbox" role="listbox">
			    <?php $i=0; foreach ($gallery as $value): 
			    list($width, $height) = getimagesize($value); ?>
			    <div class="carousel-item <?php if($i==0){echo 'active';} ?>">
			    	<figure>
			    	<a href="<?php echo $value; ?>" data-size="<?php echo $width .'x'. $height; ?>">
			      		<img class="d-block w-100" src="<?php echo $value; ?>" alt="<?php echo $row->title; ?>" >
			        </a>
			        </figure>
			    </div>
			    <?php $i++; endforeach ?>
			  </div>

			  <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>

			  <ol class="carousel-indicators">
			    <?php $i=0; foreach ($gallery as $value): ?>
			    <li data-target="#carousel-thumb" data-slide-to="<?php echo $i; ?>" <?php if($i==0){echo 'class="active"';} ?>>
			      <img src="<?php echo $value; ?>" width="100">
			    </li>
			    <?php $i++; endforeach ?>
			  </ol>
			</div>
			<p><small>#<?php echo $row->user_id ?> | <?php echo date('H:i d/m/Y', strtotime($row->created_at )); ?></small></p>

			<!-- MIEJSCE NA GOOGLE ADS -->
			<!-- <div class="text-center">
				<img src="<?php echo base_url(); ?>assets/front/img/miejsce-na-reklame-3-miejsce-na-reklame-1.jpg" class="img-fluid" width="400">
			</div> -->
			<!-- MIEJSCE NA GOOGLE ADS -->
		</div>
		<div class="col-md-5 col-lg-4 form">
			<div class="mt-3">
				<strong>
					<?php echo $osoba[$row->advertiser_type]; ?>
				</strong>
				<h4><?php echo $row->person; ?></h4>
				<hr>
				Lokalizacja: <strong><?php echo $row->municipality; ?></strong> 
				<hr>
				<div class="text-center">
					<a href="tel:<?php echo $kontakt->phone; ?>" class="btn btn-danger text-white">
	            		<i class="fas fa-phone"></i> <?php echo $kontakt->phone; ?>
	        		</a>
					<a href="<?php echo $row->url; ?>" class="btn btn-white">
	            		<img src="<?php echo base_url(); ?>assets/front/img/otomoto.png" class="img-fluid" width="20"> Link do OTOMOTO
	        		</a>
        		</div>
        		<hr>
        		<form action="<?php echo base_url() ?>mail/send_offer" method="post" onsubmit="return validateMyForm(0);">              
        			<input class="contact__input w-100 mb-3" type="hidden" name="subject" value="<?php echo ucfirst($row->make); ?> <?php echo ucfirst($row->model); ?> <?php echo ucfirst($row->version); ?>">
        			<input class="contact__input w-100 mb-3" type="text" name="first_name" placeholder="Imię i nazwisko*" id="" required>
        			<input class="contact__input w-100 mb-3" type="email" name="email" placeholder="E-mail*" id="" required>
        			<input class="contact__input w-100  mb-3" type="tel" name="phone" placeholder="Telefon*" id="" required>
        			<textarea class="contact__textarea w-100 p-3" name="message" id="" rows="3" required>Jestem zainteresowany ofertą kupna tego samochodu. Proszę o kontakt.</textarea>

		              <div class="custom-control custom-checkbox">
		                <input type="checkbox" class="custom-control-input" id="rodo" name="rodo" value="1">
		                <label class="custom-control-label" for="rodo"><small>Zgadzam się na przetwarzanie danych osobowych</small></label>
		              </div>
              		  <div class="custom-control custom-checkbox">
		                <input type="checkbox" class="custom-control-input" id="rodo2" name="rodo2" value="1">
		                <label class="custom-control-label" for="rodo2"><small>Zgadzam się na kontakt mailowy i telefoniczny</small></label>
		              </div>
		              <div class="custom-control custom-checkbox pl-md-0">
		                <label class="font-weight-bold"><small class="font-weight-bold">Podając nam swoje dane kontaktowe, wyrażasz zgodę na kontakt ze strony należącej do <?php echo $kontakt->company; ?> w
		              sprawie zaaranżowania jazdy próbnej oraz na otrzymywanie od czasu do czasu informacji na temat firmy
		              <?php echo $kontakt->company; ?>.
		              Zaznaczenie poszczególnego sposobu kontaktu oznacza zgodę na otrzymywanie informacji od <?php echo $kontakt->company; ?> w
		              ten sposób.</small></label>
		              </div>

	                <div class="g-recaptcha mt-3" data-sitekey="6LeGuq0UAAAAAAHZswDRLeirZX6_mkycqmOriDXN" style="transform: scale(0.9);"></div>
	                <span class="warning_captcha text-danger" class="text-danger"></span>
		          <div class="text-center pb-3">
		            <button type="submit" class="btn btn-danger">Wyślij</button>
		          </div>
        		</form>
			</div>

		</div>
	</div>
</div>

<div class="container-fluid bg-grey mt-md-4 pt-4">
	<div class="container py-md-5">
		<div class="row">
			<div class="col-md-4">
					<table>
						<tr>
							<td>Oferta od: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($osoba[$row->advertiser_type])) { echo $osoba[$row->advertiser_type];} 
									else {echo $row->advertiser_type;} ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Marka pojazdu: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo ucfirst($row->make); ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Model pojazdu: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo ucfirst($row->model); ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Wersja: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo $row->version; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Rok produkcji: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo $row->year; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Przebieg: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo str_replace(',', ' ', number_format($row->mileage)); ?> km
								</span>
							</td>
						</tr>
					</table>
			</div>
			<div class="col-md-4">
					<table>
						<tr>
							<td>Pojemność skokowa: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo str_replace(',', ' ', number_format($row->engine_capacity)); ?> cm<sup>3</sup>
								</span>
							</td>
						</tr>
						<tr>
							<td>Rodzaj paliwa: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($paliwo[$row->fuel_type])) { echo $paliwo[$row->fuel_type];} 
									else {echo $row->fuel_type;} ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Moc: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo $row->engine_power; ?> KM
								</span>
							</td>
						</tr>
						<tr>
							<td>Skrzynia biegów: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($skrzynia[$row->gearbox])) { echo $skrzynia[$row->gearbox];} 
									else {echo $row->gearbox;} ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Napęd: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($naped[$row->transmission])) { echo $naped[$row->transmission];} 
									else {echo $row->transmission;} ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Typ: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($nadwozie[$row->body_type])) { echo $nadwozie[$row->body_type];} 
									else {echo $row->body_type;} ?>
								</span>
							</td>
						</tr>
					</table>
			</div>
			<div class="col-md-4">
					<table cellspacing="10">
						<tr>
							<td>Liczba drzwi: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo $row->door_count; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Liczba miejsc: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php echo $row->nr_seats; ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Kolor: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if(isset($kolor[$row->color])) { echo $kolor[$row->color];} 
									else {echo $row->color;} ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>
								<?php if($row->metallic == 1){
										echo 'Metalik:';
									} elseif ($row->pearl == 1) {
										echo 'Perłowy:';
									} elseif ($row->matt == 1) {
										echo 'Matowy:';
									} elseif ($row->acrylic == 1) {
										echo 'Akrylowy:';
									}
								?>
							</td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									Tak
								</span>
							</td>
						</tr>
						<tr>
							<td>Bezwypadkowy: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if($row->no_accident == 1) {
										echo 'Tak';
										} else { echo 'Nie'; } ?>
								</span>
							</td>
						</tr>
						<tr>
							<td>Stan: </td>
							<td>
								<span class="text-red font-weight-bold ml-3">
									<?php if($row->status == 'new') {
											echo 'Nowy';
										} else { echo 'Używany'; } ?>
								</span>
							</td>
						</tr>
					</table>
			</div>
		</div>
	</div>
</div>

<div class="container py-md-5 pt-3">
	<div class="row">
		<div class="col-md-12 px-md-0">
			<h3 class="cell__title">Wyposażenie</h3>
			<div class="mt-4 mb-4 line-separate-red"></div>
		</div>
		<div class="col-md-12 px-md-0">
			<div class="row">
				<?php $params = explode(",|,", $row->params); ?>
				<?php foreach ($params as $value): ?>
					<div class="col-md-4"><i class="fas fa-check text-red"></i> 
						<?php if(isset($wyposazenie[$value])) { echo $wyposazenie[$value];} 
						else {echo $value;} ?>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<hr>
</div>

<div class="container pb-md-5">
	<div class="row">
		<div class="col-md-12 px-md-0">
			<h3 class="cell__title"><?php echo $row->title; ?></h3>
			<div class="mt-4 mb-4 line-separate-red"></div>
		</div>
		<div class="col-md-12 px-md-0">
			<?php echo nl2br($row->description); ?>
		</div>
	</div>
</div>

<div class="container-fluid mt-3 mt-md-0" style="
  background-image: url(<?php echo base_url(); ?>uploads/podstrony/<?php echo $slider->header_photo; ?>);
  padding-top: 70px;
  padding-bottom: 70px; 
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
 ">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-12 px-md-0">
				<h3 class="cell__title  text-shadow"><?php echo $slider->title; ?></h3>
				<div class="mt-4 mb-5 line-separate-red"></div>
			</div>
			<div class="col-md-12 px-md-0 text-white text-uppercase font-weight-bold text-shadow">
				<?php echo $slider->content; ?>
				<a href="<?php echo base_url(); ?>p/finansowanie" class="btn btn-danger">
					Oferty finansowania <i class="fas fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="container py-md-5">
	<div class="row">
		<div class="col-md-12 px-md-0">
			<section class="text-center my-5">
			  <h2 class="h1-responsive font-weight-bold text-center my-5">Polecane oferty</h2>


			  <div class="row">
			  	<?php foreach ($random as $value): ?>
			    <div class="col-lg-3 col-md-6 mb-lg-0 mb-4 wow fadeIn selected">
			      <div class="card collection-card z-depth-1-half">
					<a href="<?php echo base_url() ?>p/szczegoly/<?php echo $this->slugify_m->slugify($value->make.$value->model.$value->version); ?>/<?php echo $value->samochody_id; ?>" class="text-white">
			        	<div class="view zoom" style="
			        	background-image: url(<?php echo $value->photo; ?>);
			        	background-size: cover;
			        	background-position: center;
			        	background-repeat: no-repeat;
			        	cursor: pointer;
			        	height: 250px;">
			          		<div class="stripe dark">
			              		<p class="mb-0">            
			              			<?php echo ucfirst($value->make); ?>
              						<?php echo ucfirst($value->model); ?>
              						<?php echo ucfirst($value->version); ?>
			                		<i class="fas fa-angle-right"></i>
			              		</p>
			          		</div>
			        	</div>
			        </a>
			      </div>
			    </div>
			  	<?php endforeach ?>
			  </div>
			</section>
		</div>
	</div>
</div>
