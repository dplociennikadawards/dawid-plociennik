  <header class="main-header">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="main-header__container container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
          <img class="main-header__logo" src="<?php echo base_url() ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="<?php echo $ustawienia->meta_title; ?>" style="z-index: 100 !important;">
        </a>


        <div class="main-header__navbar-box">

          <div class="main-header__fast-contact-box">
            SZYBKI KONTAKT: 
              <span class="main-header__links">
                <?php echo $kontakt->name_phone1; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone; ?> "><?php echo $kontakt->phone; ?> </a> 
              &nbsp;<span class="text-danger">|</span>&nbsp;
                <?php echo $kontakt->name_phone2; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone2; ?> "><?php echo $kontakt->phone2; ?> </a> 
              &nbsp;<span class="text-danger">|</span>&nbsp;
                <?php echo $kontakt->name_phone3; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone3; ?> "><?php echo $kontakt->phone3; ?> </a> 
              &nbsp;<span class="text-danger">|</span>&nbsp;
                <?php echo $kontakt->name_phone4; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone4; ?> "><?php echo $kontakt->phone4; ?> </a> 
              &nbsp;<span class="text-danger">|</span>&nbsp;
                <?php echo $kontakt->name_email; ?> 
              <a class="main-header__contact-link" href="#"><?php echo $kontakt->email; ?></a>
            </span>
          </div>

          <div class="main-header__collapse-box">
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link 
                    <?php if($this->uri->segment(2) == ''){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url(); ?>#offers">Samochody dostępne od ręki</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'finansowanie'){echo 'main-header__nav-link--color';} ?>" 
                    href="<?php echo base_url() ?>p/finansowanie">Finansowanie</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'serwis'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/serwis">Serwis</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'lakiernia'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/lakiernia">Lakiernia/blacharnia</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'ubezpieczenia'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/ubezpieczenia">Ubezpieczenia</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'salony'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/salony">Nasze salony</a>
                </li>
              </ul>
            </div>
          </div>

        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="main-header__second-logo-box">
          <img class="main-header__second-logo" src="<?php echo base_url() ?>assets/front/img/second-logo.png" alt="logo toyota">
        </div>

      </div>
    </nav>


<?php if($this->uri->segment(2) == ''): ?>
    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
      <!--Slides-->
      <div class="carousel-inner" role="listbox">
        <?php $i=0; foreach ($slider as $value): $i++; ?>
        <!--First slide-->
        <div class="carousel-item <?php if($i == 1){echo 'active';} ?>">
          <div class="main-header__hero" style="background-image: url(<?php echo base_url(); ?>uploads/slider/<?php echo $value->photo; ?>);" title="<?php echo $value->alt; ?>">
            <div class="main-header__hero-container container">
              <div class="main-header__title-box">
                <h2 class="main-header__title"><span class="main-header__light-title"><?php echo $value->title; ?></span><br>
                  <?php echo $value->subtitle; ?></h2>

                <h3 class="main-header__subtitle"><?php echo $value->content; ?></h3>
              </div>
            </div>
          </div>
        </div>
        <!--/First slide-->
        <?php endforeach ?>
      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

<?php else: ?>
  <div class="main-header__hero" style="background-image: url(<?php echo base_url(); ?>uploads/podstrony/<?php echo $slider->header_photo; ?>);" title="<?php echo $slider->alt; ?>">
    <div class="main-header__hero-container container" style="align-items: center;">
      <div class="main-header__title-box">
        <h2 class="main-header__title">
          <span class="main-header__light-title"><?php echo $slider->title; ?></span><br>
          <small><?php echo $slider->subtitle; ?></small>
        </h2>

      </div>
    </div>
  </div>
<?php endif; ?>

  </header>