<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $ustawienia->meta_title ?></title>
  <meta name="description" content="<?php echo $ustawienia->description ?>">
  <meta name="keywords" content="<?php echo $ustawienia->meta_title ?>,<?php echo $ustawienia->description ?>">
  <meta name="author" content="Dawid Płóciennik AD Awards">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo; ?>"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url() ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url() ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url() ?>assets/front/css/fontface.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/front/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link href="<?php echo base_url() ?>assets/front/css/lightbox.css" rel="stylesheet">
</head>

<body>