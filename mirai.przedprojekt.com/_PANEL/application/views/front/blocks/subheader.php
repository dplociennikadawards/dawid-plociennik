  <header class="main-header">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="main-header__container container">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
          <img class="main-header__logo" src="<?php echo base_url() ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="<?php echo $ustawienia->meta_title; ?>" style="z-index: 100 !important;">
        </a>


        <div class="main-header__navbar-box">

          <div class="main-header__fast-contact-box">
            SZYBKI KONTAKT: 
              <span class="main-header__links">
                <?php echo $kontakt->name_phone1; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone; ?> "><?php echo $kontakt->phone; ?> </a> 
                <?php echo $kontakt->name_phone2; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone2; ?> "><?php echo $kontakt->phone2; ?> </a> 
                <?php echo $kontakt->name_phone3; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone3; ?> "><?php echo $kontakt->phone3; ?> </a> 
                <?php echo $kontakt->name_phone4; ?> 
              <a class="main-header__contact-link" href="tel:<?php echo $kontakt->phone4; ?> "><?php echo $kontakt->phone4; ?> </a> 
                <?php echo $kontakt->name_email; ?> 
              <a class="main-header__contact-link" href="#"><?php echo $kontakt->email; ?></a>
            </span>
          </div>

          <div class="main-header__collapse-box">
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link 
                    <?php if($this->uri->segment(2) == '' || $this->uri->segment(2) == 'szczegoly'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url(); ?>#offers">Samochody dostępne od ręki</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'finansowanie'){echo 'main-header__nav-link--color';} ?>" 
                    href="<?php echo base_url() ?>p/finansowanie">Finansowanie</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'serwis'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/serwis">Serwis</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'lakiernia'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/lakiernia">Lakiernia/blacharnia</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'ubezpieczenia'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/ubezpieczenia">Ubezpieczenia</a>
                </li>
                <li class="nav-item">
                  <a class="main-header__nav-link nav-link
                    <?php if($this->uri->segment(2) == 'salony'){echo 'main-header__nav-link--color';} ?>"
                    href="<?php echo base_url() ?>p/salony">Nasze salony</a>
                </li>
              </ul>
            </div>
          </div>

        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="main-header__second-logo-box">
          <img class="main-header__second-logo" src="<?php echo base_url() ?>assets/front/img/second-logo.png" alt="logo toyota">
        </div>

      </div>
    </nav>



  </header>