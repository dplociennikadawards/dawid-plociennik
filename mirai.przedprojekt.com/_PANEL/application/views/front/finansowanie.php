<section>
	<div class="container py-5">
		<div class="row">
	      <?php if($slider->content != ''): ?>
	        <div class="col-md-12 mb-5"><?php echo $slider->content; ?></div>
	      <?php endif; ?>
			<?php foreach ($rows as $value): ?>
			<div class="col-md-12 px-md-0">
				<h3 class="cell__title"><?php echo $value->title; ?></h3>
				<div class="mt-4 mb-5 line-separate-red"></div>
			</div>
			<div class="col-md-6 px-md-0"><?php echo $value->content; ?></div>
			<div class="col-md-6 px-md-0"><?php echo $value->content2; ?></div>
			<div class="col-md-12 px-md-0"><?php echo $value->content3; ?></div>
			<div class="mt-4 mb-5 line-separate"></div>
			<?php endforeach ?>
		</div>
	</div>
</section>