<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj podstronę #<?php echo $row->podstrony_id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-6">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                               <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                               <label for="formTitle">Tytuł w nagłówku<span class="text-danger">*</span></label>
                           </div>
                           <div class="md-form mb-5">
                               <input type="text" id="formSubtitle" class="form-control" name="subtitle" value="<?php echo $row->subtitle; ?>">
                               <label for="formSubtitle">Podtytuł w nagłówku</label>
                           </div>
                           <div class="md-form">
                                <textarea name="content" id="editor"><?php echo $row->content; ?></textarea>
                           </div>
                </div>

                <div class="col-md-6">

                    <div class="md-form mb-5">
                      <div class="file-field">
                        <div id="header_photo_show" class="text-center">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/podstrony/<?php echo $row->header_photo; ?>" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie nagłówka</span>
                          <input type="file" name="header_photo" id="header_photo">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_header_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie nagłówka" value="<?php echo $row->header_photo; ?>" required readonly>
                        </div>
                      </div>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formAlt" class="form-control" name="alt" value="<?php echo $row->alt; ?>">
                      <label for="formAlt">Tekst alternatywny zdjęcia</label>
                    </div>
                </div>
               </div>
            </div>
        </div>
    </section>
    </form>
</main>
