<main class="main-section water-logo">
    <?php echo form_open_multipart('', 'id="specialform"');?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj #<?php echo $row->id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="container-fluid">
                
                    <?php echo validation_errors(); ?>
                   <div class="row px-5">
                       <div class="col-md-6">

                          <div class="mb-5">
                            <label for="keywords">Słowa kluczowe</label>
                            <input type="text" id="keywords" class="form-control" name="keywords" value="<?php echo $row->keywords; ?>" data-role="tagsinput" required>
                          </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                               <label for="formTitle">Tytuł<span class="text-danger">*</span></label>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formSubTitle" class="form-control" name="subtitle" value="<?php echo $row->subtitle; ?>">
                               <label for="formSubTitle">Podtytuł</label>
                           </div>

                           <div class="md-form mb-5">
                               <textarea id="editor" name="content"><?php echo $row->content; ?></textarea>
                           </div>

                           <div class="md-form mb-5">
                               <textarea id="editor" name="content2"><?php echo $row->content2; ?></textarea>
                           </div>

                           <div class="md-form mb-5">
                               <textarea id="editor" name="content3"><?php echo $row->content3; ?></textarea>
                           </div>

                       </div>

                       <div class="col-md-6">
                           <div class="md-form mb-5">
                               <div class="file-field">
                                   <div id="photo_show" class="text-center delete_photo cursor" onclick="clear_box('');">
                                      <?php if($row->photo != ''): ?>
                                       <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/<?php echo $row->slug; ?>/<?php echo $row->photo; ?>" width="250">
                                      <?php endif; ?>
                                   </div>
                                   <div class="btn btn-primary btn-sm float-left gold-gradient">
                                       <span>Wybierz zdjęcie</span>
                                       <input type="file" name="photo" id="photo">
                                   </div>
                                   <div class="file-path-wrapper">
                                       <input id="name_photo" name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo; ?>" readonly>
                                   </div>
                               </div>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formAlt1" class="form-control" name="alt" value="<?php echo $row->alt; ?>">
                               <label for="formAlt1">Tekst alternatywny zdjęcia</label>
                           </div>


                           <div class="md-form mb-5">
                               <div class="file-field">
                                   <div id="photo_show2" class="text-center delete_photo cursor" onclick="clear_box('2');">
                                      <?php if($row->photo2 != ''): ?>
                                       <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/<?php echo $row->slug; ?>/<?php echo $row->photo2; ?>" width="250">
                                      <?php endif; ?>
                                   </div>
                                   <div class="btn btn-primary btn-sm float-left gold-gradient">
                                       <span>Wybierz zdjęcie</span>
                                       <input type="file" name="photo2" id="photo2">
                                   </div>
                                   <div class="file-path-wrapper">
                                       <input id="name_photo2" name="name_photo2" class="file-path validate" type="text" placeholder="Wybierz zdjęcie" value="<?php echo $row->photo2; ?>" readonly>
                                   </div>
                               </div>
                           </div>

                           <div class="md-form mb-5">
                               <input type="text" id="formAlt2" class="form-control" name="alt2" value="<?php echo $row->alt2; ?>">
                               <label for="formAlt2">Tekst alternatywny zdjęcia</label>
                           </div>


                           
                          <div class="md-form mb-5">
                            <div class="file-field delete_photo cursor">
                                <div class="btn btn-primary btn-sm float-left gold-gradient">
                                    <span>Stwórz galerie</span>
                                    <input type="file" name="gallery[]" id="gallery" multiple="multiple">
                                </div>
                                <div class="file-path-wrapper">
                                    <input id="name_gallery"  onclick="clear_box_gallery();" name="name_gallery" class="file-path validate" type="text" value="<?php echo $row->gallery; ?>" placeholder="Wybierz zdjęcia do galerii" readonly>
                                </div>
                            </div>
                          </div>

                        </div>
                   </div>

        </div>
    </section>
    </form>
</main>

<script type="text/javascript">
    function clear_box(number_box)
  {
    document.getElementById('photo_show'+number_box).innerHTML = '';
    document.getElementById('name_photo'+number_box).value = 'usunięte';
  }
</script>
<script type="text/javascript">
    function clear_box_gallery()
  {
    document.getElementById('name_gallery').value = '';
  }
</script>