<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container mb-5">
			<!-- Table with panel -->
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<input type="text" id="searchInput" class="form-control" onkeyup="searchFiltr(0)" placeholder="Wpisz nazwę..." title="Wpisz nazwę">
			    </div>

			    <a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(4)); ?></a>

                  <div class="text-right" style="width: 181px;">
                      <a href="<?php echo base_url(); ?>panel/controller/dodaj_kategorie/<?php echo $this->uri->segment(4); ?>">
                          <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2">
                              <i class="fas fa-plus mt-0"></i>
                          </button>
                      </a>
			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4">

			    <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
				            <th style="width: 10%;"></th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 50%;">
				              Tytuł
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(1)" style="width: 20%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->

				        <tbody>
						  	<?php foreach ($rows as $key): ?>
						  		<tr>
                                    <td></td>
						            <td class="align-middle"><?php echo $key->name; ?></td>
						            <td class="align-middle"><?php echo $key->created; ?></td>
						            <td class="align-middle text-right">
						              <a href="<?php echo base_url(); ?>panel/controller/wpisy/<?php echo $this->uri->segment(4); ?>/<?php echo $key->id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="far fa-eye mt-0"></i>
									    </button>
									  </a>
						              <a href="<?php echo base_url(); ?>panel/controller/edytuj_kategorie/<?php echo $this->uri->segment(4); ?>/<?php echo $key->id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="fas fa-pencil-alt mt-0"></i>
									    </button>
									  </a>
									  <a href="<?php echo base_url(); ?>panel/controller/usun_kategorie/<?php echo $this->uri->segment(4); ?>/<?php echo $key->id; ?>">
									      <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									      </button>
								      </a>
								  	</td>
				            	</tr>
							<?php endforeach; ?>
				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
</main>
