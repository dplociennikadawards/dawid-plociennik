<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj #<?php echo $row->id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="container">
                
                    <?php echo validation_errors(); ?>
                   <div class="row px-5">
                       <div class="col-md-12">

                           <div class="md-form mb-5">
                               <input type="text" id="formName" class="form-control" name="name" value="<?php echo $row->name; ?>" required>
                               <label for="formName">Nazwa<span class="text-danger">*</span></label>
                           </div>

                       </div>

                   </div>

        </div>
    </section>
    </form>
</main>
