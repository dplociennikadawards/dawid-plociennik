<main class="main-section water-logo">
	<?php echo form_open_multipart();?>
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container-fluid px-md-5">
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Dodaj nowy glowna</a>

			    <div class="text-right" style="width: 181px;">
				      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
				        Zapisz
				      </button>
			    </div>

			  </div>
			   <div class="row px-5">
			   	<div class="col-md-8 offset-md-2">
			   	<?php echo validation_errors(); ?>
					<div class="md-form mb-5">
                    	<textarea name="content" id="editor"></textarea>
                	</div>
				</div>
			</div>
		</div>
	</section>
	</form>
</main>

