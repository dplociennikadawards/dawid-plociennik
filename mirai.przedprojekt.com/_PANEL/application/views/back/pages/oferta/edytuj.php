<main class="main-section water-logo">
    <?php echo form_open_multipart('', 'id="specialform"');?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Edytuj - <?php echo $row->title; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-6">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formTitle" class="form-control" name="title" value="<?php echo $row->title; ?>" required>
                      <label for="formTitle">Tytuł <span class="text-danger">*</span></label>
                    </div>
                    <div class="md-form">
                      <textarea name="content" id="editor"><?php echo $row->content; ?></textarea>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="md-form mb-5">
                      <div class="file-field">
                        <div id="photo_show" class="text-center">
                            <img src="<?php echo base_url(); ?>uploads/oferta/<?php echo $this->slugify_m->slugify($row->title); ?>/<?php echo $row->photo; ?>" class="img-fluid img-thumbnail" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie</span>
                          <input type="file" name="photo" id="photo">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie główne" value="" required readonly>
                        </div>
                      </div>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formAlt2" class="form-control" name="alt" value="<?php echo $row->alt; ?>">
                      <label for="formAlt2">Tekst alternatywny zdjęcia</label>
                    </div>
                    <div class="md-form mb-5">
                    <div class="file-field">
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                            <span>Stwórz galerie</span>
                            <input type="file" name="gallery[]" id="gallery" multiple="multiple">
                        </div>
                        <div class="file-path-wrapper">
                            <input name="name_gallery" class="file-path validate" type="text" value="<?php echo $row->gallery; ?>" placeholder="Wybierz zdjęcia do galerii" required readonly>
                        </div>
                    </div>
                </div>
                    <div class="md-form mb-5">
                    <div class="file-field">
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                            <span>Dodaj zdjęcia do galerii</span>
                            <input type="file" name="gallery_add[]" id="gallery_add" multiple="multiple">
                        </div>
                        <div class="file-path-wrapper">
                            <input name="name_gallery_add" class="file-path validate" type="text" value="" placeholder="Wybierz zdjęcia do galerii" required readonly>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-md-6">

                  <div class="md-form mb-5">
                      <select class="form-control" name="category" style="display:block !important" required>
                        <option value="" disabled selected>Wybierz kategorię <span class="text-danger">*</span></option>
                        <option <?php if($row->category == 'mieszkania'){echo 'selected';} ?> value="mieszkania">Mieszkania</option>
                        <option <?php if($row->category == 'domy'){echo 'selected';} ?> value="domy">Domy</option>
                        <option <?php if($row->category == 'dzialki'){echo 'selected';} ?> value="dzialki">Działki</option>
                        <option <?php if($row->category == 'dzialki_budowlane'){echo 'selected';} ?>  value="dzialki_budowlane">Działki budowlane</option>
                        <option <?php if($row->category == 'dzialki_komercyjne'){echo 'selected';} ?>  value="dzialki_komercyjne">Działki komercyjne</option>
                        <option <?php if($row->category == 'dzialki_rekreacyjne'){echo 'selected';} ?>  value="dzialki_rekreacyjne">Działki rekreacyjne</option>
                        <option <?php if($row->category == 'dzialki_inwestycyjne'){echo 'selected';} ?>  value="dzialki_inwestycyjne">Działki inwestycyjne</option>
                        <option <?php if($row->category == 'lokale_uzytkowe'){echo 'selected';} ?> value="lokale_uzytkowe">Lokale użytkowe</option>
                        <option <?php if($row->category == 'hale_i_magazyny'){echo 'selected';} ?> value="hale_i_magazyny">Hale i magazyny</option>
                        <option <?php if($row->category == 'garaz'){echo 'selected';} ?> value="garaz">Garaże</option>
                      </select> 
                    </div>
                    <div class="md-form mb-5">
                      <select class="form-control" name="type" style="display:block !important" required>
                        <option value="" disabled selected>Wybierz typ <span class="text-danger">*</span></option>
                        <option <?php if($row->type == 'sprzedaz'){echo 'selected';} ?> value="sprzedaz">Na sprzedaż</option>
                        <option <?php if($row->type == 'wynajem'){echo 'selected';} ?> value="wynajem">Wynajem</option>
                        <option <?php if($row->type == 'inwestycje'){echo 'selected';} ?> value="inwestycje">Inwestycje</option>
                        <option <?php if($row->type == 'zamiana'){echo 'selected';} ?> value="zamiana">Zamiana</option>
                      </select>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="rooms" class="form-control" name="rooms" value="<?php echo $row->rooms; ?>">
                      <label for="rooms">Liczba pokoi</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="pietro" class="form-control" name="pietro" value="<?php echo $row->pietro; ?>">
                      <label for="pietro">Piętro</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="bathroom" class="form-control" name="bathroom" value="<?php echo $row->bathroom; ?>">
                      <label for="bathroom">Liczba łazienek</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="toilet" class="form-control" name="toilet" value="<?php echo $row->toilet; ?>">
                      <label for="toilet">Liczba toalet</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="area" class="form-control" name="area" step="0.01" value="<?php echo $row->area; ?>">
                      <label for="area">Powierzchnia całkowita [m<sup>2</sup>]</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="usable_area" class="form-control" name="usable_area" step="0.01" value="<?php echo $row->usable_area; ?>">
                      <label for="usable_area">Powierzchnia użytkowa [m<sup>2</sup>]</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="powierzchnia_dzialki" class="form-control" name="powierzchnia_dzialki" step="0.01" value="<?php echo $row->powierzchnia_dzialki; ?>">
                      <label for="powierzchnia_dzialki">Powierzchnia działki [m<sup>2</sup>]</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="heating" class="form-control" name="heating" value="<?php echo $row->heating; ?>">
                      <label for="heating">Źródło ogrzewania</label>
                    </div>
                    
                    <div class="md-form mb-5">
                      <input type="text" id="hot_water" class="form-control" name="hot_water" value="<?php echo $row->hot_water; ?>">
                      <label for="hot_water">Źródło ciepłej wody</label>
                    </div>


                </div>
                <div class="col-md-6">

                    <div class="md-form mb-5">
                      <input type="text" id="state" class="form-control" name="state" value="<?php echo $row->state; ?>">
                      <label for="state">Stan ogólny</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="charges" class="form-control" name="charges" value="<?php echo $row->charges; ?>">
                      <label for="charges">Opłaty</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="reference" class="form-control" name="reference" value="<?php echo $row->reference; ?>">
                      <label for="reference">Numer referencyjny</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="city" class="form-control" name="city" value="<?php echo $row->city; ?>">
                      <label for="city">Miejscowość</label>
                    </div>
                    
                    <div class="md-form mb-5">
                      <input type="text" id="address" class="form-control" name="address" value="<?php echo $row->address; ?>">
                      <label for="address">Adres</label>
                    </div>

                    <div class="mb-5">
                      <label for="additional">Dodatkowe wyposażenie</label>
                      <input type="text" id="additional" class="form-control" name="additional" value="<?php echo $row->additional; ?>" data-role="tagsinput" required>
                    </div>

                    <div class="md-form mb-5">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="negotiation" name="negotiation" <?php if($row->negotiation == 1){echo 'checked';} ?>>
                        <label class="form-check-label" for="negotiation">Do negocjacji</label>
                    </div>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="price" class="form-control" name="price" value="<?php echo $row->price; ?>" required>
                      <label for="price">Cena <span class="text-danger">*</span></label>
                    </div>

                </div>
               </div>
            </div>
        </div>
    </section>
    </form>
</main>


