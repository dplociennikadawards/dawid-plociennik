<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;"> Dodaj nowy </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-12">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="url" class="form-control" name="url" value="<?php echo $row->url; ?>" required>
                      <label for="url">Adres URL <span class="text-danger">*</span></label>
                    </div>
                </div>

                <div class="col-md-12">
                  <div class="md-form mb-5">
                      <input type="text" id="client_id" class="form-control" name="client_id" value="<?php echo $row->client_id; ?>" required>
                      <label for="client_id">Identyfikator klienta <span class="text-danger">*</span></label>
                    </div>
                </div>

                <div class="col-md-12">
                  <div class="md-form mb-5">
                      <input type="text" id="secret_key" class="form-control" name="secret_key" value="<?php echo $row->secret_key; ?>" required>
                      <label for="secret_key">Sekretny klucz <span class="text-danger">*</span></label>
                    </div>
                </div>


                <div class="col-md-12">
                  <div class="md-form mb-5">
                      <input type="text" id="name_account" class="form-control" name="name_account" value="<?php echo $row->name_account; ?>" required>
                      <label for="name_account">Nazwa konta <span class="text-danger">*</span></label>
                    </div>
                </div>


                <div class="col-md-12">
                  <div class="md-form mb-5">
                      <input type="text" id="login" class="form-control" name="login" value="<?php echo $row->login; ?>" required>
                      <label for="login">Login <span class="text-danger">*</span></label>
                    </div>
                </div>


                <div class="col-md-12">
                  <div class="md-form mb-5">
                      <input type="text" id="password" class="form-control" name="password" value="<?php echo $row->password; ?>" required>
                      <label for="password">Hasło <span class="text-danger">*</span></label>
                    </div>
                </div>

               </div>
            </div>
        </div>
    </section>
    </form>
</main>
