<main class="main-section p-md-3 main-background" style="height: 100vh; display: grid; align-items: center;">
  <section>
  
    <div class="container">
      
    <div class="row">

      <div class="col-lg-4 text-center my-3">

      </div>

      <div class="col-lg-4 text-center my-3">

        <div class="card card-cascade narrower mt-5">

          <div class="view view-cascade gradient-card-header narrower py-2 mx-4 mb-3 d-flex row d-flex justify-content-center bg-dark">

            <div class="py-2">
              <a href="" class="white-text mx-3">Logowanie</a>
            </div>

          </div>

          <div class="px-4">

             <form method="POST" action="" class="md-form" style="color: #757575;">
                <?php if($this->session->flashdata('flashdata')) {
                  echo $this->session->flashdata('flashdata');
                } ?>
              <div class="md-form login-form">
                <input name="login" type="text" id="login" class="form-control" pattern="[A-Za-z]{3,}" title="Minimum 3 znaki" required>
                <label for="login">Login</label>
              </div>

              <div class="md-form login-form">
                <input name="password" type="password" id="password" class="form-control" required>
                <label for="password">Hasło</label>
              </div>
              
              <div class="row d-flex justify-content-center">
              <button type="submit" class="btn btn-elegant bg-dark">Zaloguj</button>
              </div>

             </form>

          </div>

      </div>

      </div>

      <div class="col-lg-4 text-center my-3">

      </div>

    </div>

    </div>
  </section>

</main>




