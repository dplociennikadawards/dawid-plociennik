<main class="main-section water-logo">
  <section>
    <div class="container-fluid">

          <div class="row p-3">

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/glowna">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/home.svg');"></div>Strona główna
                </button>
              </a>
            </div>

            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/slider">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/image.svg');"></div>Slider
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/podstrony">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/folder.svg');"></div>Podstrony
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/zarejestrowani">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/teamwork.svg');"></div>Zarejestrowani
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/cars">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/car(1).svg');"></div>Samochody
                </button>
              </a>
            </div>
            <?php endif; ?>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/otomoto">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/otomoto.png');"></div>Otomoto
                </button>
              </a>
            </div>
            <?php endif; ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/finansowanie">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/money-bag.svg');"></div>Finansowanie i ubezpieczenia
                </button>
              </a>
            </div>

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/gwarancja">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/guarantee.svg');"></div>Gwarancja
                </button>
            </a>
            </div>  

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/program_importerski">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/upload.svg');"></div>Program importerski
                </button>
            </a>
            </div>

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/o_firmie">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/social-care.svg');"></div>O firmie
                </button>
            </a>
            </div> 

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/salony">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/enterprise.svg');"></div>Salony
                </button>
              </a>
            </div>

            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/pracownicy">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/man.svg');"></div>Pracownicy
                </button>
              </a>
            </div>

            <!-- <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/gallery/dodaj/galeria/1">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/factory.svg');"></div>{{Tworzenie galerii}}
                </button>
              </a>
            </div> -->


<!--             <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/controller/index/newsletter">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/newsletter.svg');"></div>Newsletter
                </button>
            </a>
            </div> -->
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel//kontakt">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/email.svg');"></div>Kontakt
                </button>
            </a>
            </div>
            <?php if($_SESSION['rola'] == 'admin'): ?>
            <div class="col-lg-3 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/ustawienia">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/settings.svg');"></div>Ustawienia
                </button>
            </a>
            </div>
            <?php endif; ?>

          </div>

        </div>
  </section>
</main>