<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

// KATEGORIE
public function index($page) {

    $data['rows'] = $this->base_m->get_category('category', $page);

    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/category/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj_kategorie($page) {
    $this->form_validation->set_rules('name', 'Nazwa', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/category/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

        $insert['name'] = $this->input->post('name');
        $insert['page'] = $page;

        $this->base_m->insert('category', $insert);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Kategoria została dodana!</p>');
        redirect('panel/controller/index/'.$page);

        }
}

    public function edytuj_kategorie($page, $id) {
        $data['row'] = $this->base_m->get_record2('category', $id);

        $this->form_validation->set_rules('name', 'Nazwa', 'min_length[2]|trim|required');
        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
        if ($this->form_validation->run() == FALSE) {   
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/category/edytuj', $data);
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');
        } else {

            $insert['name'] = $this->input->post('name');

        $this->base_m->update2('category', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Kategoria została edytowana</p>');
        redirect('panel/controller/index/'.$page);
        }
    }


    public function usun_kategorie($page, $id) {
        $this->base_m->drop_record2($id, 'category');
        $this->base_m->drop_wpisy($id, 'wpisy');
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś rekord!</p>');
        redirect('panel/controller/index/'.$page);
    }
// KATEGORIE


// WPISY
public function wpisy($page, $category) {

    $data['rows'] = $this->base_m->get_wpisy($category);

    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/wpisy/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj_wpis($page, $id_category) {
    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/wpisy/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

        $slug = $this->slugify_m->slugify($this->input->post('title'));
        $now = date('Y-m-d');
        if (!is_dir('uploads/'.$slug.'/'.$now)) {
            mkdir('./uploads/'.$slug.'/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/'.$slug.'/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        foreach ($_POST as $key => $value) {
            if($key == 'name_photo') {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }  elseif($value == 'usunięte') {
                    $insert['photo'] = '';
                }
            } elseif($key == 'name_photo2') {
                if ($this->upload->do_upload('photo2')) {
                    $data = $this->upload->data();
                    $insert['photo2'] = $now.'/'.$data['file_name'];        
                }  elseif($value == 'usunięte') {
                    $insert['photo2'] = '';
                }
            }   
            elseif($key == 'name_gallery'){
                if($this->input->post('name_gallery') != null && $_FILES ['gallery'] != null) {
                    $files = $_FILES;
                    $cpt = count($_FILES ['gallery'] ['name']);
                    for ($i = 0; $i < $cpt; $i ++) {
                        $name = $files ['gallery'] ['name'] [$i];
                        $_FILES ['gallery'] ['name'] = $name;
                        $_FILES ['gallery'] ['type'] = $files ['gallery'] ['type'] [$i];
                        $_FILES ['gallery'] ['tmp_name'] = $files ['gallery'] ['tmp_name'] [$i];
                        $_FILES ['gallery'] ['error'] = $files ['gallery'] ['error'] [$i];
                        $_FILES ['gallery'] ['size'] = $files ['gallery'] ['size'] [$i];
                        if(!($this->upload->do_upload('gallery')) || $files ['gallery'] ['error'] [$i] !=0) {
                            print_r($this->upload->display_errors());
                        }
                        $insert['gallery'] = $this->input->post('name_gallery'); 
                    }
                }  elseif($value == '') {
                    $insert['gallery'] = '';
                }
            }
            else {
                $insert[$key] = $value;   
            }
        }
        $insert['page'] = $page;
        $insert['category'] = $id_category;
        $insert['slug'] = $slug;

        $this->base_m->insert('wpisy', $insert);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
        redirect('panel/controller/wpisy/'.$page.'/'.$id_category);

        }
}

    public function edytuj_wpis($page, $id, $id_category) {
        $data['row'] = $this->base_m->get_record2('wpisy', $id);

        $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
        if ($this->form_validation->run() == FALSE) {   
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/wpisy/edytuj', $data);
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');
        } else {

        $slug = $this->slugify_m->slugify($this->input->post('title'));
        $now = date('Y-m-d');
        if (!is_dir('uploads/'.$slug.'/'.$now)) {
            mkdir('./uploads/'.$slug.'/' . $now, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/'.$slug.'/'.$now;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        foreach ($_POST as $key => $value) {
            if($key == 'name_photo') {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                } elseif($value == 'usunięte') {
                    $insert['photo'] = '';
                }
            } elseif($key == 'name_photo2') {
                if ($this->upload->do_upload('photo2')) {
                    $data = $this->upload->data();
                    $insert['photo2'] = $now.'/'.$data['file_name'];        
                }  elseif($value == 'usunięte') {
                    $insert['photo2'] = '';
                }
            }   
            elseif($key == 'name_gallery'){
                if($this->input->post('name_gallery') != null && $_FILES ['gallery'] != null) {
                    $files = $_FILES;
                    $cpt = count($_FILES ['gallery'] ['name']);
                    for ($i = 0; $i < $cpt; $i ++) {
                        $name = $files ['gallery'] ['name'] [$i];
                        $_FILES ['gallery'] ['name'] = $name;
                        $_FILES ['gallery'] ['type'] = $files ['gallery'] ['type'] [$i];
                        $_FILES ['gallery'] ['tmp_name'] = $files ['gallery'] ['tmp_name'] [$i];
                        $_FILES ['gallery'] ['error'] = $files ['gallery'] ['error'] [$i];
                        $_FILES ['gallery'] ['size'] = $files ['gallery'] ['size'] [$i];
                        if(!($this->upload->do_upload('gallery')) || $files ['gallery'] ['error'] [$i] !=0) {
                            print_r($this->upload->display_errors());
                        }
                        $insert['gallery'] = $this->input->post('name_gallery'); 
                    }
                }  elseif($value == '') {
                    $insert['gallery'] = '';
                }
            }
            else {
                $insert[$key] = $value;   
            }
        }
        $insert['slug'] = $slug;
        
        $this->base_m->update2('wpisy', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został edytowany</p>');
        redirect('panel/controller/wpisy/'.$page.'/'.$id_category);
        }
    }


    public function usun_wpis($page, $id, $id_category) {
        $this->base_m->drop_record2($id, 'wpisy');
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś rekord!</p>');
        redirect('panel/controller/wpisy/'.$page.'/'.$id_category);
    }
// WPISY


}


