<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

	public function index() {
		$data['rows'] = $this->base_m->get('slider');
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/slider/index', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}
	public function dodaj() {
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|is_unique[slider.photo]|required');
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/slider/dodaj');
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {

            $now = date('Y-m-d');
	        if (!is_dir('uploads/slider/'.$now)) {
		    	mkdir('./uploads/slider/' . $now, 0777, TRUE);
			}

            $config['upload_path'] = './uploads/slider/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }
            }

            $data = $this->upload->data(); 
			$insert['title'] = $this->input->post('title');
			$insert['subtitle'] = $this->input->post('subtitle');
			$insert['button_text'] = $this->input->post('button_text');
			$insert['button_link'] = $this->input->post('button_link');
			$insert['alt'] = $this->input->post('alt');

			$this->base_m->insert('slider', $insert);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
			redirect('panel/slider');
		}
	}

	public function edytuj($id) {
		$data['row'] = $this->base_m->get_record('slider', $id);
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|required');
		$this->form_validation->set_rules('title', 'tytuł', 'min_length[2]|trim|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/slider/edytuj', $data);
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/slider/'.$now)) {
		    	mkdir('./uploads/slider/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/slider/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $now.'/'.$data['file_name'];        
                }
            }
    		
			$insert['title'] = $this->input->post('title');
			$insert['subtitle'] = $this->input->post('subtitle');
			$insert['button_text'] = $this->input->post('button_text');
			$insert['button_link'] = $this->input->post('button_link');
			$insert['alt'] = $this->input->post('alt');

			$this->base_m->update('slider', $insert, $id);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
			redirect('panel/slider');
		}
	}
}


