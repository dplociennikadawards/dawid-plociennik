<?php defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	public function index() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/index');
		$this->load->view('back/blocks/footer');
	}
	public function glowna() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/glowna');
		$this->load->view('back/blocks/footer');
	}

	public function podstrony() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/podstrony');
		$this->load->view('back/blocks/footer');
	}

	public function wpis() {
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/wpis');
		$this->load->view('back/blocks/footer');
	}

	public function upPriority() {
		$data['row'] = $this->base_m->get_record($_POST['table'], $_POST['id']);
		$data['check'] = $this->base_m->priority_ASC($_POST['table']);

		$new_priority = $data['row']->priority;

		foreach ($data['check'] as $key) {
			if($new_priority < $key->priority)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->realizacje_id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where($_POST['table'].'_id', $_POST['id']);
				$this->db->update($_POST['table'], $update);		
		$update2['priority'] = $old_priority;
				$this->db->where($_POST['table'].'_id', $old_id);
				$this->db->update($_POST['table'], $update2);	
	}

	public function downPriority() {
		$data['row'] = $this->base_m->get_record($_POST['table'], $_POST['id']);
		$data['check'] = $this->base_m->priority_DESC($_POST['table']);
		print_r($data['check']);
		$new_priority = $data['row']->priority;
		foreach($data['check'] as $key) {
			if($new_priority > $key->priority)
				
				{
					$old_priority = $new_priority;
					$old_id = $key->realizacje_id;
					$new_priority = $key->priority;
					break;
			}
		}

		$update['priority'] = $new_priority;
				$this->db->where($_POST['table'].'_id', $_POST['id']);
				$this->db->update($_POST['table'], $update);		
		$update2['priority'] = $old_priority;
				$this->db->where($_POST['table'].'_id', $old_id);
				$this->db->update($_POST['table'], $update2);	
	}

	public function active() {
		$update['active'] = $this->input->post('value');
        $query = $this->base_m->update($this->input->post('table'), $update, $this->input->post('id'));
        return $query;
	}

	public function drop_record($id,$table) {
		$this->base_m->drop_record($id, $table);
		$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Pomyślnie usunąłeś wpis!</p>');
		if($table == 'users') { $table = 'zarejestrowani';}
		redirect('panel/'.$table);
	}
}
