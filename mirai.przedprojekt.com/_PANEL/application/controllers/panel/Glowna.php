<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Glowna extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

    public function index() {
        $data['rows'] = $this->base_m->get('glowna');
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/glowna/index', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    }
    public function dodaj() {
        $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
        if ($this->form_validation->run() == FALSE) {   
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/glowna/dodaj');
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');
        } else {

            $insert['content'] = $this->input->post('content');

            $this->base_m->insert('glowna', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
            redirect('panel/glowna');
        }
    }

    public function edytuj($id) {
        $data['row'] = $this->base_m->get_record('glowna', $id);
        $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
        if ($this->form_validation->run() == FALSE) {   
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/glowna/edytuj', $data);
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');
        } else {

            $insert['content'] = $this->input->post('content');

            $this->base_m->update('glowna', $insert, $id);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
            redirect('panel/glowna');
        }
    }
}


