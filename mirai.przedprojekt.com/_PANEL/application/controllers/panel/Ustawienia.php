<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ustawienia extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($_SESSION['rola'] != 'admin'){
            redirect('panel');
        }
    }

	public function index() {
		$data['row'] = $this->base_m->get_record('ustawienia', 1);
		$this->form_validation->set_rules('meta_title', 'Tytuł strony', 'trim|required');
		$this->form_validation->set_rules('name_photo', 'Logo', 'trim|required');
		$this->form_validation->set_rules('content', 'Wiadomości cookies', 'trim|required');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/ustawienia/index', $data);
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/settings/'.$now)) {
		    	mkdir('./uploads/settings/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/settings/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
    		if($this->input->post('name_photo') != null) {
	            if ($this->upload->do_upload('photo')) {
		            $data = $this->upload->data();
	            	$update['logo'] = $now.'/'.$data['file_name'];        
	            }
    		}


    		if($this->input->post('name_pdf') != null) {
		        if ($this->upload->do_upload('pdf')) {
		            $data = $this->upload->data();
	            	$update['pdf'] = $now.'/'.$data['file_name'];         
		        }
			}

			$update['meta_title'] = $this->input->post('meta_title');
			$update['cookies'] = $this->input->post('content');
			$update['fb_link'] = $this->input->post('fb_link');
			$update['inst_link'] = $this->input->post('inst_link');
			$update['yt_link'] = $this->input->post('yt_link');
			$update['cars_per_page'] = $this->input->post('cars_per_page');

			

			$this->base_m->update('ustawienia', $update, 1);
			$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Ustawienia zostały zaktualizowane!</p>');
			redirect('panel/ustawienia');
		}
	}
}
