<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Records extends CI_Controller {

	public function index() {

		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}
			//echo $filtr;

		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'
		');

		//echo 'ZAPYTANIE: SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'';

		$data['rows'] = $query->result();
		$this->load->view('front/elements/records', $data);
	}

	public function make() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr);


		$data['rows'] = $query->result();
		echo '<option value="" >marka</option>';

		foreach ($data['rows'] as $value) {
			echo '<option value="'.$value->make.'" >'.ucfirst($value->make).'</option>';
		}
	}
	public function model() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' GROUP BY model');


		$data['rows'] = $query->result();
		echo '<option value="" >model</option>';
		foreach ($data['rows'] as $value) {
			echo '<option value="'.$value->model.'" >'.ucfirst($value->model).'</option>';
		}
	}
	public function rok_od() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' GROUP BY year');


		$data['rows'] = $query->result();
		$selected = '';
		echo '<option value="" >od</option>';
		foreach ($data['rows'] as $value) {
			if($value->year == $_GET['rok_od']){
				$selected = 'selected';
			} else {
				$selected = '';
			}
			echo '<option '.$selected.' value="'.$value->year.'" >'.$value->year.'</option>';
		}
	}
	public function rok_do() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' GROUP BY year');


		$data['rows'] = $query->result();

		echo '<option value="" >do</option>';
		foreach ($data['rows'] as $value) {
			if(isset($_GET['rok_od']) && $_GET['rok_od'] < $value->year) {
				echo '<option value="'.$value->year.'" >'.$value->year.'</option>';
			} else {
				echo '<option value="'.$value->year.'" >'.$value->year.'</option>';
			}
		}
	}
	public function body_type() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' GROUP BY body_type');


		$data['rows'] = $query->result();

		$nadwozie['city-car'] = 'Auto miejskie'; 
		$nadwozie['combi'] = 'Kombi'; 
		$nadwozie['compact'] = 'Kompakt'; 
		$nadwozie['minivan'] = 'Minivan'; 
		$nadwozie['panel-van'] = 'Furgon'; 
		$nadwozie['sedan'] = 'Sedan'; 
		$nadwozie['suv'] = 'SUV'; 
		$nadwozie['vans'] = 'Samochód dostawczy'; 

		echo '<option value="" >typ nadwozia</option>';
		foreach ($data['rows'] as $value) {
			echo '<option value="'.$value->body_type.'" >'.$nadwozie[$value->body_type].'</option>';
		}
	}
	public function fuel_type() {
		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}


		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' GROUP BY fuel_type');

		$paliwo['diesel'] = 'Diesel'; 
		$paliwo['petrol'] = 'Benzyna'; 

		$data['rows'] = $query->result();

		echo '<option value="" >rodzaj paliwa</option>';
		foreach ($data['rows'] as $value) {
			echo '<option value="'.$value->fuel_type.'" >'.$paliwo[$value->fuel_type].'</option>';
		}
	}

	public function pagination() {

		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}
			if(isset($_GET['nr_page']) && !empty($_GET['nr_page']) || $_GET['nr_page'] == 0){
				$filtr .= '`samochody_id` >= ' . ($_GET['nr_page']+1).' AND ';
				$filtr .= '`samochody_id` <= ' . ($_GET['nr_page']+10).' AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}
			//echo $filtr;

		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'
		');

		//echo 'ZAPYTANIE: SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'';

		$data['rows'] = $query->result();
		$this->load->view('front/elements/records', $data);
	}

	public function paginpage() {

		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}
			//echo $filtr;

		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].'
		');

		//echo 'ZAPYTANIE: SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'';

		$data['rows'] = $query->result();
		//echo count($data['rows']);
		$output = '';
        for ($i = 0; $i < (ceil(count($data['rows'])/$data['ustawienia']->cars_per_page)); $i++) {

                $output .= '<li id="activePagination'.($i*$data['ustawienia']->cars_per_page).'" class="list__pagination-item ';


                if($i==0){$output .= ' list__pagination-item--active ';}

                $output .= ' page-item"><a id="linkPagination'.($i*$data['ustawienia']->cars_per_page).'" class=" list__pagination-link ';

                if($i==0){$output .= ' list__pagination-link--active ';}

                 $output .= ' page-link " onclick="pagination('.($i*$data['ustawienia']->cars_per_page).')">'.($i+1).'</a></li>';

        }
                 echo $output;


	}

	public function paginpage2() {

		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$filtr = '';
			if(isset($_GET['make']) && !empty($_GET['make'])){
				$filtr .= '`make` = "' . $_GET['make'].'" AND ';
			}
			if(isset($_GET['model']) && !empty($_GET['model'])){
				$filtr .= '`model` = "' . $_GET['model'].'" AND ';
			}
			if(isset($_GET['body_type']) && !empty($_GET['body_type'])){
				$filtr .= '`body_type` = "' . $_GET['body_type'].'" AND ';
			}
			if(isset($_GET['fuel_type']) && !empty($_GET['fuel_type'])){
				$filtr .= '`fuel_type` = "' . $_GET['fuel_type'].'" AND ';
			}
			if(isset($_GET['rok_od']) && !empty($_GET['rok_od'])){
				$filtr .= '`year` >= "' . $_GET['rok_od'].'" AND ';
			}
			if(isset($_GET['rok_do']) && !empty($_GET['rok_do'])){
				$filtr .= '`year` <= "' . $_GET['rok_do'].'" AND ';
			}


			if($filtr != '') {
				$filtr=rtrim($filtr," AND ");
				$filtr = 'WHERE ' . $filtr;
			}
			//echo $filtr;

		$query = $this->db->query('
			SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].'
		');

		//echo 'ZAPYTANIE: SELECT * FROM `samochody` '.$filtr.' ORDER BY '.$_GET['sort_col'].' '.$_GET['how_sort'].' LIMIT '.$data['ustawienia']->cars_per_page.'';

		$data['rows'] = $query->result();
		//echo count($data['rows']);
		$output = '';
        for ($i = 0; $i < (ceil(count($data['rows'])/$data['ustawienia']->cars_per_page)); $i++) {

                $output .= '<li id="activePaginationF'.($i*$data['ustawienia']->cars_per_page).'" class="list__pagination-item ';


                if($i==0){$output .= ' list__pagination-item--active ';}

                $output .= ' page-item"><a id="linkPaginationF'.($i*$data['ustawienia']->cars_per_page).'" class=" list__pagination-link ';

                if($i==0){$output .= ' list__pagination-link--active ';}

                 $output .= ' page-link " onclick="pagination('.($i*$data['ustawienia']->cars_per_page).')">'.($i+1).'</a></li>';

        }
                 echo $output;


	}
}
