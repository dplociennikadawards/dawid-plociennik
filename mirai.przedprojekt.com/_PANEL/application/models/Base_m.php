<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_m extends CI_Model  
{
    public function get($table) {
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_category($table,$page) {
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_page($table,$page) {
        $this->db->where(['page' => $page]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_wpisy($category) {
        $this->db->where(['category' => $category]);
        $query = $this->db->get('wpisy');
        return $query->result();
    }

    public function recommended($table) {
        $this->db->where(['active' => 1]);
        $query = $this->db->get($table);
        return $query->result();
    }

        
    public function distinct($table, $group) {
        $this->db->group_by($group);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function special_filters($table, $filtr) {
        $query = $this->db->query("SELECT * FROM oferta WHERE ".$filtr);
        return $query->result();
    }


    public function max($table, $max) {
        $this->db->select_max($max);
        $query = $this->db->get($table);
        return $query->row();
    }


    public function min($table, $min) {
        $this->db->select_min($min);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_active($table) {
        $this->db->where(['active' => '1']);
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_record($table,$id) {
        $this->db->where([$table.'_id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_record2($table,$id) {
        $this->db->where(['id' => $id]);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function get_offers($table,$slug) {
        $this->db->where(['category' => $slug]);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function category_title($slug,$id) {
        $this->db->where(['slug' => $slug]);
        $query = $this->db->get('kategorie');
        return $query->row();
    }

    public function insert($table,$data) {
        $insert_query = $this->db->insert($table, $data);
        return $insert_query;
    }


    public function update($table,$data,$id) {
        $this->db->where([$table.'_id' => $id]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }

    public function update2($table,$data,$id) {
        $this->db->where(['id' => $id]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }

    public function drop_record($id,$table) {
        $this->db->where([$table.'_id' => $id]);
        $this->db->delete($table);
    }


    public function drop_record2($id,$table) {
        $this->db->where(['id' => $id]);
        $this->db->delete($table);
    }

    public function drop_wpisy($id,$table) {
        $this->db->where(['category' => $id]);
        $this->db->delete($table);
    }

    public function priority_ASC($table) {
        $this->db->order_by('priority', 'ASC');
        $query = $this->db->get($table);
        
        return $query->result();        
    }

    public function priority_DESC($table) {
        $this->db->order_by('priority', 'DESC');
        $query = $this->db->get($table);
        
        return $query->result();        
    }

    public function get_photos() {
        $this->db->where(['item_id' => 1]);
        $this->db->where(['table_name' => 'galeria']);
        $query = $this->db->get('gallery');
        
        return $query->result();        
    }

    public function get_gallery($table,$item_id) {
        $this->db->where(['item_id' => $item_id]);
        $this->db->where(['table_name' => $table]);
        $query = $this->db->get('gallery');
        
        return $query->result();        
    }

    public function get_offer($table,$item_id) {
        $this->db->where(['category_id' => $item_id]);
        $query = $this->db->get('oferta');
        
        return $query->result();        
    }


    public function get_terminy($zabieg_id) {
        $this->db->where(['oferta_id' => $zabieg_id]);
        $query = $this->db->get('grafik');
        return $query->row();  
    }


    public function active_reserv($table, $reserv, $data) {
        $this->db->where(['hash' => $reserv]);
        $update_query = $this->db->update($table, $data);
        return $update_query;
    }


    public function get_pracownicy($zabieg_id, $date) {
        $this->db->where([
            'oferta_id' => $zabieg_id,
        ]);
        $this->db->like('free_dates', $date);
        $query = $this->db->get('grafik');
        return $query->row();  
    }




}