<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {

	public function index() {

		$data['rows'] = $this->base_m->get('glowna');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['o_firmie'] = $this->base_m->get_record('o_firmie', 1);
		$data['recommended'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);


		$data['category'] = $this->base_m->distinct('oferta', 'category');
		$data['type'] = $this->base_m->distinct('oferta', 'type');
		$data['city'] = $this->base_m->distinct('oferta', 'city');

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/header', $data);
		$this->load->view('front/index', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function o_firmie() {
		$data['rows'] = $this->base_m->get('o_firmie');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/o_firmie', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function posrednictwo() {
		$data['rows'] = $this->base_m->get('posrednictwo');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/posrednictwo', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function zarzadzanie_nieruchomosciami() {
		$data['rows'] = $this->base_m->get('zarzadzanie_nieruchomosciami');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/zarzadzanie_nieruchomosciami', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function wspolnota() {
		$data['rows'] = $this->base_m->get('wspolnota');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/wspolnota', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function weles() {
		$data['rows'] = $this->base_m->get('weles');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/weles', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function kontakt() {
		$data['rows'] = $this->base_m->get('kontakt');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['oferta'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/kontakt', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function oferty() {
		$data['rows'] = $this->base_m->get('oferta');
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['recommended'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/oferty', $data);
		$this->load->view('front/blocks/footer', $data);
	}

	public function oferta($slug, $id) {
		$data['row'] = $this->base_m->get_record('oferta', $id);
		$data['slider'] = $this->base_m->get('slider');
		$data['oferta'] = $this->base_m->get('oferta');
		$data['recommended'] = $this->base_m->recommended('oferta');
		$data['kontakt'] = $this->base_m->get_record('contact_settings', 1);
		$data['ustawienia'] = $this->base_m->get_record('ustawienia', 1);

		$this->load->view('front/blocks/head', $data);
		$this->load->view('front/blocks/subheader', $data);
		$this->load->view('front/oferta', $data);
		$this->load->view('front/blocks/footer', $data);
	}
	
}
