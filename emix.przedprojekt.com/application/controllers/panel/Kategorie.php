<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kategorie extends CI_Controller
{
    public function index()
    {
        $data['rows'] = $this->base_m->get('kategorie');
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/kategorie/index', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    }

    public function dodaj() {

        $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|is_unique[kategorie.title]|required');
        $this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|is_unique[kategorie.photo]|required');
        $this->form_validation->set_rules('name_photo2', 'Zdjęcie', 'min_length[2]|trim|is_unique[kategorie.photo]|required');
        $this->form_validation->set_rules('name_header_photo', 'Ikona', 'min_length[2]|trim|is_unique[kategorie.photo]|required');


        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/kategorie/dodaj');
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');
        } else {
            $now = date('Y-m-d');
            if (!is_dir('uploads/kategorie/'.$now)) {
                mkdir('./uploads/kategorie/' . $now, 0777, TRUE);
            }
            $config['upload_path'] = './uploads/kategorie/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;

            $this->load->library('upload',$config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('photo')) {
                $this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
                redirect('panel/kategorie/dodaj');            
            } else {
                $data = $this->upload->data();
                $insert['photo'] = $now.'/'.$data['file_name'];
                if (!$this->upload->do_upload('header_photo')) {
                    $this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
                    redirect('panel/kategorie/dodaj');            
                } else {
                $data = $this->upload->data();
                $insert['header_photo'] = $now.'/'.$data['file_name'];

            if($this->input->post('name_photo2') != null) {
                if ($this->upload->do_upload('photo2')) {
                    $data = $this->upload->data();
                    $insert['photo2'] = $now.'/'.$data['file_name'];         
                }
            }

                $insert['title'] = $this->input->post('title');
                $insert['slug'] = $this->slugify_m->slugify($insert['title']);
                $insert['alt'] = $this->input->post('alt');
                $insert['alt1'] = $this->input->post('alt1');
                $insert['alt2'] = $this->input->post('alt2');
                if($this->input->post('content') != null) {
                    $insert['content'] = $this->input->post('content');
                    $insert['content'] = str_replace('<p>', '', $insert['content']);
                    $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
                    $insert['content'] = str_replace('<div>', '', $insert['content']);
                    $insert['content'] = str_replace('</div>', '', $insert['content']);
                }

                $this->base_m->insert('kategorie', $insert);
                $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
                redirect('panel/kategorie');

            }
        }
    }

}

    public function edytuj($id)
    {

        $data['row'] = $this->base_m->get_record('kategorie', $id);

        $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');

        $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
        $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
        $this->form_validation->set_message('required', 'Pole %s jest wymagane');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('back/blocks/session');
            $this->load->view('back/blocks/head');
            $this->load->view('back/blocks/header');
            $this->load->view('back/pages/kategorie/edytuj', $data);
            $this->load->view('back/blocks/ajax');
            $this->load->view('back/blocks/footer');

        }else {
            $now = date('Y-m-d');
            if (!is_dir('uploads/kategorie/'.$now)) {
                mkdir('./uploads/kategorie/' . $now, 0777, TRUE);
            }
            $config['upload_path'] = './uploads/kategorie/'.$now;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;

            $this->load->library('upload',$config);
            $this->upload->initialize($config);

            if($this->input->post('name_photo') != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $update['photo'] = $now.'/'.$data['file_name'];        
                }
            }

            if($this->input->post('name_header_photo') != null) {
                if ($this->upload->do_upload('header_photo')) {
                    $data = $this->upload->data();
                    $update['header_photo'] = $now.'/'.$data['file_name'];         
                }
            }

            if($this->input->post('name_photo2') != null) {
                if ($this->upload->do_upload('photo2')) {
                    $data = $this->upload->data();
                    $update['photo2'] = $now.'/'.$data['file_name'];         
                }
            }

            $update['title'] = $this->input->post('title');
                $update['slug'] = $this->slugify_m->slugify($update['title']);
            $update['alt'] = $this->input->post('alt');
            $update['alt1'] = $this->input->post('alt1');
                $update['alt2'] = $this->input->post('alt2');
            if($this->input->post('content') != null) {
                $update['content'] = $this->input->post('content');
                $update['content'] = str_replace('<p>', '', $update['content']);
                $update['content'] = str_replace('</p>', '<br>', $update['content']);
                $update['content'] = str_replace('<div>', '', $update['content']);
                $update['content'] = str_replace('</div>', '', $update['content']);
            }
            $this->base_m->update('kategorie', $update, $id);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
            redirect('panel/kategorie');

        }
    }

}


