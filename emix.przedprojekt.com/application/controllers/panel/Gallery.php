<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {


	public function dodaj() {
		$id = $this->uri->segment(5);
		$table = $this->uri->segment(4);
		$this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|is_unique[gallery.img]|required');
    	$this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    	$this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    	$this->form_validation->set_message('required', 'Pole %s jest wymagane');
    	
    	if ($this->form_validation->run() == FALSE) {	
			$this->load->view('back/blocks/session');
			$this->load->view('back/blocks/head');
			$this->load->view('back/blocks/header');
			$this->load->view('back/pages/gallery/index');
			$this->load->view('back/blocks/ajax');
			$this->load->view('back/blocks/footer');
		} else {
            $now = date('Y-m-d');
	        if (!is_dir('uploads/gallery/'.$now)) {
		    	mkdir('./uploads/gallery/' . $now, 0777, TRUE);
			}
            $config['upload_path'] = './uploads/gallery/'.$now;
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
    
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('photo')) {
				$this->session->set_flashdata('flashdata', '<p class="text-danger font-weight-bold">Wystąpił problem z wczytywaniem zdjęcia. Prosimy skontaktować się z administratorem strony.</p>');
				redirect('panel/gallery/dodaj/'.$table.'/'.$id.'');            
            } else {
	            $data = $this->upload->data();
            	$insert['img'] = $now.'/'.$data['file_name'];
				$insert['table_name'] = $this->input->post('table_name');
				$insert['item_id'] = $this->input->post('item_id');
				$insert['img_alt'] = $this->input->post('alt');

				$this->base_m->insert('gallery', $insert);
				$this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
				redirect('panel/gallery/dodaj/'.$table.'/'.$id.'');
            }
		}
	}

	public function update_alt($id)
	{	
		$update['img_alt'] = $this->input->post('element');
		$this->base_m->update('gallery', $update, $id);
	}

	public function delete($id)
	{
		$this->base_m->drop_record($id,'gallery');
	}
}
