<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
	
		$data['users'] = $this->base_m->get('users');

		$this->form_validation->set_rules('login', 'Login', 'min_length[2]|trim');
		$this->form_validation->set_rules('password', 'Hasło', 'min_length[2]|trim');

		$this->form_validation->set_message('min_length', '<p>Pole %s ma za mało znaków </p>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('back/blocks/head');
        	$this->load->view('back/login');
		$this->load->view('back/blocks/footer');
		}

		else 
		{

			$options = ['cost' => 12];

			$login = strtolower($this->input->post('login'));
			$password = $this->input->post('password');
			$logged = false;

			foreach($data['users'] as $check):

				if (($login == $check->login || $login == $check->email) && (password_verify($password, $check->password)) && $check->active == '1'):

					$session_data['first_name'] = $check->first_name;
					$session_data['last_name'] = $check->last_name;
					$session_data['email'] = $check->email;
					$session_data['name'] = $check->login;
					$session_data['rola'] = $check->rola;
					$session_data['login'] = TRUE;

					$this->session->set_userdata($session_data);
					$logged = true;
					redirect('panel');
					break;

				else:
					$logged = false;
				endif;
			endforeach;

			if($logged == false)
			{
				$this->session->set_flashdata('logged_failed', '<p>Błędny login lub hasło</p>');
				redirect('panel/login');
			}

		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('p');
	}

}