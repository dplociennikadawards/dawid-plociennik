<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kontakt extends CI_Controller {
	public function index() {
		$data['rows'] = $this->base_m->get('kontakt');
		$data['sett'] = $this->base_m->get_record('contact_settings', 1);
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/kontakt/index', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}
	public function show($id) {
		$data['row'] = $this->base_m->get_record('kontakt', $id);
		$this->load->view('back/blocks/session');
		$this->load->view('back/blocks/head');
		$this->load->view('back/blocks/header');
		$this->load->view('back/pages/kontakt/show', $data);
		$this->load->view('back/blocks/ajax');
		$this->load->view('back/blocks/footer');
	}

	public function send() {
    $this->load->library('email');
    $config = array(
        'smtp_host'	=>"smtp.adawards.pl",
        'smtp_user'	=>"no-reply-car@adawards.pl",
        'smtp_pass'	=>"8lllRuWL4H",
        'smtp_port' =>"587",
        'smtp_timeout' =>"30",
        'mailtype'	=>"html",
    );
    $this->email->initialize($config);
    $this->email->from('no-reply-car@adawards.pl', 'EMIX - odpowiedź ze strony');
    $this->email->to($this->input->post('email'));
    $this->email->subject($this->input->post('subject'));
    $this->email->message($this->input->post('subject').'<br><p>'.$this->input->post('message').'</p>');

	    if ($this->email->send()) {
			$update['answer'] = 1;
			$this->base_m->update('kontakt', $update, $_POST['id']);  
	    }
	}

	public function update() {
		$update[$_POST['field']] = $_POST['value'];
		$this->base_m->update('contact_settings', $update, 1);
	}
}
