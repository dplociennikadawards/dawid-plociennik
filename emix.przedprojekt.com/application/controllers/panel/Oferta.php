<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Oferta extends CI_Controller {

public function index() {
    $data['rows'] = $this->base_m->get('oferta');
    $this->load->view('back/blocks/session');
    $this->load->view('back/blocks/head');
    $this->load->view('back/blocks/header');
    $this->load->view('back/pages/oferta/index', $data);
    $this->load->view('back/blocks/ajax');
    $this->load->view('back/blocks/footer');
}

public function dodaj() {
    $this->form_validation->set_rules('name_photo', 'Zdjęcie', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/oferta/dodaj');
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {

        $slug = $this->slugify_m->slugify($_POST['title']);
        if (!is_dir('uploads/oferta/'.$slug)) {
            mkdir('./uploads/oferta/' . $slug, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/oferta/'.$slug;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    
            if($this->input->post('name_photo') != null && $_FILES ['photo'] != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $data['file_name'];        
                } else {
                    print_r($this->upload->display_errors());
                }
            }

            if($this->input->post('name_gallery') != null && $_FILES ['gallery'] != null) {
                $files = $_FILES;
                $cpt = count($_FILES ['gallery'] ['name']);
                for ($i = 0; $i < $cpt; $i ++) {
                    $name = $files ['gallery'] ['name'] [$i];
                    $_FILES ['gallery'] ['name'] = $name;
                    $_FILES ['gallery'] ['type'] = $files ['gallery'] ['type'] [$i];
                    $_FILES ['gallery'] ['tmp_name'] = $files ['gallery'] ['tmp_name'] [$i];
                    $_FILES ['gallery'] ['error'] = $files ['gallery'] ['error'] [$i];
                    $_FILES ['gallery'] ['size'] = $files ['gallery'] ['size'] [$i];
                    if(!($this->upload->do_upload('gallery')) || $files ['gallery'] ['error'] [$i] !=0) {
                        print_r($this->upload->display_errors());
                    }
                }
            }

            $insert['title'] = $this->input->post('title');
            if($this->input->post('content') != null) {
                $insert['content'] = $this->input->post('content');
                $insert['content'] = str_replace('<p>', '', $insert['content']);
                $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
                $insert['content'] = str_replace('<div>', '', $insert['content']);
                $insert['content'] = str_replace('</div>', '', $insert['content']);
            }

            if($this->input->post('alt') != null) {
                $insert['alt'] = $this->input->post('alt');
            }
            
            if($this->input->post('name_gallery') != null) {
                $insert['gallery'] = $this->input->post('name_gallery');
            }

            $insert['category'] = $this->input->post('category');
            $insert['type'] = $this->input->post('type');

            if($this->input->post('rooms') != null) {
                $insert['rooms'] = $this->input->post('rooms');
            }

            if($this->input->post('bathroom') != null) {
                $insert['bathroom'] = $this->input->post('bathroom');
            }

            if($this->input->post('toilet') != null) {
                $insert['toilet'] = $this->input->post('toilet');
            }

            if($this->input->post('area') != null) {
                $insert['area'] = $this->input->post('area');
            }

            if($this->input->post('usable_area') != null) {
                $insert['usable_area'] = $this->input->post('usable_area');
            }

            if($this->input->post('reference') != null) {
                $insert['reference'] = $this->input->post('reference');
            }

            if($this->input->post('city') != null) {
                $insert['city'] = $this->input->post('city');
            }

            if($this->input->post('address') != null) {
                $insert['address'] = $this->input->post('address');
            }

            if($this->input->post('additional') != null) {
                $insert['additional'] = $this->input->post('additional');
            }
            
            if($this->input->post('hot_water') != null) {
                $insert['hot_water'] = $this->input->post('hot_water');
            }

            if($this->input->post('heating') != null) {
                $insert['heating'] = $this->input->post('heating');
            }

            if($this->input->post('state') != null) {
                $insert['state'] = $this->input->post('state');
            }

            if($this->input->post('charges') != null) {
                $insert['charges'] = $this->input->post('charges');
            }
            
            if($this->input->post('negotiation') == true) {
                $insert['negotiation'] = 1;
            } else { $insert['negotiation'] = 0; }

            $insert['price'] = $this->input->post('price');

            $this->base_m->insert('oferta', $insert);
            $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został dodany!</p>');
            redirect('panel/oferta');
        }
}

public function edytuj($id) {
    $data['row'] = $this->base_m->get_record('oferta', $id);

    $this->form_validation->set_rules('title', 'Tytuł', 'min_length[2]|trim|required');
    $this->form_validation->set_rules('content', 'opis', 'min_length[2]|trim|required');
    $this->form_validation->set_message('min_length', 'Pole %s ma za mało znaków');
    $this->form_validation->set_message('is_unique', 'Podany %s jest już w użyciu');
    $this->form_validation->set_message('required', 'Pole %s jest wymagane');
        
    if ($this->form_validation->run() == FALSE) {   
        $this->load->view('back/blocks/session');
        $this->load->view('back/blocks/head');
        $this->load->view('back/blocks/header');
        $this->load->view('back/pages/oferta/edytuj', $data);
        $this->load->view('back/blocks/ajax');
        $this->load->view('back/blocks/footer');
    } else {
        $slug = $this->slugify_m->slugify($_POST['title']);
        if (!is_dir('uploads/oferta/'.$slug)) {
            mkdir('./uploads/oferta/' . $slug, 0777, TRUE);
        }
        $config['upload_path'] = './uploads/oferta/'.$slug;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload',$config);
        $this->upload->initialize($config);

            if($this->input->post('name_photo') != null && $_FILES ['photo'] != null) {
                if ($this->upload->do_upload('photo')) {
                    $data = $this->upload->data();
                    $insert['photo'] = $data['file_name'];        
                } else {
                    print_r($this->upload->display_errors());
                }
            }

            if($this->input->post('name_gallery') != null && $_FILES ['gallery'] != null) {
                $files = $_FILES;
                $cpt = count($_FILES ['gallery'] ['name']);
                for ($i = 0; $i < $cpt; $i ++) {
                    $name = $files ['gallery'] ['name'] [$i];
                    $_FILES ['gallery'] ['name'] = $name;
                    $_FILES ['gallery'] ['type'] = $files ['gallery'] ['type'] [$i];
                    $_FILES ['gallery'] ['tmp_name'] = $files ['gallery'] ['tmp_name'] [$i];
                    $_FILES ['gallery'] ['error'] = $files ['gallery'] ['error'] [$i];
                    $_FILES ['gallery'] ['size'] = $files ['gallery'] ['size'] [$i];
                    if(!($this->upload->do_upload('gallery')) || $files ['gallery'] ['error'] [$i] !=0) {
                        print_r($this->upload->display_errors());
                    }
                }
            }

            $insert['title'] = $this->input->post('title');
            if($this->input->post('content') != null) {
                $insert['content'] = $this->input->post('content');
                $insert['content'] = str_replace('<p>', '', $insert['content']);
                $insert['content'] = str_replace('</p>', '<br>', $insert['content']);
                $insert['content'] = str_replace('<div>', '', $insert['content']);
                $insert['content'] = str_replace('</div>', '', $insert['content']);
            }

            if($this->input->post('alt') != null) {
                $insert['alt'] = $this->input->post('alt');
            }
            
            if($this->input->post('name_gallery') != null) {
                $insert['gallery'] = $this->input->post('name_gallery');
            }

            $insert['category'] = $this->input->post('category');
            $insert['type'] = $this->input->post('type');

            if($this->input->post('rooms') != null) {
                $insert['rooms'] = $this->input->post('rooms');
            }

            if($this->input->post('bathroom') != null) {
                $insert['bathroom'] = $this->input->post('bathroom');
            }

            if($this->input->post('toilet') != null) {
                $insert['toilet'] = $this->input->post('toilet');
            }

            if($this->input->post('area') != null) {
                $insert['area'] = $this->input->post('area');
            }

            if($this->input->post('usable_area') != null) {
                $insert['usable_area'] = $this->input->post('usable_area');
            }

            if($this->input->post('reference') != null) {
                $insert['reference'] = $this->input->post('reference');
            }

            if($this->input->post('city') != null) {
                $insert['city'] = $this->input->post('city');
            }

            if($this->input->post('address') != null) {
                $insert['address'] = $this->input->post('address');
            }

            if($this->input->post('additional') != null) {
                $insert['additional'] = $this->input->post('additional');
            }

            if($this->input->post('hot_water') != null) {
                $insert['hot_water'] = $this->input->post('hot_water');
            }

            if($this->input->post('heating') != null) {
                $insert['heating'] = $this->input->post('heating');
            }

            if($this->input->post('state') != null) {
                $insert['state'] = $this->input->post('state');
            }

            if($this->input->post('charges') != null) {
                $insert['charges'] = $this->input->post('charges');
            }

            if($this->input->post('negotiation') == true) {
                $insert['negotiation'] = 1;
            } else { $insert['negotiation'] = 0; }

            $insert['price'] = $this->input->post('price');


        $this->base_m->update('oferta', $insert, $id);
        $this->session->set_flashdata('flashdata', '<p class="text-success font-weight-bold">Wpis został zaktualizowany!</p>');
        redirect('panel/oferta');
    }
}
}
