<main class="main-section water-logo">
	<?php echo form_open_multipart();?>
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container-fluid px-md-5">
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Dodaj nowy glowna</a>

			    <div class="text-right" style="width: 181px;">
				      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
				        Zapisz
				      </button>
			    </div>

			  </div>
			   <div class="row px-5">
			   	<div class="col-md-6">
			   	<?php echo validation_errors(); ?>
				<div class="md-form mb-5">
					<input type="text" id="formTitle" class="form-control" name="title">
					<label for="formTitle">Tytuł</label>
				</div>
				<div class="md-form mb-5">
					<input type="text" id="formSubtitle" class="form-control" name="subtitle">
					<label for="formSubtitle">Podtytuł</label>
				</div>
				<div class="md-form mb-5">
                    <textarea name="content" id="editor"></textarea>
                </div>
				</div>
			   	<div class="col-md-6">
					<div class="md-form mb-5">
                        <div class="md-form">
                            <div class="file-field">
                                <div id="photo_show" class="text-center"></div>
                                    <div class="btn btn-primary btn-sm float-left gold-gradient">
                                        <span>Wybierz zdjęcie</span>
                                        <input type="file" name="photo" id="photo">
                                    </div>
                                <div class="file-path-wrapper">
                                    <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie główne" required readonly>
                                </div>
                            </div>
                        </div>
	                    <div class="md-form mb-5">
	                       	<input type="text" id="formAlt1" class="form-control" name="alt">
	                       	<label for="formAlt1">Tekst alternatywny zdjęcia</label>
	                    </div>
                    </div>
			   	</div>
			</div>
		</div>
	</section>
	</form>
</main>

