<main class="main-section water-logo">
    <?php echo form_open_multipart();?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Ustawienia #<?php echo $row->ustawienia_id; ?> </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-6">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formTitle" class="form-control" name="meta_title" value="<?php echo $row->meta_title; ?>" required>
                      <label for="formTitle">Tytuł strony</label>
                    </div>

                    <div class="md-form">
                        <textarea name="description" id="editor"><?php echo $row->description; ?></textarea>
                    </div>

                    <div class="md-form">
                      <textarea id="formContent" class="md-textarea form-control" name="content" rows="3" required><?php echo $row->cookies; ?></textarea>
                      <label for="formContent">Treść cookies</label>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="md-form">
                      <div class="file-field">
                        <div id="photo_show" class="text-center">
                            <img class="img-fluid img-thumbnail" src="<?php echo base_url(); ?>uploads/settings/<?php echo $row->logo; ?>" width="250">
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz logo</span>
                          <input type="file" name="photo" id="photo">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz logo strony" value="<?php echo $row->logo; ?>" required readonly>
                        </div>
                      </div>
                    </div>

                    <div class="md-form">
                      <div class="file-field">
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz politykę</span>
                          <input type="file" name="pdf" id="pdf">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_pdf" class="file-path validate" type="text" placeholder="Wybierz politykę prywatności" value="<?php echo $row->pdf; ?>" required readonly>
                        </div>
                      </div>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formFblink" class="form-control" name="fb_link" value="<?php echo $row->fb_link; ?>">
                      <label for="formFblink">Link do Facebooka</label>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formInslink" class="form-control" name="inst_link" value="<?php echo $row->inst_link; ?>">
                      <label for="formInslink">Link do Instagrama</label>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formYtlink" class="form-control" name="yt_link" value="<?php echo $row->yt_link; ?>">
                      <label for="formYtlink">Link do Twittera</label>
                    </div>

                </div>
               </div>
            </div>
        </div>
    </section>
    </form>
</main>
