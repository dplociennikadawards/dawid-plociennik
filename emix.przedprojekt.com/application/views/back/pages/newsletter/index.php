<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container">
			<!-- Table with panel -->
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<input type="text" id="searchInput" class="form-control" onkeyup="searchFiltr(0)" placeholder="Wpisz temat..." title="Wpisz temat">
			    </div>

			    <a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>

			    <div class="text-right" style="width: 181px;">
				      <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#fullHeightModalRight">
				        <i class="fas fa-paper-plane"></i>
				      </button>
			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4">

			    <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
				            
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 30%;">
				              Imię i nazwisko
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(1)" style="width: 25%;">
				              E-mail
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(1)" style="width: 25%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->
				        <tbody>
						  	<?php foreach ($rows as $key): ?>
						  		<tr>
						            <td class="align-middle"><?php echo $key->first_name; ?> <?php echo $key->last_name; ?></td>
						            <td class="align-middle"><?php echo $key->email; ?></td>
						            <td class="align-middle"><?php echo $key->created_data; ?></td>
						            <td class="align-middle text-right">
								      <a href="<?php echo base_url(); ?>panel/p/drop_record/<?php echo $key->newsletter_id; ?>/newsletter">
									      <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									      </button>
								      </a>
								  	</td>
				            	</tr>
							<?php endforeach; ?>
				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
</main>

<!-- Full Height Modal Right -->
<div class="modal fade right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Napisz newsletter</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form method="post" action="<?php echo base_url(); ?>panel/newsletter/send">
      		<div class="md-form mb-5">
                <input type="text" id="formTitle" class="form-control" name="subject" value="" required>
                <label for="formTitle">Temat newslettera<span class="text-danger">*</span></label>
            </div>
        	<textarea name="message" id="editor"></textarea>
        	<button type="submit" class="btn btn-outline-dark btn-block mt-4">
                
				        <i class="fas fa-paper-plane"></i>Wyślij
            </button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->

