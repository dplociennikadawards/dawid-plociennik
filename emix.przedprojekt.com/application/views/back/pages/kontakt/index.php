<main class="main-section water-logo">
	<div class="container mb-5" style="min-height: 44px;">
	<?php if($this->session->flashdata('flashdata')) {
	  echo $this->session->flashdata('flashdata');
	} ?>
	</div>
	<section>
		<div class="container">
			<!-- Table with panel -->
			<div class="card card-cascade narrower">

			  <!--Card image-->
			  <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

			    <div class="md-form my-0">
					<input type="text" id="searchInput" class="form-control" onkeyup="searchFiltr(0)" placeholder="Wpisz temat..." title="Wpisz temat">
			    </div>

			    <a href="" class="white-text mx-3"><?php echo ucfirst($this->uri->segment(2)); ?></a>

			    <div class="text-right" style="width: 181px;">
				      <button type="button" class="btn btn-outline-dark btn-rounded btn-hover-alt btn-sm px-2" data-toggle="modal" data-target="#fullHeightModalRight">
				        <i class="fas fa-cog"></i>
				      </button>
			    </div>

			  </div>
			  <!--/Card image-->

			  <div class="px-4">

			    <div class="table-wrapper">
				    <span id="refreshTable">
				      <!--Table-->
				      <table id="filtrableTable" class="table table-hover mb-0">

				        <!--Table head-->
				        <thead>
				          <tr>
				            <th style="width: 10%;">Przetwarzanie danych</th>
				            <th style="width: 10%;">Zgoda na reklamy</th>
				            <th style="width: 10%;">Odpowiedź</th>
				            <th class="th-lg cursor" onclick="sortTable(0)" style="width: 20%;">
				              Temat
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTable(1)" style="width: 15%;">
				              Imię
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th class="th-lg cursor" onclick="sortTableData(1)" style="width: 15%;">
				              Data utworzenia
				                <i class="fas fa-sort ml-1"></i>
				              
				            </th>
				            <th style="width: 20%;"></th>
				          </tr>
				        </thead>
				        <!--Table head-->

				        <!--Table body-->
				        <tbody>
						  	<?php foreach ($rows as $key): ?>
						  		<tr>
						            <th scope="row" class="align-middle">
						              <input class="form-check-input" type="checkbox" id="checkbox<?php echo $key->kontakt_id; ?>" <?php if($key->rodo == 1){echo 'checked';} ?> readonly>
						              <label class="form-check-label" for="checkbox<?php echo $key->kontakt_id; ?>" class="label-table"></label>
						            </th>
						            <th scope="row" class="align-middle">
						              <input class="form-check-input" type="checkbox" id="checkbox<?php echo $key->kontakt_id; ?>" <?php if($key->rodo2 == 1){echo 'checked';} ?> readonly>
						              <label class="form-check-label" for="checkbox<?php echo $key->kontakt_id; ?>" class="label-table"></label>
						            </th>
						            <th scope="row" class="align-middle">
						              <input class="form-check-input" type="checkbox" id="checkbox<?php echo $key->kontakt_id; ?>" <?php if($key->answer == 1){echo 'checked';} ?> readonly>
						              <label class="form-check-label" for="checkbox<?php echo $key->kontakt_id; ?>" class="label-table"></label>
						            </th>
						            <td class="align-middle"><?php echo $key->subject; ?></td>
						            <td class="align-middle"><?php echo $key->first_name; ?></td>
						            <td class="align-middle"><?php echo $key->created; ?></td>
						            <td class="align-middle text-right">
						              <a href="<?php echo base_url(); ?>panel/kontakt/show/<?php echo $key->kontakt_id; ?>">
									    <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									      <i class="fas fa-eye mt-0"></i>
									    </button>
									  </a>
								      <a href="<?php echo base_url(); ?>panel/p/drop_record/<?php echo $key->kontakt_id; ?>/kontakt">
									      <button type="button" class="btn btn-outline-dark btn-rounded btn-sm px-2">
									        <i class="far fa-trash-alt mt-0"></i>
									      </button>
								      </a>
								  	</td>
				            	</tr>
							<?php endforeach; ?>
				        </tbody>
				        <!--Table body-->
				      </table>
				      <!--Table-->
			      </span>
			    </div>

			  </div>

			</div>
			<!-- Table with panel -->
		</div>
	</section>
</main>




<!-- Full Height Modal Right -->
<div class="modal fade right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Ustawienia kontaktu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="change">Zmiany zapisują się automatycznie</p>
		<div class="md-form">
			<input type="text" id="formCompany" class="form-control" name="company" value="<?php echo $sett->company; ?>" onchange="update_contact('company');" required>
			<label for="formCompany">Nazwa firmy</label>
		</div>
		<div class="md-form">
			<input type="text" id="formName" class="form-control" name="name" value="<?php echo $sett->name; ?>" onchange="update_contact('name');" required>
			<label for="formName">Imię i nazwisko</label>
		</div>
		<div class="md-form">
			<input type="text" id="formAddress" class="form-control" name="address" value="<?php echo $sett->address; ?>" onchange="update_contact('address');" required>
			<label for="formAddress">Adres firmy</label>
		</div>
		<div class="md-form">
			<input type="text" id="formCity" class="form-control" name="city" value="<?php echo $sett->city; ?>" onchange="update_contact('city');" required>
			<label for="formAddress">Miasto</label>
		</div>
		<div class="md-form">
			<input type="text" id="formPhone" class="form-control" name="phone" value="<?php echo $sett->phone; ?>" onchange="update_contact('phone');" required>
			<label for="formPhone">Numer telefonu</label>
		</div>
		<div class="md-form">
			<input type="text" id="formPhone2" class="form-control" name="phone2" value="<?php echo $sett->phone2; ?>" onchange="update_contact('phone2');" required>
			<label for="formPhone2">Drugi nr telefonu</label>
		</div>
		<div class="md-form">
			<input type="text" id="formEmail" class="form-control" name="email" value="<?php echo $sett->email; ?>" onchange="update_contact('email');" required>
			<label for="formEmail">Adres E-mail</label>
		</div>
		<div class="md-form">
			<input type="text" id="formMap" class="form-control" name="map" value="<?php echo $sett->map; ?>" onchange="update_contact('map');" required>
			<label for="formMap">Mapa google</label>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->

    <script type="text/javascript">
      function update_contact(field)
      {
        var field = field;
        var value = document.getElementsByName(field)[0].value;
        $.ajax({  
             type: "post", 
             url:"<?php echo base_url(); ?>panel/kontakt/update", 
             data: {field:field, value:value}, 
             cache: false,
             complete:function(html)
             {
                document.getElementById('change').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Zmiany zostały wprowadzone!</span>';
             }  
        });  
      }
    </script>