<main class="main-section water-logo">
    <?php echo form_open_multipart('', 'id="specialform"');?>
    <div class="container mb-5" style="min-height: 44px;">
    <?php if($this->session->flashdata('flashdata')) {
      echo $this->session->flashdata('flashdata');
    } ?>
    </div>
    <section>
        <div class="container-fluid px-md-5 mb-5">
            <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view view-cascade gradient-card-header gold-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

                <a class="white-text mx-3 w-100" style="margin-left: 181px !important;">Dodaj wpis </a>

                <div class="text-right" style="width: 181px;">
                      <button type="submit" class="btn btn-outline-dark btn-hover-alt btn-sm px-2">
                        Zapisz
                      </button>
                </div>

              </div>
               <div class="row px-5">
                <div class="col-md-6">
                    <?php echo validation_errors(); ?>
                    <div class="md-form mb-5">
                      <input type="text" id="formTitle" class="form-control" name="title" value="" required>
                      <label for="formTitle">Tytuł <span class="text-danger">*</span></label>
                    </div>
                    <div class="md-form">
                      <textarea name="content" id="editor"></textarea>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="md-form mb-5">
                      <div class="file-field">
                        <div id="photo_show" class="text-center">
                            
                        </div>
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                          <span>Wybierz zdjęcie</span>
                          <input type="file" name="photo" id="photo">
                        </div>
                        <div class="file-path-wrapper">
                          <input name="name_photo" class="file-path validate" type="text" placeholder="Wybierz zdjęcie główne" value="" required readonly>
                        </div>
                      </div>
                    </div>
                    <div class="md-form mb-5">
                      <input type="text" id="formAlt2" class="form-control" name="alt" value="">
                      <label for="formAlt2">Tekst alternatywny zdjęcia</label>
                    </div>
                    <div class="md-form mb-5">
                    <div class="file-field">
                        <div class="btn btn-primary btn-sm float-left gold-gradient">
                            <span>Stwórz galerie</span>
                            <input type="file" name="gallery[]" id="gallery" multiple="multiple">
                        </div>
                        <div class="file-path-wrapper">
                            <input name="name_gallery" class="file-path validate" type="text" placeholder="Wybierz zdjęcia do galerii" required readonly>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-md-6">

                  <div class="md-form mb-5">
                      <select class="form-control" name="category" style="display:block !important" required>
                        <option value="" disabled selected>Wybierz kategorię <span class="text-danger">*</span></option>
                        <option value="mieszkania">Mieszkania</option>
                        <option value="domy">Domy</option>
                        <option value="dzialki">Działki</option>
                        <option value="dzialki_budowlane">Działki budowlane</option>
                        <option value="dzialki_komercyjne">Działki komercyjne</option>
                        <option value="dzialki_rekreacyjne">Działki rekreacyjne</option>
                        <option value="dzialki_inwestycyjne">Działki inwestycyjne</option>
                        <option value="lokale_uzytkowe">Lokale użytkowe</option>
                        <option value="hale_i_magazyny">Hale i magazyny</option>
                        <option value="garaz">Garaże</option>
                      </select>
                    </div>
                    <div class="md-form mb-5">
                      <select class="form-control" name="type" style="display:block !important" required>
                        <option value="" disabled selected>Wybierz typ <span class="text-danger">*</span></option>
                        <option value="sprzedaz">Na sprzedaż</option>
                        <option value="wynajem">Wynajem</option>
                        <option value="inwestycje">Inwestycje</option>
                        <option value="zamiana">Zamiana</option>
                      </select>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="rooms" class="form-control" name="rooms" value="">
                      <label for="rooms">Liczba pokoi</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="bathroom" class="form-control" name="bathroom" value="">
                      <label for="bathroom">Liczba łazienek</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="toilet" class="form-control" name="toilet" value="">
                      <label for="toilet">Liczba toalet</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="area" class="form-control" name="area" value="">
                      <label for="area">Powierzchnia całkowita [m<sup>2</sup>]</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="usable_area" class="form-control" name="usable_area" value="">
                      <label for="usable_area">Powierzchnia użytkowa [m<sup>2</sup>]</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="hot_water" class="form-control" name="hot_water">
                      <label for="hot_water">Źródło ciepłej wody</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="heating" class="form-control" name="heating">
                      <label for="heating">Źródło ogrzewania</label>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="md-form mb-5">
                      <input type="text" id="state" class="form-control" name="state">
                      <label for="state">Stan ogólny</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="charges" class="form-control" name="charges">
                      <label for="charges">Opłaty</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="reference" class="form-control" name="reference" value="">
                      <label for="reference">Numer referencyjny</label>
                    </div>

                    <div class="md-form mb-5">
                      <input type="text" id="city" class="form-control" name="city" value="">
                      <label for="city">Miejscowość</label>
                    </div>
                    
                    <div class="md-form mb-5">
                      <input type="text" id="address" class="form-control" name="address" value="">
                      <label for="address">Adres</label>
                    </div>

                    <div class="mb-5">
                      <label for="additional">Dodatkowe wyposażenie</label>
                      <input type="text" id="additional" class="form-control" name="additional" value="" data-role="tagsinput" required>
                    </div>

                    <div class="md-form mb-5">
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="negotiation" name="negotiation">
                        <label class="form-check-label" for="negotiation">Do negocjacji</label>
                    </div>
                    </div>

                    <div class="md-form mb-5">
                      <input type="number" id="price" class="form-control" name="price" value="" required>
                      <label for="price">Cena <span class="text-danger">*</span></label>
                    </div>

                </div>
               </div>
            </div>
        </div>
    </section>
    </form>
</main>


