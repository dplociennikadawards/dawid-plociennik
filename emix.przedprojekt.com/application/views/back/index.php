<main class="main-section water-logo">
	<section>
		<div class="container-fluid">

	        <div class="row p-3">

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/glowna">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/home.svg');"></div>Strona główna
                </button>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/slider">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/image.svg');"></div>Slider
                </button>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/podstrony">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/folder.svg');"></div>Podstrony
                </button>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/o_firmie">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/teamwork.svg');"></div>O firmie
                </button>
              </a>
            </div>

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/zarzadzanie_nieruchomosciami">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/house.svg');"></div>Zarządzanie nieruchomościami
		            </button>
		        </a>
	          </div>  

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/oferta">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/online-shop.svg');"></div>Oferta
                </button>
            </a>
            </div>

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/posrednictwo">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/tiles.svg');"></div>Pośrednictwo
                </button>
            </a>
            </div> 

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/wspolnota">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/social-care.svg');"></div>Wspólnota
                </button>
              </a>
            </div>

	          <!-- <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/gallery/dodaj/galeria/1">
                <button class="navigations-blocks rounded border-0 " style="cursor: pointer;">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/factory.svg');"></div>{{Tworzenie galerii}}
                </button>
              </a>
            </div> -->

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/weles">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/w3.png');"></div>Weles3
                </button>
            </a>
            </div>

            <div class="col-lg-4 col-md-6 col-12 text-center my-3">
              <a href="<?php echo base_url(); ?>panel/newsletter">
                <button class="navigations-blocks rounded border-0 shadow-alt">
                  <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/newsletter.svg');"></div>Newsletter
                </button>
            </a>
            </div>

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/kontakt">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/email.svg');"></div>Kontakt
		            </button>
		        </a>
	          </div>

	          <div class="col-lg-4 col-md-6 col-12 text-center my-3">
	            <a href="<?php echo base_url(); ?>panel/ustawienia">
		            <button class="navigations-blocks rounded border-0 shadow-alt">
		              <div class="icon-style mx-auto mb-3" style="background-image: url('<?php echo base_url(); ?>assets/back/img/icons/settings.svg');"></div>Ustawienia
		            </button>
		        </a>
	          </div>

	        </div>

      	</div>
	</section>
</main>