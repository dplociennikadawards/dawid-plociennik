<main>
	<section class="default-section">
      <div class="container">
        <h4 class="default-section__title">
          <?php echo $row->title; ?>
        </h4>
          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-lg-6">

              <!-- Featured image -->
              <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4" title="wyświetl galerię">
                <img class="img-fluid" src="<?php echo base_url() ?>uploads/oferta/<?php echo $this->slugify_m->slugify($row->title) ?>/<?php echo $row->photo; ?>" alt="<?php echo $row->alt; ?>">
                <a href="#galeria">
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-6">

              <!-- Category -->
              <a href="#!" class="icon-color">
                <h6 class="font-weight-bold mb-3">
                  <?php 
                    if($row->category == 'mieszkania'){echo 'Mieszkania';}
                    if($row->category == 'domy'){echo 'Domy';}
                    if($row->category == 'dzialki'){echo 'Działki';}
                    if($row->category == 'dzialki_budowlane'){echo 'Działki budowlane';}
                    if($row->category == 'dzialki_komercyjne'){echo 'Działki komercyjne';}
                    if($row->category == 'dzialki_rekreacyjne'){echo 'Działki rekreacyjne';}
                    if($row->category == 'dzialki_inwestycyjne'){echo 'Działki inwestycyjne';}
                    if($row->category == 'lokale_uzytkowe'){echo 'Lokale użytkowe';}
                    if($row->category == 'hale_i_magazyny'){echo 'Hale i magazyny';}
                    if($row->category == 'garaze'){echo 'Garaże';} 
                  ?> -
                  <?php 
                    if($row->type == 'sprzedaz'){echo 'Na sprzedaż';}
                    if($row->type == 'wynajem'){echo 'Wynajem';}
                    if($row->type == 'inwestycje'){echo 'Inwestycje';}
                    if($row->type == 'zamiana'){echo 'Zamiana';}
                  ?>
                </h6>
              </a>

              <!-- Excerpt -->
              <div class="list-group">
                <?php if($row->reference != ''): ?>
                <div class="list-group-item list-group-item-action">Numer referencyjny: <strong><?php echo $row->reference; ?></strong></div>
                <?php endif; ?>
                <?php if($row->state != ''): ?>
                <div class="list-group-item list-group-item-action">Stan: <strong><?php echo $row->state; ?></strong></div>
                <?php endif; ?>
                <?php if($row->area != ''): ?>
                <div class="list-group-item list-group-item-action">Powierzchnia całkowita: <strong><?php echo $row->area; ?></strong> m<sup>2</sup></div>
                <?php endif; ?>
                <?php if($row->hot_water != ''): ?>
                <div class="list-group-item list-group-item-action">Źródło ciepłej wody: <strong><?php echo $row->hot_water; ?></strong></div>
                <?php endif; ?>
                <?php if($row->heating != ''): ?>
                <div class="list-group-item list-group-item-action">Źródło ogrzewania: <strong><?php echo $row->heating; ?></strong></div>
                <?php endif; ?>
                <?php if($row->price != ''): ?>
                <div class="list-group-item list-group-item-action">Cena: <strong><?php echo $row->price; ?></strong> PLN<?php if($row->negotiation == 1){echo ', Do negocjacji';} ?></div>
                <?php endif; ?>
              </div>

            </div>
            <div class="col-lg-12 mt-5">

                <h4><strong>Szczegóły ogłoszenia</strong></h4>
                <div class="row">
                <?php if($row->rooms != ''): ?>
                <div class="col-md-4 col-6">
                  <p>Powierzchnia użytkowa: <strong><?php echo $row->usable_area; ?></strong> m<sup>2</sup></p>
                </div>
                <?php endif; ?>
                <?php if($row->rooms != ''): ?>
                <div class="col-md-4 col-6">
                  <p>Opłaty: <strong><?php echo $row->charges; ?></strong></p>
                </div>
                <?php endif; ?>
                <?php if($row->rooms != ''): ?>
                <div class="col-md-4 col-6">
                  <p>Liczba pokoi: <strong><?php echo $row->rooms; ?></strong></p>
                </div>
                <?php endif; ?>
                <?php if($row->rooms != ''): ?>
                <div class="col-md-4 col-6">
                  <p>Liczba łazienek: <strong><?php echo $row->bathroom; ?></strong></p>
                </div>
                <?php endif; ?>
                <?php if($row->rooms != ''): ?>
                <div class="col-md-4 col-6">
                  <p>Liczba toalet: <strong><?php echo $row->toilet; ?></strong></p>
                </div>
                <?php endif; ?>
                </div>

                <hr>


              <!-- Post title -->
              <h4 class="font-weight-bold mb-3"><strong>Opis</strong></h4>
              <h4 class="font-weight-bold mb-3"><strong><?php echo $row->city; ?></strong>, <?php echo $row->address; ?></h4>
              <!-- Post data -->
              <p class="mt-3"><a><strong>EMIX</strong></a>, <?php echo date('d/m/Y', strtotime($row->created)); ?></p>
              <!-- Read more button -->
            <div class="col-lg-12 mt-2 px-0"><?php echo $row->content; ?></div>


                <hr>


              <!-- Post title -->
              <h4 class="font-weight-bold mb-3"><strong>Dodatkowe informacje</strong></h4>
              <!-- Read more button -->
              <div class="col-lg-12 mt-2">
                <?php if($row->additional != ''): ?>
                  <div class="row">
                <?php $pieces = explode(",", $row->additional); 
                foreach ($pieces as $key): ?>

                  <div class="col-md-4 col-6">
                    <i class="fas fa-check text-success"></i> <?php echo $key; ?>
                  </div>

                <?php endforeach;?>
                  </div>
                <?php endif; ?>
              </div>



              <hr>


              <!-- Post title -->
              <h4  id="galeria" class="font-weight-bold mb-3"><strong>Galeria</strong></h4>
              <!-- Read more button -->
              <div class="col-lg-12 mt-2">
                <?php if($row->gallery != ''): ?>
                  <div class="row">
                <?php $pieces = explode(", ", $row->gallery); 
                foreach ($pieces as $piece): ?>

                  <div class="col-md-4 col-6 mt-4">
                    <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4" title="wyświetl galerię">
                      <div style="
                        background-image: url('<?php echo base_url() ?>uploads/oferta/<?php echo $this->slugify_m->slugify($row->title); ?>/<?php echo $piece; ?>');
                        background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        height: 300px;
                        "></div>
                      <a href="<?php echo base_url() ?>uploads/oferta/<?php echo $this->slugify_m->slugify($row->title); ?>/<?php echo $piece; ?>" data-lightbox="roadtrip">
                        <div class="mask rgba-white-slight"></div>
                      </a>
                    </div>
                  </div>

                <?php endforeach;?>
                  </div>
                <?php endif; ?>
              </div>



            </div>
            <!-- Grid column -->


          </div>
          <!-- Grid row -->
        </div>
    </section>

    <section class="default-section">
      <div class="container">
        <h4 class="default-section__title">
          Polecane
        </h4>
        <div class="default-section__row">
          <?php foreach ($recommended as $key): ?>
          <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->oferta_id ?>" class="default-section__recommended-cell">
            <div class="default-section__recommended-img" style="background-image: url('<?php echo base_url(); ?>uploads/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->photo; ?>');"></div>
            <div class="default-section__recommended-title">
              <?php echo $key->title; ?>
            </div>
          </a>
          <?php endforeach ?>
        </div>
      </div>
    </section>
</main>