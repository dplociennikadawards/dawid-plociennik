
  <?php if($this->session->flashdata('flashdata')) {
    echo '<div class="info">'.$this->session->flashdata('flashdata').'</div>';
  } ?>

  <main>
    <section class="default-section">
      <div class="container">
        <h4 class="default-section__title">
          Najnowsze oferty
        </h4>

        <div class="default-section__row">
          <?php $i=0; foreach (array_reverse($oferta) as $key): $i++;?>
          <div class="default-section__cell" style="background-image: url('<?php echo base_url(); ?>uploads/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->photo; ?>')">
            <h4 class="default-section__cell-title"><?php echo $key->title; ?></h4>

            <div class="default-section__movable-panel">
              <h4 class="default-section__cell-title default-section__cell-title--dark"><?php echo $key->title; ?></h4>

              <span class="default-section__panel-desc">
                <?php echo $key->city; ?><br>
                Powierzchnia (m2): <?php echo $key->area; ?><br>
                Pokoje: <?php echo $key->rooms; ?>
              </span>

              <div class="default-section__price-box">
                <span class="default-section__price"><?php echo $key->price; ?> PLN</span>

                <a class="default-section__btn" href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->oferta_id ?>">sprawdź</a>
              </div>
            </div>
          </div>
          <?php if($i == 6){break;}; endforeach; ?>

        <a class="default-section__big-btn" href="<?php echo base_url(); ?>p/oferty">Pokaż więcej ofert</a>
      </div>

    </section>

    <section class="default-section default-section--background">
      <div class="container">
        <h4 class="default-section__title">
          Emix poleca
        </h4>
      </div>

      <div class="carousel-box">
        <div class="container">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">


              <?php $i=0; foreach ($rows as $key): ?>
              <div class="carousel-box__slide carousel-item <?php if($i == 0){echo 'active';} ?>">
                <div class="carousel-box__row">

                  <?php $j=0; foreach ($rows as $key_cell): if($j >= $i): if($j == ($i+3)){break;}?>
                  <div class="carousel-box__cell">
                    <div class="carousel-box__icon-frame">
                      <img class="carousel-box__icon" src="<?php echo base_url(); ?>uploads/glowna/<?php echo $key_cell->photo; ?>">
                    </div>

                    <h4 class="carousel-box__title"><?php echo $key_cell->title; ?></h4>

                    <p class="carousel-box__paragraph"><?php echo $key_cell->content; ?></p>
                  </div>
                  <?php endif; $j++; endforeach; ?>

                </div>
              </div>
              <?php  $i+=3; endforeach; ?>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <i class="fas fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <i class="fas fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="default-section">
      <div class="container">
        <div class="row">
          <div class="d-flex justify-content-center align-items-center col-lg-4">
            <img class="default-section__img" src="<?php echo base_url(); ?>uploads/o_firmie/<?php echo $o_firmie->photo; ?>">
          </div>

          <div class="col-lg-8">
            <h4 class="default-section__title default-section__title--text-left">
              <?php echo $o_firmie->title; ?>
            </h4>

            <p class="default-section__text">
              <span class="default-section__bold"><?php echo $o_firmie->content; ?>
            </p>

            <div class="d-flex align-items-end">
              <div class="default-section__separator"></div>
              <a class="default-section__btn default-section__btn--big" href="<?php echo base_url() ?>p/o_firmie">wiecej</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="default-section">
      <div class="container">
        <h4 class="default-section__title">
          Polecane
        </h4>
        <div class="default-section__row">
          <?php foreach ($recommended as $key): ?>
          <a href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->oferta_id ?>" class="default-section__recommended-cell mt-3">
            <div class="default-section__recommended-img" style="background-image: url('<?php echo base_url(); ?>uploads/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->photo; ?>');"></div>
            <div class="default-section__recommended-title">
              <?php echo $key->title; ?>
            </div>
          </a>
          <?php endforeach ?>
        </div>
      </div>
    </section>

    <section class="default-section default-section--background">
      <div class="container">
        <h4 class="default-section__title">
          Kontakt
        </h4>

        <div class="tabs__box">
          <ul class="tabs__list">
            <li class="tabs__link tabs__link-active" data-tab="tab-1">dane kontaktowe</li>
            <li class="tabs__link" data-tab="tab-2">lokalizacja</li>
          </ul>

          <div id="tab-1" class="tabs__content tabs__content-active">
            <div class="row">
              <div class="tabs__col col-lg-6">
                <h4 class="tabs__title">Napisz w czym możemy Ci pomóc.</h4>

                <form action="<?php echo base_url(); ?>mail/send" method="post">
                  <input class="tabs__input" type="text" name="name" id="" placeholder="Imię i nazwisko">
                  <input class="tabs__input" type="email" name="email" id="" placeholder="E-mail">
                  <input class="tabs__input" type="tel" name="phone" id="" placeholder="Telefon">
                  <textarea class="tabs__textarea" name="message" id="" cols="30" rows="10"
                    placeholder="Wpisz wiadomość"></textarea>

                  <div class="tabs__submit-box d-flex">
                    <input class="default-section__btn" type="submit" value="wyślij">
                    <div>
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="default1" name="rodo">
                        <label class="custom-control-label" for="default1">Oświadczam, że zapoznałem się z
                          Polityką prywatności.</label>
                      </div>

                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="default2" name="rodo2">
                        <label class="custom-control-label" for="default2">Wyrażam zgodę na przetwarzanie danych osobowych
                        </label>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-lg-6">
                <h4 class="tabs__title">Dane kontaktowe firmy</h4>
                <span class="tabs__separator"></span>

                <div class="d-flex flex-column justify-content-between"><span class="tabs__text">
                    Emix Biuro Nieruchomości, mgr. Elżbieta Wiązek<br>
                    <?php echo $kontakt->address ?>, <?php echo $kontakt->city ?>
                  </span>

                  <span class="tabs__bold-text">
                    Tel. <a class="tabs__smlink" href="tel:<?php echo $kontakt->phone ?>"><?php echo $kontakt->phone ?></a><br>
                    Tel.kom.: <a class="tabs__smlink" href="tel:<?php echo $kontakt->phone2 ?>"><?php echo $kontakt->phone2 ?></a><br>
                    E-mail: <a class="tabs__smlink" href="mailto:<?php echo $kontakt->email ?>"><?php echo $kontakt->email ?></a>
                  </span>

                  <span class="tabs__text">
                    Wpis do Ewidencji Działalności Gospodarczej Nr: 1630/90 BDO 000027834
                    Wpisana do Centralnej Ewidencji i Informacji o Działalności Gospodarczej
                    Rzeczypospolitej Polskiej prowadzonej przez ministra właściwego do spraw gospod.
                  </span></div>
              </div>
            </div>
          </div>
          <div id="tab-2" class="tabs__content">
            <iframe src="<?php echo $kontakt->map ?>" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

        </div>

        <div class="newsletter">
          <div class="newsletter__left">
            <h4 class="newsletter__title">newsletter</h4>
            <span>Zapisz się do naszego miesięcznego newslettera <br>
              i bądź na bieżąco z nasza ofertą.</span>
          </div>
          <div class="newsletter__right">
            <form class="newsletter__form" action="<?php echo base_url(); ?>mail/newsletter" method="post">
              <input class="newsletter__input" type="text" name="name" id="" placeholder="Imię i nazwisko">
              <input class="newsletter__input" type="email" name="email" id="" placeholder="Podaj adres e-mail">
              <input class="newsletter__submit" type="submit" value="zapisz się">
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>