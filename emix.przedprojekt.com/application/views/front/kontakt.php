
 <section class=" p-md-5 py-3 py-md-0">

  <div class="row">
    <div class="col-md-4">
      <iframe src="<?php echo $kontakt->map; ?>" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-8">
       <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Napisz w czym możemy Ci pomóc.</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5"></p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="<?php echo base_url(); ?>mail/send" method="POST"  onsubmit="return validateMyForm(0);">


                <!--Grid row-->
                <div class="row px-3 px-md-0">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Imię i nazwisko</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">
                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="phone" name="phone" class="form-control">
                            <label for="phone" class="">Telefon</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="email" id="email" name="email" class="form-control" required>
                            <label for="email" class="">E-mail</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea" required></textarea>
                            <label for="message">Wpisz wiadomość</label>
                        </div>
                        <div class="form-check mb-3">
                            <input name="zgoda" value="1" type="checkbox" class="form-check-input" id="materialUnchecked">
                            <label class="form-check-label" for="materialUnchecked"><small>Oświadczam, że zapoznałem się z
                          Polityką prywatności.</small></label>
                            <input name="zgoda2" value="1" type="checkbox" class="form-check-input" id="materialUnchecked2">
                            <label class="form-check-label" for="materialUnchecked2"><small>Wyrażam zgodę na przetwarzanie danych osobowych</small></label>
                        </div>
                    </div>
                </div>
                <center>
                <div class="g-recaptcha" data-sitekey="6LeHN5gUAAAAALga6MyyDCjhTIDKq4opVcvHzcSM"></div>
              <span class="warning_captcha text-danger" class="text-danger"></span></center>
                <!--Grid row-->
                <!--Grid row-->
                <div class="row px-3 px-md-0">

                    <!--Grid column-->
                    <div class="col-md-12">



                    </div>
                </div>
                <!--Grid row-->
            <div class="text-center text-md-left px-3 px-md-0">
                <button type="submit" class="btn mt-3 btn-color text-shadow">Wyślij</button>
            </div>
            <div class="status"></div>
            </form>


        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x icon-color"></i>
                    <p><?php echo $kontakt->address; ?>, <?php echo $kontakt->city; ?></p>
                </li>

                <li>
                    <a href="tel:<?php echo $kontakt->phone; ?>" class="text-dark">
                        <i class="fas fa-phone mt-4 fa-2x icon-color"></i>
                        <p><?php echo $kontakt->phone; ?></p>
                    </a>
                </li>

                <li>
                    <a href="tel:<?php echo $kontakt->phone2; ?>" class="text-dark">
                        <i class="fas fa-phone mt-4 fa-2x icon-color"></i>
                        <p><?php echo $kontakt->phone2; ?></p>
                    </a>
                </li>

                <li>
                    <a href="mailto:<?php echo $kontakt->email; ?>" class="text-dark">
                        <i class="fas fa-envelope mt-4 fa-2x icon-color"></i>
                        <p><?php echo $kontakt->email; ?></p>
                    </a>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
    </div>
  </div>
 </section>