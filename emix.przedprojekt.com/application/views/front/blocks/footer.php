
  <footer class="main-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <div>
            <img class="main-footer__logo" src="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="logo emix">
          </div>
        </div>

        <div class="col-lg-9">
          <ul class="main-footer__list">
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>">Strona główna</a></li>
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>p/o_firmie">O firmie</a></li>
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>p/posrednictwo">Pośrednictwo</a></li>
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>p/zarzadzanie_nieruchomosciami">Zarządzenie Nieruchomościami</a>
            </li>
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>p/wspolnota">Wspólnota</a></li>
            <li class="main-footer__list-item"><a class="main-footer__link" href="<?php echo base_url(); ?>p/kontakt">Kontakt</a></li>
          </ul>
        </div>
      </div>

      <div class="main-footer__row">
        <div>
          <span>
            <?php echo $kontakt->city; ?> <?php echo $kontakt->address; ?><br>
            tel. <?php echo $kontakt->phone; ?>, <?php echo $kontakt->phone2; ?>, <?php echo base_url(); ?>
          </span>
        </div>


        <?php if($ustawienia->fb_link != '' && $ustawienia->inst_link != '' && $ustawienia->yt_link != ''): ?>
        <div class="d-flex justify-content-between">
          <span class="d-flex align-items-center"> 
            Znajdź nas w internecie
          </span>
          <div class="main-footer__social-box">
            <?php if($ustawienia->fb_link != ''): ?>
            <a class="main-footer__social-link" href="<?php echo $ustawienia->fb_link; ?>">
              <i class="fab fa-facebook-f"></i>
            </a>
            <?php endif; ?>
            <?php if($ustawienia->inst_link != ''): ?>
            <a class="main-footer__social-link" href="<?php echo $ustawienia->inst_link; ?>">
              <i class="fab fa-instagram"></i>
            </a>
            <?php endif; ?>
            <?php if($ustawienia->yt_link != ''): ?>
            <a class="main-footer__social-link" href="<?php echo $ustawienia->yt_link; ?>">
              <i class="fab fa-twitter"></i>
            </a>
            <?php endif; ?>
          </div>

        </div>
        <?php endif; ?>
      </div>

      <div class="main-footer__separator"></div>

      <div class="copyrights">
        <div class="copyrights__text"> 2019 © EMIX Wszystkie prawa zastrzeżone</div>
        <div class="copyrights__text">Projekt i wdrożenie: <a class="copyrights__link" href="https://agencjamedialna.pro/">AdAwards</a> | <a href="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->pdf ?>" class="copyrights__link">Polityka prywatności</a></div>
      </div>
    </div>
  </footer>

  <!-- /Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/mdb.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/foundation/6.2/foundation.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>assets/front/dist/js/lightbox.js"></script>
  <script type="text/javascript">
    $('.carousel').carousel({
        interval: 0
      })
  </script>
</body>

</html>