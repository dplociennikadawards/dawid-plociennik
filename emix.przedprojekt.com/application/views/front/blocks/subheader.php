<header class="main-header">

    <div class="main-header__nav-box">
      <nav class="main-header__top-navbar">
        <div class="main-header__top-container container">
          <a href="<?php echo base_url(); ?>" class="main-header__logo-link">
            <img class="main-header__logo" src="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="logo emix">
          </a>

          <span class="main-header__link-box">
            <span>Zapraszamy do kontaktu: <a href="tel:<?php echo $kontakt->phone2; ?>" class="main-header__topnav-link">tel. <?php echo $kontakt->phone2; ?></a></span>
            <span>tel.kom.: <a href="tel:<?php echo $kontakt->phone; ?>" class="main-header__topnav-link"><?php echo $kontakt->phone; ?></a></span>
            <a href="<?php echo $kontakt->email; ?>" class="main-header__mail-link"><?php echo $kontakt->email; ?></a>
          </span>

          <a href="<?php echo base_url(); ?>" class="main-header__lang">
            <img class="main-header__pl" src="<?php echo base_url(); ?>assets/front/img/pl.png">
          </a>
        </div>
      </nav>
      <nav class="main-header__navbar navbar navbar-expand-lg navbar-light">

        <div class="main-header__nav-container container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link text-center" href="<?php echo base_url(); ?>">strona<br>główna</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/o_firmie">o firmie</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/posrednictwo">pośrednictwo</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-center" href="<?php echo base_url(); ?>p/zarzadzanie_nieruchomosciami">zarządzenie<br>Nieruchomościami</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/wspolnota">wspólnota</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/weles">Weles3</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
              </li>
            </ul>

            <div class="main-header__social-box">
              <?php if($ustawienia->fb_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->fb_link ?>">
                <i class="fab fa-facebook-f"></i>
              </a>
              <?php endif; ?>
              <?php if($ustawienia->yt_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->yt_link ?>">
                <i class="fab fa-twitter"></i>
              </a>
              <?php endif; ?>
              <?php if($ustawienia->inst_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->inst_link ?>">
                <i class="fab fa-instagram"></i>
              </a>
              <?php endif; ?>
            </div>
          </div>
        </div>

      </nav>
    </div>

    <?php foreach ($slider as $key):?>
    <div class="main-header__background" style="
        background: url(<?php echo base_url(); ?>uploads/slider/<?php echo $key->photo; ?>);
        background-size: cover;
        background-repeat: no-repeat;">
      <div class="main-header__container container">
        <h1 class="main-header__title" style="margin-top: 90px; margin-bottom: 140px;"><?php echo $key->title; ?><br>
          <?php echo $key->subtitle ; ?></h1>

      </div>
    </div>
    <?php break; endforeach; ?>

  </header>