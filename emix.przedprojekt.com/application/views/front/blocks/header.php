<header class="main-header">

    <div class="main-header__nav-box">
      <nav class="main-header__top-navbar">
        <div class="main-header__top-container container">
          <a href="<?php echo base_url(); ?>" class="main-header__logo-link">
            <img class="main-header__logo" src="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo; ?>" alt="logo emix">
          </a>

          <span class="main-header__link-box">
            <span>Zapraszamy do kontaktu: <a href="tel:<?php echo $kontakt->phone2; ?>" class="main-header__topnav-link">tel. <?php echo $kontakt->phone2; ?></a></span>
            <span>tel.kom.: <a href="tel:<?php echo $kontakt->phone; ?>" class="main-header__topnav-link"><?php echo $kontakt->phone; ?></a></span>
            <a href="<?php echo $kontakt->email; ?>" class="main-header__mail-link"><?php echo $kontakt->email; ?></a>
          </span>

          <a href="<?php echo base_url(); ?>" class="main-header__lang">
            <img class="main-header__pl" src="<?php echo base_url(); ?>assets/front/img/pl.png">
          </a>
        </div>
      </nav>
      <nav class="main-header__navbar navbar navbar-expand-lg navbar-light">

        <div class="main-header__nav-container container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link text-center" href="<?php echo base_url(); ?>">strona<br>główna</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/o_firmie">o firmie</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/posrednictwo">pośrednictwo</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-center" href="<?php echo base_url(); ?>p/zarzadzanie_nieruchomosciami">zarządzenie<br>Nieruchomościami</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/wspolnota">wspólnota</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/weles">Weles3</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>p/kontakt">kontakt</a>
              </li>
            </ul>

            <div class="main-header__social-box">
              <?php if($ustawienia->fb_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->fb_link ?>">
                <i class="fab fa-facebook-f"></i>
              </a>
              <?php endif; ?>
              <?php if($ustawienia->yt_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->yt_link ?>">
                <i class="fab fa-twitter"></i>
              </a>
              <?php endif; ?>
              <?php if($ustawienia->inst_link != ''): ?>
              <a class="main-header__social-link" href="<?php echo $ustawienia->inst_link ?>">
                <i class="fab fa-instagram"></i>
              </a>
              <?php endif; ?>
            </div>
          </div>
        </div>

      </nav>
    </div>


    <?php if(count($slider) <= 1): 
      foreach ($slider as $key):?>
    <div class="main-header__background" style="
        background: url(<?php echo base_url(); ?>uploads/slider/<?php echo $key->photo; ?>);
        background-size: cover;
        background-repeat: no-repeat;">
      <div class="main-header__container container">
        <h1 class="main-header__title"><?php echo $key->title; ?><br>
          <?php echo $key->subtitle ; ?></h1>

        <div class="search-box">

          <div class="row">

            <div class="search-box__col col-lg-4">

              <div>
                <h4 class="search-box__title">nieruchomości</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($category as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="<?php echo $key->category; ?>">
                    <?php 
                    if($key->category == 'mieszkania'){echo 'Mieszkania';}
                    if($key->category == 'domy'){echo 'Domy';}
                    if($key->category == 'dzialki'){echo 'Działki';}
                    if($key->category == 'dzialki_budowlane'){echo 'Działki budowlane';}
                    if($key->category == 'dzialki_komercyjne'){echo 'Działki komercyjne';}
                    if($key->category == 'dzialki_rekreacyjne'){echo 'Działki rekreacyjne';}
                    if($key->category == 'dzialki_inwestycyjne'){echo 'Działki inwestycyjne';}
                    if($key->category == 'lokale_uzytkowe'){echo 'Lokale użytkowe';}
                    if($key->category == 'hale_i_magazyny'){echo 'Hale i magazyny';}
                    if($key->category == 'garaze'){echo 'Garaże';}
                    ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div>
                <h4 class="search-box__title">powierzchnia</h4>

                <div>
                  <div class="slider" data-slider data-initial-start="200" data-start="0" data-initial-end="800"
                    data-end="1000" data-step="10">
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput1"></span>
                    <span class="slider-fill" data-slider-fill></span>
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput2"></span>
                  </div>
                </div>
              </div>
              <div class="search-box__input-box">
                <input class="search-box__input search-box__input--left" type="number" id="sliderOutput1">
                <span>m<sup>2</sup></span>
                <span class="search-box__dash">-</span>
                <input class="search-box__input search-box__input--left" type="number" id="sliderOutput2">
                <span>m<sup>2</sup></span>
              </div>
            </div>

            <div class="search-box__col col-lg-4">

              <div>
                <h4 class="search-box__title">rodzaj</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($type as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="">
                    <?php 
                    if($key->type == 'sprzedaz'){echo 'Na sprzedaż';}
                    if($key->type == 'wynajem'){echo 'Wynajem';}
                    if($key->type == 'inwestycje'){echo 'Inwestycje';}
                    if($key->type == 'zamiana'){echo 'Zamiana';}
                    ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div>
                <h4 class="search-box__title">cena</h4>

                <div>
                  <div class="slider" data-slider data-initial-start="200000" data-start="0" data-initial-end="800000"
                    data-end="1000000" data-step="100">
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput3"></span>
                    <span class="slider-fill" data-slider-fill></span>
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput4"></span>
                  </div>
                </div>
                <div class="search-box__input-box">
                  <input class="search-box__input search-box__input--left" type="number" id="sliderOutput3">
                  <span>zł</span>
                  <span class="search-box__dash">-</span>
                  <input class="search-box__input search-box__input--left" type="number" id="sliderOutput4">
                  <span>zł</span>
                </div>
              </div>
            </div>

            <div class="search-box__col col-lg-4">
              <div>
                <h4 class="search-box__title">miejscowość</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($city as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="">
                    <?php echo $key->city; ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="search-box__submit-box d-flex">
                <!-- <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input" id="customSwitches">
                  <label class="custom-control-label" for="customSwitches">Rozwiń więcej</label>
                </div> -->
                <button class="search-box__btn btn-block">wyszukaj <i class="search-box__icon fas fa-search"></i></button>
              </div>
            </div>

          </div>

        </div>

      </div>
    </div>
    <?php endforeach; ?>


    <?php else: ?>
<!--Carousel Wrapper-->
<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Slides-->
  <div class="carousel-inner" role="listbox">



    <?php $i=0; foreach ($slider as $key): $i++; ?>
    <div class="carousel-item <?php if($i == 1){echo 'active';} ?>">
      



      <div class="main-header__background" style="
        background: url(<?php echo base_url(); ?>uploads/slider/<?php echo $key->photo; ?>);
        background-size: cover;
        background-repeat: no-repeat;">
      <div class="main-header__container container">
        <h1 class="main-header__title"><?php echo $key->title; ?><br>
          <?php echo $key->subtitle ; ?></h1>

        <?php if($i == 1): ?>
        <div class="search-box">

          <div class="row">

            <div class="search-box__col col-lg-4">

              <div>
                <h4 class="search-box__title">nieruchomości</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($category as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="<?php echo $key->category; ?>">
                    <?php 
                    if($key->category == 'mieszkania'){echo 'Mieszkania';}
                    if($key->category == 'domy'){echo 'Domy';}
                    if($key->category == 'dzialki'){echo 'Działki';}
                    if($key->category == 'dzialki_budowlane'){echo 'Działki budowlane';}
                    if($key->category == 'dzialki_komercyjne'){echo 'Działki komercyjne';}
                    if($key->category == 'dzialki_rekreacyjne'){echo 'Działki rekreacyjne';}
                    if($key->category == 'dzialki_inwestycyjne'){echo 'Działki inwestycyjne';}
                    if($key->category == 'lokale_uzytkowe'){echo 'Lokale użytkowe';}
                    if($key->category == 'hale_i_magazyny'){echo 'Hale i magazyny';}
                    if($key->category == 'garaze'){echo 'Garaże';}
                    ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div>
                <h4 class="search-box__title">powierzchnia</h4>

                <div>
                  <div class="slider" data-slider data-initial-start="200" data-start="0" data-initial-end="800"
                    data-end="1000" data-step="10">
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput1"></span>
                    <span class="slider-fill" data-slider-fill></span>
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput2"></span>
                  </div>
                </div>
              </div>
              <div class="search-box__input-box">
                <input class="search-box__input search-box__input--left" type="number" id="sliderOutput1">
                <span>m<sup>2</sup></span>
                <span class="search-box__dash">-</span>
                <input class="search-box__input search-box__input--left" type="number" id="sliderOutput2">
                <span>m<sup>2</sup></span>
              </div>
            </div>

            <div class="search-box__col col-lg-4">

              <div>
                <h4 class="search-box__title">rodzaj</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($type as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="">
                    <?php 
                    if($key->type == 'sprzedaz'){echo 'Na sprzedaż';}
                    if($key->type == 'wynajem'){echo 'Wynajem';}
                    if($key->type == 'inwestycje'){echo 'Inwestycje';}
                    if($key->type == 'zamiana'){echo 'Zamiana';}
                    ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div>
                <h4 class="search-box__title">cena</h4>

                <div>
                  <div class="slider" data-slider data-initial-start="200000" data-start="0" data-initial-end="800000"
                    data-end="1000000" data-step="100">
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput3"></span>
                    <span class="slider-fill" data-slider-fill></span>
                    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"
                      aria-controls="sliderOutput4"></span>
                  </div>
                </div>
                <div class="search-box__input-box">
                  <input class="search-box__input search-box__input--left" type="number" id="sliderOutput3">
                  <span>zł</span>
                  <span class="search-box__dash">-</span>
                  <input class="search-box__input search-box__input--left" type="number" id="sliderOutput4">
                  <span>zł</span>
                </div>
              </div>
            </div>

            <div class="search-box__col col-lg-4">
              <div>
                <h4 class="search-box__title">miejscowość</h4>

                <select class="search-box__select">
                  <?php $i=0; foreach ($city as $key): $i++;?>
                  <option <?php if($i==1){echo 'selected disabled';} ?> class="search-box__option" value="">
                    <?php echo $key->city; ?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class="search-box__submit-box d-flex">
                <!-- <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input" id="customSwitches">
                  <label class="custom-control-label" for="customSwitches">Rozwiń więcej</label>
                </div> -->
                <button class="search-box__btn btn-block">wyszukaj <i class="search-box__icon fas fa-search"></i></button>
              </div>
            </div>

          </div>

        </div>
        <?php endif; ?>

      </div>
    </div>



    </div>
    <?php endforeach ?>


  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
<?php endif; ?>



  </header>