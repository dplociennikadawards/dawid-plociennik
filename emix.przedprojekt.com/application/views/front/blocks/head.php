<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $ustawienia->meta_title; ?></title>
  <meta name="author" content="Dawid Płóciennik">
  <meta name="description" content="<?php echo $ustawienia->description; ?>">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo; ?>"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Poppins:500,600|Quicksand:500&display=swap"
    rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/front/css/mdb.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/foundation/6.2/foundation.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/front/dist/css/lightbox.css" rel="stylesheet">
</head>

<body>