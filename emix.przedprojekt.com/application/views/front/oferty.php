<main>
	<section class="default-section">
      <div class="container">
        <h4 class="default-section__title">
          Nasza oferty
        </h4>

        <div class="default-section__row">
          <?php $i=0; foreach (array_reverse($oferta) as $key): $i++;?>
          <div class="default-section__cell mt-3" style="background-image: url('<?php echo base_url(); ?>uploads/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->photo; ?>')">
            <h4 class="default-section__cell-title"><?php echo $key->title; ?></h4>

            <div class="default-section__movable-panel">
              <h4 class="default-section__cell-title default-section__cell-title--dark"><?php echo $key->title; ?></h4>

              <span class="default-section__panel-desc">
                <?php echo $key->city; ?><br>
                Powierzchnia (m2): <?php echo $key->area; ?><br>
                Pokoje: <?php echo $key->rooms; ?>
              </span>

              <div class="default-section__price-box">
                <span class="default-section__price"><?php echo $key->price; ?> PLN</span>

                <a class="default-section__btn" href="<?php echo base_url(); ?>p/oferta/<?php echo $this->slugify_m->slugify($key->title) ?>/<?php echo $key->oferta_id ?>">sprawdź</a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>

      </div>

    </section>
</main>