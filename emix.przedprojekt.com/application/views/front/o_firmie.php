<main>
	
	<section class="default-section">
      <div class="container">

      	<?php foreach ($rows as $key): ?>
        <div class="row">

        <?php if($key->photo != ''): ?>
          <div class="d-flex justify-content-center align-items-center col-lg-4">
            <img class="default-section__img" src="<?php echo base_url(); ?>uploads/o_firmie/<?php echo $key->photo; ?>">
          </div>
      	<?php endif; ?>

          <div class="col-lg-8 <?php if($key->photo == ''){ echo 'col-lg-12';}?>">
            <h4 class="default-section__title default-section__title--text-left">
              <?php echo $key->title; ?>
            </h4>

            <p class="default-section__text">
              <span class="default-section__bold"><?php echo $key->content; ?>
            </p>
          </div>
        </div>
        <hr class="my-4">
      	<?php endforeach ?>
      </div>
    </section>
</main>