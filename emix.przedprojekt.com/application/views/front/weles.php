<!-- Card -->
<div class="card card-image">

  <!-- Content -->
  <div class="rgba-black-strong py-5 px-4 bg-white">
    <div class="row d-flex justify-content-center">
      <div class="col-md-12 col-xl-12">

        <!--Accordion wrapper-->
        <div class="accordion md-accordion accordion-5" id="accordionEx5" role="tablist"
          aria-multiselectable="true">

          <?php $i=0; foreach ($rows as $key): $i++;?>
          <!-- Accordion card -->
          <div class="card mb-4">

            <!-- Card header -->
            <div class="card-header p-0 z-depth-1" role="tab" id="heading<?php echo $key->weles_id; ?>">
              <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse<?php echo $key->weles_id; ?>" aria-expanded="true"
                aria-controls="collapse<?php echo $key->weles_id; ?>">
                <img class="p-3 mr-4 float-left black-text" src="<?php echo base_url(); ?>uploads/settings/<?php echo $ustawienia->logo ?>" width="150">
                <h4 class="text-uppercase white-text mb-0 py-3" style="height: 100%; display: flex; align-items: center;">
                  <?php echo $key->title; ?>
                </h4>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapse<?php echo $key->weles_id; ?>" class="collapse <?php if($i==1){echo 'show';} ?>" role="tabpanel" aria-labelledby="heading<?php echo $key->weles_id; ?>"
              data-parent="#accordionEx5">
              <div class="card-body rgba-black-light white-text z-depth-1">

                <div class="row">
                  <?php if($key->photo != ''): 
                    list($width, $height) = getimagesize(base_url().'uploads/weles/'.$key->photo);
                    ?>
                  <div class="col-md-4 text-center">
                    <div class="view overlay rounded z-depth-2 mb-4" style="
                      background-image: url('<?php echo base_url(); ?>uploads/weles/<?php echo $key->photo; ?>'); 
                      height: 300px; 
                     /* width: <?php echo $height; ?>px; */
                      background-size: cover;    
                      background-position: center;
                      background-repeat: no-repeat; "></div>
                  </div>
                  <div class="col-md-8">
                    <div class="p-md-4 mb-0" style="font-size: 1.1rem;"><?php echo $key->content; ?></div>
                  </div>
                  <?php else: ?>
                  <div class="col-md-12">
                    <div class="p-md-4 mb-0" style="font-size: 1.1rem;"><?php echo $key->content; ?></div>
                  </div>
                  <?php endif; ?>
                  <?php if($key->link != ''): ?>
                    <iframe src="<?php echo $key->link; ?>" style="width: 100%; height: 100vh;"></iframe>
                  <?php endif; ?>
                </div>


              </div>
            </div>
          </div>
          <!-- Accordion card -->
          <?php endforeach ?>

        </div>
        <!--/.Accordion wrapper-->

      </div>
    </div>
  </div>
  <!-- Content -->
</div>
<!-- Card -->