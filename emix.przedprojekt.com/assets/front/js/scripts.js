$(document).foundation(); 

jQuery(document).ready(function(){
	/* tabs switching */
	$('.tabs__link').click(function(){
        var tab_id = $(this).attr('data-tab');
        
		$('.tabs__link').removeClass('tabs__link-active');
		$('.tabs__content').removeClass('tabs__content-active');

		$(this).addClass('tabs__link-active');
		$("#"+tab_id).addClass('tabs__content-active');
	});
});